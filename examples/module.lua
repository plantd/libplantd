#!/usr/bin/env lua

local lgi = require 'lgi'
local Plantd = lgi.require('Plantd')

-- Create package
local Module = lgi.package 'Module'

Module:class('App', Plantd.Application)

--function Module.App:do_get_configuration()
  --print("get-configuration")
  --local response = Plantd.ConfigurationResponse()
  --return response
--end

--function Module.App:do_get_status()
  --print("get-status")
--end

--function Module.App:do_get_settings()
  --print("get-settings")
--end

--function Module.App:do_get_job()
  --print("get-job")
--end

--function Module.App:do_get_jobs()
  --print("get-jobs")
--end

--function Module.App:do_get_active_jobs()
  --print("get-active-job")
--end

--function Module.App:do_cancel_job()
  --print("cancel-job")
--end

--function Module.App:do_submit_job()
  --print("submit-job")
--end

--function Module.App:do_submit_event()
  --print("submit-event")
--end

--function Module.App:do_available_events()
  --print("available-events")
--end

local app = Module.App { application_id = 'org.plantd.Module' }

--[[
   [-- these don't exist, will need to handle these somehow
   [local function execute_actions()
   [  local resp = Plantd.Response()
   [  -- configuration
   [  resp = app:read_configuration(conf_id)
   [  resp = app:update_configuration(conf_id, conf)
   [  resp = app:read_unit_configuration(service)
   [  -- bus functions
   [  resp = app:register_bus(service, bus_id)
   [  resp = app:connect_bus(service, bus_id)
   [  resp = app:get_bus(service, bus_id)
   [  resp = app:get_buses(service)
   [  -- job functions
   [  resp = app:get_job(service, job_id)
   [  resp = app:get_jobs(service)
   [  resp = app:get_job_status(service, job_id)
   [end
   ]]

-- run the module
app:set_inactivity_timeout(10000)
app:set_endpoint("tcp://localhost:7201")
app:set_service("acquire-genicam")
app:run { arg[0], ... }

-- vim:ft=lua tabstop=2 shiftwidth=2 softtabstop=2
