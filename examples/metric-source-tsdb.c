#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <glib.h>
#include <gio/gio.h>

#include "plantd/plantd.h"

static void
activate (GApplication *application)
{
  g_debug ("activated");
}

gint
main (gint argc, gchar *argv[])
{
  gint status, n, count, delay;
  g_autoptr (GApplication) app = NULL;
  g_autoptr (PdSource) source = NULL;
  g_autoptr (PdMetric) metric = NULL;
  g_autoptr (PdMetricTable) table = NULL;
  g_autoptr (PdMetricTableHeader) header = NULL;
  g_autoptr (GRand) rand = NULL;

  g_autofree gchar *endpoint = NULL;
  g_autofree gchar *envelope = NULL;

  count = 100;
  delay = 100000;

  GOptionEntry entries[] = {
    { "endpoint", 'e', G_OPTION_FLAG_NONE, G_OPTION_ARG_STRING, &endpoint, NULL, NULL },
    { "envelope", 'p', G_OPTION_FLAG_NONE, G_OPTION_ARG_STRING, &envelope, NULL, NULL },
    { "count", 'n', G_OPTION_FLAG_NONE, G_OPTION_ARG_INT, &count, NULL, NULL },
    { "delay", 'd', G_OPTION_FLAG_NONE, G_OPTION_ARG_INT, &delay, NULL, NULL },
    { NULL }
  };

  app = g_application_new ("org.plantd.PubExample", 0);

  g_application_add_main_option_entries (app, entries);

  g_application_set_option_context_parameter_string (app, "- metric source for testing");
  g_application_set_option_context_summary (app,
                                            "Summary:\n"
                                            "Just a simple Plantd metric producer.");
  g_application_set_option_context_description (app,
                                                "Description:\n"
                                                "This is meant to be used with a unit service "
                                                "and one or more test metric sinks.\n");

  g_signal_connect (app, "activate", G_CALLBACK (activate), NULL);
  g_application_set_inactivity_timeout (app, 10000);

  status = g_application_run (app, argc, argv);

  if (!endpoint)
    endpoint = g_strdup ("tcp://*:14000");
  if (!envelope)
    envelope = g_strdup ("");

  g_info ("Publishing messages on '%s'", endpoint);
  g_info ("Publishing messages with envelope '%s'", envelope);
  g_info ("Publishing %d messages", count);

  source = pd_source_new (endpoint, envelope);
  pd_source_start (source);

  rand = g_rand_new ();
  table = pd_metric_table_new ();
  header = pd_metric_table_header_new ("calibration");

  pd_metric_table_header_add_column (header, "speed_motor_x0", 1.0);
  pd_metric_table_header_add_column (header, "speed_motor_x1", 10.0);
  pd_metric_table_header_add_column (header, "speed_set_motor_x0", 2.0);
  pd_metric_table_header_add_column (header, "speed_set_motor_x1", 20.0);
  pd_metric_table_header_add_column (header, "torque_motor_x0", 3.0);
  pd_metric_table_header_add_column (header, "torque_motor_x1", 30.0);
  pd_metric_table_header_add_column (header, "gforce_x0", 4.0);
  pd_metric_table_header_add_column (header, "gforce_x1", 40.0);
  pd_metric_table_header_add_column (header, "gforce_set_x0", 5.0);
  pd_metric_table_header_add_column (header, "gforce_set_x1", 50.0);
  pd_metric_table_header_add_column (header, "vibration_motor_x0", 6.0);
  pd_metric_table_header_add_column (header, "vibration_motor_x1", 60.0);

  pd_metric_table_set_name (table, "centrifuge");
  pd_metric_table_set_timestamp (table, "2019-04-09 14:00:0.00-00");
  pd_metric_table_set_header (table, header);
  pd_metric_table_add_entry (table, "speed_rpm", 0.0);
  pd_metric_table_add_entry (table, "speed_set_rpm", 0.0);
  pd_metric_table_add_entry (table, "torque_nm", 0.0);
  pd_metric_table_add_entry (table, "gforce", 0.0);
  pd_metric_table_add_entry (table, "gforce_set", 0.0);
  pd_metric_table_add_entry (table, "vibration_volts", 0.0);

  metric = pd_metric_new_full (g_uuid_string_random (), "log-data", table);

  n = 0;
  while (n < count)
    {
      g_autoptr (GDateTime) dt = NULL;
      g_autofree gchar *msg;
      g_autofree gchar *ts;

      dt = g_date_time_new_now_local ();
      ts = g_strdup_printf ("%d-%02d-%02d %02d:%02d:%d.%06d",
                            g_date_time_get_year (dt),
                            g_date_time_get_month (dt),
                            g_date_time_get_day_of_month (dt),
                            g_date_time_get_hour (dt),
                            g_date_time_get_minute (dt),
                            g_date_time_get_second (dt),
                            g_date_time_get_microsecond (dt));

      // add_entry replaces if it already exists
      pd_metric_table_set_timestamp (table, ts);
      pd_metric_table_add_entry (table, "speed_rpm", g_rand_double (rand));
      pd_metric_table_add_entry (table, "speed_set_rpm", g_rand_double (rand));
      pd_metric_table_add_entry (table, "torque_Nm", g_rand_double (rand));
      pd_metric_table_add_entry (table, "gforce", g_rand_double (rand));
      pd_metric_table_add_entry (table, "gforce_set", g_rand_double (rand));
      pd_metric_table_add_entry (table, "vibration_volts", g_rand_double (rand));

      msg = pd_metric_serialize (metric);
      pd_source_queue_message (source, msg);

      g_usleep (delay);
      g_print ("n = %d\n", n);
      n++;
    }

  pd_source_stop (source);

  return status;
}
