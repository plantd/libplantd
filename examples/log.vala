using Pd;

public static int main (string[] args) {
    Pd.log_init (true, null);

    Pd.log_increase_verbosity ();

    critical ("critical");
    warning ("warning");
    message ("message");
    info ("info");
    debug ("debug");

    Pd.log_shutdown ();

    return 0;
}
