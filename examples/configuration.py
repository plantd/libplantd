#! /usr/bin/env python

import gi

gi.require_version("Pd", "1.0")

from gi.repository import Pd


class ConfigurationExample:
    def __init__(self):
        Pd.message("configuration example")
        self.configuration = Pd.Configuration()
        self.configuration.set_id("0xDEADBEEF")
        self.configuration.set_namespace(Pd.ConfigurationNamespace.ACQUIRE)
        self.configuration.add_property(Pd.Property.new("foo", "bar"))
        obj = Pd.Object.new("baz")
        obj.add_property(Pd.Property.new("baz-foo", "baz-bar"))
        self.configuration.add_object(obj)

    def dump(self):
        Pd.message(self.configuration.serialize())

    def test(self):
        objs = self.configuration.get_objects()
        props = objs["baz"].get_properties()
        val = props["baz-foo"].get_value()
        Pd.message(val)


if __name__ == "__main__":
    Pd.log_init(True, "configuration.log")
    app = ConfigurationExample()
    app.dump()
    app.test()
    app.test()
    Pd.log_shutdown()
