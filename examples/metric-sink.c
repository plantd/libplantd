#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <glib.h>
#include <gio/gio.h>

#include "plantd/plantd.h"

static gint count = 0;

#define METRIC_TYPE_SINK metric_sink_get_type ()
/*G_DECLARE_FINAL_TYPE (MetricSink, metric_sink, METRIC, SINK, PdSink)*/

typedef PdSink MetricSink;
typedef PdSinkClass MetricSinkClass;

G_DEFINE_TYPE (MetricSink, metric_sink, PD_TYPE_SINK)

static void
metric_sink_handle_message (PdSink      *self,
                            const gchar *msg)
{
  g_autoptr (PdMetric) metric = NULL;
  g_autoptr (GDateTime) dt = NULL;

  PdMetricTable *table;

  metric = pd_metric_new ();
  pd_metric_deserialize (metric, msg);

  if (!PD_IS_METRIC (metric))
    {
      g_print ("Received a data not for a metric\n");
      return;
    }

  table = pd_metric_get_data (metric);

  g_print ("%s: %.6f, %.6f, %.6f\n",
           pd_metric_table_get_timestamp (table),
           pd_metric_table_get_entry (table, "value_1"),
           pd_metric_table_get_entry (table, "value_2"),
           pd_metric_table_get_entry (table, "value_3"));
  count++;
}

static void
metric_sink_finalize (GObject *object)
{
  G_OBJECT_CLASS (metric_sink_parent_class)->finalize (object);
}

static void
metric_sink_class_init (MetricSinkClass *klass)
{
  G_OBJECT_CLASS (klass)->finalize = metric_sink_finalize;
  PD_SINK_CLASS (klass)->handle_message = metric_sink_handle_message;
}

static void
metric_sink_init (MetricSink *self)
{
}

PdSink *
metric_sink_new (const gchar *endpoint,
                 const gchar *filter)
{
  return g_object_new (METRIC_TYPE_SINK,
                       "endpoint", endpoint,
                       "filter", filter,
                       NULL);
}

static void
activate (GApplication *application)
{
  g_debug ("activated");
}

gint
main (gint argc, gchar *argv[])
{
  gint status, n, duration;
  g_autoptr (GApplication) app = NULL;
  g_autoptr (PdSink) sink = NULL;

  g_autofree gchar *endpoint = NULL;
  g_autofree gchar *filter = NULL;

  const gchar *context_string = "- metric sink for testing";
  const gchar *context_summary = "Summary:\n"
                                 "Just a simple Plantd metric consumer.";
  const gchar *context_description = "Description:\n"
                                     "This is meant to be used with a unit service "
                                     "and one or more metric producers.\n";

  duration = 60;

  GOptionEntry entries[] = {
    { "endpoint", 'e', G_OPTION_FLAG_NONE, G_OPTION_ARG_STRING, &endpoint, NULL, NULL },
    { "filter", 'f', G_OPTION_FLAG_NONE, G_OPTION_ARG_STRING, &filter, NULL, NULL },
    { "duration", 'd', G_OPTION_FLAG_NONE, G_OPTION_ARG_INT, &duration, NULL, NULL },
    { NULL }
  };

  app = g_application_new ("org.plantd.SubExample", 0);

  g_application_add_main_option_entries (app, entries);

  g_application_set_option_context_parameter_string (app, context_string);
  g_application_set_option_context_summary (app, context_summary);
  g_application_set_option_context_description (app, context_description);

  g_signal_connect (app, "activate", G_CALLBACK (activate), NULL);
  g_application_set_inactivity_timeout (app, 10000);

  status = g_application_run (app, argc, argv);

  /* Set defaults */
  if (!endpoint)
    endpoint = g_strdup ("tcp://localhost:14000");
  if (!filter)
    filter = g_strdup ("");

  g_info ("Subscribing to messages on '%s'", endpoint);
  g_info ("Subscribing to messages with filter '%s'", filter);
  g_info ("Subscribing for %d seconds", duration);

  sink = metric_sink_new (endpoint, filter);
  pd_sink_start (sink);

  n = 0;
  while (n < duration)
    {
      g_print ("Received %d metrics\n", count);
      g_usleep (1000000);
      n++;
    }

  pd_sink_stop (sink);

  return status;
}
