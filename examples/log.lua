package.path = package.path .. ";" .. os.getenv("HOME") .. "/.luarocks/share/lua/5.3/?.lua"

local lgi = require("lgi")
local pd = lgi.require("Pd")

pd.log_init(true, nil)
pd.increase_log_verbosity()

pd.critical("critical")
pd.debug("debug")
pd.info("info")
pd.message("message")
pd.warning("warning")

pd.log_shutdown()

-- vim:ft=lua tabstop=2 shiftwidth=2 softtabstop=2
