#! /usr/bin/env python

import gi
import sys
import time

gi.require_version("Pd", "1.0")

from gi.repository import GLib, Pd


def main(argv):
    source = Pd.Source.new(">tcp://localhost:11000", "")
    source.start()
    for _ in range(10):
        Pd.info("queuing message")
        source.queue_message("{fuuuuuuuuuuuuuuuck}")
        time.sleep(.5)
    source.stop()
    running = True
    while running:
        time.sleep(.5)
        Pd.info("waiting for shutdown")
        running = source.running()
    Pd.info("source shutdown complete")


if __name__ == "__main__":
    SystemExit(main(sys.argv))
