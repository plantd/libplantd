#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <glib.h>
#include <gio/gio.h>
#include <zmq.h>

#include "plantd/plantd.h"

#define G_LOG_DOMAIN "client"

static void
activate_cb (GApplication *application)
{
  g_print ("activated\n");
}

gint
main (gint argc, gchar *argv[])
{
  gint status;
  g_autoptr (PdWorker) worker = NULL;
  g_autoptr (GApplication) app = NULL;

  const gchar *endpoint = "tcp://localhost:5555";
  const gchar *service = "echo";

  pd_log_init (TRUE, NULL);
  pd_log_increase_verbosity ();
  pd_log_increase_verbosity ();
  pd_log_increase_verbosity ();
  pd_log_increase_verbosity ();

  app = g_application_new ("org.plantd.example.Worker", 0);
  g_signal_connect (app, "activate", G_CALLBACK (activate_cb), NULL);
  g_application_set_inactivity_timeout (app, 10000);

  status = g_application_run (app, argc, argv);

  worker = pd_worker_new (endpoint, service);
  pd_worker_connect (worker);
  g_info ("`%s' service worker created for: %s", service, endpoint);

  while (TRUE)
    {
      zframe_t *reply = NULL;
      zmsg_t *request = NULL;

      request = pd_worker_recv (worker, &reply);

      if (request == NULL)
        break; // Worker was interrupted

      // Echo received message
      pd_worker_send (worker, &request, reply);
      zframe_destroy (&reply);
    }

  pd_log_shutdown ();

  return status;
}
