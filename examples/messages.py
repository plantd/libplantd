#! /usr/bin/env python

import gi
import sys

gi.require_version("Pd", "1.0")

from gi.repository import GLib, Pd


def _log():
    Pd.critical("Critical level")
    Pd.debug("Debug level")
    Pd.info("Info level")
    Pd.message("Message level")
    Pd.warning("Warning level")


def _object():
    obj = Pd.Object.new("obj0")
    Pd.message("ID: %s" % obj.get_id())


def _job():
    job = Pd.Job.new()
    job.set_id(GLib.uuid_string_random())
    Pd.message("PdJob:\n%s" % job.serialize())


def _event_request():
    msg = Pd.EventRequest.new()
    msg.set_event_id(1234)
    Pd.message(
        "PdEventRequest:\nID: %s\nEvent ID: %d"
        % (msg.get_id(), msg.get_event_id())
    )


def main(argv):
    _log()
    # types
    _object()
    _job()
    # messages
    _event_request()


if __name__ == "__main__":
    SystemExit(main(sys.argv))
