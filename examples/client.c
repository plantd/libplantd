#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <glib.h>
#include <zmq.h>

#include "plantd/plantd.h"

#define G_LOG_DOMAIN "worker"

gint
main (gint argc, gchar *argv[])
{
  gint count;
  const gchar *endpoint = "tcp://localhost:5555";
  g_autoptr (PdClient) client = NULL;

  pd_log_init (TRUE, NULL);
  pd_log_increase_verbosity ();
  pd_log_increase_verbosity ();
  pd_log_increase_verbosity ();
  pd_log_increase_verbosity ();

  client = pd_client_new (endpoint);
  g_message ("client created with endpoint: %s", endpoint);

  for (count = 0; count < 1000; count++)
    {
      zmsg_t *request = NULL;
      zmsg_t *reply = NULL;

      g_message ("send test message");
      request = zmsg_new ();
      zmsg_pushstr (request, "test");
      pd_client_send (client, "echo", &request);

      reply = pd_client_recv (client, NULL);
      if (reply)
        zmsg_destroy (&reply);
      else
        break; // Interrupted by Ctrl-C
    }

  g_message ("%d replies received\n", count);

  pd_log_shutdown ();

  return 0;
}
