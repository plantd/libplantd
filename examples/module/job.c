/*
 * Copyright © the Plantd contributors. All rights reserved.
 *
 * This file is part of libplantd, distributed under the MIT license.
 * For full terms see the included LICENSE file.
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include "job.h"

typedef PdJob PdModuleExampleJob;
typedef PdJobClass PdModuleExampleJobClass;

G_DEFINE_TYPE (PdModuleExampleJob, pd_module_example_job, PD_TYPE_JOB)

static void
pd_module_example_job_task (PdJob *self)
{
  g_message ("Running task");
  for (gint i = 0; i < 20; i++)
    {
      g_message ("running...");
      g_usleep (500000);
    }
}

static void
pd_module_example_job_finalize (GObject *object)
{
  G_OBJECT_CLASS (pd_module_example_job_parent_class)->finalize (object);
}

static void
pd_module_example_job_class_init (PdModuleExampleJobClass *klass)
{
  G_OBJECT_CLASS (klass)->finalize = pd_module_example_job_finalize;
  PD_JOB_CLASS (klass)->task = pd_module_example_job_task;
}

static void
pd_module_example_job_init (PdModuleExampleJob *self)
{
}

PdJob *
pd_module_example_job_new (void)
{
  return g_object_new (PD_TYPE_MODULE_EXAMPLE_JOB, NULL);
}
