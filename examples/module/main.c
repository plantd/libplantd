#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <glib-unix.h>

#include "plantd/plantd.h"

#include "module.h"

gint
main (gint argc, gchar *argv[])
{
  gint status;
  g_autoptr (PdApplication) app = NULL;
  /* g_autoptr (PdConfiguration) configuration = NULL; */
  /* g_autoptr (GPtrArray) properties = NULL; */

  pd_log_init (TRUE, NULL);

  app = pd_module_example_new ();
  pd_application_set_id (app, "org.plantd.ModuleExample");
  pd_application_set_endpoint (app, "tcp://localhost:5555");
  pd_application_set_service (app, "example");

  g_application_set_inactivity_timeout (G_APPLICATION (app), 10000);

/*
 *  pd_application_load_config (app, "/tmp/test.json", NULL);
 *  configuration = pd_application_get_configuration (app);
 *  properties = pd_configuration_get_properties (configuration);
 *
 *  for (gint i = 0; i < properties->len; i++)
 *    {
 *      g_autoptr (PdProperty) property = NULL;
 *
 *      property = g_ptr_array_index (properties, i);
 *      if (!property)
 *        {
 *          g_error ("null property");
 *          break;
 *        }
 *
 *      g_message ("%s: %s", pd_property_get_key (property), pd_property_get_value (property));
 *    }
 */

  status = g_application_run (G_APPLICATION (app), argc, argv);

  pd_log_shutdown ();

  return status;
}
