/*
 * Copyright © the Plantd contributors. All rights reserved.
 *
 * This file is part of libplantd, distributed under the MIT license.
 * For full terms see the included LICENSE file.
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include "module.h"
#include "job.h"

typedef PdApplication PdModuleExample;
typedef PdApplicationClass PdModuleExampleClass;

G_DEFINE_TYPE (PdModuleExample, pd_module_example, PD_TYPE_APPLICATION)

static PdConfigurationResponse *
pd_module_example_get_configuration (PdApplication  *self,
                                     GError        **error)
{
  g_autofree const gchar *data = NULL;
  g_autoptr (PdConfiguration) configuration = NULL;
  PdConfigurationResponse *response = NULL;

  g_message ("execute get-configuration in module example");

  configuration = pd_configuration_new ();
  pd_configuration_set_id (configuration, g_uuid_string_random ());
  pd_configuration_set_namespace (configuration, PD_CONFIGURATION_NAMESPACE_ACQUIRE);

  response = pd_configuration_response_new ();
  pd_configuration_response_set_configuration (response, configuration);

  // Use response
  g_assert (response != NULL);
  data = pd_configuration_response_serialize (response);
  g_debug ("response: %s", data);

  return response;
}

static PdSettingsResponse *
pd_module_example_get_settings (PdApplication  *self,
                                GError        **error)
{
  g_autoptr (PdSettingsResponse) response = NULL;

  g_message ("execute get-settings in module example");
  response = pd_settings_response_new ();

  return response;
}

static PdJobResponse *
pd_module_example_submit_job (PdApplication  *self,
                              const gchar    *job_id,
                              const gchar    *job_value,
                              GHashTable     *job_properties,
                              GError        **error)
{
  g_autoptr (PdJob) job = NULL;
  /*g_autoptr (PdJobResponse) response = NULL;*/
  PdJobResponse *response;

  g_message ("The %s job was requested", job_id);

  job = pd_module_example_job_new ();
  pd_job_set_id (PD_JOB (job), g_uuid_string_random ());
  response = pd_job_response_new ();

  pd_job_response_set_job (response, PD_JOB (job));

  return response;
}

static void
pd_module_example_finalize (GObject *object)
{
  G_OBJECT_CLASS (pd_module_example_parent_class)->finalize (object);
}

static void
pd_module_example_class_init (PdModuleExampleClass *klass)
{
  G_OBJECT_CLASS (klass)->finalize = pd_module_example_finalize;
  PD_APPLICATION_CLASS (klass)->get_configuration = pd_module_example_get_configuration;
  PD_APPLICATION_CLASS (klass)->get_settings = pd_module_example_get_settings;
  PD_APPLICATION_CLASS (klass)->submit_job = pd_module_example_submit_job;

  g_debug ("module example class init");
  g_assert (PD_APPLICATION_CLASS (klass)->get_configuration != NULL);
}

static void
pd_module_example_init (PdModuleExample *self)
{
  g_debug ("module example init");
}

PdApplication *
pd_module_example_new (void)
{
  return g_object_new (PD_TYPE_MODULE_EXAMPLE, NULL);
}
