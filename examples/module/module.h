/*
 * Copyright © the Plantd contributors. All rights reserved.
 *
 * This file is part of libplantd, distributed under the MIT license.
 * For full terms see the included LICENSE file.
 */

#include <glib.h>
#include <gio/gio.h>

#include "plantd/plantd.h"

G_BEGIN_DECLS

#define PD_TYPE_MODULE_EXAMPLE pd_module_example_get_type ()
//G_DECLARE_FINAL_TYPE (PdModuleExample, pd_module_example, PD, MODULE_EXAMPLE, PdApplication)

PdApplication *pd_module_example_new (void);

G_END_DECLS
