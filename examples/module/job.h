/*
 * Copyright © the Plantd contributors. All rights reserved.
 *
 * This file is part of libplantd, distributed under the MIT license.
 * For full terms see the included LICENSE file.
 */

#include <glib.h>

#include "plantd/plantd.h"

G_BEGIN_DECLS

#define PD_TYPE_MODULE_EXAMPLE_JOB pd_module_example_job_get_type ()
//G_DECLARE_FINAL_TYPE (PdModuleExampleJob, pd_module_example_job, PD, MODULE_EXAMPLE_JOB, PdJob)

PdJob *pd_module_example_job_new (void);

G_END_DECLS
