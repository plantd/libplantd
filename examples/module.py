#! /usr/bin/env python

"""
To run:

    meson configure -Dshared-lib=true _build
    ninja -C _build
    export GI_TYPELIB_PATH=./_build/plantd/:$GI_TYPELIB_PATH
    export LD_LIBRARY_PATH=./_build/plantd/:$LD_LIBRARY_PATH
    python examples/module.py
"""

import gi
import sys
import signal

gi.require_version("Pd", "1.0")

from gi.repository import Gio, GLib, Pd


class ModuleExampleJob(Pd.Job):
    __gtype_name__ = "ModuleExampleJob"

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    # pd_job_task override
    def do_task(self):
        Pd.message("task function")
        for i in range(20):
            Pd.message("task loop - %d" % i)
            GLib.usleep(500000)


class ModuleExample(Pd.Application):
    __gtype_name__ = "ModuleExample"

    def __init__(self, *args, **kwargs):
        Pd.info("init")
        super().__init__(
            *args, application_id="org.plantd.ModuleExample", flags=0, **kwargs
        )

    # pd_application_get_configuration override
    def do_get_configuration(self):
        Pd.info("get-configuration")
        configuration = Pd.Configuration.new()
        configuration.set_id(GLib.uuid_string_random())
        configuration.set_namespace(Pd.ConfigurationNamespace.ACQUIRE)
        response = Pd.ConfigurationResponse.new()
        response.set_configuration(configuration)
        Pd.message("%s" % configuration.serialize())
        Pd.message("%s" % response.serialize())
        return response

    # pd_application_submit_job override
    def do_submit_job(self, job_name):
        job = ModuleExampleJob()
        response = Pd.JobResponse.new()
        response.set_job(job)
        return response


if __name__ == "__main__":
    Pd.log_init(True, "module.log")

    app = ModuleExample()
    signal.signal(signal.SIGTERM, signal.SIG_DFL)

    app.set_endpoint("tcp://localhost:7202")
    app.set_service("analyze-cant")
    app.set_inactivity_timeout(10000)
    app.run(sys.argv)

    Pd.log_shutdown()
