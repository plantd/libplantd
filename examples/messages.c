#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <glib.h>

#include "plantd/plantd.h"

/**
 * Message initialization.
 *
 * TODO: refactor base type initialization into `init_message`
 * TODO: refactor envelope type initialization into `init_message_envelope`
 */

static void
init_channel (PdChannel *message)
{
  pd_channel_set_endpoint (message, "ipc://*:9999");
  pd_channel_set_envelope (message, "example");
}

static void
init_configuration (PdConfiguration *message)
{
  pd_configuration_set_id (message, g_uuid_string_random ());
  pd_configuration_set_namespace (message, PD_CONFIGURATION_NAMESPACE_ACQUIRE);
}

static void
init_event (PdEvent *message)
{
  pd_event_set_id (message, 1);
  pd_event_set_name (message, "example");
  pd_event_set_description (message, "Example event");
}

static void
init_job (PdJob *message)
{
  pd_job_set_id (message, g_uuid_string_random ());
}

static void
init_channel_request (PdChannelRequest *message)
{
  pd_channel_request_set_id (message, g_uuid_string_random ());
}

static void
init_channel_response (PdChannelResponse *message,
                       PdChannel         *channel)
{
  pd_channel_response_set_channel (message, channel);
}

static void
init_channels_response (PdChannelsResponse *message,
                        PdChannel          *channel)
{
  pd_channels_response_add_channel (message, channel);
}

static void
init_configuration_request (PdConfigurationRequest *message)
{
  pd_configuration_request_set_id (message, g_uuid_string_random ());
  pd_configuration_request_set_namespace (message, PD_CONFIGURATION_NAMESPACE_ACQUIRE);
}

static void
init_configuration_response (PdConfigurationResponse *message,
                             PdConfiguration         *configuration)
{
  pd_configuration_response_set_configuration (message, configuration);
}

static void
init_configurations_response (PdConfigurationsResponse *message,
                              PdConfiguration          *configuration)
{
  pd_configurations_response_add_configuration (message, configuration);
}

static void
init_event_request (PdEventRequest *message,
                    PdEvent        *event)
{
  pd_event_request_set_event (message, event);
}

// XXX: need to change PdEventResponse -> PdEventsResponse
/*
 *static void
 *init_events_response (PdEventsResponse *message,
 *                      PdEvent          *event)
 *{
 *}
 */

static void
init_job_response (PdJobResponse *message,
                   PdJob         *job)
{
  pd_job_response_set_job (message, job);
}

static void
init_job_status_response (PdJobStatusResponse *message,
                          PdJob               *job)
{
  pd_job_status_response_set_job (message, job);
}

static void
init_jobs_response (PdJobsResponse *message,
                    PdJob          *job)
{
  pd_jobs_response_add_job (message, job);
}

static void
init_settings_request (PdSettingsRequest *message)
{
}

static void
init_settings_response (PdSettingsResponse *message)
{
}

static void
init_status_request (PdStatusRequest *message)
{
}

static void
init_status_response (PdStatusResponse *message)
{
}

/**
 * JSON message section.
 */

static void
json_print_message (GObject *message)
{
  g_autofree gchar *json = NULL;
  g_autofree gchar *type = NULL;

  g_assert_nonnull (message);

  /* Message body types */
  if (PD_IS_CHANNEL (message))
    {
      json = pd_channel_serialize (PD_CHANNEL (message));
      type = g_strdup (g_type_name (PD_TYPE_CHANNEL));
    }
  else if (PD_IS_CONFIGURATION (message))
    {
      json = pd_configuration_serialize (PD_CONFIGURATION (message));
      type = g_strdup (g_type_name (PD_TYPE_CONFIGURATION));
    }
  else if (PD_IS_EVENT (message))
    {
      json = pd_event_serialize (PD_EVENT (message));
      type = g_strdup (g_type_name (PD_TYPE_EVENT));
    }
  else if (PD_IS_JOB (message))
    {
      json = pd_job_serialize (PD_JOB (message));
      type = g_strdup (g_type_name (PD_TYPE_JOB));
    }
  else if (PD_IS_MODULE (message))
    {
      json = g_strdup ("{\"msg\":\"not implemented\"}");
      /*json = pd_module_serialize (PD_MODULE (message));*/
      type = g_strdup (g_type_name (PD_TYPE_MODULE));
    }
  else if (PD_IS_OBJECT (message))
    {
      json = pd_object_serialize (PD_OBJECT (message));
      type = g_strdup (g_type_name (PD_TYPE_OBJECT));
    }
  else if (PD_IS_PROPERTY (message))
    {
      json = pd_property_serialize (PD_PROPERTY (message));
      type = g_strdup (g_type_name (PD_TYPE_PROPERTY));
    }
  /*
   *else if (PD_IS_SERVICE (message))
   *  {
   *    json = pd_service_serialize (PD_SERVICE (message));
   *    type = g_strdup (g_type_name (PD_TYPE_SERVICE));
   *  }
   */
  else if (PD_IS_STATE (message))
    {
      json = pd_state_serialize (PD_STATE (message));
      type = g_strdup (g_type_name (PD_TYPE_STATE));
    }
  else if (PD_IS_STATUS (message))
    {
      json = g_strdup ("{\"msg\":\"not implemented\"}");
      /*json = pd_status_serialize (PD_STATUS (message));*/
      type = g_strdup (g_type_name (PD_TYPE_STATUS));
    }
  else if (PD_IS_SYSTEM (message))
    {
      json = pd_system_serialize (PD_SYSTEM (message));
      type = g_strdup (g_type_name (PD_TYPE_SYSTEM));
    }
  /* Request and Response message envelopes */
  else if (PD_IS_CHANNEL_REQUEST (message))
    {
      json = pd_channel_request_serialize (PD_CHANNEL_REQUEST (message));
      type = g_strdup (g_type_name (PD_TYPE_CHANNEL_REQUEST));
    }
  else if (PD_IS_CHANNEL_RESPONSE (message))
    {
      json = pd_channel_response_serialize (PD_CHANNEL_RESPONSE (message));
      type = g_strdup (g_type_name (PD_TYPE_CHANNEL_RESPONSE));
    }
  else if (PD_IS_CHANNELS_RESPONSE (message))
    {
      json = pd_channels_response_serialize (PD_CHANNELS_RESPONSE (message));
      type = g_strdup (g_type_name (PD_TYPE_CHANNELS_RESPONSE));
    }
  else if (PD_IS_CONFIGURATION_REQUEST (message))
    {
      json = pd_configuration_request_serialize (PD_CONFIGURATION_REQUEST (message));
      type = g_strdup (g_type_name (PD_TYPE_CONFIGURATION_REQUEST));
    }
  else if (PD_IS_CONFIGURATION_RESPONSE (message))
    {
      json = pd_configuration_response_serialize (PD_CONFIGURATION_RESPONSE (message));
      type = g_strdup (g_type_name (PD_TYPE_CONFIGURATION_RESPONSE));
    }
  else if (PD_IS_CONFIGURATIONS_RESPONSE (message))
    {
      json = pd_configurations_response_serialize (PD_CONFIGURATIONS_RESPONSE (message));
      type = g_strdup (g_type_name (PD_TYPE_CONFIGURATIONS_RESPONSE));
    }
  else if (PD_IS_JOB_RESPONSE (message))
    {
      json = pd_job_response_serialize (PD_JOB_RESPONSE (message));
      type = g_strdup (g_type_name (PD_TYPE_JOB_RESPONSE));
    }
  else if (PD_IS_JOB_STATUS_RESPONSE (message))
    {
      json = pd_job_status_response_serialize (PD_JOB_STATUS_RESPONSE (message));
      type = g_strdup (g_type_name (PD_TYPE_JOB_STATUS_RESPONSE));
    }
  else if (PD_IS_JOBS_RESPONSE (message))
    {
      json = pd_jobs_response_serialize (PD_JOBS_RESPONSE (message));
      type = g_strdup (g_type_name (PD_TYPE_JOBS_RESPONSE));
    }
  /*
   *else if (PD_IS_ (message))
   *  {
   *    json = pd__serialize (PD_ (message));
   *    type = g_strdup (g_type_name (PD_TYPE_));
   *  }
   */
  else
    {
      return;
    }

  g_message ("\n%s:\n%s\n", type, json);
}

/**
 * Application section.
 */

gint
main (gint argc, gchar *argv[])
{
  /* TODO: use GOptions for these */
  gboolean json = TRUE;

  pd_log_init (TRUE, NULL);
  pd_log_increase_verbosity ();
  pd_log_increase_verbosity ();
  pd_log_increase_verbosity ();

  /* Core types */
  g_autoptr (PdChannel) channel             = NULL;
  g_autoptr (PdConfiguration) configuration = NULL;
  g_autoptr (PdEvent) event                 = NULL;
  g_autoptr (PdJob) job                     = NULL;
  /*g_autoptr (PdModule) module               = NULL;*/
  g_autoptr (PdObject) object               = NULL;
  g_autoptr (PdProperty) property           = NULL;
  g_autoptr (PdService) service             = NULL;
  g_autoptr (PdState) state                 = NULL;
  g_autoptr (PdStatus) status               = NULL;
  g_autoptr (PdSystem) system               = NULL;

  /* Request/response message envelopes */
  g_autoptr (PdChannelRequest) channel_req                 = NULL;
  g_autoptr (PdChannelResponse) channel_resp               = NULL;
  g_autoptr (PdChannelsResponse) channels_resp             = NULL;
  g_autoptr (PdConfigurationRequest) configuration_req     = NULL;
  g_autoptr (PdConfigurationResponse) configuration_resp   = NULL;
  g_autoptr (PdConfigurationsResponse) configurations_resp = NULL;
  g_autoptr (PdEmpty) empty                                = NULL;
  g_autoptr (PdEventRequest) event_req                     = NULL;
  /*g_autoptr (PdEventsResponse) events_resp                 = NULL;*/
  g_autoptr (PdJobRequest) job_req                         = NULL;
  g_autoptr (PdJobResponse) job_resp                       = NULL;
  g_autoptr (PdJobStatusResponse) job_status_resp          = NULL;
  g_autoptr (PdJobsResponse) jobs_resp                     = NULL;
  g_autoptr (PdSettingsRequest) settings_req               = NULL;
  g_autoptr (PdSettingsResponse) settings_resp             = NULL;
  g_autoptr (PdStatusRequest) status_req                   = NULL;
  g_autoptr (PdStatusResponse) status_resp                 = NULL;

  if (json)
    {
      channel = pd_channel_new ();
      configuration = pd_configuration_new ();
      event = pd_event_new ();
      job = pd_job_new ();
      /*module = pd_module_new ();*/
      object = pd_object_new (g_uuid_string_random ());
      property = pd_property_new ("example-key", "example-value");
      service = pd_service_new ("org.plantd.example.Messages");
      state = pd_state_new ();
      status = pd_status_new ();
      system = pd_system_new ();

      channel_req = pd_channel_request_new ();
      channel_resp = pd_channel_response_new ();
      channels_resp = pd_channels_response_new ();
      configuration_req = pd_configuration_request_new ();
      configuration_resp = pd_configuration_response_new ();
      configurations_resp = pd_configurations_response_new ();
      empty = pd_empty_new ();
      event_req = pd_event_request_new ();
      /*events_resp = pd_events_response_new ();*/
      job_req = pd_job_request_new (g_uuid_string_random ());
      job_resp = pd_job_response_new ();
      job_status_resp = pd_job_status_response_new ();
      jobs_resp = pd_jobs_response_new ();
      settings_req = pd_settings_request_new ();
      settings_resp = pd_settings_response_new ();
      status_req = pd_status_request_new ();
      status_resp = pd_status_response_new ();

      init_channel (channel);
      init_configuration (configuration);
      init_event (event);
      init_job (job);
      /*init_service (service);*/
      /*init_state (state);*/
      /*init_status (status);*/
      /*init_system (system);*/

      init_channel_request (channel_req);
      init_channel_response (channel_resp, channel);
      init_channels_response (channels_resp, channel);
      init_configuration_request (configuration_req);
      init_configuration_response (configuration_resp, configuration);
      init_configurations_response (configurations_resp, configuration);
      init_event_request (event_req, event);
      /*init_events_response (events_resp);*/
      init_job_response (job_resp, job);
      init_job_status_response (job_status_resp, job);
      init_jobs_response (jobs_resp, job);
      init_settings_request (settings_req);
      init_settings_response (settings_resp);
      init_status_request (status_req);
      init_status_response (status_resp);
    }

  if (json)
    {
      g_info ("JSON serialization");

      json_print_message (G_OBJECT (channel));
      json_print_message (G_OBJECT (configuration));
      json_print_message (G_OBJECT (event));
      json_print_message (G_OBJECT (job));
      /*json_print_message (G_OBJECT (module));*/
      json_print_message (G_OBJECT (object));
      json_print_message (G_OBJECT (property));
      json_print_message (G_OBJECT (service));
      json_print_message (G_OBJECT (state));
      json_print_message (G_OBJECT (status));
      json_print_message (G_OBJECT (system));

      json_print_message (G_OBJECT (channel_req));
      json_print_message (G_OBJECT (channel_resp));
      json_print_message (G_OBJECT (channels_resp));
      json_print_message (G_OBJECT (configuration_req));
      json_print_message (G_OBJECT (configuration_resp));
      json_print_message (G_OBJECT (configurations_resp));
      json_print_message (G_OBJECT (empty));
      json_print_message (G_OBJECT (event_req));
      /*json_print_message (G_OBJECT (events_resp));*/
      json_print_message (G_OBJECT (job_req));
      json_print_message (G_OBJECT (job_resp));
      json_print_message (G_OBJECT (job_status_resp));
      json_print_message (G_OBJECT (jobs_resp));
      json_print_message (G_OBJECT (settings_req));
      json_print_message (G_OBJECT (settings_resp));
      json_print_message (G_OBJECT (status_req));
      json_print_message (G_OBJECT (status_resp));
    }

  pd_log_shutdown ();

  return 0;
}
