#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <glib.h>
#include <gio/gio.h>
#include <zmq.h>

#include "plantd/plantd.h"

static void
activate (GApplication *application)
{
}

gint
main (gint argc, gchar *argv[])
{
  gint status;
  g_autoptr (GApplication) app = NULL;
  g_autofree gchar *frontend = NULL;
  g_autofree gchar *backend = NULL;

  GOptionEntry entries[] = {
    { "frontend", 'f', G_OPTION_FLAG_NONE, G_OPTION_ARG_STRING, &frontend, NULL, NULL },
    { "backend", 'b', G_OPTION_FLAG_NONE, G_OPTION_ARG_STRING, &backend, NULL, NULL },
    { NULL }
  };

  app = g_application_new ("org.plantd.ProxyExample", 0);

  g_application_add_main_option_entries (app, entries);

  g_application_set_option_context_parameter_string (app, "- proxy for testing");
  g_application_set_option_context_summary (app,
                                            "Summary:\n"
                                            "Just a simple ZeroMQ PUB/SUB proxy.");
  g_application_set_option_context_description (app,
                                                "Description:\n"
                                                "This is meant to be used with other "
                                                "publishers and subscribers.\n");

  g_signal_connect (app, "activate", G_CALLBACK (activate), NULL);
  g_application_set_inactivity_timeout (app, 10000);

  status = g_application_run (app, argc, argv);

  /* Set defaults */
  if (!frontend)
    frontend = g_strdup ("tcp://*:9201");
  if (!backend)
    backend = g_strdup ("tcp://*:9200");

  g_info ("Proxy messages from '%s'", frontend);
  g_info ("Proxy messages to '%s'", backend);

  zactor_t *proxy = zactor_new (zproxy, NULL);
  g_assert_nonnull (proxy);

  zstr_sendx (proxy, "FRONTEND", "XSUB", frontend, NULL);
  zsock_wait (proxy);
  zstr_sendx (proxy, "BACKEND", "XPUB", backend, NULL);
  zsock_wait (proxy);

  zsock_t *capture = zsock_new_pull ("inproc://capture");
  g_assert_nonnull (capture);

  // Switch on capturing, check that it works
  zstr_sendx (proxy, "CAPTURE", "inproc://capture", NULL);
  zsock_wait (proxy);

  while (TRUE)
    {
      g_autofree gchar *msg = NULL;
      msg = zstr_recv (capture);
      g_message ("%s", msg);
    }

  zsock_destroy (&capture);
  zactor_destroy (&proxy);

  return status;
}
