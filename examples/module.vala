using Pd;

class MyJob : Pd.Job {

    public override void task () {
        message ("run run run");
    }
}

class Module : Pd.Application {

    bool cancel;

    public Module () {
        GLib.Object (application_id: "org.plantd.TestModule",
                     flags: ApplicationFlags.FLAGS_NONE);

        Unix.signal_add (Posix.Signal.INT, () => {
            cancel = true;
            return true;
        });
    }

    public override Pd.JobResponse submit_job (string job_name,
                                                   string job_value,
                                                   GLib.HashTable<string,string> job_properties) {
        Pd.JobResponse response = new Pd.JobResponse ();
        message ("submit a job");
        Pd.Job job = new MyJob ();
        response.set_job (job);

        return response;
    }

    public static int main (string[] args) {
        Pd.log_init (true, null);
        //Pd.log_enable_json ();

        var app = new Module ();
        app.set_endpoint ("tcp://localhost:7205");
        app.set_service ("test-module");
        app.set_inactivity_timeout (10000);

        int status = app.run (args);

        Pd.log_shutdown ();

        return status;
    }
}
