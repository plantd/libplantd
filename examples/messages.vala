using Pd;

public static int main (string[] args) {
    Pd.ModuleJobRequest req;

    Pd.log_init (true, null);
    Pd.log_increase_verbosity ();

    // FIXME: fails if id/job-id/job-value unset
    req = new Pd.ModuleJobRequest ();
    var prop = new Pd.Property ("foo", "bar");
    req.set_id ("foo");
    req.set_job_id ("bar");
    req.set_job_value ("baz");
    req.add (prop);

    var msg = req.serialize ();
    message (msg);

    Pd.log_shutdown ();

    return 0;
}
