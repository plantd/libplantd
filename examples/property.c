#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <glib.h>

#include "plantd/plantd.h"

gint
main (gint argc, gchar *argv[])
{
  g_autoptr (PdProperty) prop = NULL;
  g_autoptr (PdProperty) deser = NULL;
  g_autofree gchar *json = NULL;

  prop = pd_property_new ("key", "value");

  json = pd_property_serialize (prop);

  g_message ("%s", json);

  deser = pd_property_new ("deserialized", NULL);
  pd_property_deserialize (deser, json);

  return 0;
}
