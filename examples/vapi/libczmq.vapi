[CCode (cheader_filename = "czmq.h")]
namespace CZmq {

	[Compact]
	[CCode (cname = "zmsg_t", free_function = "zmsg_destroy")]
    public class Message {
		[CCode (cname = "zmsg_new")]
		public Message ();
    }

	[Compact]
	[CCode (cname = "zframe_t", free_function = "zframe_destroy")]
    public class Frame<T> {
		[CCode (cname = "zframe_new")]
		public Frame (T data, size_t size);
    }
}
