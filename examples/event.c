#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <glib.h>

#include "plantd/plantd.h"

#define G_LOG_DOMAIN "event"

gint
main (gint argc, gchar *argv[])
{
  g_autoptr (PdEvent) event = NULL;
  g_autoptr (PdEvent) event2 = NULL;
  g_autofree gchar *serialized = NULL;

  event = pd_event_new_full (1999, "name", "description");

  serialized = pd_event_to_data (event);

  g_print ("%s\n", serialized);

  event2 = pd_event_from_data (serialized);

  g_print ("%d, %s, %s\n",
           pd_event_get_id (event2),
           pd_event_get_name (event2),
           pd_event_get_description (event2));

  return 0;
}
