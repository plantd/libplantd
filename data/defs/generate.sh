#!/bin/bash

if [[ "$(pwd | sed 's/.*libplantd/x/')" != "x" ]]; then
  echo "Run from top level of project"
  exit 1
fi

for file in data/defs/message/*.json; do
  [[ -e "$file" ]] || break
  test_file="${file//^.*plantd\(.*\)\.json$/test\1.c}"
  if [[ -e tests/message/$test_file ]]; then
    echo "$test_file" already exists
  else
    gob > tests/message/"$test_file" < "$file"
  fi
done
