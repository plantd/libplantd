libplantd (0.4.0-1) unstable; urgency=medium

  * Refactor namespace to Plantd

 -- Geoff Johnson <geoff.jay@gmail.com>  Sun, 09 Jan 2022 22:39:12 -0700

libapex (0.3.3-1) unstable; urgency=medium

  * Add standalone mode to application base
  * Refactor job queue into separate class
  * Refactor message handler into separate class
  * Add message handler interface
  * Add message broker class

 -- Geoff Johnson <geoff.jay@gmail.com>  Fri, 13 Sep 2019 22:13:21 -0700

libapex (0.3.2-1) unstable; urgency=medium

  * Use correct transfer annotation
  * Simplify unit test report generate
  * Change response data mutability

 -- Geoff Johnson <geoff.jay@gmail.com>  Fri, 9 Aug 2019 14:31:52 -0700

libapex (0.3.1-1) unstable; urgency=medium

  * Use test data in build
  * Clean up test assertions
  * Use table wrapper in configuration
  * Use table wrapper in object
  * Use table wrapper in metric table
  * Use table wrapper in metric header
  * Add leak free hash table wrapper
  * Memory leaks
  * Drop missing build stage

 -- Geoff Johnson <geoff.jay@gmail.com>  Thu, 8 Aug 2019 14:31:52 -0700

libapex (0.3.0-1) unstable; urgency=medium

  * Bump version
  * Move build check
  * Add Dockerfiles
  * Formatting
  * Add coverage generation
  * Add CI build and test scripts
  * Property method docstring
  * Use proper container transfer
  * Add property method annotations
  * Handle empty lists on serialize
  * Handle zero length arrays
  * Add unit test for error enum

 -- Geoff Johnson <geoff.jay@gmail.com>  Tue, 21 Jul 2019 19:28:47 -0700

libapex (0.2.2-1) unstable; urgency=medium

  * Clean up compiler warnings
  * Metric object leaks
  * Metric related memory leaks
  * Various memory leaks
  * Plug various minor leaks
  * Raise log level in example
  * Minor debian control edits
  * Update debian control file

 -- Geoff Johnson <geoff.jay@gmail.com>  Tue, 12 Jul 2019 09:37:21 -0700

libapex (0.2.1-1) unstable; urgency=medium

  * Add HUP signal for shutdown
  * Add verbosity args to application
  * Add vala examples
  * Add logging facility
  * Add missing header
  * Clean up logging

 -- Geoff Johnson <geoff.jay@gmail.com>  Tue, 25 Jun 2019 00:57:50 -0700

libapex (0.2.0-1) unstable; urgency=medium

  * Improve serialization
  * Add transfer annotations
  * Fix memory leaks
  * Refactor includes

 -- Geoff Johnson <geoff.jay@gmail.com>  Tue, 14 May 2019 08:12:55 -0700

libapex (0.1.0-1) unstable; urgency=medium

  * Initial release

 -- Geoff Johnson <geoff.jay@gmail.com>  Sat, 11 May 2019 10:46:57 -0700
