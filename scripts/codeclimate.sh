#!/bin/bash

function _setup {
  curl -L https://github.com/codeclimate/codeclimate/archive/master.tar.gz | tar xvz
  cd codeclimate-* && sudo make install
}

function _prepare {
  codeclimate engines:install
}

function _analyze {
  codeclimate analyze
}

# This is here for initial setup, subsequent calls to analyze will be performed
# in CI.

_setup
_prepare
_analyze
