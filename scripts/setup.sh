#!/bin/bash

PROJECT=libplantd

# this only needs to happen once to setup

PYTHON_VERSION=$(cat .python-version)

# python setup
pyenv install "$PYTHON_VERSION"
pyenv virtualenv "$PYTHON_VERSION" $PROJECT
pyenv activate $PROJECT

pip install pip-tools

pip-compile --output-file=requirements.txt requirements.in
pip install -Ur requirements.txt
