/*
 * Copyright © the Plantd contributors. All rights reserved.
 *
 * This file is part of libplantd, distributed under the MIT license.
 * For full terms see the included LICENSE file.
 */

#define G_LOG_DOMAIN "plantd-hub"

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <czmq.h>

#include "pd-hub.h"

/*
 * PdHub:
 *
 * #PdHub is a PUB/SUB proxy device.
 */
struct _PdHub
{
  GObject parent;

  gchar       *frontend;
  gchar       *backend;
  gboolean     running;

  /*< private >*/
  zsock_t     *capture;
  zactor_t    *proxy;
};

enum {
  PROP_0,
  PROP_FRONTEND,
  PROP_BACKEND,
  N_PROPS
};

static GParamSpec *properties [N_PROPS];

G_DEFINE_TYPE (PdHub, pd_hub, G_TYPE_OBJECT)

static void
pd_hub_finalize (GObject *object)
{
  PdHub *self = (PdHub *)object;

  pd_hub_stop (self);

  g_clear_pointer (&self->frontend, g_free);
  g_clear_pointer (&self->backend, g_free);

  G_OBJECT_CLASS (pd_hub_parent_class)->finalize (object);
}

static void
pd_hub_get_property (GObject    *object,
                     guint       prop_id,
                     GValue     *value,
                     GParamSpec *pspec)
{
  PdHub *self = PD_HUB (object);

  switch (prop_id)
    {
    case PROP_FRONTEND:
      g_value_take_string (value, pd_hub_dup_frontend (self));
      break;

    case PROP_BACKEND:
      g_value_take_string (value, pd_hub_dup_backend (self));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
pd_hub_set_property (GObject      *object,
                     guint         prop_id,
                     const GValue *value,
                     GParamSpec   *pspec)
{
  PdHub *self = PD_HUB (object);

  switch (prop_id)
    {
    case PROP_FRONTEND:
      pd_hub_set_frontend (self, g_value_get_string (value));
      break;

    case PROP_BACKEND:
      pd_hub_set_backend (self, g_value_get_string (value));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
pd_hub_class_init (PdHubClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->finalize = pd_hub_finalize;
  object_class->get_property = pd_hub_get_property;
  object_class->set_property = pd_hub_set_property;

  properties [PROP_FRONTEND] =
    g_param_spec_string ("frontend",
                         "Frontend",
                         "The frontend endpoint for the proxy to connect or bind.",
                         NULL,
                         (G_PARAM_READWRITE | G_PARAM_EXPLICIT_NOTIFY | G_PARAM_STATIC_STRINGS));

  properties [PROP_BACKEND] =
    g_param_spec_string ("backend",
                         "Backend",
                         "The backend endpoint for the proxy to connect or bind.",
                         NULL,
                         (G_PARAM_READWRITE | G_PARAM_EXPLICIT_NOTIFY | G_PARAM_STATIC_STRINGS));

  g_object_class_install_properties (object_class, N_PROPS, properties);
}

static void
pd_hub_init (PdHub *self)
{
  self->running = FALSE;
}

PdHub *
pd_hub_new (const gchar *frontend,
            const gchar *backend)
{
  return g_object_new (PD_TYPE_HUB,
                       "frontend", frontend,
                       "backend", backend,
                       NULL);
}

void
pd_hub_start (PdHub *self)
{
  self->running = TRUE;

  g_info ("Proxy messages from '%s'", self->frontend);
  g_info ("Proxy messages to '%s'", self->backend);

  self->proxy = zactor_new (zproxy, NULL);
  g_assert_nonnull (self->proxy);

  zstr_sendx (self->proxy, "FRONTEND", "XSUB", self->frontend, NULL);
  zsock_wait (self->proxy);
  zstr_sendx (self->proxy, "BACKEND", "XPUB", self->backend, NULL);
  zsock_wait (self->proxy);

  self->capture = zsock_new_pull ("inproc://capture");
  g_assert_nonnull (self->capture);

  /* Switch on capturing, check that it works */
  zstr_sendx (self->proxy, "CAPTURE", "inproc://capture", NULL);
  zsock_wait (self->proxy);

  /* FIXME: move into a thread */
  /*
   *while (true)
   *  {
   *    g_autofree gchar *msg = NULL;
   *    msg = zstr_recv (self->capture);
   *    g_message ("%s", msg);
   *  }
   */
}

void
pd_hub_stop (PdHub *self)
{
  if (self->running == FALSE)
    return;

  self->running = FALSE;

  // FIXME: this should probably happen when a thread completes
  zsock_destroy (&(self->capture));
  zactor_destroy (&(self->proxy));
}

/**
 * pd_hub_dup_frontend:
 *
 * Copies the frontend of the hub and returns it to the caller (after locking
 * the object). A copy is used to avoid thread-races.
 */
gchar *
pd_hub_dup_frontend (PdHub *self)
{
  gchar *ret;

  g_return_val_if_fail (PD_IS_HUB (self), NULL);

  /*pd_object_lock (PD_OBJECT (self));*/
  ret = g_strdup (self->frontend);
  /*pd_object_unlock (PD_OBJECT (self));*/

  return g_steal_pointer (&ret);
}

void
pd_hub_set_frontend (PdHub       *self,
                     const gchar *frontend)
{
  g_return_if_fail (PD_IS_HUB (self));

  if (g_strcmp0 (frontend, self->frontend) != 0)
    {
      g_free (self->frontend);
      self->frontend = g_strdup (frontend);
      g_object_notify_by_pspec (G_OBJECT (self), properties [PROP_FRONTEND]);
    }
}

/**
 * pd_hub_dup_backend:
 *
 * Copies the backend of the hub and returns it to the caller (after locking
 * the object). A copy is used to avoid thread-races.
 */
gchar *
pd_hub_dup_backend (PdHub *self)
{
  gchar *ret;

  g_return_val_if_fail (PD_IS_HUB (self), NULL);

  /*pd_object_lock (PD_OBJECT (self));*/
  ret = g_strdup (self->backend);
  /*pd_object_unlock (PD_OBJECT (self));*/

  return g_steal_pointer (&ret);
}

void
pd_hub_set_backend (PdHub       *self,
                    const gchar *backend)
{
  g_return_if_fail (PD_IS_HUB (self));

  if (g_strcmp0 (backend, self->backend) != 0)
    {
      g_free (self->backend);
      self->backend = g_strdup (backend);
      g_object_notify_by_pspec (G_OBJECT (self), properties [PROP_BACKEND]);
    }
}
