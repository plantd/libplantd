/*
 * Copyright © the Plantd contributors. All rights reserved.
 *
 * This file is part of libplantd, distributed under the MIT license.
 * For full terms see the included LICENSE file.
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <json-glib/json-glib.h>

#include <plantd/plantd-enum-types.h>

#include "pd-configuration.h"
#include "pd-object.h"
#include "pd-property.h"
#include "pd-table.h"

/* XXX: this is the manual way of creating an enum type, might be better than the template way */
/*
 *#define PD_TYPE_CONFIGURATION_NAMESPACE (pd_configuration_namespace_get_type ())
 *static GType
 *pd_configuration_namespace_get_type (void)
 *{
 *  static GType configuration_namespace_type = 0;
 *
 *  if (!configuration_namespace_type)
 *    {
 *      static GEnumValue namespace_types[] = {
 *        { PD_CONFIGURATION_NAMESPACE_ACQUIRE,    "Acquire",    "acquire" },
 *        { PD_CONFIGURATION_NAMESPACE_ANALYZE,    "Analyze",    "analyze" },
 *        { PD_CONFIGURATION_NAMESPACE_CONTROL,    "Control",    "control" },
 *        { PD_CONFIGURATION_NAMESPACE_EXPERIMENT, "Experiment", "experiment" },
 *        { PD_CONFIGURATION_NAMESPACE_MONITOR,    "Monitor",    "monitor" },
 *        { PD_CONFIGURATION_NAMESPACE_PRESENT,    "Present",    "present" },
 *        { PD_CONFIGURATION_NAMESPACE_RECORD,     "Record",     "record" },
 *        { PD_CONFIGURATION_NAMESPACE_STATE,      "State",      "state" },
 *        { 0, NULL, NULL }
 *      };
 *
 *      configuration_namespace_type =
 *        g_enum_register_static ("PdConfigurationNamespaceType", namespace_types);
 *    }
 *
 *  return configuration_namespace_type;
 *}
 */

struct _PdConfiguration
{
  GObject                     parent;
  gchar                      *id;
  PdConfigurationNamespace  namespace;
  PdTable                  *properties;
  PdTable                  *objects;
};

enum {
  PROP_0,
  PROP_ID,
  PROP_NAMESPACE,
  PROP_PROPERTIES,
  PROP_OBJECTS,
  N_PROPS
};

static GParamSpec *class_properties [N_PROPS];

static void json_serializable_iface_init (gpointer g_iface);

G_DEFINE_TYPE_WITH_CODE (PdConfiguration, pd_configuration, G_TYPE_OBJECT,
                         G_IMPLEMENT_INTERFACE (JSON_TYPE_SERIALIZABLE,
                                                json_serializable_iface_init));

static JsonNode *
pd_configuration_serialize_property (JsonSerializable *serializable,
                                     const gchar      *name,
                                     const GValue     *value,
                                     GParamSpec       *pspec)
{
  JsonNode *retval = NULL;

  if (g_strcmp0 (name, "properties") == 0)
    {
      if (value != NULL && G_VALUE_HOLDS_OBJECT (value))
        retval = pd_table_serialize (PD_TABLE (g_value_get_object (value)),
                                       JSON_TYPE_ARRAY);
    }
  else if (g_strcmp0 (name, "objects") == 0)
    {
      if (value != NULL && G_VALUE_HOLDS_OBJECT (value)) {
        retval = pd_table_serialize (PD_TABLE (g_value_get_object (value)),
                                       JSON_TYPE_ARRAY);
      }
    }
  else if (g_strcmp0 (name, "namespace") == 0)
    {
      GValue nsval = { 0, };
      PdConfigurationNamespace namespace;

      retval = json_node_new (JSON_NODE_VALUE);

      namespace = g_value_get_enum (value);
      g_value_init (&nsval, G_TYPE_STRING);

      switch (namespace)
        {
        case PD_CONFIGURATION_NAMESPACE_ACQUIRE:
          g_value_set_static_string (&nsval, "ACQUIRE");
          break;
        case PD_CONFIGURATION_NAMESPACE_ANALYZE:
          g_value_set_static_string (&nsval, "ANALYZE");
          break;
        case PD_CONFIGURATION_NAMESPACE_CONTROL:
          g_value_set_static_string (&nsval, "CONTROL");
          break;
        case PD_CONFIGURATION_NAMESPACE_EXPERIMENT:
          g_value_set_static_string (&nsval, "EXPERIMENT");
          break;
        case PD_CONFIGURATION_NAMESPACE_MONITOR:
          g_value_set_static_string (&nsval, "MONITOR");
          break;
        case PD_CONFIGURATION_NAMESPACE_PRESENT:
          g_value_set_static_string (&nsval, "PRESENT");
          break;
        case PD_CONFIGURATION_NAMESPACE_RECORD:
          g_value_set_static_string (&nsval, "RECORD");
          break;
        case PD_CONFIGURATION_NAMESPACE_STATE:
          g_value_set_static_string (&nsval, "STATE");
          break;
        default:
          g_value_set_static_string (&nsval, "ACQUIRE");
          break;
        }

      json_node_set_value (retval, &nsval);
      g_value_unset (&nsval);
    }
  else
    {
      GValue copy = { 0, };

      retval = json_node_new (JSON_NODE_VALUE);

      g_value_init (&copy, G_PARAM_SPEC_VALUE_TYPE (pspec));
      g_value_copy (value, &copy);
      json_node_set_value (retval, &copy);
      g_value_unset (&copy);
    }

  return retval;
}

static gboolean
pd_configuration_deserialize_property (JsonSerializable *serializable,
                                       const gchar      *name,
                                       GValue           *value,
                                       GParamSpec       *pspec,
                                       JsonNode         *property_node)
{
  gboolean retval = FALSE;

  if (g_strcmp0 (name, "properties") == 0)
    {
      g_autoptr (PdTable) properties = NULL;

      properties = pd_table_new (PD_TYPE_PROPERTY);
      pd_table_deserialize (properties, property_node);

      g_value_take_object (value, g_steal_pointer (&properties));

      retval = TRUE;
    }
  else if (g_strcmp0 (name, "objects") == 0)
    {
      g_autoptr (PdTable) objects = NULL;

      objects = pd_table_new (PD_TYPE_OBJECT);
      pd_table_deserialize (objects, property_node);

      g_value_take_object (value, g_steal_pointer (&objects));

      retval = TRUE;
    }
  else if (g_strcmp0 (name, "namespace") == 0)
    {
      g_autofree const gchar *str = NULL;
      GValue nsval = { 0, };
      PdConfigurationNamespace namespace;

      json_node_get_value (property_node, &nsval);
      str = g_value_get_string (&nsval);

      // TODO: really should convert to lower/upper
      if (g_strcmp0 (str, "ACQUIRE") == 0)
        namespace = PD_CONFIGURATION_NAMESPACE_ACQUIRE;
      else if (g_strcmp0 (str, "ANALYZE") == 0)
        namespace = PD_CONFIGURATION_NAMESPACE_ANALYZE;
      else if (g_strcmp0 (str, "CONTROL") == 0)
        namespace = PD_CONFIGURATION_NAMESPACE_CONTROL;
      else if (g_strcmp0 (str, "EXPERIMENT") == 0)
        namespace = PD_CONFIGURATION_NAMESPACE_EXPERIMENT;
      else if (g_strcmp0 (str, "MONITOR") == 0)
        namespace = PD_CONFIGURATION_NAMESPACE_MONITOR;
      else if (g_strcmp0 (str, "PRESENT") == 0)
        namespace = PD_CONFIGURATION_NAMESPACE_PRESENT;
      else if (g_strcmp0 (str, "RECORD") == 0)
        namespace = PD_CONFIGURATION_NAMESPACE_RECORD;
      else if (g_strcmp0 (str, "STATE") == 0)
        namespace = PD_CONFIGURATION_NAMESPACE_STATE;
      else
        namespace = PD_CONFIGURATION_NAMESPACE_ACQUIRE;

      g_value_set_enum (value, namespace);

      retval = TRUE;
    }

  return retval;
}

static void
json_serializable_iface_init (gpointer g_iface)
{
  JsonSerializableIface *iface = g_iface;

  iface->serialize_property = pd_configuration_serialize_property;
  iface->deserialize_property = pd_configuration_deserialize_property;
}

static void
pd_configuration_finalize (GObject *object)
{
  PdConfiguration *self = (PdConfiguration *)object;

  g_debug ("!!!! Configuration finalize");

  /*pd_table_flush (self->objects);*/
  /*pd_table_flush (self->properties);*/

  g_clear_pointer (&self->id, g_free);
  g_clear_object (&self->objects);
  g_clear_object (&self->properties);

  G_OBJECT_CLASS (pd_configuration_parent_class)->finalize (object);

  g_debug ("!!!! Configuration finalize finish");
}

static void
pd_configuration_get_property (GObject    *object,
                               guint       prop_id,
                               GValue     *value,
                               GParamSpec *pspec)
{
  PdConfiguration *self = PD_CONFIGURATION (object);

  switch (prop_id)
    {
    case PROP_ID:
      g_value_set_string (value, pd_configuration_get_id (self));
      break;

    case PROP_NAMESPACE:
      g_value_set_enum (value, pd_configuration_get_namespace (self));
      break;

    case PROP_PROPERTIES:
      g_value_take_object (value, pd_configuration_ref_properties (self));
      break;

    case PROP_OBJECTS:
      g_value_take_object (value, pd_configuration_ref_objects (self));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
pd_configuration_set_property (GObject      *object,
                               guint         prop_id,
                               const GValue *value,
                               GParamSpec   *pspec)
{
  PdConfiguration *self = PD_CONFIGURATION (object);

  switch (prop_id)
    {
    case PROP_ID:
      pd_configuration_set_id (self, g_value_get_string (value));
      break;

    case PROP_NAMESPACE:
      pd_configuration_set_namespace (self, g_value_get_enum (value));
      break;

    case PROP_PROPERTIES:
      pd_configuration_set_properties (self, g_value_get_object (value));
      break;

    case PROP_OBJECTS:
      pd_configuration_set_objects (self, g_value_get_object (value));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
pd_configuration_class_init (PdConfigurationClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->finalize = pd_configuration_finalize;
  object_class->get_property = pd_configuration_get_property;
  object_class->set_property = pd_configuration_set_property;

  class_properties [PROP_ID] =
    g_param_spec_string ("id",
                         "ID",
                         "The configuration ID.",
                         NULL,
                         (G_PARAM_READWRITE | G_PARAM_EXPLICIT_NOTIFY | G_PARAM_STATIC_STRINGS));

  class_properties [PROP_NAMESPACE] =
    g_param_spec_enum ("namespace",
                       "Namespace",
                       "The configuration namespace.",
                       PD_TYPE_CONFIGURATION_NAMESPACE,
                       PD_CONFIGURATION_NAMESPACE_ACQUIRE,
                       (G_PARAM_READWRITE | G_PARAM_EXPLICIT_NOTIFY | G_PARAM_STATIC_STRINGS));

  class_properties [PROP_PROPERTIES] =
    g_param_spec_object ("properties",
                         "Properties",
                         "List of properties.",
                         PD_TYPE_TABLE,
                         (G_PARAM_READWRITE | G_PARAM_EXPLICIT_NOTIFY | G_PARAM_STATIC_STRINGS));

  class_properties [PROP_OBJECTS] =
    g_param_spec_object ("objects",
                         "Objects",
                         "List of objects.",
                         PD_TYPE_TABLE,
                         (G_PARAM_READWRITE | G_PARAM_EXPLICIT_NOTIFY | G_PARAM_STATIC_STRINGS));

  g_object_class_install_properties (object_class, N_PROPS, class_properties);
}

static void
pd_configuration_init (PdConfiguration *self)
{
  self->properties = pd_table_new (PD_TYPE_PROPERTY);
  self->objects = pd_table_new (PD_TYPE_OBJECT);
}

PdConfiguration *
pd_configuration_new (void)
{
  return g_object_new (PD_TYPE_CONFIGURATION, NULL);
}

gchar *
pd_configuration_serialize (PdConfiguration *self)
{
  g_return_val_if_fail (PD_IS_CONFIGURATION (self), NULL);

  return json_gobject_to_data (G_OBJECT (self), NULL);
}

void
pd_configuration_deserialize (PdConfiguration *self,
                              const gchar     *data)
{
  g_autoptr (GObject) object = NULL;
  g_autoptr (PdTable) objects = NULL;
  g_autoptr (PdTable) properties = NULL;
  g_autofree gchar *id = NULL;
  PdConfigurationNamespace namespace;
  GError *err = NULL;

  object = json_gobject_from_data (PD_TYPE_CONFIGURATION,
                                   data,
                                   -1,
                                   &err);

  if (err != NULL)
    {
      g_critical ("%s", err->message);
      g_error_free (err);
    }

  g_return_if_fail (object != NULL);
  g_return_if_fail (PD_IS_CONFIGURATION (object));

  id = g_strdup (pd_configuration_get_id (PD_CONFIGURATION (object)));
  namespace = pd_configuration_get_namespace (PD_CONFIGURATION (object));
  properties = pd_configuration_ref_properties (PD_CONFIGURATION (object));
  objects = pd_configuration_ref_objects (PD_CONFIGURATION (object));

  pd_configuration_set_id (self, id);
  pd_configuration_set_namespace (self, namespace);
  pd_configuration_set_properties (self, properties);
  pd_configuration_set_objects (self, objects);

  g_clear_object (&object);
}

void
pd_configuration_load (PdConfiguration  *self,
                       const gchar      *filename,
                       GError          **error)
{
  g_autoptr (GFile) file = NULL;
  g_autoptr (GFileInputStream) in = NULL;
  g_autoptr (GString) contents = NULL;
  g_autofree gchar *buf = NULL;
  gssize read;

  g_return_if_fail (PD_IS_CONFIGURATION (self));

  file = g_file_new_for_path (filename);
  g_return_if_fail (file != NULL);

  in = g_file_read (file, NULL, error);
  g_return_if_fail (in != NULL);
  g_return_if_fail (error == NULL || *error == NULL);

  contents = g_string_new (NULL);
  buf = g_malloc (1024);

  while (TRUE)
    {
      read = g_input_stream_read (G_INPUT_STREAM(in), buf, 1024 - 1, NULL, NULL);
      if (read > 0)
        {
          buf[read] = '\0';
          contents = g_string_append (contents, buf);
        }
      else if (read < 0)
        {
          // error = ...
          break;
        }
      else
        {
          break;
        }
    }

  pd_configuration_deserialize (self, contents->str);

  g_input_stream_close (G_INPUT_STREAM (in), NULL, error);
}

void
pd_configuration_save (PdConfiguration  *self,
                       const gchar      *filename,
                       GError          **error)
{
  g_autoptr (GFile) file = NULL;
  g_autoptr (GFileOutputStream) out = NULL;
  g_autofree char *data = NULL;
  gsize wrote;

  g_return_if_fail (PD_IS_CONFIGURATION (self));

  file = g_file_new_for_path (filename);
  g_return_if_fail (file != NULL);

  out = g_file_replace (file, NULL, FALSE,
                        G_FILE_CREATE_REPLACE_DESTINATION,
                        NULL, error);

  data = pd_configuration_serialize (self);

  g_output_stream_write_all (G_OUTPUT_STREAM (out),
                             data, strlen (data),
                             &wrote, NULL, error);
}

/**
 * pd_configuration_get_id:
 * @self: a #PdConfiguration
 *
 * Returns: (transfer none): The id, or %NULL
 */
const gchar *
pd_configuration_get_id (PdConfiguration *self)
{
  g_return_val_if_fail (PD_IS_CONFIGURATION (self), NULL);

  return self->id;
}

/**
 * pd_configuration_set_id:
 * @self: a #PdConfiguration
 * @id: (nullable): a string containing the id, or %NULL
 *
 * Sets the #PdConfiguration:id property.
 */
void
pd_configuration_set_id (PdConfiguration *self,
                         const gchar     *id)
{
  g_return_if_fail (PD_IS_CONFIGURATION (self));

  if (g_strcmp0 (id, self->id) != 0)
    {
      g_free (self->id);
      self->id = g_strdup (id);
      g_object_notify_by_pspec (G_OBJECT (self), class_properties [PROP_ID]);
    }
}

PdConfigurationNamespace
pd_configuration_get_namespace (PdConfiguration *self)
{
  /* FIXME: this should be a none/unknown type */
  g_return_val_if_fail (PD_IS_CONFIGURATION (self), PD_CONFIGURATION_NAMESPACE_ACQUIRE);

  return self->namespace;
}

void
pd_configuration_set_namespace (PdConfiguration          *self,
                                PdConfigurationNamespace  namespace)
{
  g_return_if_fail (PD_IS_CONFIGURATION (self));

  self->namespace = namespace;
}

/**
 * pd_configuration_add_object:
 * @self: a #PdConfiguration
 * @object: (transfer full): object to add
 */
void
pd_configuration_add_object (PdConfiguration *self,
                             PdObject        *object)
{
  GValue val = G_VALUE_INIT;
  char *id;

  g_return_if_fail (PD_IS_CONFIGURATION (self));
  g_return_if_fail (PD_IS_OBJECT (object));

  if (self->objects == NULL)
    self->objects = pd_table_new (PD_TYPE_OBJECT);

  g_value_init (&val, PD_TYPE_OBJECT);
  g_value_take_object (&val, object);
  id = g_strdup (pd_object_get_id (object));

  pd_table_add (self->objects, id, &val);
  g_object_notify_by_pspec (G_OBJECT (self), class_properties [PROP_OBJECTS]);

  g_value_unset (&val);
  g_free (id);
}

void
pd_configuration_remove_object (PdConfiguration *self,
                                const gchar     *id)
{
  g_return_if_fail (PD_IS_CONFIGURATION (self));
  g_return_if_fail (self->objects != NULL);

  pd_table_remove (self->objects, id);
  g_object_notify_by_pspec (G_OBJECT (self), class_properties [PROP_OBJECTS]);
}

/**
 * pd_configuration_lookup_object:
 * @self: a #PdConfiguration
 * @id: the ID of the object to retrieve
 *
 * Returns: (transfer none): The object if found, NULL otherwise.
 */
PdObject *
pd_configuration_lookup_object (PdConfiguration *self,
                                const gchar     *id)
{
  g_return_val_if_fail (PD_IS_CONFIGURATION (self), NULL);
  g_return_val_if_fail (self->objects != NULL, NULL);

  return g_value_get_object (pd_table_get (self->objects, id));
}

gboolean
pd_configuration_has_object (PdConfiguration *self,
                             const gchar     *id)
{
  gboolean ret;

  g_return_val_if_fail (PD_IS_CONFIGURATION (self), FALSE);
  g_return_val_if_fail (self->objects != NULL, FALSE);
  g_return_val_if_fail (id != NULL, FALSE);

  ret = pd_table_has (self->objects, id);

  return ret;
}

/**
 * pd_configuration_add_property:
 * @self: a #PdConfiguration
 * @property: (transfer full): property to add
 */
void
pd_configuration_add_property (PdConfiguration *self,
                               PdProperty      *property)
{
  GValue val = G_VALUE_INIT;
  gchar *key;

  g_return_if_fail (PD_IS_CONFIGURATION (self));
  g_return_if_fail (PD_IS_PROPERTY (property));

  if (self->properties == NULL)
    self->properties = pd_table_new (PD_TYPE_PROPERTY);

  g_value_init (&val, PD_TYPE_PROPERTY);
  g_value_take_object (&val, property);
  key = g_strdup (pd_property_get_key (property));

  pd_table_add (self->properties, key, &val);
  g_object_notify_by_pspec (G_OBJECT (self), class_properties [PROP_PROPERTIES]);

  g_value_unset (&val);
  g_free (key);
}

void
pd_configuration_remove_property (PdConfiguration *self,
                                  const gchar     *key)
{
  g_return_if_fail (PD_IS_CONFIGURATION (self));
  g_return_if_fail (self->properties != NULL);

  pd_table_remove (self->properties, key);
  g_object_notify_by_pspec (G_OBJECT (self), class_properties [PROP_PROPERTIES]);
}

/**
 * pd_configuration_lookup_property:
 * @self: a #PdConfiguration
 * @key: the key of the property to retrieve
 *
 * Returns: (transfer none): The property if found, NULL otherwise.
 */
PdProperty *
pd_configuration_lookup_property (PdConfiguration *self,
                                  const gchar     *key)
{
  g_return_val_if_fail (PD_IS_CONFIGURATION (self), NULL);
  g_return_val_if_fail (self->properties != NULL, NULL);

  return g_value_get_object (pd_table_get (self->properties, key));
}

gboolean
pd_configuration_has_property (PdConfiguration *self,
                               const gchar     *key)
{
  gboolean ret;

  g_return_val_if_fail (PD_IS_CONFIGURATION (self), FALSE);
  g_return_val_if_fail (self->properties != NULL, FALSE);
  g_return_val_if_fail (key != NULL, FALSE);

  ret = pd_table_has (self->properties, key);

  return ret;
}

/**
 * pd_configuration_get_objects:
 * @self: a #PdConfiguration
 *
 * Returns: (element-type utf8 Pd.Object) (transfer full): an table of
 *          #PdObject objects, free the table with g_hash_table_destroy
 *          when done.
 */
GHashTable *
pd_configuration_get_objects (PdConfiguration *self)
{
  g_autoptr (GHashTable) objects = NULL;
  GList *keys;

  g_return_val_if_fail (PD_IS_CONFIGURATION (self), NULL);

  objects = g_hash_table_new_full (g_str_hash,
                                   g_str_equal,
                                   g_free,
                                   g_object_unref);

  keys = pd_table_get_keys (self->objects);
  for (GList *key = keys; key != NULL; key = key->next)
    {
      GValue *val = g_new0 (GValue, 1);

      val = g_value_init (val, PD_TYPE_OBJECT);
      g_value_copy (pd_table_get (self->objects, key->data), val);
      g_hash_table_insert (objects,
                           g_strdup (key->data),
                           g_value_get_object (val));

      g_free (val);
    }

  g_list_free (keys);

  return g_hash_table_ref (objects);
}

/**
 * pd_configuration_ref_objects:
 *
 * Gets the object list, and returns a new reference
 * to the #PdTable.
 *
 * Returns: (transfer full) (nullable): a #PdTable or %NULL
 */
PdTable *
pd_configuration_ref_objects (PdConfiguration *self)
{
  PdTable *ret = NULL;

  g_return_val_if_fail (PD_IS_CONFIGURATION (self), NULL);

  g_set_object (&ret, self->objects);

  return g_steal_pointer (&ret);
}

/**
 * pd_configuration_set_objects:
 * @self: a #PdConfiguration
 * @objects: An #PdTable of #PdObject objects to set.
 */
void
pd_configuration_set_objects (PdConfiguration *self,
                              PdTable         *objects)
{
  g_return_if_fail (PD_IS_CONFIGURATION (self));
  g_return_if_fail (PD_IS_TABLE (self->objects));

  if (g_set_object (&self->objects, objects))
    g_object_notify_by_pspec (G_OBJECT (self), class_properties [PROP_OBJECTS]);
}

/**
 * pd_configuration_get_properties:
 * @self: a #PdConfiguration
 *
 * Returns: (element-type utf8 Pd.Property) (transfer none): a hash table of
 *          #PdProperty objects, free the table with g_hash_table_destroy when
 *          done.
 */
GHashTable *
pd_configuration_get_properties (PdConfiguration *self)
{
  g_autoptr (GHashTable) properties = NULL;
  GList *keys;

  g_return_val_if_fail (PD_IS_CONFIGURATION (self), NULL);

  properties = g_hash_table_new_full (g_str_hash,
                                      g_str_equal,
                                      g_free,
                                      g_object_unref);

  keys = pd_table_get_keys (self->properties);
  for (GList *key = keys; key != NULL; key = key->next)
    {
      GValue *val = g_new0 (GValue, 1);

      val = g_value_init (val, PD_TYPE_PROPERTY);
      g_value_copy (pd_table_get (self->properties, key->data), val);
      g_hash_table_insert (properties,
                           g_strdup (key->data),
                           g_object_ref (g_value_get_object (val)));

      g_free (val);
    }

  g_list_free (keys);

  return g_hash_table_ref (properties);
}

/**
 * pd_configuration_ref_properties:
 *
 * Gets the property list, and returns a new reference
 * to the #PdTable.
 *
 * Returns: (transfer full) (nullable): a #PdTable or %NULL
 */
PdTable *
pd_configuration_ref_properties (PdConfiguration *self)
{
  PdTable *ret = NULL;

  g_return_val_if_fail (PD_IS_CONFIGURATION (self), NULL);

  g_set_object (&ret, self->properties);

  return g_steal_pointer (&ret);
}

/**
 * pd_configuration_set_properties:
 * @self: a #PdConfiguration
 * @properties: An #PdTable of #PdProperty objects to set.
 */
void
pd_configuration_set_properties (PdConfiguration *self,
                                 PdTable         *properties)
{
  g_return_if_fail (PD_IS_CONFIGURATION (self));
  g_return_if_fail (PD_IS_TABLE (self->properties));

  if (g_set_object (&self->properties, properties))
    g_object_notify_by_pspec (G_OBJECT (self), class_properties [PROP_PROPERTIES]);
}
