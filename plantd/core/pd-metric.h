/*
 * Copyright © the Plantd contributors. All rights reserved.
 *
 * This file is part of libplantd, distributed under the MIT license.
 * For full terms see the included LICENSE file.
 */

#pragma once

#if !defined (PLANTD_INSIDE) && !defined (PLANTD_COMPILATION)
# error "Only <plantd/plantd.h> can be included directly."
#endif

#include <glib-object.h>

#include <plantd/plantd-types.h>

G_BEGIN_DECLS

#define PD_TYPE_METRIC pd_metric_get_type ()
G_DECLARE_FINAL_TYPE (PdMetric, pd_metric, PD, METRIC, GObject)

PdMetric      *pd_metric_new          (void);
PdMetric      *pd_metric_new_full     (const gchar   *id,
                                       const gchar   *name,
                                       PdMetricTable *data);

gchar         *pd_metric_serialize    (PdMetric      *self);
void           pd_metric_deserialize  (PdMetric      *self,
                                       const gchar   *data);

const gchar   *pd_metric_get_id       (PdMetric      *self);
void           pd_metric_set_id       (PdMetric      *self,
                                       const gchar   *id);
gchar         *pd_metric_dup_id       (PdMetric      *self);

const gchar   *pd_metric_get_name     (PdMetric      *self);
void           pd_metric_set_name     (PdMetric      *self,
                                       const gchar   *name);
gchar         *pd_metric_dup_name     (PdMetric      *self);

PdMetricTable *pd_metric_get_data     (PdMetric      *self);
PdMetricTable *pd_metric_ref_data     (PdMetric      *self);
void           pd_metric_set_data     (PdMetric      *self,
                                       PdMetricTable *data);

G_END_DECLS
