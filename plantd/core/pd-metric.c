/*
 * Copyright © the Plantd contributors. All rights reserved.
 *
 * This file is part of libplantd, distributed under the MIT license.
 * For full terms see the included LICENSE file.
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <json-glib/json-glib.h>

#include "pd-metric.h"
#include "pd-metric-table.h"

/*
 * PdMetric:
 *
 * Represents a ...
 */
struct _PdMetric
{
  GObject          parent;
  gchar           *id;
  gchar           *name;
  PdMetricTable *data;
};

enum {
  PROP_0,
  PROP_ID,
  PROP_NAME,
  PROP_DATA,
  N_PROPS
};

static GParamSpec *properties [N_PROPS];

G_DEFINE_TYPE (PdMetric, pd_metric, G_TYPE_OBJECT)

static void
pd_metric_finalize (GObject *object)
{
  PdMetric *self = (PdMetric *)object;

  g_clear_pointer (&self->id, g_free);
  g_clear_pointer (&self->name, g_free);
  g_clear_object (&self->data);

  G_OBJECT_CLASS (pd_metric_parent_class)->finalize (object);
}

static void
pd_metric_get_property (GObject    *object,
                        guint       prop_id,
                        GValue     *value,
                        GParamSpec *pspec)
{
  PdMetric *self = PD_METRIC (object);

  switch (prop_id)
    {
    case PROP_ID:
      g_value_take_string (value, pd_metric_dup_id (self));
      break;

    case PROP_NAME:
      g_value_take_string (value, pd_metric_dup_name (self));
      break;

    case PROP_DATA:
      g_value_take_object (value, pd_metric_ref_data (self));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
pd_metric_set_property (GObject      *object,
                        guint         prop_id,
                        const GValue *value,
                        GParamSpec   *pspec)
{
  PdMetric *self = PD_METRIC (object);

  switch (prop_id)
    {
    case PROP_ID:
      pd_metric_set_id (self, g_value_get_string (value));
      break;

    case PROP_NAME:
      pd_metric_set_name (self, g_value_get_string (value));
      break;

    case PROP_DATA:
      pd_metric_set_data (self, g_value_get_object (value));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
pd_metric_class_init (PdMetricClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->finalize = pd_metric_finalize;
  object_class->get_property = pd_metric_get_property;
  object_class->set_property = pd_metric_set_property;

  properties [PROP_ID] =
    g_param_spec_string ("id",
                         "ID",
                         "The metric message identifier",
                         NULL,
                         (G_PARAM_READWRITE | G_PARAM_EXPLICIT_NOTIFY | G_PARAM_STATIC_STRINGS));

  properties [PROP_NAME] =
    g_param_spec_string ("name",
                         "Name",
                         "A name that represents the metric",
                         NULL,
                         (G_PARAM_READWRITE | G_PARAM_EXPLICIT_NOTIFY | G_PARAM_STATIC_STRINGS));

  properties [PROP_DATA] =
    g_param_spec_object ("data",
                         "Data",
                         "Metric data table",
                         PD_TYPE_METRIC_TABLE,
                         (G_PARAM_READWRITE | G_PARAM_EXPLICIT_NOTIFY | G_PARAM_STATIC_STRINGS));

  g_object_class_install_properties (object_class, N_PROPS, properties);
}

static void
pd_metric_init (PdMetric *self)
{
}

PdMetric *
pd_metric_new (void)
{
  return g_object_new (PD_TYPE_METRIC, NULL);
}

PdMetric *
pd_metric_new_full (const gchar   *id,
                    const gchar   *name,
                    PdMetricTable *data)
{
  return g_object_new (PD_TYPE_METRIC,
                       "id", id,
                       "name", name,
                       "data", data,
                       NULL);
}

gchar *
pd_metric_serialize (PdMetric *self)
{
  g_return_val_if_fail (PD_IS_METRIC (self), NULL);

  return json_gobject_to_data (G_OBJECT (self), NULL);
}

void
pd_metric_deserialize (PdMetric    *self,
                       const gchar *data)
{
  g_autoptr (GObject) object = NULL;
  g_autoptr (PdMetricTable) table = NULL;
  g_autofree gchar *id = NULL;
  g_autofree gchar *name = NULL;

  GError *err = NULL;
  object = json_gobject_from_data (PD_TYPE_METRIC,
                                   data,
                                   -1,
                                   &err);

  if (err != NULL)
    {
      g_critical ("%s", err->message);
      g_error_free (err);
    }

  g_return_if_fail (object != NULL);
  g_return_if_fail (PD_IS_METRIC (object));

  id = pd_metric_dup_id (PD_METRIC (object));
  name = pd_metric_dup_name (PD_METRIC (object));
  table = pd_metric_ref_data (PD_METRIC (object));

  pd_metric_set_id (self, id);
  pd_metric_set_name (self, name);
  pd_metric_set_data (self, table);

  g_clear_object (&object);
}

const gchar *
pd_metric_get_id (PdMetric *self)
{
  g_return_val_if_fail (PD_IS_METRIC (self), NULL);

  return self->id;
}

void
pd_metric_set_id (PdMetric    *self,
                  const gchar *id)
{
  g_return_if_fail (PD_IS_METRIC (self));

  if (g_strcmp0 (id, self->id) != 0)
    {
      g_free (self->id);
      self->id = g_strdup (id);
      g_object_notify_by_pspec (G_OBJECT (self), properties [PROP_ID]);
    }
}

/**
 * pd_metric_dup_id:
 *
 * Copies the id of the metric and returns it to the caller (after locking
 * the object). A copy is used to avoid thread-races.
 */
gchar *
pd_metric_dup_id (PdMetric *self)
{
  gchar *ret;

  g_return_val_if_fail (PD_IS_METRIC (self), NULL);

  /*pd_object_lock (PD_OBJECT (self));*/
  ret = g_strdup (self->id);
  /*pd_object_unlock (PD_OBJECT (self));*/

  return g_steal_pointer (&ret);
}

const gchar *
pd_metric_get_name (PdMetric *self)
{
  g_return_val_if_fail (PD_IS_METRIC (self), NULL);

  return self->name;
}

void
pd_metric_set_name (PdMetric    *self,
                    const gchar *name)
{
  g_return_if_fail (PD_IS_METRIC (self));

  if (g_strcmp0 (name, self->name) != 0)
    {
      g_free (self->name);
      self->name = g_strdup (name);
      g_object_notify_by_pspec (G_OBJECT (self), properties [PROP_NAME]);
    }
}

/**
 * pd_metric_dup_name:
 *
 * Copies the name of the metric and returns it to the caller (after locking
 * the object). A copy is used to avoid thread-races.
 */
gchar *
pd_metric_dup_name (PdMetric *self)
{
  gchar *ret;

  g_return_val_if_fail (PD_IS_METRIC (self), NULL);

  /*pd_object_lock (PD_OBJECT (self));*/
  ret = g_strdup (self->name);
  /*pd_object_unlock (PD_OBJECT (self));*/

  return g_steal_pointer (&ret);
}

/**
 * pd_metric_get_data:
 * @self: a #PdMetric
 *
 * Retrieve the #PdMetricTable held as the metric data.
 *
 * Returns: (transfer full): a #PdMetricTable if one is set.
 */
PdMetricTable *
pd_metric_get_data (PdMetric *self)
{
  PdMetricTable *data;

  g_return_val_if_fail (PD_IS_METRIC (self), NULL);

  g_object_get (self, "data", &data, NULL);

  return data;
}

/**
 * pd_metric_ref_data:
 * @self: a #PdMetric
 *
 * Gets the data for the metric, and returns a new reference
 * to the #PdMetricTable.
 *
 * Returns: (transfer full) (nullable): a #PdMetricTable or %NULL
 */
PdMetricTable *
pd_metric_ref_data (PdMetric *self)
{
  PdMetricTable *ret = NULL;

  g_return_val_if_fail (PD_IS_METRIC (self), NULL);

  /*pd_object_lock (G_OBJECT (self));*/
  g_set_object (&ret, self->data);
  /*pd_object_unlock (G_OBJECT (self));*/

  return g_steal_pointer (&ret);
}

void
pd_metric_set_data (PdMetric      *self,
                    PdMetricTable *data)
{
  g_return_if_fail (PD_IS_METRIC (self));
  g_return_if_fail (PD_IS_METRIC_TABLE (data));

  if (g_set_object (&self->data, data))
    g_object_notify_by_pspec (G_OBJECT (self), properties [PROP_DATA]);
}
