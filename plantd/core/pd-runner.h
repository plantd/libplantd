/*
 * Copyright © the Plantd contributors. All rights reserved.
 *
 * This file is part of libplantd, distributed under the MIT license.
 * For full terms see the included LICENSE file.
 */

#pragma once

#if !defined (PLANTD_INSIDE) && !defined (PLANTD_COMPILATION)
# error "Only <plantd/plantd.h> can be included directly."
#endif

#include <glib.h>

#include <plantd/plantd-types.h>

G_BEGIN_DECLS

#define PD_TYPE_RUNNER pd_runner_get_type ()
G_DECLARE_DERIVABLE_TYPE (PdRunner, pd_runner, PD, RUNNER, GObject)

struct _PdRunnerClass
{
  /*< private >*/
  GObject parent_class;

  /*< public >*/
  /* signals */
  void (*application_registered) (PdRunner      *self,
                                  PdApplication *application);

  void (*application_launched)   (PdRunner      *self,
                                  PdApplication *application);

  // Others: ?
  //  - application_stopped
  //  - application_removed

  /*< private >*/
  gpointer padding[12];
};

PdRunner      *pd_runner_new                (void);

void           pd_runner_add_application    (PdRunner       *self,
                                             const gchar    *name,
                                             PdApplication  *application);
void           pd_runner_remove_application (PdRunner       *self,
                                             const gchar    *name);
PdApplication *pd_runner_lookup_application (PdRunner       *self,
                                             const gchar    *name);
gboolean       pd_runner_has_application    (PdRunner       *self,
                                             const gchar    *name);
int            pd_runner_launch_application (PdRunner       *self,
                                             const gchar    *name,
                                             gint            argc,
                                             gchar         **argv,
                                             GError        **error);

G_END_DECLS
