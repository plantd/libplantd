/*
 * Copyright © the Plantd contributors. All rights reserved.
 *
 * This file is part of libplantd, distributed under the MIT license.
 * For full terms see the included LICENSE file.
 */

#define G_LOG_DOMAIN "plantd-source"

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <json-glib/json-glib.h>
#include <czmq.h>
#include <plantd/plantd.h>

#include "pd-source.h"

#define SOURCE_TIMEOUT 1000000

/*
 * PdSource:
 *
 * #PdSource is a data producer structure meant to be used with
 * implementations of a #PdApplication.
 */
struct _PdSource
{
  GObject parent;

  gchar       *endpoint;
  gchar       *envelope;      // TODO: make this an enum
  gboolean     running;
  GAsyncQueue *queue;
};

enum {
  PROP_0,
  PROP_ENDPOINT,
  PROP_ENVELOPE,
  N_PROPS
};

static GParamSpec *properties [N_PROPS];

G_DEFINE_TYPE (PdSource, pd_source, G_TYPE_OBJECT)

static void
pd_source_finalize (GObject *object)
{
  PdSource *self = (PdSource *)object;

  g_clear_pointer (&self->endpoint, g_free);
  g_clear_pointer (&self->envelope, g_free);

  g_async_queue_unref (self->queue);

  G_OBJECT_CLASS (pd_source_parent_class)->finalize (object);
}

static void
pd_source_get_property (GObject    *object,
                        guint       prop_id,
                        GValue     *value,
                        GParamSpec *pspec)
{
  PdSource *self = PD_SOURCE (object);

  switch (prop_id)
    {
    case PROP_ENDPOINT:
      g_value_set_string (value, pd_source_get_endpoint (self));
      break;

    case PROP_ENVELOPE:
      g_value_set_string (value, pd_source_get_envelope (self));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
pd_source_set_property (GObject      *object,
                        guint         prop_id,
                        const GValue *value,
                        GParamSpec   *pspec)
{
  PdSource *self = PD_SOURCE (object);

  switch (prop_id)
    {
    case PROP_ENDPOINT:
      pd_source_set_endpoint (self, g_value_get_string (value));
      break;

    case PROP_ENVELOPE:
      pd_source_set_envelope (self, g_value_get_string (value));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
pd_source_class_init (PdSourceClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->finalize = pd_source_finalize;
  object_class->get_property = pd_source_get_property;
  object_class->set_property = pd_source_set_property;

  properties [PROP_ENDPOINT] =
    g_param_spec_string ("endpoint",
                         "Endpoint",
                         "The endpoint for the source to connect or bind.",
                         NULL,
                         (G_PARAM_READWRITE | G_PARAM_EXPLICIT_NOTIFY));

  properties [PROP_ENVELOPE] =
    g_param_spec_string ("envelope",
                         "Envelope",
                         "The envelope to add to the messages.",
                         NULL,
                         (G_PARAM_READWRITE | G_PARAM_EXPLICIT_NOTIFY));

  g_object_class_install_properties (object_class, N_PROPS, properties);
}

static void
pd_source_init (PdSource *self)
{
  self->running = FALSE;
  self->queue = g_async_queue_new ();
}

PdSource *
pd_source_new (const gchar *endpoint,
               const gchar *envelope)
{
  return g_object_new (PD_TYPE_SOURCE,
                       "endpoint", endpoint,
                       "envelope", envelope,
                       NULL);
}

static void
_run_publisher_cb (GObject      *source_object,
                   GAsyncResult *result,
                   gpointer      user_data)
{
  g_debug ("message publisher finished");
}

static void
_run_publisher_cancel (PdSource *self,
                       gpointer  data)
{
  GCancellable *cancellable G_GNUC_UNUSED;

  /*cancellable = G_CANCELLABLE (cancellable);*/

  self->running = FALSE;

  g_debug ("message publisher cancelled");
}

static void
_run_publisher_thread (GTask        *task,
                       gpointer      source_object,
                       gpointer      task_data,
                       GCancellable *cancellable)
{
  PdSource *self;
  zsock_t *publisher;

  g_assert (source_object == g_task_get_source_object (task));
  g_assert (cancellable == g_task_get_cancellable (task));

  self = PD_SOURCE (source_object);

  g_debug ("source thread binding to %s", self->endpoint);
  publisher = zsock_new_pub (self->endpoint);

  self->running = TRUE;

  while (self->running)
    {
      gpointer ptr;
      g_autofree gchar *msg = NULL;

      if (g_cancellable_is_cancelled (cancellable))
        {
          /*g_task_return_new_error (task,*/
                                   /*G_IO_ERROR, G_IO_ERROR_CANCELLED,*/
                                   /*"Publisher task cancelled");*/
          /*return;*/
          break;
        }

      g_async_queue_lock (self->queue);

      PD_TRACE_MSG("source thread waiting for data");
      /* XXX: consider putting the envelope be added as the first frame in a message? */
      ptr = g_async_queue_timeout_pop_unlocked (self->queue, SOURCE_TIMEOUT);
      if (ptr == NULL)
        {
          PD_TRACE_MSG("source thread timeout received");
          g_async_queue_unlock (self->queue);
          continue;
        }

      msg = g_strdup_printf ("%s%s",
                             self->envelope,
                             ((GString *) ptr)->str);
      PD_TRACE_MSG("source thread received data: %s", msg);

      /* the data received here should always be a GString so free with that */
      g_string_free (ptr, TRUE);

      if (zstr_send (publisher, msg) == -1)
        {
          g_error ("source failed to send data");
          break;
        }

      g_async_queue_unlock (self->queue);
    }
  
  g_debug ("destroying source socket for %s", self->endpoint);
  zsock_destroy (&publisher);

  g_task_return_boolean (task, TRUE);
}

static void
_run_publisher_async (PdSource *self)
{
  g_autoptr (GTask) task = NULL;
  g_autoptr (GCancellable) cancellable = NULL;

  g_return_if_fail (PD_IS_SOURCE (self));

  cancellable = g_cancellable_new ();
  task = g_task_new (self, cancellable, _run_publisher_cb, NULL);

  g_task_run_in_thread (task, _run_publisher_thread);
}

void
pd_source_start (PdSource *self)
{
  /* Start publisher for sending messages */
  _run_publisher_async (self);
}

void
pd_source_stop (PdSource *self)
{
  /* Stop publisher for sending messages */
  _run_publisher_cancel (self, NULL);
}

void
pd_source_queue_message (PdSource    *self,
                         const gchar *data)
{
  GString *wrap;

  g_return_if_fail (PD_IS_SOURCE (self));

  wrap = g_string_new (data);

  PD_TRACE_MSG ("queue message");
  g_async_queue_lock (self->queue);
  g_async_queue_push_unlocked (self->queue, wrap);
  g_async_queue_unlock (self->queue);
}

gchar *
pd_source_serialize (PdSource *self)
{
  g_return_val_if_fail (PD_IS_SOURCE (self), NULL);

  return json_gobject_to_data (G_OBJECT (self), NULL);
}

void
pd_source_deserialize (PdSource    *self,
                       const gchar *data)
{
  GError *err = NULL;
  GObject *object = json_gobject_from_data (PD_TYPE_SOURCE,
                                            data,
                                            -1,
                                            &err);

  if (err != NULL)
    {
      g_critical ("%s", err->message);
      g_error_free (err);
    }

  g_return_if_fail (object != NULL);
  g_return_if_fail (PD_IS_SOURCE (object));

  pd_source_set_endpoint (self, pd_source_get_endpoint (PD_SOURCE (object)));
  pd_source_set_envelope (self, pd_source_get_envelope (PD_SOURCE (object)));

  g_object_unref (object);
}

gboolean
pd_source_running (PdSource *self)
{
  g_return_val_if_fail (PD_IS_SOURCE (self), FALSE);

  return self->running;
}

const gchar *
pd_source_get_endpoint (PdSource *self)
{
  g_return_val_if_fail (PD_IS_SOURCE (self), NULL);

  return self->endpoint;
}

void
pd_source_set_endpoint (PdSource    *self,
                        const gchar *endpoint)
{
  g_return_if_fail (PD_IS_SOURCE (self));

  if (g_strcmp0 (endpoint, self->endpoint) != 0)
    {
      g_free (self->endpoint);
      self->endpoint = g_strdup (endpoint);
      g_object_notify_by_pspec (G_OBJECT (self), properties [PROP_ENDPOINT]);
    }
}

const gchar *
pd_source_get_envelope (PdSource *self)
{
  g_return_val_if_fail (PD_IS_SOURCE (self), NULL);

  return self->envelope;
}

void
pd_source_set_envelope (PdSource    *self,
                        const gchar *envelope)
{
  g_return_if_fail (PD_IS_SOURCE (self));

  if (g_strcmp0 (envelope, self->envelope) != 0)
    {
      g_free (self->envelope);
      self->envelope = g_strdup (envelope);
      g_object_notify_by_pspec (G_OBJECT (self), properties [PROP_ENVELOPE]);
    }
}
