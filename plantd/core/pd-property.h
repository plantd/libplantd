/*
 * Copyright © the Plantd contributors. All rights reserved.
 *
 * This file is part of libplantd, distributed under the MIT license.
 * For full terms see the included LICENSE file.
 */

#pragma once

#if !defined (PLANTD_INSIDE) && !defined (PLANTD_COMPILATION)
# error "Only <plantd/plantd.h> can be included directly."
#endif

#include <glib-object.h>

G_BEGIN_DECLS

#define PD_TYPE_PROPERTY pd_property_get_type ()
G_DECLARE_FINAL_TYPE (PdProperty, pd_property, PD, PROPERTY, GObject)

PdProperty  *pd_property_new         (const gchar *key,
                                      const gchar *value);

gchar       *pd_property_serialize   (PdProperty  *self);
void         pd_property_deserialize (PdProperty  *self,
                                      const gchar *data);

const gchar *pd_property_get_key     (PdProperty  *self);
void         pd_property_set_key     (PdProperty  *self,
                                      const gchar *key);

const gchar *pd_property_get_value   (PdProperty  *self);
void         pd_property_set_value   (PdProperty  *self,
                                      const gchar *value);

G_END_DECLS
