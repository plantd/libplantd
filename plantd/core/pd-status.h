/*
 * Copyright © the Plantd contributors. All rights reserved.
 *
 * This file is part of libplantd, distributed under the MIT license.
 * For full terms see the included LICENSE file.
 */

#pragma once

#if !defined (PLANTD_INSIDE) && !defined (PLANTD_COMPILATION)
# error "Only <plantd/plantd.h> can be included directly."
#endif

#include <glib-object.h>

G_BEGIN_DECLS

#define PD_TYPE_STATUS pd_status_get_type ()
G_DECLARE_FINAL_TYPE (PdStatus, pd_status, PD, STATUS, GObject)

PdStatus    *pd_status_new            (void);
//PdStatus    *pd_status_new_from_data  (const guint8 *data);

//guint8      *pd_status_to_data        (PdStatus     *self);

gchar       *pd_status_serialize      (PdStatus    *self);
void         pd_status_deserialize    (PdStatus    *self,
                                       const gchar *data);

gboolean     pd_status_get_enabled    (PdStatus    *self);
void         pd_status_set_enabled    (PdStatus    *self,
                                       gboolean     enabled);

gboolean     pd_status_get_loaded     (PdStatus    *self);
void         pd_status_set_loaded     (PdStatus    *self,
                                       gboolean     loaded);

gboolean     pd_status_get_active     (PdStatus    *self);
void         pd_status_set_active     (PdStatus    *self,
                                       gboolean     active);

GHashTable  *pd_status_get_details    (PdStatus    *self);
void         pd_status_set_details    (PdStatus    *self,
                                       GHashTable  *details);

void         pd_status_add_detail     (PdStatus    *self,
                                       const gchar *key,
                                       const gchar *value);
void         pd_status_remove_detail  (PdStatus    *self,
                                       const gchar *key);
const gchar *pd_status_get_detail     (PdStatus    *self,
                                       const gchar *key);

G_END_DECLS
