/*
 * Copyright © the Plantd contributors. All rights reserved.
 *
 * This file is part of libplantd, distributed under the MIT license.
 * For full terms see the included LICENSE file.
 */

#pragma once

#if !defined (PLANTD_INSIDE) && !defined (PLANTD_COMPILATION)
# error "Only <plantd/plantd.h> can be included directly."
#endif

#include <glib-object.h>

#include <plantd/plantd-types.h>

G_BEGIN_DECLS

#define PD_TYPE_SOURCE pd_source_get_type ()
G_DECLARE_FINAL_TYPE (PdSource, pd_source, PD, SOURCE, GObject)

PdSource    *pd_source_new           (const gchar *endpoint,
                                      const gchar *envelope);

gchar       *pd_source_serialize     (PdSource    *self);
void         pd_source_deserialize   (PdSource    *self,
                                      const gchar *data);

void         pd_source_start         (PdSource    *self);
void         pd_source_stop          (PdSource    *self);

void         pd_source_queue_message (PdSource    *self,
                                      const gchar *data);

gboolean     pd_source_running       (PdSource    *self);

const gchar *pd_source_get_endpoint  (PdSource    *self);
void         pd_source_set_endpoint  (PdSource    *self,
                                      const gchar *endpoint);

const gchar *pd_source_get_envelope  (PdSource    *self);
void         pd_source_set_envelope  (PdSource    *self,
                                      const gchar *envelope);

G_END_DECLS
