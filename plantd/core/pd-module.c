/*
 * Copyright © the Plantd contributors. All rights reserved.
 *
 * This file is part of libplantd, distributed under the MIT license.
 * For full terms see the included LICENSE file.
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <json-glib/json-glib.h>

#include "pd-module.h"

/*
 *message Module {
 *  string id = 1;
 *  string name = 2;
 *  string service_name = 3;
 *  string configuration_id = 4;
 *  Status status = 5;
 *  State state = 6;
 *  repeated string services = 7;
 *}
 */

struct _PdModule
{
  GObject     parent;
  gchar      *id;
  gchar      *name;
  gchar      *service_name;
  gchar      *configuration_id;
  PdStatus *status;
  PdState  *state;
  /*GPtrArray   services;*/
};

enum {
  PROP_0,
  PROP_ID,
  PROP_NAME,
  PROP_SERVICE_NAME,
  PROP_CONFIGURATION_ID,
  PROP_STATUS,
  PROP_STATE,
  /*PROP_SERVICES,*/
  N_PROPS
};

static GParamSpec *properties [N_PROPS];

G_DEFINE_TYPE (PdModule, pd_module, G_TYPE_OBJECT)

static void
pd_module_finalize (GObject *object)
{
  PdModule *self = (PdModule *)object;

  g_clear_pointer (&self->id, g_free);
  g_clear_pointer (&self->name, g_free);
  g_clear_pointer (&self->service_name, g_free);
  g_clear_pointer (&self->configuration_id, g_free);

  G_OBJECT_CLASS (pd_module_parent_class)->finalize (object);
}

static void
pd_module_get_property (GObject    *object,
                        guint       prop_id,
                        GValue     *value,
                        GParamSpec *pspec)
{
  PdModule *self = PD_MODULE (object);

  switch (prop_id)
    {
    case PROP_ID:
      g_value_set_string (value, pd_module_get_id (self));
      break;

    case PROP_NAME:
      g_value_set_string (value, pd_module_get_name (self));
      break;

    case PROP_SERVICE_NAME:
      g_value_set_string (value, pd_module_get_service_name (self));
      break;

    case PROP_CONFIGURATION_ID:
      g_value_set_string (value, pd_module_get_configuration_id (self));
      break;

    case PROP_STATUS:
      g_value_set_object (value, pd_module_get_status (self));
      break;

    case PROP_STATE:
      g_value_set_object (value, pd_module_get_state (self));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
pd_module_set_property (GObject      *object,
                        guint         prop_id,
                        const GValue *value,
                        GParamSpec   *pspec)
{
  PdModule *self = PD_MODULE (object);

  switch (prop_id)
    {
    case PROP_ID:
      pd_module_set_id (self, g_value_get_string (value));
      break;

    case PROP_NAME:
      pd_module_set_name (self, g_value_get_string (value));
      break;

    case PROP_SERVICE_NAME:
      pd_module_set_service_name (self, g_value_get_string (value));
      break;

    case PROP_CONFIGURATION_ID:
      pd_module_set_configuration_id (self, g_value_get_string (value));
      break;

    case PROP_STATUS:
      pd_module_set_status (self, g_value_get_object (value));
      break;

    case PROP_STATE:
      pd_module_set_state (self, g_value_get_object (value));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
pd_module_class_init (PdModuleClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->finalize = pd_module_finalize;
  object_class->get_property = pd_module_get_property;
  object_class->set_property = pd_module_set_property;

  properties [PROP_ID] =
    g_param_spec_string ("id",
                         "Id",
                         "The id of the application.",
                         NULL,
                         (G_PARAM_READWRITE | G_PARAM_EXPLICIT_NOTIFY | G_PARAM_STATIC_STRINGS));

  g_object_class_install_properties (object_class, N_PROPS, properties);
}

static void
pd_module_init (PdModule *self)
{
}

PdModule *
pd_module_new (void)
{
  return g_object_new (PD_TYPE_MODULE,
                       "id", g_uuid_string_random (),
                       NULL);
}

/**
 * pd_module_get_id:
 * @self: a #PdModule
 *
 * Gets the application id of @self.
 *
 * Returns: (nullable): the application id, if one is set.
 */
const gchar *
pd_module_get_id (PdModule *self)
{
  g_return_val_if_fail (PD_IS_MODULE (self), NULL);

  return self->id;
}

/**
 * pd_module_set_id:
 * @self: a #PdModule
 * @id: (nullable): the application id to use
 *
 * Sets (or unsets) the application id of @self.
 */
void
pd_module_set_id (PdModule    *self,
                  const gchar *id)
{
  g_return_if_fail (PD_IS_MODULE (self));

  if (g_strcmp0 (id, self->id) != 0)
    {
      g_free (self->id);
      self->id = g_strdup (id);
      g_object_notify_by_pspec (G_OBJECT (self), properties [PROP_ID]);
    }
}

/**
 * pd_module_get_name:
 * @self: a #PdModule
 *
 * Gets the application service name of @self.
 *
 * Returns: (nullable): the application name, if one is set.
 */
const gchar *
pd_module_get_name (PdModule *self)
{
  g_return_val_if_fail (PD_IS_MODULE (self), NULL);

  return self->name;
}

/**
 * pd_module_set_name:
 * @self: a #PdModule
 * @name: (nullable): the application name to use
 *
 * Sets (or unsets) the module name to @self to register with the broker.
 */
void
pd_module_set_name (PdModule    *self,
                    const gchar *name)
{
  g_return_if_fail (PD_IS_MODULE (self));

  if (g_strcmp0 (name, self->name) != 0)
    {
      g_free (self->name);
      self->name = g_strdup (name);
      g_object_notify_by_pspec (G_OBJECT (self), properties [PROP_NAME]);
    }
}

/**
 * pd_module_get_service_name:
 * @self: a #PdModule
 *
 * Gets the application service name of @self.
 *
 * Returns: (nullable): the application service name, if one is set.
 */
const gchar *
pd_module_get_service_name (PdModule *self)
{
  g_return_val_if_fail (PD_IS_MODULE (self), NULL);

  return self->service_name;
}

/**
 * pd_module_set_service_name:
 * @self: a #PdModule
 * @service_name: (nullable): the service name the module is connected to
 *
 * Sets (or unsets) the module service name @self is connected to.
 */
void
pd_module_set_service_name (PdModule    *self,
                            const gchar *service_name)
{
  g_return_if_fail (PD_IS_MODULE (self));

  if (g_strcmp0 (service_name, self->service_name) != 0)
    {
      g_free (self->service_name);
      self->service_name = g_strdup (service_name);
      g_object_notify_by_pspec (G_OBJECT (self), properties [PROP_SERVICE_NAME]);
    }
}

/**
 * pd_module_get_configuration_id:
 * @self: a #PdModule
 *
 * Gets the application configuration_id of @self.
 *
 * Returns: (nullable): the application configuration_id, if one is set.
 */
const gchar *
pd_module_get_configuration_id (PdModule *self)
{
  g_return_val_if_fail (PD_IS_MODULE (self), NULL);

  return self->configuration_id;
}

/**
 * pd_module_set_configuration_id:
 * @self: a #PdModule
 * @configuration_id: (nullable): the configuration ID the module uses
 *
 * Sets (or unsets) the configuration ID of @self.
 */
void
pd_module_set_configuration_id (PdModule    *self,
                                const gchar *configuration_id)
{
  g_return_if_fail (PD_IS_MODULE (self));

  if (g_strcmp0 (configuration_id, self->configuration_id) != 0)
    {
      g_free (self->configuration_id);
      self->configuration_id = g_strdup (configuration_id);
      g_object_notify_by_pspec (G_OBJECT (self), properties [PROP_CONFIGURATION_ID]);
    }
}

/**
 * pd_module_get_status:
 * @self: a #PdModule
 *
 * Gets the application status of @self.
 *
 * Returns: (transfer none): (nullable): a #PdStatus, if one is set.
 */
PdStatus *
pd_module_get_status (PdModule *self)
{
  PdStatus *status;

  g_return_val_if_fail (PD_IS_MODULE (self), NULL);

  g_object_get (self, "status", &status, NULL);

  return status;
}

/**
 * pd_module_set_status:
 * @self: a #PdModule
 * @status: (nullable): the module status as a #PdStatus
 *
 * Sets (or unsets) the status of @self.
 */
void
pd_module_set_status (PdModule *self,
                      PdStatus *status)
{
  g_return_if_fail (PD_IS_MODULE (self));

  if (self->status)
    g_object_unref (self->status);

  if (status)
    g_object_ref (status);

  self->status = status;
}

/**
 * pd_module_get_state:
 * @self: a #PdModule
 *
 * Gets the application state of @self.
 *
 * Returns: (transfer none): (nullable): a #PdState, if one is set.
 */
PdState *
pd_module_get_state (PdModule *self)
{
  PdState *state;

  g_return_val_if_fail (PD_IS_MODULE (self), NULL);

  g_object_get (self, "state", &state, NULL);

  return state;
}

/**
 * pd_module_set_state:
 * @self: a #PdModule
 * @state: (nullable): the module state as a #PdState
 *
 * Sets (or unsets) the state of @self.
 */
void
pd_module_set_state (PdModule *self,
                     PdState  *state)
{
  g_return_if_fail (PD_IS_MODULE (self));

  if (self->state)
    g_object_unref (self->state);

  if (state)
    g_object_ref (state);

  self->state = state;
}
