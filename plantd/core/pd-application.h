/*
 * Copyright © the Plantd contributors. All rights reserved.
 *
 * This file is part of libplantd, distributed under the MIT license.
 * For full terms see the included LICENSE file.
 */

#pragma once

#if !defined (PLANTD_INSIDE) && !defined (PLANTD_COMPILATION)
# error "Only <plantd/plantd.h> can be included directly."
#endif

#include <glib.h>
#include <gio/gio.h>

#include <plantd/plantd-types.h>

G_BEGIN_DECLS

#define PD_TYPE_APPLICATION pd_application_get_type ()
G_DECLARE_DERIVABLE_TYPE (PdApplication, pd_application, PD, APPLICATION, GApplication)

struct _PdApplicationClass
{
  /*< private >*/
  GApplicationClass parent_class;

  /*< public >*/
  /* signals */
  void                     (*property_changed)  (PdApplication  *self,
                                                 PdProperty     *property);

  /* vfuncs */
  PdConfigurationResponse *(*get_configuration) (PdApplication  *self,
                                                 GError        **error);

  PdStatusResponse        *(*get_status)        (PdApplication  *self,
                                                 GError        **error);

  PdSettingsResponse      *(*get_settings)      (PdApplication  *self,
                                                 GError        **error);

  PdJobResponse           *(*get_job)           (PdApplication  *self,
                                                 const gchar    *job_id,
                                                 GError        **error);

  PdJobsResponse          *(*get_jobs)          (PdApplication  *self,
                                                 GError        **error);

  PdJobResponse           *(*get_active_job)    (PdApplication  *self,
                                                 GError        **error);

  PdJobResponse           *(*cancel_job)        (PdApplication  *self,
                                                 const gchar    *job_id,
                                                 GError        **error);

  PdJobResponse           *(*submit_job)        (PdApplication  *self,
                                                 const gchar    *job_id,
                                                 const gchar    *job_value,
                                                 GHashTable     *job_properties,
                                                 GError        **error);

  PdJobResponse           *(*submit_event)      (PdApplication  *self,
                                                 const gint      event_id,
                                                 GError        **error);

  PdEventResponse         *(*available_events)  (PdApplication  *self,
                                                 GError        **error);

  /*< private >*/
  gpointer padding[12];
};

PdApplication           *pd_application_new               (const gchar        *application_id,
                                                           GApplicationFlags   flags);

PdConfigurationResponse *pd_application_get_configuration (PdApplication      *self,
                                                           GError            **error);

PdStatusResponse        *pd_application_get_status        (PdApplication      *self,
                                                           GError            **error);

PdSettingsResponse      *pd_application_get_settings      (PdApplication      *self,
                                                           GError            **error);

PdJobResponse           *pd_application_get_job           (PdApplication      *self,
                                                           const gchar        *job_id,
                                                           GError            **error);

PdJobsResponse          *pd_application_get_jobs          (PdApplication      *self,
                                                           GError            **error);

PdJobResponse           *pd_application_get_active_job    (PdApplication      *self,
                                                           GError            **error);

PdJobResponse           *pd_application_cancel_job        (PdApplication      *self,
                                                           const gchar        *job_id,
                                                           GError            **error);

PdJobResponse           *pd_application_submit_job        (PdApplication      *self,
                                                           const gchar        *job_id,
                                                           const gchar        *job_value,
                                                           GHashTable         *job_properties,
                                                           GError            **error);

PdJobResponse           *pd_application_submit_event      (PdApplication      *self,
                                                           const gint          event_id,
                                                           GError            **error);

PdEventResponse         *pd_application_available_events  (PdApplication      *self,
                                                           GError            **error);

void                     pd_application_load_config       (PdApplication      *self,
                                                           const gchar        *config,
                                                           GError            **error);

PdConfiguration         *pd_application_get_loaded_config (PdApplication      *self);

void                     pd_application_add_property      (PdApplication      *self,
                                                           PdProperty         *property);
void                     pd_application_remove_property   (PdApplication      *self,
                                                           const gchar        *key);
PdProperty              *pd_application_lookup_property   (PdApplication      *self,
                                                           const gchar        *key);
gboolean                 pd_application_update_property   (PdApplication      *self,
                                                           const gchar        *key,
                                                           const gchar        *value);
gboolean                 pd_application_has_property      (PdApplication      *self,
                                                           const gchar        *key);

void                     pd_application_add_sink          (PdApplication      *self,
                                                           const gchar        *name,
                                                           PdSink             *sink);
void                     pd_application_remove_sink       (PdApplication      *self,
                                                           const gchar        *name);
PdSink                  *pd_application_get_sink          (PdApplication      *self,
                                                           const gchar        *name);
gboolean                 pd_application_has_sink          (PdApplication      *self,
                                                           const gchar        *name);

void                     pd_application_add_source        (PdApplication      *self,
                                                           const gchar        *name,
                                                           PdSource           *source);
void                     pd_application_remove_source     (PdApplication      *self,
                                                           const gchar        *name);
PdSource                *pd_application_get_source        (PdApplication      *self,
                                                           const gchar        *name);
gboolean                 pd_application_has_source        (PdApplication      *self,
                                                           const gchar        *name);

void                     pd_application_send_event        (PdApplication      *self,
                                                           PdEvent            *event,
                                                           GError            **error);
void                     pd_application_send_metric       (PdApplication      *self,
                                                           PdMetric           *metric,
                                                           GError            **error);

void                     pd_application_enqueue_job       (PdApplication      *self,
                                                           PdJob              *job);
void                     pd_application_register_job      (PdApplication      *self,
                                                           PdJob              *job);

const gchar             *pd_application_get_id            (PdApplication      *self);
void                     pd_application_set_id            (PdApplication      *self,
                                                           const gchar        *id);

const gchar             *pd_application_get_endpoint      (PdApplication      *self);
void                     pd_application_set_endpoint      (PdApplication      *self,
                                                           const gchar        *endpoint);

const gchar             *pd_application_get_service       (PdApplication      *self);
void                     pd_application_set_service       (PdApplication      *self,
                                                           const gchar        *service);

G_END_DECLS
