/*
 * Copyright © the Plantd contributors. All rights reserved.
 *
 * This file is part of libplantd, distributed under the MIT license.
 * For full terms see the included LICENSE file.
 */

#define G_LOG_DOMAIN "plantd-handler"

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include "pd-handler.h"

enum {
  SIGNAL_SHUTDOWN_REQUESTED,
  N_SIGNALS
};

static guint signals [N_SIGNALS];

G_DEFINE_INTERFACE (PdHandler, pd_handler, G_TYPE_OBJECT)

static void
pd_handler_default_init (PdHandlerInterface *iface)
{
  /**
   * PdHandler::shutdown_requested
   * @self: the message handler
   *
   * The ::shutdown_requested signal is emitted when the message handler
   * implementation receives the `shutdown' message.
   */
  signals [SIGNAL_SHUTDOWN_REQUESTED] =
    g_signal_new ("shutdown-requested", PD_TYPE_HANDLER, G_SIGNAL_RUN_FIRST,
                  G_STRUCT_OFFSET (PdHandlerInterface, shutdown_requested),
                  NULL, NULL, NULL,
                  G_TYPE_NONE, 0, NULL);
}

void
pd_handler_run (PdHandler  *self,
                gpointer    data,
                GError    **error)
{
  g_return_if_fail (PD_IS_HANDLER (self));
  g_return_if_fail (error == NULL || *error == NULL);

  if (PD_HANDLER_GET_IFACE (self)->run)
    PD_HANDLER_GET_IFACE (self)->run (self, data, error);
}

void
pd_handler_cancel (PdHandler *self)
{
  g_return_if_fail (PD_IS_HANDLER (self));

  if (PD_HANDLER_GET_IFACE (self)->cancel)
    PD_HANDLER_GET_IFACE (self)->cancel (self);
}
