/*
 * Copyright © the Plantd contributors. All rights reserved.
 *
 * This file is part of libplantd, distributed under the MIT license.
 * For full terms see the included LICENSE file.
 */

#define G_LOG_DOMAIN "plantd-object"

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <json-glib/json-glib.h>

#include "pd-object.h"
#include "pd-property.h"
#include "pd-table.h"

struct _PdObject
{
  GObject     parent;
  gchar      *id;
  gchar      *name;
  PdTable  *properties;
  PdTable  *objects;
};

enum {
  PROP_0,
  PROP_ID,
  PROP_NAME,
  PROP_PROPERTIES,
  PROP_OBJECTS,
  N_PROPS
};

static GParamSpec *class_properties [N_PROPS];

static JsonSerializableIface *serializable_iface = NULL;

static void json_serializable_iface_init (gpointer g_iface);

G_DEFINE_TYPE_WITH_CODE (PdObject, pd_object, G_TYPE_OBJECT,
                         G_IMPLEMENT_INTERFACE (JSON_TYPE_SERIALIZABLE,
                                                json_serializable_iface_init));

static JsonNode *
pd_object_serialize_property (JsonSerializable *serializable,
                              const gchar      *name,
                              const GValue     *value,
                              GParamSpec       *pspec)
{
  JsonNode *retval = NULL;

  if (g_strcmp0 (name, "properties") == 0)
    {
      if (value != NULL && G_VALUE_HOLDS_OBJECT (value))
        retval = pd_table_serialize (PD_TABLE (g_value_get_object (value)), JSON_TYPE_ARRAY);
    }
  else if (g_strcmp0 (name, "objects") == 0)
    {
      if (value != NULL && G_VALUE_HOLDS_OBJECT (value))
        retval = pd_table_serialize (PD_TABLE (g_value_get_object (value)), JSON_TYPE_ARRAY);
    }
  else
    {
      retval = serializable_iface->serialize_property (serializable,
                                                       name,
                                                       value,
                                                       pspec);

      /*GValue copy = { 0, };*/

      /*retval = json_node_new (JSON_NODE_VALUE);*/

      /*g_value_init (&copy, G_PARAM_SPEC_VALUE_TYPE (pspec));*/
      /*g_value_copy (value, &copy);*/
      /*json_node_set_value (retval, &copy);*/
      /*g_value_unset (&copy);*/
    }

  return retval;
}

static gboolean
pd_object_deserialize_property (JsonSerializable *serializable,
                                const gchar      *name,
                                GValue           *value,
                                GParamSpec       *pspec,
                                JsonNode         *property_node)
{
  gboolean retval = FALSE;

  if (g_strcmp0 (name, "properties") == 0)
    {
      g_autoptr (PdTable) properties = NULL;

      properties = pd_table_new (PD_TYPE_PROPERTY);
      pd_table_deserialize (properties, property_node);

      g_value_take_object (value, g_object_ref (properties));

      retval = TRUE;
    }
  else if (g_strcmp0 (name, "objects") == 0)
    {
      g_autoptr (PdTable) objects = NULL;

      objects = pd_table_new (PD_TYPE_OBJECT);
      pd_table_deserialize (objects, property_node);

      g_value_take_object (value, g_object_ref (objects));

      retval = TRUE;
    }

  return retval;
}

static void
json_serializable_iface_init (gpointer g_iface)
{
  JsonSerializableIface *iface = g_iface;

  serializable_iface = g_type_default_interface_peek (JSON_TYPE_SERIALIZABLE);

  iface->serialize_property = pd_object_serialize_property;
  iface->deserialize_property = pd_object_deserialize_property;
}

static void
pd_object_finalize (GObject *object)
{
  PdObject *self = (PdObject *)object;

  g_debug ("Finaliiiiiiiize!!!!: %s (%s)", self->id, self->name);

  g_clear_pointer (&self->id, g_free);
  g_clear_pointer (&self->name, g_free);
  g_clear_object (&self->objects);
  g_clear_object (&self->properties);

  G_OBJECT_CLASS (pd_object_parent_class)->finalize (object);
}

static void
pd_object_get_property (GObject    *object,
                        guint       prop_id,
                        GValue     *value,
                        GParamSpec *pspec)
{
  PdObject *self = PD_OBJECT (object);

  switch (prop_id)
	  {
    case PROP_ID:
      g_value_set_string (value, pd_object_get_id (self));
      break;

    case PROP_NAME:
      g_value_set_string (value, pd_object_get_name (self));
      break;

    case PROP_PROPERTIES:
      g_value_take_object (value, pd_object_ref_properties (self));
			break;

    case PROP_OBJECTS:
      g_value_take_object (value, pd_object_ref_objects (self));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
  	}
}

static void
pd_object_set_property (GObject      *object,
                        guint         prop_id,
                        const GValue *value,
                        GParamSpec   *pspec)
{
  PdObject *self = PD_OBJECT (object);

  switch (prop_id)
  	{
    case PROP_ID:
      pd_object_set_id (self, g_value_get_string (value));
      break;

    case PROP_NAME:
      pd_object_set_name (self, g_value_get_string (value));
      break;

		case PROP_PROPERTIES:
      pd_object_set_properties (self, g_value_get_object (value));
      break;

    case PROP_OBJECTS:
      pd_object_set_objects (self, g_value_get_object (value));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
  	}
}

static void
pd_object_class_init (PdObjectClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->finalize = pd_object_finalize;
  object_class->get_property = pd_object_get_property;
  object_class->set_property = pd_object_set_property;

  class_properties [PROP_ID] =
    g_param_spec_string ("id",
                         "ID",
                         "The object ID.",
                         NULL,
                         (G_PARAM_READWRITE | G_PARAM_EXPLICIT_NOTIFY | G_PARAM_STATIC_STRINGS));

  class_properties [PROP_NAME] =
    g_param_spec_string ("name",
                         "Name",
                         "The object name.",
                         NULL,
                         (G_PARAM_READWRITE | G_PARAM_EXPLICIT_NOTIFY | G_PARAM_STATIC_STRINGS));

	class_properties [PROP_PROPERTIES] =
	  g_param_spec_object ("properties",
                         "Properties",
                         "List of properties.",
                         PD_TYPE_TABLE,
                         (G_PARAM_READWRITE | G_PARAM_EXPLICIT_NOTIFY | G_PARAM_STATIC_STRINGS));

  class_properties [PROP_OBJECTS] =
    g_param_spec_object ("objects",
                         "Objects",
                         "List of objects.",
                         PD_TYPE_TABLE,
                         (G_PARAM_READWRITE | G_PARAM_EXPLICIT_NOTIFY | G_PARAM_STATIC_STRINGS));

  g_object_class_install_properties (object_class, N_PROPS, class_properties);
}

static void
pd_object_init (PdObject *self)
{
  self->properties = pd_table_new (PD_TYPE_PROPERTY);
  self->objects = pd_table_new (PD_TYPE_OBJECT);
}

PdObject *
pd_object_new (const gchar *id)
{
  return g_object_new (PD_TYPE_OBJECT,
                       "id", id,
                       "name", "Object",
                       NULL);
}

// TODO: get rid of this
void
pd_object_dump (PdObject *self)
{
  g_return_if_fail (PD_IS_OBJECT (self));

  g_print ("id:   %s\n", pd_object_get_id (self));
  g_print ("name: %s\n", pd_object_get_name (self));

  if (self->properties != NULL)
    {
      g_print ("properties:\n");
      g_print (" - TODO: removed hash table iteration\n");
    }

  // TODO: implement a depth argument to the call to dump
  if (self->objects != NULL)
    {
      g_print ("objects:\n");
      g_print (" - TODO: removed hash table iteration\n");
    }
}

gchar *
pd_object_serialize (PdObject *self)
{
  g_return_val_if_fail (PD_IS_OBJECT (self), NULL);

  return json_gobject_to_data (G_OBJECT (self), NULL);
}

void
pd_object_deserialize (PdObject    *self,
                       const gchar *data)
{
  g_autoptr (GObject) object = NULL;
  g_autoptr (PdTable) objects = NULL;
  g_autoptr (PdTable) properties = NULL;
  g_autofree gchar *id = NULL;
  g_autofree gchar *name = NULL;
  GError *err = NULL;

  object = json_gobject_from_data (PD_TYPE_OBJECT,
                                   data,
                                   -1,
                                   &err);

  if (err != NULL)
    {
      g_critical ("%s", err->message);
      g_error_free (err);
    }

  g_return_if_fail (object != NULL);
  g_return_if_fail (PD_IS_OBJECT (object));

  id = g_strdup (pd_object_get_id (PD_OBJECT (object)));
  name = g_strdup (pd_object_get_name (PD_OBJECT (object)));
  properties = pd_object_ref_properties (PD_OBJECT (object));
  objects = pd_object_ref_objects (PD_OBJECT (object));

  pd_object_set_id (self, id);
  pd_object_set_name (self, name);
  pd_object_set_properties (self, properties);
  pd_object_set_objects (self, objects);

  g_clear_object (&object);
}

GObject *
pd_gobject_from_data (const gchar *data)
{
  GError *err = NULL;
  GObject *object = json_gobject_from_data (PD_TYPE_OBJECT,
                                            data,
                                            -1,
                                            &err);

  if (err != NULL)
    {
      g_critical ("%s", err->message);
      g_error_free (err);
    }

  g_return_val_if_fail (object != NULL, NULL);
  g_return_val_if_fail (PD_IS_OBJECT (object), NULL);

  return object;
}

/**
 * pd_object_get_id:
 * @self: a #PdObject
 *
 * Returns: (transfer none): The object id, or %NULL
 */
const gchar *
pd_object_get_id (PdObject *self)
{
  g_return_val_if_fail (PD_IS_OBJECT (self), NULL);

  return self->id;
}

/**
 * pd_object_set_id:
 * @self: a #PdObject
 * @id: (nullable): a string containing the id, or %NULL
 *
 * Sets the #PdObject:id property.
 */
void
pd_object_set_id (PdObject    *self,
                  const gchar *id)
{
  g_return_if_fail (PD_IS_OBJECT (self));

  if (g_strcmp0 (id, self->id) != 0)
    {
      g_free (self->id);
      self->id = g_strdup (id);
      g_object_notify_by_pspec (G_OBJECT (self), class_properties [PROP_ID]);
    }
}

/**
 * pd_object_get_name:
 * @self: a #PdObject
 *
 * Returns: (transfer none): The object name, or %NULL
 */
const gchar *
pd_object_get_name (PdObject *self)
{
  g_return_val_if_fail (PD_IS_OBJECT (self), NULL);

  return self->name;
}

/**
 * pd_object_set_name:
 * @self: a #PdObject
 * @name: (nullable): a string containing the name, or %NULL
 *
 * Sets the #PdObject:name property.
 */
void
pd_object_set_name (PdObject    *self,
                    const gchar *name)
{
  g_return_if_fail (PD_IS_OBJECT (self));

  if (g_strcmp0 (name, self->name) != 0)
    {
      g_free (self->name);
      self->name = g_strdup (name);
      g_object_notify_by_pspec (G_OBJECT (self), class_properties [PROP_NAME]);
    }
}

/**
 * pd_object_add_object:
 * @self: a #PdObject
 * @object: (transfer full): object to add
 */
void
pd_object_add_object (PdObject *self,
                      PdObject *object)
{
  GValue val = G_VALUE_INIT;
  gchar *id;

  g_return_if_fail (PD_IS_OBJECT (self));
  g_return_if_fail (PD_IS_OBJECT (object));

  if (self->objects == NULL)
    self->objects = pd_table_new (PD_TYPE_OBJECT);

  g_value_init (&val, PD_TYPE_OBJECT);
  /*g_value_set_object (&val, object);*/
  g_value_take_object (&val, object);
  id = g_strdup (pd_object_get_id (object));

  pd_table_add (self->objects, id, &val);
  g_object_notify_by_pspec (G_OBJECT (self), class_properties [PROP_OBJECTS]);

  g_value_unset (&val);
  g_free (id);
}

void
pd_object_remove_object (PdObject    *self,
                         const gchar *id)
{
  g_return_if_fail (PD_IS_OBJECT (self));
  g_return_if_fail (self->objects != NULL);

  pd_table_remove (self->objects, id);
  g_object_notify_by_pspec (G_OBJECT (self), class_properties [PROP_OBJECTS]);
}

/**
 * pd_object_lookup_object:
 * @self: a #PdObject
 * @id: the ID of the object to retrieve
 *
 * Returns: (transfer none): The object if found, NULL otherwise.
 */
PdObject *
pd_object_lookup_object (PdObject    *self,
                         const gchar *id)
{
  g_return_val_if_fail (PD_IS_OBJECT (self), NULL);
  g_return_val_if_fail (self->objects != NULL, NULL);

  return g_value_get_object (pd_table_get (self->objects, id));
}

gboolean
pd_object_has_object (PdObject    *self,
                      const gchar *id)
{
  gboolean ret;

  g_return_val_if_fail (PD_IS_OBJECT (self), FALSE);
  g_return_val_if_fail (self->objects != NULL, FALSE);
  g_return_val_if_fail (id != NULL, FALSE);

  ret = pd_table_has (self->objects, id);

  return ret;
}

/**
 * pd_object_add_property:
 * @self: a #PdObject
 * @property: (transfer full): property to add
 */
void
pd_object_add_property (PdObject   *self,
                        PdProperty *property)
{
  GValue val = G_VALUE_INIT;
  gchar *key;

  g_return_if_fail (PD_IS_OBJECT (self));
  g_return_if_fail (PD_IS_PROPERTY (property));

  if (self->properties == NULL)
    self->properties = pd_table_new (PD_TYPE_PROPERTY);

  g_value_init (&val, PD_TYPE_PROPERTY);
  g_value_take_object (&val, property);
  key = g_strdup (pd_property_get_key (property));

  pd_table_add (self->properties, key, &val);
  g_object_notify_by_pspec (G_OBJECT (self), class_properties [PROP_PROPERTIES]);

  g_value_unset (&val);
  g_free (key);
}

void
pd_object_remove_property (PdObject    *self,
                           const gchar *key)
{
  g_return_if_fail (PD_IS_OBJECT (self));
  g_return_if_fail (self->properties != NULL);

  pd_table_remove (self->properties, key);
  g_object_notify_by_pspec (G_OBJECT (self), class_properties [PROP_PROPERTIES]);
}

/**
 * pd_object_lookup_property:
 * @self: a #PdObject
 * @key: the key of the property to retrieve
 *
 * Returns: (transfer none): The property if found, NULL otherwise.
 */
PdProperty *
pd_object_lookup_property (PdObject    *self,
                           const gchar *key)
{
  g_return_val_if_fail (PD_IS_OBJECT (self), NULL);
  g_return_val_if_fail (self->properties != NULL, NULL);

  return g_value_get_object (pd_table_get (self->properties, key));
}

gboolean
pd_object_has_property (PdObject    *self,
                        const gchar *key)
{
  gboolean ret;

  g_return_val_if_fail (PD_IS_OBJECT (self), FALSE);
  g_return_val_if_fail (self->properties != NULL, FALSE);
  g_return_val_if_fail (key != NULL, FALSE);

  ret = pd_table_has (self->properties, key);

  return ret;
}

/**
 * pd_object_get_objects:
 * @self: a #PdObject
 *
 * Retrieve the #PdTable containing an object list.
 *
 * Returns: (transfer none): a #PdTable of objects if one is set.
 */
PdTable *
pd_object_get_objects (PdObject *self)
{
  PdTable *objects;

  g_return_val_if_fail (PD_IS_OBJECT (self), NULL);

  g_object_get (self, "objects", &objects, NULL);

  return objects;
}

/**
 * pd_object_ref_objects:
 *
 * Gets the object list, and returns a new reference
 * to the #PdTable.
 *
 * Returns: (transfer full) (nullable): a #PdTable or %NULL
 */
PdTable *
pd_object_ref_objects (PdObject *self)
{
  PdTable *ret = NULL;

  g_return_val_if_fail (PD_IS_OBJECT (self), NULL);

  g_set_object (&ret, self->objects);

  return g_steal_pointer (&ret);
}

/**
 * pd_object_set_objects:
 * @self: a #PdObject
 * @objects: An #PdTable of #PdObject objects to set.
 */
void
pd_object_set_objects (PdObject *self,
                       PdTable  *objects)
{
  g_return_if_fail (PD_IS_OBJECT (self));
  g_return_if_fail (PD_IS_TABLE (self->objects));

  if (g_set_object (&self->objects, objects))
    g_object_notify_by_pspec (G_OBJECT (self), class_properties [PROP_OBJECTS]);
}

/**
 * pd_object_get_properties:
 * @self: a #PdProperty
 *
 * Retrieve the #PdTable containing a property list.
 *
 * Returns: (transfer none): a #PdTable of objects if one is set.
 */
PdTable *
pd_object_get_properties (PdObject *self)
{
  PdTable *properties;

  g_return_val_if_fail (PD_IS_OBJECT (self), NULL);

  g_object_get (self, "properties", &properties, NULL);

  return properties;
}

/**
 * pd_object_ref_properties:
 *
 * Gets the object list, and returns a new reference
 * to the #PdTable.
 *
 * Returns: (transfer full) (nullable): a #PdTable or %NULL
 */
PdTable *
pd_object_ref_properties (PdObject *self)
{
  PdTable *ret = NULL;

  g_return_val_if_fail (PD_IS_OBJECT (self), NULL);

  g_set_object (&ret, self->properties);

  return g_steal_pointer (&ret);
}

/**
 * pd_object_set_properties:
 * @self: a #PdObject
 * @properties: An #PdTable of #PdProperty objects to set.
 */
void
pd_object_set_properties (PdObject *self,
                          PdTable  *properties)
{
  g_return_if_fail (PD_IS_OBJECT (self));
  g_return_if_fail (PD_IS_TABLE (self->properties));

  if (g_set_object (&self->properties, properties))
    g_object_notify_by_pspec (G_OBJECT (self), class_properties [PROP_PROPERTIES]);
}
