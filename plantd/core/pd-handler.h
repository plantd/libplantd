/*
 * Copyright © the Plantd contributors. All rights reserved.
 *
 * This file is part of libplantd, distributed under the MIT license.
 * For full terms see the included LICENSE file.
 */

#pragma once

#if !defined (PLANTD_INSIDE) && !defined (PLANTD_COMPILATION)
# error "Only <plantd/plantd.h> can be included directly."
#endif

#include <glib-object.h>

#include <plantd/plantd-types.h>

G_BEGIN_DECLS

#define PD_TYPE_HANDLER pd_handler_get_type ()
G_DECLARE_INTERFACE (PdHandler, pd_handler, PD, HANDLER, GObject)

struct _PdHandlerInterface
{
  GTypeInterface parent_iface;

  /*< public >*/
  /* signals */
  void (*shutdown_requested) (PdHandler *self);

  /* vfuncs */
  void (*run)    (PdHandler  *self,
                  gpointer    data,
                  GError    **error);
  void (*cancel) (PdHandler  *self);
};

void
pd_handler_run    (PdHandler  *self,
                   gpointer    data,
                   GError    **error);
void
pd_handler_cancel (PdHandler  *self);

G_END_DECLS
