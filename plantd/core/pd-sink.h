/*
 * Copyright © the Plantd contributors. All rights reserved.
 *
 * This file is part of libplantd, distributed under the MIT license.
 * For full terms see the included LICENSE file.
 */

#pragma once

#if !defined (PLANTD_INSIDE) && !defined (PLANTD_COMPILATION)
# error "Only <plantd/plantd.h> can be included directly."
#endif

#include <glib-object.h>

G_BEGIN_DECLS

#define PD_TYPE_SINK pd_sink_get_type ()
G_DECLARE_DERIVABLE_TYPE (PdSink, pd_sink, PD, SINK, GObject)

struct _PdSinkClass
{
  /*< private >*/
  GObjectClass parent_class;

  /* vfuncs */

  void (*handle_message) (PdSink      *self,
                          const gchar *msg);

  /*< private >*/
  gpointer padding[12];
};

PdSink      *pd_sink_new          (const gchar *endpoint,
                                   const gchar *filter);

void         pd_sink_start        (PdSink      *self);
void         pd_sink_stop         (PdSink      *self);

gchar       *pd_sink_serialize    (PdSink      *self);
void         pd_sink_deserialize  (PdSink      *self,
                                   const gchar *data);

gboolean     pd_sink_running      (PdSink      *self);

const gchar *pd_sink_get_endpoint (PdSink      *self);
void         pd_sink_set_endpoint (PdSink      *self,
                                   const gchar *endpoint);

const gchar *pd_sink_get_filter   (PdSink      *self);
void         pd_sink_set_filter   (PdSink      *self,
                                   const gchar *filter);

G_END_DECLS
