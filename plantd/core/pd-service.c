/*
 * Copyright © the Plantd contributors. All rights reserved.
 *
 * This file is part of libplantd, distributed under the MIT license.
 * For full terms see the included LICENSE file.
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <json-glib/json-glib.h>

#include "pd-service.h"
#include "pd-status.h"
/*#include "pd-state-machine.h"*/

typedef struct
{
  GObject          parent;
  PdModel       *model;
  gchar           *name;
  PdStatus      *status;
  /*PdState       *state;*/
} PdServicePrivate;

enum {
  PROP_0,
  /*PROP_MODEL,*/
  PROP_NAME,
  /*PROP_STATUS,*/
  /*PROP_STATE,*/
  N_PROPS
};

static GParamSpec *properties [N_PROPS];

G_DEFINE_TYPE_WITH_PRIVATE (PdService, pd_service, G_TYPE_OBJECT)

static void
pd_service_finalize (GObject *object)
{
  PdService *self = (PdService *)object;
  PdServicePrivate *priv = pd_service_get_instance_private (self);

  g_clear_pointer (&priv->name, g_free);

  G_OBJECT_CLASS (pd_service_parent_class)->finalize (object);
}

static void
pd_service_get_property (GObject    *object,
                         guint       prop_id,
                         GValue     *value,
                         GParamSpec *pspec)
{
  PdService *self = PD_SERVICE (object);

  switch (prop_id)
    {
    case PROP_NAME:
      g_value_take_string (value, pd_service_dup_name (self));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
pd_service_set_property (GObject      *object,
                         guint         prop_id,
                         const GValue *value,
                         GParamSpec   *pspec)
{
  PdService *self = PD_SERVICE (object);

  switch (prop_id)
    {
    case PROP_NAME:
      pd_service_set_name (self, g_value_get_string (value));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
pd_service_class_init (PdServiceClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->finalize = pd_service_finalize;
  object_class->get_property = pd_service_get_property;
  object_class->set_property = pd_service_set_property;

  properties [PROP_NAME] =
    g_param_spec_string ("name",
                         "Name",
                         "The name of the service.",
                         NULL,
                         (G_PARAM_READWRITE | G_PARAM_EXPLICIT_NOTIFY | G_PARAM_STATIC_STRINGS));

  g_object_class_install_properties (object_class, N_PROPS, properties);
}

static void
pd_service_init (PdService *self)
{
}

PdService *
pd_service_new (const gchar *name)
{
  return g_object_new (PD_TYPE_SERVICE,
                       "name", name,
                       NULL);
}

/*
 *PdService *
 *pd_service_new_with_model (const gchar *name,
 *                             PdModel   *model)
 *{
 *  return g_object_new (PD_TYPE_SERVICE,
 *                       "name", name,
 *                       "model", model,
 *                       NULL);
 *}
 */

/**
 * pd_service_dup_name:
 *
 * Copies the name of the service and returns it to the caller (after locking
 * the object). A copy is used to avoid thread-races.
 */
gchar *
pd_service_dup_name (PdService *self)
{
  PdServicePrivate *priv;
  gchar *ret;

  g_return_val_if_fail (PD_IS_SERVICE (self), NULL);

  priv = pd_service_get_instance_private (self);

  /*pd_object_lock (PD_OBJECT (self));*/
  ret = g_strdup (priv->name);
  /*pd_object_unlock (PD_OBJECT (self));*/

  return g_steal_pointer (&ret);
}

void
pd_service_set_name (PdService   *self,
                     const gchar *name)
{
  PdServicePrivate *priv;

  g_return_if_fail (PD_IS_SERVICE (self));

  priv = pd_service_get_instance_private (self);

  if (g_strcmp0 (name, priv->name) != 0)
    {
      g_free (priv->name);
      priv->name = g_strdup (name);
      g_object_notify_by_pspec (G_OBJECT (self), properties [PROP_NAME]);
    }
}

/**
 * pd_service_ref_status:
 * @self: a #PdService
 *
 * Gets the application status of @self.
 *
 * Returns: (transfer full): (nullable): a #PdStatus or %NULL
 */
PdStatus *
pd_service_ref_status (PdService *self)
{
  PdServicePrivate *priv;
  PdStatus *ret = NULL;

  g_return_val_if_fail (PD_IS_SERVICE (self), NULL);

  priv = pd_service_get_instance_private (self);

  g_set_object (&ret, priv->status);

  return g_steal_pointer (&ret);
}

/**
 * pd_service_set_status:
 * @self: a #PdService
 * @status: (nullable): the module status as a #PdStatus
 *
 * Sets (or unsets) the status of @self.
 */
void
pd_service_set_status (PdService *self,
                       PdStatus  *status)
{
  PdServicePrivate *priv;

  g_return_if_fail (PD_IS_SERVICE (self));
  g_return_if_fail (PD_IS_STATUS (status));

  priv = pd_service_get_instance_private (self);

  /*if (g_set_object (&priv->status, status))*/
    /*g_object_notify_by_pspec (G_OBJECT (self), properties [PROP_STATUS]);*/
}

/**
 * pd_service_ref_state:
 * @self: a #PdService
 *
 * Gets the application state of @self.
 *
 * Returns: (transfer full): (nullable): a #PdState or %NULL
 */
/*
 *PdState *
 *pd_service_ref_state (PdService *self)
 *{
 *  PdServicePrivate *priv;
 *  PdState *ret = NULL;
 *
 *  g_return_val_if_fail (PD_IS_SERVICE (self), NULL);
 *
 *  priv = pd_service_get_instance_private (self);
 *
 *  g_set_object (&ret, priv->state);
 *
 *  return g_steal_pointer (&ret);
 *}
 */

/**
 * pd_service_set_state:
 * @self: a #PdService
 * @state: (nullable): the service state as a #PdState
 *
 * Sets (or unsets) the state of @self.
 */
/*
 *void
 *pd_service_set_state (PdService *self,
 *                        PdState   *state)
 *{
 *  PdServicePrivate *priv;
 *
 *  g_return_if_fail (PD_IS_SERVICE (self));
 *  g_return_if_fail (PD_IS_STATE (state));
 *
 *  priv = pd_service_get_instance_private (self);
 *
 *  if (g_set_object (&priv->state, state))
 *    g_object_notify_by_pspec (G_OBJECT (self), properties [PROP_STATE]);
 *}
 */
