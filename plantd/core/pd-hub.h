/*
 * Copyright © the Plantd contributors. All rights reserved.
 *
 * This file is part of libplantd, distributed under the MIT license.
 * For full terms see the included LICENSE file.
 */

#pragma once

#if !defined (PLANTD_INSIDE) && !defined (PLANTD_COMPILATION)
# error "Only <plantd/plantd.h> can be included directly."
#endif

#include <glib-object.h>

G_BEGIN_DECLS

#define PD_TYPE_HUB pd_hub_get_type ()
G_DECLARE_FINAL_TYPE (PdHub, pd_hub, PD, HUB, GObject)

PdHub  *pd_hub_new          (const gchar *frontend,
                             const gchar *backend);

void    pd_hub_start        (PdHub       *self);
void    pd_hub_stop         (PdHub       *self);

gchar  *pd_hub_dup_frontend (PdHub       *self);
void    pd_hub_set_frontend (PdHub       *self,
                             const gchar *frontend);

gchar  *pd_hub_dup_backend  (PdHub       *self);
void    pd_hub_set_backend  (PdHub       *self,
                             const gchar *backend);

G_END_DECLS
