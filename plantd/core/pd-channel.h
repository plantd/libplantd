/*
 * Copyright © the Plantd contributors. All rights reserved.
 *
 * This file is part of libplantd, distributed under the MIT license.
 * For full terms see the included LICENSE file.
 */

#pragma once

#if !defined (PLANTD_INSIDE) && !defined (PLANTD_COMPILATION)
# error "Only <plantd/plantd.h> can be included directly."
#endif

#include <glib-object.h>

G_BEGIN_DECLS

#define PD_TYPE_CHANNEL pd_channel_get_type ()
G_DECLARE_FINAL_TYPE (PdChannel, pd_channel, PD, CHANNEL, GObject)

PdChannel   *pd_channel_new          (void);

gchar       *pd_channel_serialize    (PdChannel   *self);
void         pd_channel_deserialize  (PdChannel   *self,
                                      const gchar *data);

const gchar *pd_channel_get_endpoint (PdChannel   *self);
void         pd_channel_set_endpoint (PdChannel   *self,
                                      const gchar *endpoint);

const gchar *pd_channel_get_envelope (PdChannel   *self);
void         pd_channel_set_envelope (PdChannel   *self,
                                      const gchar *envelope);

G_END_DECLS
