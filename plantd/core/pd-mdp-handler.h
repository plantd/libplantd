/*
 * Copyright © the Plantd contributors. All rights reserved.
 *
 * This file is part of libplantd, distributed under the MIT license.
 * For full terms see the included LICENSE file.
 */

#pragma once

#if !defined (PLANTD_INSIDE) && !defined (PLANTD_COMPILATION)
# error "Only <plantd/plantd.h> can be included directly."
#endif

#include <glib-object.h>

#include <plantd/plantd-types.h>

G_BEGIN_DECLS

#define PD_TYPE_MDP_HANDLER pd_mdp_handler_get_type ()
G_DECLARE_FINAL_TYPE (PdMdpHandler, pd_mdp_handler, PD, MDP_HANDLER, GObject)

PdMdpHandler *pd_mdp_handler_new (void);

G_END_DECLS
