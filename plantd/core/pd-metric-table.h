/*
 * Copyright © the Plantd contributors. All rights reserved.
 *
 * This file is part of libplantd, distributed under the MIT license.
 * For full terms see the included LICENSE file.
 */

#pragma once

#if !defined (PLANTD_INSIDE) && !defined (PLANTD_COMPILATION)
# error "Only <plantd/plantd.h> can be included directly."
#endif

#include <glib-object.h>

#include <plantd/plantd-types.h>

G_BEGIN_DECLS

#define PD_TYPE_METRIC_TABLE pd_metric_table_get_type ()
G_DECLARE_FINAL_TYPE (PdMetricTable, pd_metric_table, PD, METRIC_TABLE, GObject)

PdMetricTable       *pd_metric_table_new           (void);

gchar               *pd_metric_table_serialize     (PdMetricTable       *self);
void                 pd_metric_table_deserialize   (PdMetricTable       *self,
                                                    const gchar         *data);

gchar               *pd_metric_table_dup_name      (PdMetricTable       *self);
const gchar         *pd_metric_table_get_name      (PdMetricTable       *self);
void                 pd_metric_table_set_name      (PdMetricTable       *self,
                                                    const gchar         *name);

gchar               *pd_metric_table_dup_timestamp (PdMetricTable       *self);
const gchar         *pd_metric_table_get_timestamp (PdMetricTable       *self);
void                 pd_metric_table_set_timestamp (PdMetricTable       *self,
                                                    const gchar         *timestamp);

PdMetricTableHeader *pd_metric_table_get_header    (PdMetricTable       *self);
PdMetricTableHeader *pd_metric_table_ref_header    (PdMetricTable       *self);
void                 pd_metric_table_set_header    (PdMetricTable       *self,
                                                    PdMetricTableHeader *header);

PdTable             *pd_metric_table_get_entries   (PdMetricTable       *self);
PdTable             *pd_metric_table_ref_entries   (PdMetricTable       *self);
void                 pd_metric_table_set_entries   (PdMetricTable       *self,
                                                    PdTable             *entries);

void                 pd_metric_table_add_entry     (PdMetricTable       *self,
                                                    const gchar         *key,
                                                    gdouble              value);
void                 pd_metric_table_remove_entry  (PdMetricTable       *self,
                                                    const gchar         *key);
gdouble              pd_metric_table_get_entry     (PdMetricTable       *self,
                                                    const gchar         *key);
gboolean             pd_metric_table_has_entry     (PdMetricTable       *self,
                                                    const gchar         *key);

G_END_DECLS
