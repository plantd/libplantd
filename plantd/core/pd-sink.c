/*
 * Copyright © the Plantd contributors. All rights reserved.
 *
 * This file is part of libplantd, distributed under the MIT license.
 * For full terms see the included LICENSE file.
 */

#define G_LOG_DOMAIN "plantd-sink"

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <json-glib/json-glib.h>
#include <czmq.h>

#include "pd-sink.h"

/**
 * SECTION:plantd-sink
 * @short_description: Sink base class
 *
 * An #PdSink is used by applications that connect to message buses.
 */

/**
 * PdSink:
 *
 * #PdSink is a data consumer structure meant to be used with implementations
 * of a #PdApplication.
 */

/**
 * PdSinkClass:
 * @handle_message: invoked by the data subscriber when a message is received.
 *
 * Virtual function table for #PdSink.
 */

typedef struct
{
  /*< public >*/
  gchar *endpoint;
  gchar *filter;

  /*< private >*/
  gboolean running;
  guint64  msg_count;
  guint64  bytes_received;
  /*gdouble  msg_per_sec;*/
  /*guint    avg_msg_size;*/
  /*gdouble  avg_bytes_per_sec;*/
} PdSinkPrivate;

enum {
  PROP_0,
  PROP_ENDPOINT,
  PROP_FILTER,
  N_PROPS
};

static GParamSpec *properties [N_PROPS];

void _run_subscriber_async (PdSink *self);

G_DEFINE_TYPE_WITH_PRIVATE (PdSink, pd_sink, G_TYPE_OBJECT)

static void
pd_sink_finalize (GObject *object)
{
  PdSinkPrivate *priv;
  PdSink *self = (PdSink *)object;

  priv = pd_sink_get_instance_private (self);

  g_clear_pointer (&priv->endpoint, g_free);
  g_clear_pointer (&priv->filter, g_free);

  G_OBJECT_CLASS (pd_sink_parent_class)->finalize (object);
}

static void
pd_sink_get_property (GObject    *object,
                      guint       prop_id,
                      GValue     *value,
                      GParamSpec *pspec)
{
  PdSink *self = PD_SINK (object);

  switch (prop_id)
    {
    case PROP_ENDPOINT:
      g_value_set_string (value, pd_sink_get_endpoint (self));
      break;

    case PROP_FILTER:
      g_value_set_string (value, pd_sink_get_filter (self));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
pd_sink_set_property (GObject      *object,
                      guint         prop_id,
                      const GValue *value,
                      GParamSpec   *pspec)
{
  PdSink *self = PD_SINK (object);

  switch (prop_id)
    {
    case PROP_ENDPOINT:
      pd_sink_set_endpoint (self, g_value_get_string (value));
      break;

    case PROP_FILTER:
      pd_sink_set_filter (self, g_value_get_string (value));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
pd_sink_real_handle_message (PdSink      *self,
                             const gchar *msg)
{
  PdSinkPrivate *priv;

  g_return_if_fail (PD_IS_SINK (self));

  priv = pd_sink_get_instance_private (self);

  priv->msg_count++;
  priv->bytes_received += sizeof (msg);

  g_debug ("%s", msg);
}

static void
pd_sink_class_init (PdSinkClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->finalize = pd_sink_finalize;
  object_class->get_property = pd_sink_get_property;
  object_class->set_property = pd_sink_set_property;

  klass->handle_message = pd_sink_real_handle_message;

  properties [PROP_ENDPOINT] =
    g_param_spec_string ("endpoint",
                         "Endpoint",
                         "The endpoint for the sink to connect or bind.",
                         NULL,
                         (G_PARAM_READWRITE | G_PARAM_EXPLICIT_NOTIFY));

  properties [PROP_FILTER] =
    g_param_spec_string ("filter",
                         "Filter",
                         "The filter for incoming messages.",
                         NULL,
                         (G_PARAM_READWRITE | G_PARAM_EXPLICIT_NOTIFY));

  g_object_class_install_properties (object_class, N_PROPS, properties);
}

static void
pd_sink_init (PdSink *self)
{
}

PdSink *
pd_sink_new (const gchar *endpoint,
             const gchar *filter)
{
  return g_object_new (PD_TYPE_SINK,
                       "endpoint", endpoint,
                       "filter", filter,
                       NULL);
}

static void
_handle_message (PdSink      *self,
                 const gchar *msg)
{
  PdSinkClass *klass;

  g_return_if_fail (PD_IS_SINK (self));

  klass = PD_SINK_GET_CLASS (self);
  g_return_if_fail (klass->handle_message != NULL);

  return klass->handle_message (self, msg);
}

static void
_run_subscriber_cb (GObject      *source_object,
                    GAsyncResult *result,
                    gpointer      user_data)
{
  g_debug ("message publisher finished");
}

static void
_run_subscriber_cancel (PdSink   *self,
                        gpointer  data)
{
  PdSinkPrivate *priv;
  GCancellable *cancellable G_GNUC_UNUSED;

  priv = pd_sink_get_instance_private (self);

  /*cancellable = G_CANCELLABLE (cancellable);*/

  priv->running = FALSE;

  g_debug ("message subscriber cancelled");
}

static void
_run_subscriber_thread (GTask        *task,
                        gpointer      source_object,
                        gpointer      task_data,
                        GCancellable *cancellable)
{
  PdSinkPrivate *priv;
  zsock_t *subscriber;
  zpoller_t *poller;

  g_assert (source_object == g_task_get_source_object (task));
  g_assert (cancellable == g_task_get_cancellable (task));

  priv = pd_sink_get_instance_private (source_object);

  subscriber = zsock_new (ZMQ_SUB);
  //zsock_set_rcvhwm (subscriber, 0);
  zsock_set_subscribe (subscriber, priv->filter);
  zsock_connect (subscriber, "%s", priv->endpoint);

  poller = zpoller_new (subscriber, NULL);

  priv->running = TRUE;

  while (priv->running)
    {
      zsock_t *sock;

      if (g_cancellable_is_cancelled (cancellable))
        {
          /*g_task_return_new_error (task,*/
                                   /*G_IO_ERROR, G_IO_ERROR_CANCELLED,*/
                                   /*"Subscriber task cancelled");*/
          /*return;*/
          break;
        }

      sock = (zsock_t *) zpoller_wait (poller, 1000);
      if (sock == subscriber)
        {
          g_autofree gchar *msg = NULL;

          msg = zstr_recv (sock);
          if (!msg)
            break;

          _handle_message (PD_SINK (source_object), msg);
        }
    }

  g_debug ("destroying sink socket: %s", priv->endpoint);
  zpoller_destroy (&poller);
  zsock_destroy (&subscriber);

  g_task_return_boolean (task, TRUE);
}

void
_run_subscriber_async (PdSink *self)
{
  g_autoptr (GTask) task = NULL;
  g_autoptr (GCancellable) cancellable = NULL;

  g_return_if_fail (PD_IS_SINK (self));

  cancellable = g_cancellable_new ();
  task = g_task_new (self, cancellable, _run_subscriber_cb, NULL);

  g_task_run_in_thread (task, _run_subscriber_thread);
}

void
pd_sink_start (PdSink *self)
{
  /* Start subscribing to messages */
  _run_subscriber_async (self);
}

void
pd_sink_stop (PdSink *self)
{
  /* Stop subscribing to messages */
  _run_subscriber_cancel (self, NULL);
}

gchar *
pd_sink_serialize (PdSink *self)
{
  g_return_val_if_fail (PD_IS_SINK (self), NULL);

  return json_gobject_to_data (G_OBJECT (self), NULL);
}

void
pd_sink_deserialize (PdSink      *self,
                     const gchar *data)
{
  GError *err = NULL;
  GObject *object = json_gobject_from_data (PD_TYPE_SINK,
                                            data,
                                            -1,
                                            &err);

  if (err != NULL)
    {
      g_critical ("%s", err->message);
      g_error_free (err);
    }

  g_return_if_fail (object != NULL);
  g_return_if_fail (PD_IS_SINK (object));

  pd_sink_set_endpoint (self, pd_sink_get_endpoint (PD_SINK (object)));
  pd_sink_set_filter (self, pd_sink_get_filter (PD_SINK (object)));

  g_object_unref (object);
}

gboolean
pd_sink_running (PdSink *self)
{
  PdSinkPrivate *priv;

  g_return_val_if_fail (PD_IS_SINK (self), FALSE);

  priv = pd_sink_get_instance_private (self);

  return priv->running;
}

const gchar *
pd_sink_get_endpoint (PdSink *self)
{
  PdSinkPrivate *priv;

  g_return_val_if_fail (PD_IS_SINK (self), NULL);

  priv = pd_sink_get_instance_private (self);

  return priv->endpoint;
}

void
pd_sink_set_endpoint (PdSink      *self,
                      const gchar *endpoint)
{
  PdSinkPrivate *priv;

  g_return_if_fail (PD_IS_SINK (self));

  priv = pd_sink_get_instance_private (self);

  if (g_strcmp0 (endpoint, priv->endpoint) != 0)
    {
      g_free (priv->endpoint);
      priv->endpoint = g_strdup (endpoint);
      g_object_notify_by_pspec (G_OBJECT (self), properties [PROP_ENDPOINT]);
    }
}

const gchar *
pd_sink_get_filter (PdSink *self)
{
  PdSinkPrivate *priv;

  g_return_val_if_fail (PD_IS_SINK (self), NULL);

  priv = pd_sink_get_instance_private (self);

  return priv->filter;
}

void
pd_sink_set_filter (PdSink      *self,
                    const gchar *filter)
{
  PdSinkPrivate *priv;

  g_return_if_fail (PD_IS_SINK (self));

  priv = pd_sink_get_instance_private (self);

  if (g_strcmp0 (filter, priv->filter) != 0)
    {
      g_free (priv->filter);
      priv->filter = g_strdup (filter);
      g_object_notify_by_pspec (G_OBJECT (self), properties [PROP_FILTER]);
    }
}
