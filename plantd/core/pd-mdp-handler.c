/*
 * Copyright © the Plantd contributors. All rights reserved.
 *
 * This file is part of libplantd, distributed under the MIT license.
 * For full terms see the included LICENSE file.
 */

#define G_LOG_DOMAIN "plantd-mdp-handler"

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <plantd-debug.h>

#include "pd-mdp-handler.h"
#include "pd-handler.h"
#include "pd-application.h"
#include "mdp/pd-worker.h"
#include "message/pd-message.h"

struct _PdMdpHandler
{
  GObject        parent;
  gboolean       running;
  gboolean       cancelled;
  PdApplication *app;
};

static void pd_handler_interface_init (gpointer *iface);

G_DEFINE_TYPE_WITH_CODE (PdMdpHandler, pd_mdp_handler, G_TYPE_OBJECT,
                         G_IMPLEMENT_INTERFACE (PD_TYPE_HANDLER,
                                                pd_handler_interface_init));

static void     s_handler_async  (PdMdpHandler         *self,
                                  GCancellable         *cancellable,
                                  GAsyncReadyCallback   callback,
                                  gpointer              user_data);
static void     s_handler_cancel (PdMdpHandler         *self,
                                  gpointer              user_data);
static gboolean s_handler_finish (PdMdpHandler         *self,
                                  GAsyncResult         *result,
                                  GError              **error);

/**
 * pd_mdp_handler_run:
 * @self: a #PdMdpHandler
 * @user_data: a #gpointer that expects a #PdApplication
 * @error: return location for a GError, or NULL
 *
 * Starts the thread that monitors the messages received from the parent
 * service.
 */
static void
pd_mdp_handler_run (PdHandler  *self,
                    gpointer    user_data,
                    GError    **error)
{
  PD_MDP_HANDLER (self)->app = PD_APPLICATION (user_data);
  s_handler_async (PD_MDP_HANDLER (self), NULL, (GAsyncReadyCallback) s_handler_finish, NULL);
}

/**
 * pd_mdp_handler_cancel:
 * @self: a #PdMdpHandler
 *
 * Stops the thread that monitors the messages received from the parent
 * service.
 */
static void
pd_mdp_handler_cancel (PdHandler *self)
{
  s_handler_cancel (PD_MDP_HANDLER (self), NULL);
}

static void
pd_handler_interface_init (gpointer *g_iface)
{
  PdHandlerInterface *iface = (PdHandlerInterface *) g_iface;

  iface->run = pd_mdp_handler_run;
  iface->cancel = pd_mdp_handler_cancel;
}

static void
pd_mdp_handler_finalize (GObject *object)
{
  G_GNUC_UNUSED PdMdpHandler *self = (PdMdpHandler *)object;

  G_OBJECT_CLASS (pd_mdp_handler_parent_class)->finalize (object);
}

static void
pd_mdp_handler_class_init (PdMdpHandlerClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->finalize = pd_mdp_handler_finalize;
}

static void
pd_mdp_handler_init (PdMdpHandler *self)
{
}

PdMdpHandler *
pd_mdp_handler_new (void)
{
  return g_object_new (PD_TYPE_MDP_HANDLER, NULL);
}

// ---

static gchar *
pd_mdp_handler_get_configuration (PdMdpHandler *self)
{
  g_autoptr (GError) error = NULL;
  g_autoptr (PdConfigurationResponse) response = NULL;
  gchar *data;

  response = pd_application_get_configuration (self->app, &error);
  g_assert ((response == NULL && error != NULL) || (response != NULL && error == NULL));
  if (error != NULL)
    {
      // Report error to user, and free error
      g_assert (response == NULL);
      g_warning ("Error during `get-configuration': %s\n", error->message);
    }
  else
    {
      // Use response
      g_assert (response != NULL);
      g_assert (PD_IS_CONFIGURATION_RESPONSE (response));
      data = pd_configuration_response_serialize (response);
    }

  return g_steal_pointer (&data);
}

static gchar *
pd_mdp_handler_get_property (PdMdpHandler      *self,
                             PdPropertyRequest *request)
{
  g_autoptr (GError) error = NULL;
  g_autoptr (PdPropertyResponse) response = NULL;
  PdProperty *property = NULL;
  const gchar *key;
  gchar *data;

  response = pd_property_response_new ();
  key = pd_property_request_get_key (request);
  property = pd_application_lookup_property (self->app, key);
  pd_property_response_set (response, property);

  g_assert ((response == NULL && error != NULL) || (response != NULL && error == NULL));
  if (error != NULL)
    {
      // Report error to user, and free error
      g_assert (response == NULL);
      g_warning ("Error during `get-property': %s\n", error->message);
    }
  else
    {
      // Use response
      g_assert (response != NULL);
      data = pd_property_response_serialize (response);
    }

  return g_steal_pointer (&data);
}

static gchar *
pd_mdp_handler_set_property (PdMdpHandler      *self,
                             PdPropertyRequest *request)
{
  g_autoptr (GError) error = NULL;
  g_autoptr (PdPropertyResponse) response = NULL;
  PdProperty *property;
  const gchar *key;
  const gchar *value;
  gchar *data;

  response = pd_property_response_new ();
  key = pd_property_request_get_key (request);
  value = pd_property_request_get_value (request);

  property = pd_application_lookup_property (self->app, key);
  pd_application_update_property (self->app, key, value);
  pd_property_response_set (response, property);

  g_assert ((response == NULL && error != NULL) || (response != NULL && error == NULL));
  if (error != NULL)
    {
      // Report error to user, and free error
      g_assert (response == NULL);
      g_warning ("Error during `set-property': %s\n", error->message);
    }
  else
    {
      // Use response
      g_assert (response != NULL);
      data = pd_property_response_serialize (response);
    }

  return g_steal_pointer (&data);
}

static gchar *
pd_mdp_handler_get_properties (PdMdpHandler        *self,
                               PdPropertiesRequest *request)
{
  g_autoptr (GError) error = NULL;
  g_autoptr (PdPropertiesResponse) response = NULL;
  g_autoptr (PdMessageError) err = NULL;
  GHashTable *props = NULL;
  GHashTableIter iter;
  gpointer key, val;
  gchar *data;

  response = pd_properties_response_new ();
  err = pd_message_error_new ();
  props = pd_properties_request_get_list (request);

  g_hash_table_iter_init (&iter, props);
  while (g_hash_table_iter_next (&iter, &key, &val))
    {
      if (pd_application_has_property (self->app, key))
        {
          pd_properties_response_add (response, pd_application_lookup_property (self->app, key));
        }
    }

  g_assert ((response == NULL && error != NULL) || (response != NULL && error == NULL));
  if (error != NULL)
    {
      // Report error to user, and free error
      //g_assert (response == NULL);
      pd_message_error_set_code (err, 500);
      g_warning ("Error during `get-properties': %s\n", error->message);
    }
  else
    {
      // Use response
      g_assert (response != NULL);
      pd_message_error_set_code (err, 200);
    }

  // there should always be a response
  pd_response_set_error (PD_RESPONSE (response), err);
  data = pd_properties_response_serialize (response);

  return g_steal_pointer (&data);
}

static gchar *
pd_mdp_handler_set_properties (PdMdpHandler        *self,
                               PdPropertiesRequest *request)
{
  g_autoptr (GError) error = NULL;
  g_autoptr (PdPropertiesResponse) response = NULL;
  GHashTable *props = NULL;
  GHashTableIter iter;
  gpointer key, val;
  gchar *data;

  response = pd_properties_response_new ();
  props = pd_properties_request_get_list (request);

  g_hash_table_iter_init (&iter, props);
  while(g_hash_table_iter_next (&iter, &key, &val))
    {
      if (pd_application_has_property (self->app, key))
        {
          pd_application_update_property (self->app, key, val);
          pd_properties_response_add (response,
              pd_application_lookup_property (self->app, key));
        }
    }

  g_assert ((response == NULL && error != NULL) || (response != NULL && error == NULL));
  if (error != NULL)
    {
      // Report error to user, and free error
      g_assert (response == NULL);
      g_warning ("Error during `set-properties': %s\n", error->message);
    }
  else
    {
      // Use response
      g_assert (response != NULL);
      data = pd_properties_response_serialize (response);
    }

  return g_steal_pointer (&data);
}

static gchar *
pd_mdp_handler_get_status (PdMdpHandler *self)
{
  g_autoptr (GError) error = NULL;
  g_autoptr (PdStatusResponse) response = NULL;
  gchar *data;

  response = pd_application_get_status (self->app, &error);
  g_assert ((response == NULL && error != NULL) || (response != NULL && error == NULL));
  if (error != NULL)
    {
      // Report error to user, and free error
      g_assert (response == NULL);
      g_warning ("Error during `get-status': %s\n", error->message);
    }
  else
    {
      // Use response
      g_assert (response != NULL);
      data = pd_status_response_serialize (response);
    }

  return g_steal_pointer (&data);
}

static gchar *
pd_mdp_handler_get_settings (PdMdpHandler *self)
{
  g_autoptr (GError) error = NULL;
  g_autoptr (PdSettingsResponse) response = NULL;
  gchar *data;

  response = pd_application_get_settings (self->app, &error);
  g_assert ((response == NULL && error != NULL) || (response != NULL && error == NULL));
  if (error != NULL)
    {
      // Report error to user, and free error
      g_assert (response == NULL);
      g_warning ("Error during `get-settings': %s\n", error->message);
    }
  else
    {
      // Use response
      g_assert (response != NULL);
      data = pd_settings_response_serialize (response);
    }

  return g_steal_pointer (&data);
}

static gchar *
pd_mdp_handler_get_job (PdMdpHandler *self)
{
  g_autoptr (GError) error = NULL;
  // TODO: deserialize request message
  // TODO: get job_id from request message
  const gchar *job_id = "job0";
  g_autoptr (PdJobResponse) response = NULL;
  gchar *data;

  response = pd_application_get_job (self->app, job_id, &error);
  g_assert ((response == NULL && error != NULL) || (response != NULL && error == NULL));
  if (error != NULL)
    {
      // Report error to user, and free error
      g_assert (response == NULL);
      g_warning ("Error during `get-job': %s\n", error->message);
    }
  else
    {
      // Use response
      g_assert (response != NULL);
      data = pd_job_response_serialize (response);
    }

  return g_steal_pointer (&data);
}

static gchar *
pd_mdp_handler_get_jobs (PdMdpHandler *self)
{
  g_autoptr (GError) error = NULL;
  g_autoptr (PdJobsResponse) response = NULL;
  gchar *data;

  response = pd_application_get_jobs (self->app, &error);
  g_assert ((response == NULL && error != NULL) || (response != NULL && error == NULL));
  if (error != NULL)
    {
      // Report error to user, and free error
      g_assert (response == NULL);
      g_warning ("Error during `get-jobs': %s\n", error->message);
    }
  else
    {
      // Use response
      g_assert (response != NULL);
      data = pd_jobs_response_serialize (response);
    }

  return g_steal_pointer (&data);
}

static gchar *
pd_mdp_handler_get_active_job (PdMdpHandler *self)
{
  g_autoptr (GError) error = NULL;
  g_autoptr (PdJobResponse) response = NULL;
  gchar *data;

  response = pd_application_get_active_job (self->app, &error);
  g_assert ((response == NULL && error != NULL) || (response != NULL && error == NULL));
  if (error != NULL)
    {
      // Report error to user, and free error
      g_assert (response == NULL);
      g_warning ("Error during `get-active-job': %s\n", error->message);
    }
  else
    {
      // Use response
      g_assert (response != NULL);
      data = pd_job_response_serialize (response);
    }

  return g_steal_pointer (&data);
}

static gchar *
pd_mdp_handler_cancel_job (PdMdpHandler *self)
{
  g_autoptr (GError) error = NULL;
  // TODO: deserialize request message
  // TODO: get job_id from request message
  const gchar *job_id = "job0";
  g_autoptr (PdJobResponse) response = NULL;
  gchar *data;

  response = pd_application_cancel_job (self->app, job_id, &error);
  g_assert ((response == NULL && error != NULL) || (response != NULL && error == NULL));
  if (error != NULL)
    {
      // Report error to user, and free error
      g_assert (response == NULL);
      g_warning ("Error during `cancel-job': %s\n", error->message);
    }
  else
    {
      // Use response
      g_assert (response != NULL);
      data = pd_job_response_serialize (response);
    }

  return g_steal_pointer (&data);
}

static gchar *
pd_mdp_handler_submit_job (PdMdpHandler       *self,
                           PdJobRequest *request)
{
  g_autoptr (GError) error = NULL;
  g_autoptr (PdJobResponse) response = NULL;
  const gchar *job_id;
  const gchar *job_value;
  GHashTable *job_properties;
  gchar *data;

  job_id = pd_job_request_get_job_id (request);
  job_value = pd_job_request_get_job_value (request);
  job_properties = pd_job_request_get_list (request);

  response = pd_application_submit_job (self->app,
                                        job_id,
                                        job_value,
                                        job_properties,
                                        &error);
  g_assert ((response == NULL && error != NULL) || (response != NULL && error == NULL));
  if (error != NULL)
    {
      // Report error to user, and free error
      g_assert (response == NULL);
      g_warning ("Error during `submit-job': %s\n", error->message);
    }
  else
    {
      // Use response
      g_assert (response != NULL);
      // TODO: check that response->job != NULL
      pd_application_enqueue_job (self->app,
                                    pd_job_response_get_job (response));
      data = pd_job_response_serialize (response);
      PD_TRACE_MSG ("data: %s", data);
    }

  return g_steal_pointer (&data);
}

static gchar *
pd_mdp_handler_submit_event (PdMdpHandler *self)
{
  g_autoptr (GError) error = NULL;
  // TODO: deserialize request message
  // TODO: get job_id from request message
  const gint event_id = 0;
  g_autoptr (PdJobResponse) response = NULL;
  gchar *data;

  response = pd_application_submit_event (self->app, event_id, &error);
  g_assert ((response == NULL && error != NULL) || (response != NULL && error == NULL));
  if (error != NULL)
    {
      // Report error to user, and free error
      g_assert (response == NULL);
      g_warning ("Error during `submit-event': %s\n", error->message);
    }
  else
    {
      // Use response
      g_assert (response != NULL);
      data = pd_job_response_serialize (response);
    }

  return g_steal_pointer (&data);
}

static gchar *
pd_mdp_handler_available_events (PdMdpHandler *self)
{
  g_autoptr (GError) error = NULL;
  g_autoptr (PdEventResponse) response = NULL;
  gchar *data;

  response = pd_application_available_events (self->app, &error);
  g_assert ((response == NULL && error != NULL) || (response != NULL && error == NULL));
  if (error != NULL)
    {
      // Report error to user, and free error
      g_assert (response == NULL);
      g_warning ("Error during `available-events': %s\n", error->message);
    }
  else
    {
      // Use response
      g_assert (response != NULL);
      /*data = pd_event_response_serialize (response);*/
    }

  return g_steal_pointer (&data);
}

// ---

//__attribute__ ((__noreturn__))
static void
s_handler_cb (GObject      *source_object,
              GAsyncResult *result,
              gpointer      user_data)
{
  // g_task_run_in_thread_sync shouldn't get there
  /*g_assert_not_reached ();*/

  g_debug ("EHRMAHGHERD");
}

static void
s_handler_cancel (PdMdpHandler *self,
                  gpointer      data)
{
  GCancellable *cancellable;

  if (self->cancelled)
    return;

  cancellable = G_CANCELLABLE (data);
  g_cancellable_cancel (cancellable);

  self->cancelled = TRUE;

  // TODO: something more robust may be in order here
  self->running = FALSE;

  g_debug ("message handler cancelled");
}

// TODO: consider moving this into interface and just provide handlers in implementation
static void
s_handler_thread (GTask        *task,
                  gpointer      source_object,
                  gpointer      task_data,
                  GCancellable *cancellable)
{
  g_autoptr (PdWorker) worker = NULL;
  /*gboolean shutdown_requested = FALSE;*/

  // XXX: should these just be checks that fail gracefully?
  g_assert (source_object == g_task_get_source_object (task));
  g_assert (task_data == g_task_get_task_data (task));
  g_assert (cancellable == g_task_get_cancellable (task));

  g_application_hold (G_APPLICATION (PD_MDP_HANDLER (source_object)->app));

  PD_MDP_HANDLER (source_object)->cancelled = FALSE;
  PD_MDP_HANDLER (source_object)->running = TRUE;
  worker = pd_worker_new (pd_application_get_endpoint (PD_MDP_HANDLER (source_object)->app),
                            pd_application_get_service (PD_MDP_HANDLER (source_object)->app));

  // XXX: shouldn't be necessary, here to test signals
  g_usleep (100000);
  pd_worker_connect (worker);

  while (PD_MDP_HANDLER (source_object)->running)
    {
      g_autoptr (GError) error = NULL;
      g_autofree gchar *message = NULL;
      g_autofree gchar *body = NULL;
      g_autofree gchar *data = NULL;
      zframe_t *reply;
      zmsg_t *request;

      request = pd_worker_recv (worker, &reply);

      /* Check if worker was interrupted */
      if (request == NULL)
        break;

      message = zframe_strdup (zmsg_first (request));
      body = zframe_strdup (zmsg_next (request));

      PD_TRACE_MSG ("message handler received message: %s", message);
      PD_TRACE_MSG ("%s", body);

      if (g_strcmp0 (message, "get-configuration") == 0)
        {
          data = pd_mdp_handler_get_configuration (PD_MDP_HANDLER (source_object));
        }
      else if (g_strcmp0 (message, "get-property") == 0)
        {
          g_autoptr (PdPropertyRequest) req = NULL;

          req = pd_property_request_new (NULL, NULL, NULL);
          pd_property_request_deserialize (req, body);
          data = pd_mdp_handler_get_property (PD_MDP_HANDLER (source_object), req);
        }
      else if (g_strcmp0 (message, "set-property") == 0)
        {
          g_autoptr (PdPropertyRequest) req = NULL;

          req = pd_property_request_new (NULL, NULL, NULL);
          pd_property_request_deserialize (req, body);
          data = pd_mdp_handler_set_property (PD_MDP_HANDLER (source_object), req);
        }
      else if (g_strcmp0 (message, "get-properties") == 0)
        {
          g_autoptr (PdPropertiesRequest) req = NULL;

          req = pd_properties_request_new (NULL);
          pd_properties_request_deserialize (req, body);
          data = pd_mdp_handler_get_properties (PD_MDP_HANDLER (source_object), req);
        }
      else if (g_strcmp0 (message, "set-properties") == 0)
        {
          g_autoptr (PdPropertiesRequest) req = NULL;

          req = pd_properties_request_new (NULL);
          pd_properties_request_deserialize (req, body);
          data = pd_mdp_handler_set_properties (PD_MDP_HANDLER (source_object), req);
        }
      else if (g_strcmp0 (message, "get-status") == 0)
        {
          data = pd_mdp_handler_get_status (PD_MDP_HANDLER (source_object));
        }
      else if (g_strcmp0 (message, "get-settings") == 0)
        {
          data = pd_mdp_handler_get_settings (PD_MDP_HANDLER (source_object));
        }
      else if (g_strcmp0 (message, "get-job") == 0)
        {
          data = pd_mdp_handler_get_job (PD_MDP_HANDLER (source_object));
        }
      else if (g_strcmp0 (message, "get-jobs") == 0)
        {
          data = pd_mdp_handler_get_jobs (PD_MDP_HANDLER (source_object));
        }
      else if (g_strcmp0 (message, "get-active-job") == 0)
        {
          data = pd_mdp_handler_get_active_job (PD_MDP_HANDLER (source_object));
        }
      else if (g_strcmp0 (message, "cancel-job") == 0)
        {
          data = pd_mdp_handler_cancel_job (PD_MDP_HANDLER (source_object));
        }
      else if (g_strcmp0 (message, "submit-job") == 0)
        {
          g_autoptr (PdJobRequest) req = NULL;

          req = pd_job_request_new (NULL);
          pd_job_request_deserialize (req, body);
          data = pd_mdp_handler_submit_job (PD_MDP_HANDLER (source_object), req);
        }
      else if (g_strcmp0 (message, "submit-event") == 0)
        {
          data = pd_mdp_handler_submit_event (PD_MDP_HANDLER (source_object));
        }
      else if (g_strcmp0 (message, "available-events") == 0)
        {
          data = pd_mdp_handler_available_events (PD_MDP_HANDLER (source_object));
        }
      else if (g_strcmp0 (message, "shutdown") == 0)
        {
          // XXX: this way could be causing the occasional need for a double call
          /*shutdown_requested = TRUE;*/
          data = g_strdup ("{\"shutdown\":true}");
          g_debug ("received shutdown");
          g_signal_emit_by_name (PD_MDP_HANDLER (source_object), "shutdown-requested");
          s_handler_cancel (PD_MDP_HANDLER (source_object), cancellable);
        }
      else
        {
          g_autoptr (PdResponse) response = NULL;
          g_autoptr (PdMessageError) err = NULL;
          response = pd_response_new ();
          err = pd_message_error_new ();
          pd_message_error_set_code (err, 501);
          pd_response_set_error (response, err);
          data = pd_response_serialize (response);
        }

      // Add data to response
      if (data)
        {
          zframe_t *frame = zmsg_last (request);
          zmsg_remove (request, frame);
          zmsg_addstr (request, data);
          zframe_destroy (&frame);
        }

      // Send response
      pd_worker_send (worker, &request, reply);
      zframe_destroy (&reply);
      /*zmsg_destroy (&request);*/
    }

  g_application_release (G_APPLICATION (PD_MDP_HANDLER (source_object)->app));

  g_task_return_boolean (task, TRUE);
}

static void
s_handler_async (PdMdpHandler        *self,
                 GCancellable        *cancellable,
                 GAsyncReadyCallback  callback,
                 gpointer             user_data)
{
  // TODO: receive error as (out) parameter?
  /*GError *error = NULL;*/
  g_autoptr (GTask) task = NULL;
  /*g_autoptr (GCancellable) cancellable = NULL;*/

  /*cancellable = g_cancellable_new ();*/
  task = g_task_new (self, cancellable, callback, user_data);
  g_task_set_source_tag (task, s_handler_async);
  g_task_set_return_on_cancel (task, TRUE);

  // TODO: find a way to handle cancelling properly, this is carry over from the application
  /*
   *g_signal_connect (G_APPLICATION (self),
   *                  "shutdown",
   *                  G_CALLBACK (s_handler_cancel),
   *                  cancellable);
   */

  g_task_run_in_thread (task, s_handler_thread);

  // TODO: replace assertions with checks/errors

  /*g_assert (thread_ran == TRUE);*/
  /*g_assert (task != NULL);*/
  /*g_assert (!g_task_had_error (task));*/

  /*ret = g_task_propagate_int (task, &error);*/
  /*g_assert_no_error (error);*/
  /*g_assert_cmpint (ret, ==, 0);*/
}

static gboolean
s_handler_finish (PdMdpHandler  *self,
                  GAsyncResult  *result,
                  GError       **error)
{
  g_return_val_if_fail (g_task_is_valid (result, self), FALSE);
  g_return_val_if_fail (error == NULL || *error == NULL, FALSE);

  return g_task_propagate_boolean (G_TASK (result), error);
}
