/*
 * Copyright © the Plantd contributors. All rights reserved.
 *
 * This file is part of libplantd, distributed under the MIT license.
 * For full terms see the included LICENSE file.
 */

#pragma once

#if !defined (PLANTD_INSIDE) && !defined (PLANTD_COMPILATION)
# error "Only <plantd/plantd.h> can be included directly."
#endif

#include <glib-object.h>

#include <plantd/plantd-types.h>

G_BEGIN_DECLS

#define PD_TYPE_SERVICE pd_service_get_type ()
G_DECLARE_FINAL_TYPE (PdService, pd_service, PD, SERVICE, GObject)

PdService     *pd_service_new                  (void);

gchar         *pd_service_serialize            (PdService     *self);
void           pd_service_deserialize          (PdService     *self,
                                                const gchar   *data);

const gchar   *pd_service_get_name             (PdService     *self);
void           pd_service_set_name             (PdService     *self,
                                                const gchar   *name);

const gchar   *pd_service_get_description      (PdService     *self);
void           pd_service_set_description      (PdService     *self,
                                                const gchar   *description);

const gchar   *pd_service_get_configuration_id (PdService     *self);
void           pd_service_set_configuration_id (PdService     *self,
                                                const gchar   *configuration_id);

PdStatus      *pd_service_get_status           (PdService     *self);
void           pd_service_set_status           (PdService     *self,
                                                PdStatus      *status);

PdState       *pd_service_get_state            (PdService     *self);
void           pd_service_set_state            (PdService     *self,
                                                PdState       *state);

G_END_DECLS
