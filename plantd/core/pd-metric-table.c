/*
 * Copyright © the Plantd contributors. All rights reserved.
 *
 * This file is part of libplantd, distributed under the MIT license.
 * For full terms see the included LICENSE file.
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <json-glib/json-glib.h>

#include "pd-metric-table.h"
#include "pd-metric-table-header.h"
#include "pd-table.h"

/*
 * PdMetricTable:
 *
 * Represents a ...
 */
struct _PdMetricTable
{
  GObject                parent;
  gchar                 *name;
  gchar                 *timestamp;
  PdMetricTableHeader *header;
  PdTable             *entries;
};

enum {
  PROP_0,
  PROP_NAME,
  PROP_TIMESTAMP,
  PROP_HEADER,
  PROP_ENTRIES,
  N_PROPS
};

static GParamSpec *properties [N_PROPS];

static void json_serializable_iface_init (gpointer g_iface);

G_DEFINE_TYPE_WITH_CODE (PdMetricTable, pd_metric_table, G_TYPE_OBJECT,
                         G_IMPLEMENT_INTERFACE (JSON_TYPE_SERIALIZABLE,
                                                json_serializable_iface_init));

static JsonNode *
pd_metric_table_serialize_property (JsonSerializable *serializable,
                                    const gchar      *name,
                                    const GValue     *value,
                                    GParamSpec       *pspec)
{
  JsonNode *retval = NULL;

  if (g_strcmp0 (name, "entries") == 0)
    {
      if (value != NULL && G_VALUE_HOLDS_OBJECT (value))
        retval = pd_table_serialize (PD_TABLE (g_value_get_object (value)), JSON_TYPE_OBJECT);
    }
  else if (g_strcmp0 (name, "header") == 0)
    {
      if (value != NULL && G_VALUE_HOLDS_OBJECT (value))
        {
          PdMetricTableHeader *header;

          header = g_value_get_object (value);
          retval = json_gobject_serialize (G_OBJECT (header));
        }
    }
  else
    {
      GValue copy = { 0, };

      retval = json_node_new (JSON_NODE_VALUE);

      g_value_init (&copy, G_PARAM_SPEC_VALUE_TYPE (pspec));
      g_value_copy (value, &copy);
      json_node_set_value (retval, &copy);
      g_value_unset (&copy);
    }

  return retval;
}

static gboolean
pd_metric_table_deserialize_property (JsonSerializable *serializable,
                                      const gchar      *name,
                                      GValue           *value,
                                      GParamSpec       *pspec,
                                      JsonNode         *property_node)
{
  gboolean retval = FALSE;

  if (g_strcmp0 (name, "entries") == 0)
    {
      /*g_autoptr (PdTable) entries = NULL;*/
      PdTable *entries;

      entries = pd_table_new (G_TYPE_DOUBLE);
      pd_table_deserialize (entries, property_node);

      /*g_value_take_object (value, g_object_ref (entries));*/
      g_value_take_object (value, entries);

      retval = TRUE;

      /*g_clear_object (&entries);*/
    }

  return retval;
}

static void
json_serializable_iface_init (gpointer g_iface)
{
  JsonSerializableIface *iface = g_iface;

  iface->serialize_property = pd_metric_table_serialize_property;
  iface->deserialize_property = pd_metric_table_deserialize_property;
}

static void
pd_metric_table_finalize (GObject *object)
{
  PdMetricTable *self = (PdMetricTable *)object;

  g_clear_pointer (&self->name, g_free);
  g_clear_pointer (&self->timestamp, g_free);
  g_clear_object (&self->header);
  g_clear_object (&self->entries);

  G_OBJECT_CLASS (pd_metric_table_parent_class)->finalize (object);
}

static void
pd_metric_table_get_property (GObject    *object,
                              guint       prop_id,
                              GValue     *value,
                              GParamSpec *pspec)
{
  PdMetricTable *self = PD_METRIC_TABLE (object);

  switch (prop_id)
    {
    case PROP_NAME:
      g_value_take_string (value, pd_metric_table_dup_name (self));
      break;

    case PROP_TIMESTAMP:
      g_value_take_string (value, pd_metric_table_dup_timestamp (self));
      break;

    case PROP_HEADER:
      g_value_take_object (value, pd_metric_table_ref_header (self));
      break;

    case PROP_ENTRIES:
      g_value_take_object (value, pd_metric_table_ref_entries (self));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
pd_metric_table_set_property (GObject      *object,
                              guint         prop_id,
                              const GValue *value,
                              GParamSpec   *pspec)
{
  PdMetricTable *self = PD_METRIC_TABLE (object);

  switch (prop_id)
    {
    case PROP_NAME:
      pd_metric_table_set_name (self, g_value_get_string (value));
      break;

    case PROP_TIMESTAMP:
      pd_metric_table_set_timestamp (self, g_value_get_string (value));
      break;

    case PROP_HEADER:
      pd_metric_table_set_header (self, g_value_get_object (value));
      break;

    case PROP_ENTRIES:
      pd_metric_table_set_entries (self, g_value_get_object (value));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
pd_metric_table_class_init (PdMetricTableClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->finalize = pd_metric_table_finalize;
  object_class->get_property = pd_metric_table_get_property;
  object_class->set_property = pd_metric_table_set_property;

  properties [PROP_NAME] =
    g_param_spec_string ("name",
                         "Name",
                         "The name of the table",
                         NULL,
                         (G_PARAM_READWRITE | G_PARAM_EXPLICIT_NOTIFY | G_PARAM_STATIC_STRINGS));

  properties [PROP_TIMESTAMP] =
    g_param_spec_string ("timestamp",
                         "Timestamp",
                         "Time stamp for the recorded metric values",
                         NULL,
                         (G_PARAM_READWRITE | G_PARAM_EXPLICIT_NOTIFY | G_PARAM_STATIC_STRINGS));

  properties [PROP_HEADER] =
    g_param_spec_object ("header",
                         "Header",
                         "Table header data",
                         PD_TYPE_METRIC_TABLE_HEADER,
                         (G_PARAM_READWRITE | G_PARAM_EXPLICIT_NOTIFY | G_PARAM_STATIC_STRINGS));

  properties [PROP_ENTRIES] =
    g_param_spec_object ("entries",
                         "Entries",
                         "List of metric value entries",
                         PD_TYPE_TABLE,
                         (G_PARAM_READWRITE | G_PARAM_EXPLICIT_NOTIFY | G_PARAM_STATIC_STRINGS));

  g_object_class_install_properties (object_class, N_PROPS, properties);
}

static void
pd_metric_table_init (PdMetricTable *self)
{
  self->entries = pd_table_new (G_TYPE_DOUBLE);
}

PdMetricTable *
pd_metric_table_new (void)
{
  return g_object_new (PD_TYPE_METRIC_TABLE, NULL);
}

gchar *
pd_metric_table_serialize (PdMetricTable *self)
{
  g_return_val_if_fail (PD_IS_METRIC_TABLE (self), NULL);

  return json_gobject_to_data (G_OBJECT (self), NULL);
}

void
pd_metric_table_deserialize (PdMetricTable *self,
                             const gchar   *data)
{
  g_autoptr (GObject) object = NULL;
  g_autoptr (PdMetricTableHeader) header = NULL;
  g_autoptr (PdTable) entries = NULL;
  g_autofree gchar *name = NULL;
  g_autofree gchar *timestamp = NULL;

  GError *err = NULL;
  object = json_gobject_from_data (PD_TYPE_METRIC_TABLE,
                                   data,
                                   -1,
                                   &err);

  if (err != NULL)
    {
      g_critical ("%s", err->message);
      g_error_free (err);
    }

  g_return_if_fail (object != NULL);
  g_return_if_fail (PD_IS_METRIC_TABLE (object));

  name = pd_metric_table_dup_name (PD_METRIC_TABLE (object));
  timestamp = pd_metric_table_dup_timestamp (PD_METRIC_TABLE (object));
  header = pd_metric_table_ref_header (PD_METRIC_TABLE (object));
  entries = pd_metric_table_ref_entries (PD_METRIC_TABLE (object));

  pd_metric_table_set_name (self, name);
  pd_metric_table_set_timestamp (self, timestamp);
  pd_metric_table_set_header (self, header);
  pd_metric_table_set_entries (self, entries);

  g_clear_object (&object);
}

/**
 * pd_metric_table_dup_name:
 *
 * Copies the name of the metric table and returns it to the caller (after
 * locking the object). A copy is used to avoid thread-races.
 */
gchar *
pd_metric_table_dup_name (PdMetricTable *self)
{
  gchar *ret;

  g_return_val_if_fail (PD_IS_METRIC_TABLE (self), NULL);

  /*pd_object_lock (PD_OBJECT (self));*/
  ret = g_strdup (self->name);
  /*pd_object_unlock (PD_OBJECT (self));*/

  return g_steal_pointer (&ret);
}

const gchar *
pd_metric_table_get_name (PdMetricTable *self)
{
  g_return_val_if_fail (PD_IS_METRIC_TABLE (self), NULL);

  return self->name;
}

void
pd_metric_table_set_name (PdMetricTable *self,
                          const gchar   *name)
{
  g_return_if_fail (PD_IS_METRIC_TABLE (self));

  if (g_strcmp0 (name, self->name) != 0)
    {
      g_free (self->name);
      self->name = g_strdup (name);
      g_object_notify_by_pspec (G_OBJECT (self), properties [PROP_NAME]);
    }
}

/**
 * pd_metric_table_dup_timestamp:
 *
 * Copies the timestamp of the metric table and returns it to the caller (after
 * locking the object). A copy is used to avoid thread-races.
 */
gchar *
pd_metric_table_dup_timestamp (PdMetricTable *self)
{
  gchar *ret;

  g_return_val_if_fail (PD_IS_METRIC_TABLE (self), NULL);

  /*pd_object_lock (PD_OBJECT (self));*/
  ret = g_strdup (self->timestamp);
  /*pd_object_unlock (PD_OBJECT (self));*/

  return g_steal_pointer (&ret);
}

const gchar *
pd_metric_table_get_timestamp (PdMetricTable *self)
{
  g_return_val_if_fail (PD_IS_METRIC_TABLE (self), NULL);

  return self->timestamp;
}

void
pd_metric_table_set_timestamp (PdMetricTable *self,
                               const gchar   *timestamp)
{
  g_return_if_fail (PD_IS_METRIC_TABLE (self));

  if (g_strcmp0 (timestamp, self->timestamp) != 0)
    {
      g_free (self->timestamp);
      self->timestamp = g_strdup (timestamp);
      g_object_notify_by_pspec (G_OBJECT (self), properties [PROP_TIMESTAMP]);
    }
}

/**
 * pd_metric_table_get_header:
 * @self: a #PdMetricTable
 *
 * Retrieve the #PdMetricTableHeader containing the table header information.
 *
 * Returns: (transfer full): a #PdMetricTableHeader if one is set.
 */
PdMetricTableHeader *
pd_metric_table_get_header (PdMetricTable *self)
{
  PdMetricTableHeader *header;

  g_return_val_if_fail (PD_IS_METRIC_TABLE (self), NULL);

  g_object_get (self, "header", &header, NULL);

  return header;
}

/**
 * pd_metric_table_ref_header:
 * @self: a #PdMetricTable
 *
 * Gets the header for the metric table, and returns a new reference
 * to the #PdMetricTableHeader.
 *
 * Returns: (transfer full) (nullable): a #PdMetricTableHeader or %NULL
 */
PdMetricTableHeader *
pd_metric_table_ref_header (PdMetricTable *self)
{
  PdMetricTableHeader *ret = NULL;

  g_return_val_if_fail (PD_IS_METRIC_TABLE (self), NULL);

  /*pd_object_lock (G_OBJECT (self));*/
  g_set_object (&ret, self->header);
  /*pd_object_unlock (G_OBJECT (self));*/

  return g_steal_pointer (&ret);
}

void
pd_metric_table_set_header (PdMetricTable       *self,
                            PdMetricTableHeader *header)
{
  g_return_if_fail (PD_IS_METRIC_TABLE (self));
  g_return_if_fail (PD_IS_METRIC_TABLE_HEADER (header));

  if (g_set_object (&self->header, header))
    g_object_notify_by_pspec (G_OBJECT (self), properties [PROP_HEADER]);
}

/**
 * pd_metric_table_get_entries:
 * @self: a #PdMetricTable
 *
 * Retrieve the #PdTable containing table entries.
 *
 * Returns: (transfer full): a #PdTable of entries if one is set.
 */
PdTable *
pd_metric_table_get_entries (PdMetricTable *self)
{
  PdTable *entries;

  g_return_val_if_fail (PD_IS_METRIC_TABLE (self), NULL);

  g_object_get (self, "entries", &entries, NULL);

  return entries;
}

/**
 * pd_metric_table_ref_entries:
 *
 * Gets the table entries, and returns a new reference
 * to the #PdTable.
 *
 * Returns: (transfer full) (nullable): a #PdTable or %NULL
 */
PdTable *
pd_metric_table_ref_entries (PdMetricTable *self)
{
  PdTable *ret = NULL;

  g_return_val_if_fail (PD_IS_METRIC_TABLE (self), NULL);

  g_set_object (&ret, self->entries);

  return g_steal_pointer (&ret);
}

void
pd_metric_table_set_entries (PdMetricTable *self,
                             PdTable       *entries)
{
  g_return_if_fail (PD_IS_METRIC_TABLE (self));
  g_return_if_fail (PD_IS_TABLE (self->entries));

  if (g_set_object (&self->entries, entries))
    g_object_notify_by_pspec (G_OBJECT (self), properties [PROP_ENTRIES]);
}

void
pd_metric_table_add_entry (PdMetricTable *self,
                           const gchar   *key,
                           gdouble        value)
{
  GValue val = G_VALUE_INIT;

  g_return_if_fail (PD_IS_METRIC_TABLE (self));

  g_value_init (&val, G_TYPE_DOUBLE);
  g_value_set_double (&val, value);

  pd_table_add (self->entries, key, &val);
  g_object_notify_by_pspec (G_OBJECT (self), properties [PROP_ENTRIES]);

  g_value_unset (&val);
}

void
pd_metric_table_remove_entry (PdMetricTable *self,
                              const gchar   *key)
{
  g_return_if_fail (PD_IS_METRIC_TABLE (self));
  g_return_if_fail (self->entries != NULL);

  pd_table_remove (self->entries, key);
  g_object_notify_by_pspec (G_OBJECT (self), properties [PROP_ENTRIES]);
}

gdouble
pd_metric_table_get_entry (PdMetricTable *self,
                           const gchar   *key)
{
  g_return_val_if_fail (PD_IS_METRIC_TABLE (self), G_MINDOUBLE);
  g_return_val_if_fail (self->entries != NULL, G_MINDOUBLE);

  return g_value_get_double (pd_table_get (self->entries, key));
}

gboolean
pd_metric_table_has_entry (PdMetricTable *self,
                           const gchar   *key)
{
  gboolean ret;

  g_return_val_if_fail (PD_IS_METRIC_TABLE (self), FALSE);
  g_return_val_if_fail (self->entries != NULL, FALSE);
  g_return_val_if_fail (key != NULL, FALSE);

  ret = pd_table_has (self->entries, key);

  return ret;
}
