/*
 * Copyright © the Plantd contributors. All rights reserved.
 *
 * This file is part of libplantd, distributed under the MIT license.
 * For full terms see the included LICENSE file.
 */

#define G_LOG_DOMAIN "plantd-runner"

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <glib-unix.h>
#include <plantd-debug.h>

#include "pd-runner.h"
#include "pd-application.h"
#include "plantd-error.h"

/**
 * SECTION:plantd-runner
 * @short_description: Application runner base class
 *
 * An #PdRunner is a helper device for running applications.
 */

/**
 * PdRunner:
 *
 * #PdRunner is meant to be used to launch services that integrate with a
 * Plantd network.
 */

/**
 * PdRunnerClass:
 * @application_registered: invoked after an application has been registered
 * @application_launched: invoked after an application has been launched
 *
 * Virtual function table for #PdRunner.
 */

typedef struct
{
  /*< private >*/
  GHashTable *application_table;
} PdRunnerPrivate;

enum {
  SIGNAL_APPLICATION_REGISTERED,
  SIGNAL_APPLICATION_LAUNCHED,
  N_SIGNALS
};

static guint signals [N_SIGNALS];

G_DEFINE_TYPE_WITH_PRIVATE (PdRunner, pd_runner, G_TYPE_OBJECT)

/* GObject setup {{{1 */
static void
pd_runner_finalize (GObject *object)
{
  PdRunner *self = (PdRunner *)object;
  PdRunnerPrivate *priv = pd_runner_get_instance_private (self);

  g_debug ("application finalize");

  if (priv->application_table)
    g_hash_table_unref (priv->application_table);

  G_OBJECT_CLASS (pd_runner_parent_class)->finalize (object);
}

static void
pd_runner_class_init (PdRunnerClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->finalize = pd_runner_finalize;

  /**
   * PdRunner::application_registered:
   * @self: the runner
   *
   * The ::application_registered signal is emitted on the primary instance
   * after an application has been added.
   */
  signals [SIGNAL_APPLICATION_REGISTERED] =
    g_signal_new ("application-registered", PD_TYPE_APPLICATION, G_SIGNAL_RUN_FIRST,
                  G_STRUCT_OFFSET (PdRunnerClass, application_registered),
                  NULL, NULL, NULL,
                  G_TYPE_NONE, 1, PD_TYPE_APPLICATION);

  /**
   * PdRunner::application_launched:
   * @self: the runner
   *
   * The ::application_launched signal is emitted on the primary instance
   * after an application has been started.
   */
  signals [SIGNAL_APPLICATION_LAUNCHED] =
    g_signal_new ("application-launched", PD_TYPE_APPLICATION, G_SIGNAL_RUN_FIRST,
                  G_STRUCT_OFFSET (PdRunnerClass, application_launched),
                  NULL, NULL, NULL,
                  G_TYPE_NONE, 1, PD_TYPE_APPLICATION);
}

static void
pd_runner_init (PdRunner *self)
{
  PdRunnerPrivate *priv;

  priv = pd_runner_get_instance_private (self);

  priv->application_table = g_hash_table_new_full (g_str_hash,
                                                   g_str_equal,
                                                   g_free,
                                                   NULL);
}

/**
 * pd_runner_new:
 *
 * Creates a new #PdRunner instance.
 *
 * Returns: a new #PdRunner instance
 */
PdRunner *
pd_runner_new (void)
{
  return g_object_new (PD_TYPE_RUNNER, NULL);
}

/**
 * pd_runner_add_application:
 * @self: a #PdRunner
 * @name: name of the #PdApplication to add or replace
 * @application: and #PdApplication
 *
 * Add or replace an existing application to @self.
 */
void
pd_runner_add_application (PdRunner      *self,
                           const gchar   *name,
                           PdApplication *application)
{
  PdRunnerPrivate *priv;

  g_return_if_fail (PD_IS_RUNNER (self));
  g_return_if_fail (PD_IS_APPLICATION (application));

  priv = pd_runner_get_instance_private (self);

  if (priv->application_table == NULL)
    priv->application_table = g_hash_table_new_full (g_str_hash,
                                                     g_str_equal,
                                                     g_free,
                                                     NULL);

  if (g_hash_table_contains (priv->application_table, name))
    g_hash_table_remove (priv->application_table, name);

  g_hash_table_insert (priv->application_table, g_strdup (name), g_object_ref (application));
}

/**
 * pd_runner_remove_application:
 * @self: a #PdRunner
 * @name: name of the #PdApplication to remove
 *
 * Remove an application from @self.
 */
void
pd_runner_remove_application (PdRunner    *self,
                              const gchar *name)
{
  PdRunnerPrivate *priv;

  g_return_if_fail (PD_IS_RUNNER (self));

  priv = pd_runner_get_instance_private (self);

  g_return_if_fail (priv->application_table != NULL);

  if (g_hash_table_contains (priv->application_table, name))
    g_hash_table_remove (priv->application_table, name);
}

/**
 * pd_runner_lookup_application:
 * @self: a #PdRunner
 * @name: name of the application to lookup
 *
 * Retrieve a #PdApplication by name.
 *
 * Returns: (transfer none): a #PdApplication if one is available with @name.
 */
PdApplication *
pd_runner_lookup_application (PdRunner    *self,
                              const gchar *name)
{
  PdRunnerPrivate *priv;

  g_return_val_if_fail (PD_IS_RUNNER (self), NULL);

  priv = pd_runner_get_instance_private (self);

  g_return_val_if_fail (priv->application_table != NULL, NULL);

  if (g_hash_table_contains (priv->application_table, name))
    return g_hash_table_lookup (priv->application_table, name);

  return NULL;
}

/**
 * pd_runner_has_application:
 * @self: a #PdRunner
 * @name: name of the application to check for
 *
 * Check if a #PdApplication is available by name.
 *
 * Returns: `true' if the application is available, `false' otherwise
 */
gboolean
pd_runner_has_application (PdRunner    *self,
                           const gchar *name)
{
  PdRunnerPrivate *priv;

  g_return_val_if_fail (PD_IS_RUNNER (self), FALSE);

  priv = pd_runner_get_instance_private (self);

  g_return_val_if_fail (priv->application_table != NULL, FALSE);

  return g_hash_table_contains (priv->application_table, name);
}

/**
 * pd_runner_launch_application:
 * @self: a #PdRunner
 * @name: name of the #PdApplication to launch
 * @argc: the argc from main() (or 0 if @argv is %NULL)
 * @argv: (array length=argc) (element-type filename) (nullable):
 *     the argv from main(), or %NULL
 * @error: return location for a #GError, or NULL
 *
 * Attempts to launch a #PdApplication for a given name.
 *
 * Returns: the exit status
 */
int
pd_runner_launch_application (PdRunner     *self,
                              const gchar  *name,
                              gint          argc,
                              gchar       **argv,
                              GError      **error)
{
  PdRunnerPrivate *priv;
  PdApplication *application;
  gint status;

  g_autofree gchar *app_name = NULL;

  g_return_val_if_fail (PD_IS_RUNNER (self), -1);
  g_return_val_if_fail (error == NULL || *error == NULL, -1);

  priv = pd_runner_get_instance_private (self);

  g_return_val_if_fail (priv->application_table != NULL, -1);

  /* Check if --mock was provided as an argument */
  for (gint i = 0; i < argc; i++)
    {
      if (g_strcmp0 (argv[i], "--mock"))
        {
          g_debug ("Requested the mock application");
          app_name = g_strdup ("mock");
          /* remove it if it was found */
          g_free (argv[i]);
          argc--;
          for (int j = i; argv[j]; j++)
            argv[j] = argv[j + 1];
        }
    }

  /* Use the name that was provided if --mock wasn't seen */
  if (app_name == NULL)
    app_name = g_strdup (name);

  if (priv->application_table == NULL || !pd_runner_has_application (self, app_name))
    {
      g_set_error (error,
                   PD_ERROR,
                   PD_ERROR_NOT_FOUND,
                   "Couldn't find an application with name: %s",
                   app_name);
      return -1;
    }

  g_debug ("Attempting to launch: %s", app_name);
  application = g_hash_table_lookup (priv->application_table, app_name);

  g_application_set_inactivity_timeout (G_APPLICATION (application), 10000);
  /* XXX: consider using an *exit_status parameter so the caller can launch multiple */
  status = g_application_run (G_APPLICATION (application), argc, argv);

  return status;
}
