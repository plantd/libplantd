/*
 * Copyright © the Plantd contributors. All rights reserved.
 *
 * This file is part of libplantd, distributed under the MIT license.
 * For full terms see the included LICENSE file.
 */

#pragma once

#if !defined (PLANTD_INSIDE) && !defined (PLANTD_COMPILATION)
# error "Only <plantd/plantd.h> can be included directly."
#endif

#include <glib-object.h>

G_BEGIN_DECLS

#define PD_TYPE_EVENT pd_event_get_type ()
G_DECLARE_FINAL_TYPE (PdEvent, pd_event, PD, EVENT, GObject)

PdEvent     *pd_event_new             (void);
PdEvent     *pd_event_new_full        (gint         id,
                                       const gchar *name,
                                       const gchar *description);

gchar       *pd_event_serialize       (PdEvent     *self);
void         pd_event_deserialize     (PdEvent     *self,
                                       const gchar *data);

gchar       *pd_event_to_data         (PdEvent     *self);
PdEvent     *pd_event_from_data       (const gchar *data);

gint         pd_event_get_id          (PdEvent     *self);
void         pd_event_set_id          (PdEvent     *self,
                                       gint         id);

const gchar *pd_event_get_name        (PdEvent     *self);
gchar       *pd_event_dup_name        (PdEvent     *self);
void         pd_event_set_name        (PdEvent     *self,
                                       const gchar *name);

const gchar *pd_event_get_description (PdEvent     *self);
gchar       *pd_event_dup_description (PdEvent     *self);
void         pd_event_set_description (PdEvent     *self,
                                       const gchar *description);

G_END_DECLS
