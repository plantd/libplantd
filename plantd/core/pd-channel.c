/*
 * Copyright © the Plantd contributors. All rights reserved.
 *
 * This file is part of libplantd, distributed under the MIT license.
 * For full terms see the included LICENSE file.
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <json-glib/json-glib.h>

#include "pd-channel.h"

/* TODO: add prop things for type */
struct _PdChannel
{
  GObject          parent;
  /*PdChannelType  channel_type;*/
  gchar           *endpoint;
  gchar           *envelope;
};

enum {
  PROP_0,
  /*PROP_CHANNEL_TYPE,*/
  PROP_ENDPOINT,
  PROP_ENVELOPE,
  N_PROPS
};

static GParamSpec *properties [N_PROPS];

G_DEFINE_TYPE (PdChannel, pd_channel, G_TYPE_OBJECT)

static void
pd_channel_finalize (GObject *object)
{
  PdChannel *self = (PdChannel *)object;

  g_clear_pointer (&self->endpoint, g_free);
  g_clear_pointer (&self->envelope, g_free);

  G_OBJECT_CLASS (pd_channel_parent_class)->finalize (object);
}

static void
pd_channel_get_property (GObject    *object,
                         guint       prop_id,
                         GValue     *value,
                         GParamSpec *pspec)
{
  PdChannel *self = PD_CHANNEL (object);

  switch (prop_id)
  {
    /*case PROP_CHANNEL_TYPE:*/
      /*g_value_set_enum (value, pd_channel_get_channel_type (self));*/
      /*break;*/

    case PROP_ENDPOINT:
      g_value_set_string (value, pd_channel_get_endpoint (self));
      break;

    case PROP_ENVELOPE:
      g_value_set_string (value, pd_channel_get_envelope (self));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
  }
}

static void
pd_channel_set_property (GObject      *object,
                         guint         prop_id,
                         const GValue *value,
                         GParamSpec   *pspec)
{
  PdChannel *self = PD_CHANNEL (object);

  switch (prop_id)
  {
    /*case PROP_CHANNEL_TYPE:*/
      /*pd_channel_set_channel_type (self, g_value_get_enum (value));*/
      /*break;*/

    case PROP_ENDPOINT:
      pd_channel_set_endpoint (self, g_value_get_string (value));
      break;

    case PROP_ENVELOPE:
      pd_channel_set_envelope (self, g_value_get_string (value));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
  }
}

static void
pd_channel_class_init (PdChannelClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->finalize = pd_channel_finalize;
  object_class->get_property = pd_channel_get_property;
  object_class->set_property = pd_channel_set_property;

  properties [PROP_ENDPOINT] =
    g_param_spec_string ("endpoint",
                         "Endpoint",
                         "The channel endpoint.",
                         NULL,
                         (G_PARAM_READWRITE | G_PARAM_EXPLICIT_NOTIFY | G_PARAM_STATIC_STRINGS));

  properties [PROP_ENVELOPE] =
    g_param_spec_string ("envelope",
                         "Envelope",
                         "The channel envelope.",
                         NULL,
                         (G_PARAM_READWRITE | G_PARAM_EXPLICIT_NOTIFY | G_PARAM_STATIC_STRINGS));

  g_object_class_install_properties (object_class, N_PROPS, properties);
}

static void
pd_channel_init (PdChannel *self)
{
}

PdChannel *
pd_channel_new (void)
{
  PdChannel *object = g_object_new (PD_TYPE_CHANNEL, NULL);

  // TODO: add setup

  return object;
}

gchar *
pd_channel_serialize (PdChannel *self)
{
  g_return_val_if_fail (PD_IS_CHANNEL (self), NULL);

  return json_gobject_to_data (G_OBJECT (self), NULL);
}

void
pd_channel_deserialize (PdChannel   *self,
                        const gchar *data)
{
  GError *err = NULL;
  GObject *object = json_gobject_from_data (PD_TYPE_CHANNEL,
                                            data,
                                            -1,
                                            &err);

  if (err != NULL)
  {
    g_critical ("%s", err->message);
    g_error_free (err);
  }

  g_return_if_fail (object != NULL);
  g_return_if_fail (PD_IS_CHANNEL (object));

  pd_channel_set_endpoint (self, pd_channel_get_endpoint (PD_CHANNEL (object)));
  pd_channel_set_envelope (self, pd_channel_get_envelope (PD_CHANNEL (object)));

  g_clear_pointer (&PD_CHANNEL (object)->endpoint, g_free);
  g_clear_pointer (&PD_CHANNEL (object)->envelope, g_free);
  g_object_unref (object);
}

const gchar *
pd_channel_get_endpoint (PdChannel *self)
{
  g_return_val_if_fail (PD_IS_CHANNEL (self), NULL);

  return self->endpoint;
}

void
pd_channel_set_endpoint (PdChannel   *self,
                         const gchar *endpoint)
{
  g_return_if_fail (PD_IS_CHANNEL (self));

  if (g_strcmp0 (endpoint, self->endpoint) != 0)
    {
      g_free (self->endpoint);
      self->endpoint = g_strdup (endpoint);
      g_object_notify_by_pspec (G_OBJECT (self), properties [PROP_ENDPOINT]);
    }
}

const gchar *
pd_channel_get_envelope (PdChannel *self)
{
  g_return_val_if_fail (PD_IS_CHANNEL (self), NULL);

  return self->envelope;
}

void
pd_channel_set_envelope (PdChannel   *self,
                         const gchar *envelope)
{
  g_return_if_fail (PD_IS_CHANNEL (self));

  if (g_strcmp0 (envelope, self->envelope) != 0)
    {
      g_free (self->envelope);
      self->envelope = g_strdup (envelope);
      g_object_notify_by_pspec (G_OBJECT (self), properties [PROP_ENVELOPE]);
    }
}
