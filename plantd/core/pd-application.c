/*
 * Copyright © the Plantd contributors. All rights reserved.
 *
 * This file is part of libplantd, distributed under the MIT license.
 * For full terms see the included LICENSE file.
 */

#define G_LOG_DOMAIN "plantd-application"

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <glib-unix.h>
#include <czmq.h>
#include <plantd-debug.h>

#include "plantd-error.h"

#include "pd-application.h"
#include "pd-configuration.h"
#include "pd-event.h"
#include "pd-handler.h"
#include "pd-job.h"
#include "pd-job-queue.h"
#include "pd-log.h"
#include "pd-mdp-handler.h"
#include "pd-metric.h"
#include "pd-sink.h"
#include "pd-source.h"
#include "pd-property.h"
#include "mdp/pd-broker.h"
#include "message/pd-message.h"

/**
 * SECTION:plantd-application
 * @short_description: Application base class
 *
 * An #PdApplication is used by applications that should support message handling.
 */

/**
 * PdApplication:
 *
 * #PdApplication is meant to be derived by services that integrate with a larger
 * Plantd network, and are accessed using the functions defined here.
 */

/**
 * PdApplicationClass:
 * @property_changed: invoked after a stored property has been changed
 * @get_configuration: invoked by the message handler when the
 *    'get-configuration' message has been received.
 * @get_status: invoked by the message handler when the
 *    'get-status' message has been received.
 * @get_settings: invoked by the message handler when the
 *    'get-settings' message has been received.
 * @get_job: invoked by the message handler when the
 *    'get-job' message has been received.
 * @get_jobs: invoked by the message handler when the
 *    'get-jobs' message has been received.
 * @get_active_job: invoked by the message handler when the
 *    'get-active-job' message has been received.
 * @submit_job: invoked by the message handler when the
 *    'submit-job' message has been received.
 * @cancel_job: invoked by the message handler when the
 *    'cancel-job' message has been received.
 * @submit_event: invoked by the message handler when the
 *    'submit-event' message has been received.
 * @available_events: invoked by the message handler when the
 *    'available-events' message has been received.
 *
 * Virtual function table for #PdApplication.
 */

typedef struct
{
  /*< public >*/
  gchar           *id;
  gchar           *endpoint;
  gchar           *service;
  PdConfiguration *configuration;

  /*< private >*/
  gboolean         enable_handler;
  gboolean         enable_job_queue;
  gboolean         enable_standalone;
  GAction         *shutdown_action;
  GHashTable      *property_table;
  GHashTable      *sink_table;
  GHashTable      *source_table;
  gchar           *broker_endpoint;
  PdBroker        *broker;
  PdHandler       *handler;
  PdJobQueue      *job_queue;
} PdApplicationPrivate;

enum {
  PROP_0,
  PROP_ID,
  PROP_ENDPOINT,
  PROP_SERVICE,
  PROP_CONFIG,
  N_PROPS
};

static GParamSpec *properties [N_PROPS];

enum {
  SIGNAL_PROPERTY_CHANGED,
  N_SIGNALS
};

static guint signals [N_SIGNALS];

G_DEFINE_TYPE_WITH_PRIVATE (PdApplication, pd_application, G_TYPE_APPLICATION)

/* TODO: do real signals that are useful */
static void
s_shutdown_action_cb (GAction  *action,
                      GVariant *parameter,
                      gpointer  data)
{
  GApplication *app = NULL;

  app = G_APPLICATION (data);

  g_application_hold (app);
  g_info ("Action %s activated\n", g_action_get_name (action));
  g_application_release (app);
}

static void
s_add_actions (PdApplication *self)
{
  PdApplicationPrivate *priv;
  /*GAction *action = NULL;*/

  priv = pd_application_get_instance_private (self);

  priv->shutdown_action = G_ACTION (g_simple_action_new ("shutdown-action", NULL));
  g_signal_connect (priv->shutdown_action, "activate", G_CALLBACK (s_shutdown_action_cb), self);
  g_action_map_add_action (G_ACTION_MAP (self), G_ACTION (priv->shutdown_action));
  /*g_object_unref (action);*/
}

static gboolean
s_signal_handler (gpointer user_data)
{
  GApplication *app = NULL;

  app = G_APPLICATION (user_data);

  g_info ("%s\n", "Stop signal received, exiting");
  g_application_quit (app);

  return G_SOURCE_REMOVE;
}

static void
s_add_signals (PdApplication *self)
{
  g_debug ("add signal interrupts");

  g_source_set_name_by_id (g_unix_signal_add (SIGUSR1,
                                              s_signal_handler,
                                              self),
                           "[PlantdApplication] SIGUSR1 listener");
  g_source_set_name_by_id (g_unix_signal_add (SIGHUP,
                                              s_signal_handler,
                                              self),
                           "[PlantdApplication] SIGHUP listener");
  g_source_set_name_by_id (g_unix_signal_add (SIGTERM,
                                              s_signal_handler,
                                              self),
                           "[PlantdApplication] SIGTERM listener");
  g_source_set_name_by_id (g_unix_signal_add (SIGINT,
                                              s_signal_handler,
                                              self),
                           "[PlantdApplication] SIGINT listener");
}

static void
s_process_env (PdApplication *self)
{
  PdApplicationPrivate *priv;

  priv = pd_application_get_instance_private (self);

  /* Setup the defaults */
  priv->enable_handler = TRUE;
  priv->enable_job_queue = TRUE;
  priv->enable_standalone = FALSE;

  if (g_getenv ("PLANTD_MODULE_HANDLER") != NULL)
    {
      g_autofree gchar *env = NULL;
      env = g_utf8_strdown (g_getenv ("PLANTD_MODULE_HANDLER"), -1);
      g_debug ("[PLANTD_MODULE_HANDLER]: %s", env);
      if (g_strcmp0 (env, "false") == 0 ||
          g_strcmp0 (env, "no") == 0 ||
          g_strcmp0 (env, "0") == 0)
        priv->enable_handler = FALSE;
    }

  if (g_getenv ("PLANTD_MODULE_JOB_QUEUE") != NULL)
    {
      g_autofree gchar *env = NULL;
      env = g_utf8_strdown (g_getenv ("PLANTD_MODULE_JOB_QUEUE"), -1);
      g_debug ("[PLANTD_MODULE_JOB_QUEUE]: %s", env);
      if (g_strcmp0 (env, "false") == 0 ||
          g_strcmp0 (env, "no") == 0 ||
          g_strcmp0 (env, "0") == 0)
        priv->enable_job_queue = FALSE;
    }

  if (g_getenv ("PLANTD_MODULE_STANDALONE") != NULL)
    {
      g_autofree gchar *env = NULL;
      env = g_utf8_strdown (g_getenv ("PLANTD_MODULE_STANDALONE"), -1);
      g_debug ("[PLANTD_MODULE_STANDALONE]: %s", env);
      if (g_strcmp0 (env, "true") == 0 ||
          g_strcmp0 (env, "yes") == 0 ||
          g_strcmp0 (env, "1") == 0)
        priv->enable_standalone = TRUE;
    }

  /* XXX: these may need to be handled after application run otherwise the
   * environment will be lower priority than values set by the application */

  if (g_getenv ("PLANTD_MODULE_ENDPOINT") != NULL)
    {
      const gchar *env;
      env = g_getenv ("PLANTD_MODULE_ENDPOINT");
      pd_application_set_endpoint (self, env);
      g_debug ("[PLANTD_MODULE_ENDPOINT]: %s", env);
    }

  if (g_getenv ("PLANTD_MODULE_BROKER") != NULL)
    {
      const gchar *env;
      env = g_getenv ("PLANTD_MODULE_BROKER");
      priv->broker_endpoint = g_strdup (env);
      g_debug ("[PLANTD_MODULE_BROKER]: %s", env);
    }
  else
    {
      priv->broker_endpoint = g_strdup ("tcp://*:5555");
    }
}

static void
s_run_standalone (PdApplication *self)
{
  PdApplicationPrivate *priv;

  g_debug ("run in standalone mode");

  priv = pd_application_get_instance_private (self);

  priv->broker = pd_broker_new (priv->broker_endpoint);
  pd_broker_bind (priv->broker);
  pd_broker_run (priv->broker);
}

/* vfunc defaults {{{1 */
static PdConfigurationResponse *
pd_application_real_get_configuration (PdApplication  *self,
                                       GError        **error)
{
  PdApplicationPrivate *priv;
  PdConfigurationResponse *response;

  g_return_val_if_fail (PD_IS_APPLICATION (self), NULL);

  priv = pd_application_get_instance_private (self);

  response = pd_configuration_response_new ();
  pd_configuration_response_set_configuration (response, priv->configuration);

  return response;
}

static PdStatusResponse *
pd_application_real_get_status (PdApplication  *self,
                                GError        **error)
{
  PdStatusResponse *response;

  g_return_val_if_fail (PD_IS_APPLICATION (self), NULL);

  response = pd_status_response_new ();

  return response;
}

static PdSettingsResponse *
pd_application_real_get_settings (PdApplication  *self,
                                  GError        **error)
{
  PdSettingsResponse *response;

  g_return_val_if_fail (PD_IS_APPLICATION (self), NULL);

  response = pd_settings_response_new ();

  return response;
}

static PdJobResponse *
pd_application_real_get_job (PdApplication  *self,
                             const gchar    *id,
                             GError        **error)
{
  PdJobResponse *response;

  g_return_val_if_fail (PD_IS_APPLICATION (self), NULL);

  response = pd_job_response_new ();

  return response;
}

static PdJobsResponse *
pd_application_real_get_jobs (PdApplication  *self,
                              GError        **error)
{
  PdJobsResponse *response;

  g_return_val_if_fail (PD_IS_APPLICATION (self), NULL);

  response = pd_jobs_response_new ();

  return response;
}

static PdJobResponse *
pd_application_real_get_active_job (PdApplication  *self,
                                    GError        **error)
{
  PdJobResponse *response;

  g_return_val_if_fail (PD_IS_APPLICATION (self), NULL);

  response = pd_job_response_new ();

  return response;
}

static void
pd_application_startup (PdApplication *self)
{
  GApplication *app;

  g_return_if_fail (PD_IS_APPLICATION (self));

  app = G_APPLICATION (self);

  g_application_hold (app);
  g_debug ("startup");
  g_application_release (app);
}

static void
pd_application_activate (PdApplication *self)
{
  PdApplicationPrivate *priv;
  GApplication *app;

  g_return_if_fail (PD_IS_APPLICATION (self));

  priv = pd_application_get_instance_private (self);

  app = G_APPLICATION (self);

  g_application_hold (app);
  g_debug ("activate");

  /* Setup */
  s_add_actions (self);
  s_add_signals (self);

  /* Apply configuration from environment */
  s_process_env (self);

  /* Launch async tasks for message handling and job queue */
  if (priv->enable_handler)
    pd_handler_run (priv->handler, self, NULL);

  if (priv->enable_job_queue)
    pd_job_queue_run (priv->job_queue, NULL);

  if (priv->enable_standalone)
    s_run_standalone (self);

  g_debug ("finished activation");
  g_application_release (app);
}

static void
pd_application_shutdown (PdApplication *self)
{
  PdApplicationPrivate *priv;
  GApplication *app;
  GHashTableIter source_iter;
  GHashTableIter sink_iter;
  gpointer key, val;

  g_return_if_fail (PD_IS_APPLICATION (self));

  priv = pd_application_get_instance_private (self);

  app = G_APPLICATION (self);

  g_debug ("shutdown");

  /* Clean up source sockets */
  if (priv->source_table)
    {
      g_debug ("cleaning up source sockets");
      g_hash_table_iter_init (&source_iter, priv->source_table);
      while (g_hash_table_iter_next (&source_iter, &key, &val))
        {
          g_debug ("stopping source: %s", (gchar *)key);
          pd_source_stop (PD_SOURCE (val));
        }
    }

  /* Clean up sink sockets */
  if (priv->sink_table)
    {
      g_debug ("cleaning up sink sockets");
      g_hash_table_iter_init (&sink_iter, priv->sink_table);
      while (g_hash_table_iter_next (&sink_iter, &key, &val))
        {
          g_debug ("stopping sink: %s", (gchar *)key);
          pd_sink_stop (PD_SINK (val));
        }
    }

  if (priv->enable_standalone)
    {
      g_debug ("stopping broker");
      pd_broker_stop (priv->broker);
    }

  g_debug ("cancelling job queue task");
  pd_job_queue_cancel (priv->job_queue);
  g_debug ("cancelling message handler task");
  pd_handler_cancel (priv->handler);

  /* Wait for other things to finish, probably not necessary */
  g_usleep (250000);

  g_application_quit (app);
}

static void
s_handle_shutdown (PdHandler     *handler,
                   PdApplication *app)
{
  g_debug ("received shutdown event from handler");

  pd_application_shutdown (app);
}

/* early param check test start */

static gboolean
s_verbose_cb (const gchar  *option_name,
              const gchar  *value,
              gpointer      data,
              GError      **error)
{
  pd_log_increase_verbosity ();
  return TRUE;
}

static void
s_param_check (gint    *argc,
               gchar ***argv)
{
  g_autoptr(GOptionContext) context = NULL;
  GOptionEntry entries[] = {
    { "verbose", 'v', G_OPTION_FLAG_NO_ARG, G_OPTION_ARG_CALLBACK, s_verbose_cb },
    { NULL }
  };

  context = g_option_context_new (NULL);
  g_option_context_set_ignore_unknown_options (context, TRUE);
  g_option_context_set_help_enabled (context, FALSE);
  g_option_context_add_main_entries (context, entries, NULL);
  g_option_context_parse (context, argc, argv, NULL);
}

/* early param check test stop */

/* TODO: remove this? */
static gboolean
pd_application_local_command_line (GApplication   *self,
                                   gchar        ***arguments,
                                   gint           *exit_status)
{
  gchar **argv;
  /*gint i, j;*/
  gint argc;

  argv = *arguments;

  /*i = 1;*/
  argc = 0;
  while (argv[argc])
  /*while (argv[i])*/
    {
/*
 *      if (g_str_has_prefix (argv[i], "--local-"))
 *        {
 *          g_debug ("handling argument %s locally", argv[i]);
 *
 *          if (g_str_has_prefix (argv[i], "--local-config"))
 *            {
 *              // TODO: check for --local-config= as well
 *              // TODO: check for null i+1
 *              g_debug ("Load config: %s", argv[i+1]);
 *            }
 *
 *          g_free (argv[i]);
 *          for (j = i; argv[j]; j++)
 *            argv[j] = argv[j + 1];
 *        }
 *      else
 *        {
 *          g_debug ("not handling argument %s locally", argv[i]);
 *          i++;
 *        }
 */

      argc++;
    }

  s_param_check (&argc, &argv);

  *exit_status = 0;

  return G_APPLICATION_CLASS (
      pd_application_parent_class)->local_command_line
        (G_APPLICATION (self), arguments, exit_status);
}

/* GObject setup {{{1 */
static void
pd_application_finalize (GObject *object)
{
  PdApplication *self = (PdApplication *)object;
  PdApplicationPrivate *priv = pd_application_get_instance_private (self);

  g_debug ("application finalize");

  g_clear_pointer (&priv->id, g_free);
  g_clear_pointer (&priv->endpoint, g_free);
  g_clear_pointer (&priv->service, g_free);
  g_clear_pointer (&priv->broker_endpoint, g_free);
  g_clear_object (&priv->broker);
  g_clear_object (&priv->handler);
  g_clear_object (&priv->job_queue);

  if (priv->configuration)
    g_object_unref (priv->configuration);

  if (priv->property_table)
    g_hash_table_unref (priv->property_table);

  if (priv->sink_table)
    g_hash_table_unref (priv->sink_table);

  if (priv->source_table)
    g_hash_table_unref (priv->source_table);

  if (priv->shutdown_action)
    g_object_unref (priv->shutdown_action);

  G_OBJECT_CLASS (pd_application_parent_class)->finalize (object);
}

static void
pd_application_get_property (GObject    *object,
                             guint       prop_id,
                             GValue     *value,
                             GParamSpec *pspec)
{
  PdApplicationPrivate *priv;
  PdApplication *self = PD_APPLICATION (object);

  priv = pd_application_get_instance_private (self);

  switch (prop_id)
    {
    case PROP_ID:
      g_value_set_string (value, pd_application_get_id (self));
      break;

    case PROP_ENDPOINT:
      g_value_set_string (value, pd_application_get_endpoint (self));
      break;

    case PROP_SERVICE:
      g_value_set_string (value, pd_application_get_service (self));
      break;

    case PROP_CONFIG:
      g_value_set_object (value, priv->configuration);
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
pd_application_set_property (GObject      *object,
                             guint         prop_id,
                             const GValue *value,
                             GParamSpec   *pspec)
{
  PdApplication *self = PD_APPLICATION (object);

  switch (prop_id)
    {
    case PROP_ID:
      pd_application_set_id (self, g_value_get_string (value));
      break;

    case PROP_ENDPOINT:
      pd_application_set_endpoint (self, g_value_get_string (value));
      break;

    case PROP_SERVICE:
      pd_application_set_service (self, g_value_get_string (value));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
pd_application_class_init (PdApplicationClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->finalize = pd_application_finalize;
  object_class->get_property = pd_application_get_property;
  object_class->set_property = pd_application_set_property;

  /*
   *G_APPLICATION_CLASS (klass)->activate = pd_application_activate;
   *G_APPLICATION_CLASS (klass)->startup = pd_application_startup;
   *G_APPLICATION_CLASS (klass)->shutdown = pd_application_shutdown;
   */
  G_APPLICATION_CLASS (klass)->local_command_line = pd_application_local_command_line;

  klass->get_configuration = pd_application_real_get_configuration;
  klass->get_status = pd_application_real_get_status;
  klass->get_settings = pd_application_real_get_settings;
  klass->get_job = pd_application_real_get_job;
  klass->get_jobs = pd_application_real_get_jobs;
  klass->get_active_job = pd_application_real_get_active_job;

  properties [PROP_ID] =
    g_param_spec_string ("id",
                         "Id",
                         "The id of the application.",
                         NULL,
                         (G_PARAM_READWRITE | G_PARAM_EXPLICIT_NOTIFY | G_PARAM_STATIC_STRINGS));

  properties [PROP_ENDPOINT] =
    g_param_spec_string ("endpoint",
                         "Endpoint",
                         "The message queue endpoint.",
                         NULL,
                         (G_PARAM_READWRITE | G_PARAM_EXPLICIT_NOTIFY | G_PARAM_STATIC_STRINGS));

  properties [PROP_SERVICE] =
    g_param_spec_string ("service",
                         "Service",
                         "The message queue service name.",
                         NULL,
                         (G_PARAM_READWRITE | G_PARAM_EXPLICIT_NOTIFY | G_PARAM_STATIC_STRINGS));

  properties [PROP_CONFIG] =
    g_param_spec_object ("config",
                         "Config",
                         "The configuration in the response.",
                         PD_TYPE_CONFIGURATION,
                         (G_PARAM_READABLE | G_PARAM_STATIC_STRINGS));

  g_object_class_install_properties (object_class, N_PROPS, properties);

  /**
   * PdApplication::property_changed:
   * @self: the application
   *
   * The ::property_changed signal is emitted on the primary instance
   * after a stored property has been updated.
   */
  signals [SIGNAL_PROPERTY_CHANGED] =
    g_signal_new ("property-changed", PD_TYPE_APPLICATION, G_SIGNAL_RUN_FIRST,
                  G_STRUCT_OFFSET (PdApplicationClass, property_changed),
                  NULL, NULL, NULL,
                  G_TYPE_NONE, 1, PD_TYPE_PROPERTY);
}

static void
pd_application_init (PdApplication *self)
{
  PdApplicationPrivate *priv;

  priv = pd_application_get_instance_private (self);

  priv->id = g_strdup ("org.plantd.Module");
  priv->endpoint = g_strdup ("tcp://localhost:5555");
  priv->service = g_strdup ("module");

  priv->property_table = g_hash_table_new_full (g_str_hash,
                                                g_str_equal,
                                                g_free,
                                                NULL);

  priv->sink_table = g_hash_table_new_full (g_str_hash,
                                            g_str_equal,
                                            g_free,
                                            NULL);

  priv->source_table = g_hash_table_new_full (g_str_hash,
                                              g_str_equal,
                                              g_free,
                                              NULL);

  priv->job_queue = pd_job_queue_new ();
  priv->handler = PD_HANDLER (pd_mdp_handler_new ());

  g_signal_connect (self, "activate", G_CALLBACK (pd_application_activate), NULL);
  g_signal_connect (self, "startup", G_CALLBACK (pd_application_startup), NULL);
  g_signal_connect (self, "shutdown", G_CALLBACK (pd_application_shutdown), NULL);

  g_signal_connect (PD_HANDLER (priv->handler),
                    "shutdown-requested",
                    G_CALLBACK (s_handle_shutdown),
                    self);
}

/**
 * pd_application_new:
 * @application_id: (allow-none): The application ID.
 * @flags: the application flags
 *
 * Creates a new #PdApplication instance.
 *
 * If non-%NULL, the application ID must be valid.  See
 * g_application_id_is_valid().
 *
 * If no application ID is given then some features (most notably application
 * uniqueness) will be disabled.
 *
 * Returns: a new #PdApplication instance
 */
PdApplication *
pd_application_new (const gchar       *application_id,
                    GApplicationFlags  flags)
{
  g_return_val_if_fail (application_id == NULL ||
                        g_application_id_is_valid (application_id), NULL);

  return g_object_new (PD_TYPE_APPLICATION,
                       "application-id", application_id,
                       "flags", flags | G_APPLICATION_HANDLES_COMMAND_LINE,
                       NULL);
}

/**
 * pd_application_get_configuration:
 * @self: #PdApplication instance
 * @error: return location for a GError, or NULL
 *
 * Returns: (transfer full): a response message, free after
 */
PdConfigurationResponse *
pd_application_get_configuration (PdApplication  *self,
                                  GError        **error)
{
  PdApplicationClass *klass;

  g_return_val_if_fail (PD_IS_APPLICATION (self), NULL);
  g_return_val_if_fail (error == NULL || *error == NULL, NULL);

  klass = PD_APPLICATION_GET_CLASS (self);
  g_return_val_if_fail (klass->get_configuration != NULL, NULL);

  return klass->get_configuration (self, error);
}

/**
 * pd_application_get_status:
 * @self: #PdApplication instance
 * @error: return location for a GError, or NULL
 *
 * Returns: (transfer full): a response message, free after
 */
PdStatusResponse *
pd_application_get_status (PdApplication  *self,
                           GError        **error)
{
  PdApplicationClass *klass;

  g_return_val_if_fail (PD_IS_APPLICATION (self), NULL);
  g_return_val_if_fail (error == NULL || *error == NULL, NULL);

  klass = PD_APPLICATION_GET_CLASS (self);
  g_return_val_if_fail (klass->get_status != NULL, NULL);

  return klass->get_status (self, error);
}

/**
 * pd_application_get_settings:
 * @self: #PdApplication instance
 * @error: return location for a GError, or NULL
 *
 * Returns: (transfer full): a response message, free after
 */
PdSettingsResponse *
pd_application_get_settings (PdApplication  *self,
                             GError        **error)
{
  PdApplicationClass *klass;

  g_return_val_if_fail (PD_IS_APPLICATION (self), NULL);
  g_return_val_if_fail (error == NULL || *error == NULL, NULL);

  klass = PD_APPLICATION_GET_CLASS (self);
  g_return_val_if_fail (klass->get_settings != NULL, NULL);

  return klass->get_settings (self, error);
}

/**
 * pd_application_get_job:
 * @self: #PdApplication instance
 * @error: return location for a GError, or NULL
 *
 * Returns: (transfer full): a response message, free after
 */
PdJobResponse *
pd_application_get_job (PdApplication  *self,
                        const gchar    *job_id,
                        GError        **error)
{
  PdApplicationClass *klass;

  g_return_val_if_fail (PD_IS_APPLICATION (self), NULL);
  g_return_val_if_fail (error == NULL || *error == NULL, NULL);

  klass = PD_APPLICATION_GET_CLASS (self);
  g_return_val_if_fail (klass->get_job != NULL, NULL);

  return klass->get_job (self, job_id, error);
}

/**
 * pd_application_get_jobs:
 * @self: #PdApplication instance
 * @error: return location for a GError, or NULL
 *
 * Returns: (transfer full): a response message, free after
 */
PdJobsResponse *
pd_application_get_jobs (PdApplication  *self,
                         GError        **error)
{
  PdApplicationClass *klass;

  g_return_val_if_fail (PD_IS_APPLICATION (self), NULL);
  g_return_val_if_fail (error == NULL || *error == NULL, NULL);

  klass = PD_APPLICATION_GET_CLASS (self);
  g_return_val_if_fail (klass->get_jobs != NULL, NULL);

  return klass->get_jobs (self, error);
}

/**
 * pd_application_get_active_job:
 * @self: #PdApplication instance
 * @error: return location for a GError, or NULL
 *
 * Returns: (transfer full): a response message, free after
 */
PdJobResponse *
pd_application_get_active_job (PdApplication  *self,
                               GError        **error)
{
  PdApplicationClass *klass;

  g_return_val_if_fail (PD_IS_APPLICATION (self), NULL);
  g_return_val_if_fail (error == NULL || *error == NULL, NULL);

  klass = PD_APPLICATION_GET_CLASS (self);
  g_return_val_if_fail (klass->get_active_job != NULL, NULL);

  return klass->get_active_job (self, error);
}

/**
 * pd_application_cancel_job:
 * @self: #PdApplication instance
 * @job_id: ID of the job to cancel
 * @error: return location for a GError, or NULL
 *
 * Returns: (transfer full): a response message, free after
 */
PdJobResponse *
pd_application_cancel_job (PdApplication  *self,
                           const gchar    *job_id,
                           GError        **error)
{
  PdApplicationClass *klass;

  g_return_val_if_fail (PD_IS_APPLICATION (self), NULL);
  g_return_val_if_fail (error == NULL || *error == NULL, NULL);

  klass = PD_APPLICATION_GET_CLASS (self);
  g_return_val_if_fail (klass->cancel_job != NULL, NULL);

  return klass->cancel_job (self, job_id, error);
}

/**
 * pd_application_submit_job:
 * @self: #PdApplication instance
 * @job_id: incorrectly named param for the job to execute
 * @job_value: additional data to go with the job
 * @job_properties: (element-type utf8 utf8) (transfer none): A #GHashTable of properties
 * @error: return location for a GError, or NULL
 *
 * Returns: (transfer full): a response message, free after
 */
PdJobResponse *
pd_application_submit_job (PdApplication  *self,
                           const gchar    *job_id,
                           const gchar    *job_value,
                           GHashTable     *job_properties,
                           GError        **error)
{
  PdApplicationClass *klass;

  g_return_val_if_fail (PD_IS_APPLICATION (self), NULL);
  g_return_val_if_fail (error == NULL || *error == NULL, NULL);

  klass = PD_APPLICATION_GET_CLASS (self);
  g_return_val_if_fail (klass->submit_job != NULL, NULL);

  return klass->submit_job (self, job_id, job_value, job_properties, error);
}

/**
 * pd_application_submit_event:
 * @self: #PdApplication instance
 * @event_id: ID of the event to submit
 * @error: return location for a GError, or NULL
 *
 * Returns: (transfer full): a response message, free after
 */
PdJobResponse *
pd_application_submit_event (PdApplication  *self,
                             const gint      event_id,
                             GError        **error)
{
  PdApplicationClass *klass;

  g_return_val_if_fail (PD_IS_APPLICATION (self), NULL);
  g_return_val_if_fail (error == NULL || *error == NULL, NULL);

  klass = PD_APPLICATION_GET_CLASS (self);
  g_return_val_if_fail (klass->submit_event != NULL, NULL);

  return klass->submit_event (self, event_id, error);
}

/**
 * pd_application_available_events:
 * @self: #PdApplication instance
 * @error: return location for a GError, or NULL
 *
 * Returns: (transfer full): a response message, free after
 */
PdEventResponse *
pd_application_available_events (PdApplication  *self,
                                 GError        **error)
{
  PdApplicationClass *klass;

  g_return_val_if_fail (PD_IS_APPLICATION (self), NULL);
  g_return_val_if_fail (error == NULL || *error == NULL, NULL);

  klass = PD_APPLICATION_GET_CLASS (self);
  g_return_val_if_fail (klass->available_events != NULL, NULL);

  return klass->available_events (self, error);
}

void pd_application_load_config (PdApplication  *self,
                                 const gchar    *config,
                                 GError        **error)
{
  PdApplicationPrivate *priv;

  g_return_if_fail (PD_IS_APPLICATION (self));

  priv = pd_application_get_instance_private (self);

  if (priv->configuration)
    g_object_unref (priv->configuration);

  priv->configuration = pd_configuration_new ();
  pd_configuration_load (priv->configuration, config, error);
}

/**
 * pd_application_get_loaded_config:
 * @self: #PdApplication instance
 *
 * Returns: (transfer none): The #PdConfiguration
 */
PdConfiguration *
pd_application_get_loaded_config (PdApplication *self)
{
  PdConfiguration *configuration;

  g_return_val_if_fail (PD_IS_APPLICATION (self), NULL);

  g_object_get (self, "config", &configuration, NULL);

  return configuration;
}

/**
 * pd_application_add_property:
 * @self: #PdApplication instance
 * @property: #PdProperty to add
 *
 * Add or replace a property of @self.
 */
void
pd_application_add_property (PdApplication *self,
                             PdProperty    *property)
{
  PdApplicationPrivate *priv;
  const gchar *key;
  const gchar *value;

  g_return_if_fail (PD_IS_APPLICATION (self));
  g_return_if_fail (PD_IS_PROPERTY (property));

  priv = pd_application_get_instance_private (self);

  if (priv->property_table == NULL)
    priv->property_table = g_hash_table_new_full (g_str_hash,
                                                  g_str_equal,
                                                  g_free,
                                                  NULL);

  key = pd_property_get_key (property);
  value = pd_property_get_value (property);

  if (g_hash_table_contains (priv->property_table, key))
    g_hash_table_remove (priv->property_table, key);

  g_hash_table_insert (priv->property_table, g_strdup (key), pd_property_new (key, value));
}

/**
 * pd_application_remove_property:
 * @self: #PdApplication instance
 * @key: Key of the property to remove
 *
 * Remove a property of @self by @key.
 */
void
pd_application_remove_property (PdApplication *self,
                                const gchar   *key)
{
  PdApplicationPrivate *priv;

  g_return_if_fail (PD_IS_APPLICATION (self));

  priv = pd_application_get_instance_private (self);

  g_return_if_fail (priv->property_table != NULL);

  if (g_hash_table_contains (priv->property_table, key))
    g_hash_table_remove (priv->property_table, key);
}

/**
 * pd_application_lookup_property:
 * @self: #PdApplication instance
 * @key: key of the property to lookup
 *
 * Retrieve a #PdProperty by key.
 *
 * Returns: (transfer full): a #PdProperty if one is available with @key.
 */
PdProperty *
pd_application_lookup_property (PdApplication *self,
                                const gchar   *key)
{
  PdApplicationPrivate *priv;

  g_return_val_if_fail (PD_IS_APPLICATION (self), NULL);

  priv = pd_application_get_instance_private (self);

  g_return_val_if_fail (priv->property_table != NULL, NULL);

  if (g_hash_table_contains (priv->property_table, key))
    {
      PdProperty *prop;
      prop = g_hash_table_lookup (priv->property_table, key);
      g_return_val_if_fail (PD_IS_PROPERTY (prop), NULL);
      return prop;
    }

  return NULL;
}

gboolean
pd_application_update_property (PdApplication *self,
                                const gchar   *key,
                                const gchar   *value)
{
  PdApplicationPrivate *priv;

  g_return_val_if_fail (PD_IS_APPLICATION (self), FALSE);

  priv = pd_application_get_instance_private (self);

  g_return_val_if_fail (priv->property_table != NULL, FALSE);

  if (g_hash_table_contains (priv->property_table, key))
    {
      PdProperty *prop;

      prop = g_hash_table_lookup (priv->property_table, key);
      pd_property_set_value (prop, value);

      g_signal_emit (self, signals [SIGNAL_PROPERTY_CHANGED], 0, prop);

      return TRUE;
    }

  return FALSE;
}

gboolean
pd_application_has_property (PdApplication *self,
                             const gchar   *key)
{
  PdApplicationPrivate *priv;

  g_return_val_if_fail (PD_IS_APPLICATION (self), FALSE);

  priv = pd_application_get_instance_private (self);

  g_return_val_if_fail (priv->property_table != NULL, FALSE);

  return g_hash_table_contains (priv->property_table, key);
}

/**
 * pd_application_add_sink:
 * @self: #PdApplication instance
 * @name: the name of the sink
 * @sink: and #PdSink
 *
 * Add a message sink (subscriber) to @self.
 */
void
pd_application_add_sink (PdApplication *self,
                         const gchar   *name,
                         PdSink        *sink)
{
  PdApplicationPrivate *priv;

  g_return_if_fail (PD_IS_APPLICATION (self));

  priv = pd_application_get_instance_private (self);

  if (priv->sink_table == NULL)
    priv->sink_table = g_hash_table_new_full (g_str_hash, g_str_equal, g_free, NULL);

  // TODO: review this, seems a little unnecessary
  if (g_hash_table_contains (priv->sink_table, name))
    {
      g_autoptr (PdSink) s = NULL;
      s = g_hash_table_lookup (priv->sink_table, name);
      pd_sink_stop (s);
      g_hash_table_remove (priv->sink_table, name);
    }

  g_hash_table_insert (priv->sink_table, g_strdup (name), sink);
}

/**
 * pd_application_remove_sink:
 * @self: #PdApplication instance
 * @name: the name of the sink
 *
 * Remove a message sink (subscriber) from @self.
 */
void
pd_application_remove_sink (PdApplication *self,
                            const gchar   *name)
{
  PdApplicationPrivate *priv;

  g_return_if_fail (PD_IS_APPLICATION (self));

  priv = pd_application_get_instance_private (self);

  g_return_if_fail (priv->sink_table != NULL);

  if (g_hash_table_contains (priv->sink_table, name))
    {
      g_autoptr (PdSink) sink = NULL;
      sink = g_hash_table_lookup (priv->sink_table, name);
      pd_sink_stop (sink);
      g_hash_table_remove (priv->sink_table, name);
    }
}

/**
 * pd_application_get_sink:
 * @self: #PdApplication instance
 * @name: name of the sink
 *
 * Retrieve a #PdSink by name.
 *
 * Returns: (transfer none): a #PdSink if one is available with @name.
 */
PdSink *
pd_application_get_sink (PdApplication *self,
                         const gchar   *name)
{
  PdApplicationPrivate *priv;

  g_return_val_if_fail (PD_IS_APPLICATION (self), NULL);

  priv = pd_application_get_instance_private (self);

  g_return_val_if_fail (priv->sink_table != NULL, NULL);

  if (g_hash_table_contains (priv->sink_table, name))
    return g_hash_table_lookup (priv->sink_table, name);

  return NULL;
}

/**
 * pd_application_has_sink:
 * @self: #PdApplication instance
 * @name: name of the sink
 *
 * Check if the #PdSource exists by name.
 *
 * Returns: `True' if a sink with @name has been found, `False' otherwise.
 */
gboolean
pd_application_has_sink (PdApplication *self,
                         const gchar   *name)
{
  PdApplicationPrivate *priv;

  g_return_val_if_fail (PD_IS_APPLICATION (self), FALSE);

  priv = pd_application_get_instance_private (self);

  g_return_val_if_fail (priv->sink_table != NULL, FALSE);

  return g_hash_table_contains (priv->sink_table, name);
}

/**
 * pd_application_add_source:
 * @self: #PdApplication instance
 * @name: the name of the source
 * @source: and #PdSource
 *
 * Add a message source (publisher) to @self.
 */
void
pd_application_add_source (PdApplication *self,
                           const gchar   *name,
                           PdSource      *source)
{
  PdApplicationPrivate *priv;

  g_return_if_fail (PD_IS_APPLICATION (self));

  priv = pd_application_get_instance_private (self);

  if (priv->source_table == NULL)
    priv->source_table = g_hash_table_new_full (g_str_hash, g_str_equal, g_free, NULL);

  // TODO: review this, seems a little unnecessary
  if (g_hash_table_contains (priv->source_table, name))
    {
      g_autoptr (PdSource) s = NULL;
      s = g_hash_table_lookup (priv->source_table, name);
      pd_source_stop (s);
      g_hash_table_remove (priv->source_table, name);
    }

  g_hash_table_insert (priv->source_table, g_strdup (name), source);
}

/**
 * pd_application_remove_source:
 * @self: #PdApplication instance
 * @name: the name of the source
 *
 * Remove a message source (publisher) from @self.
 */
void
pd_application_remove_source (PdApplication *self,
                              const gchar   *name)
{
  PdApplicationPrivate *priv;

  g_return_if_fail (PD_IS_APPLICATION (self));

  priv = pd_application_get_instance_private (self);

  g_return_if_fail (priv->source_table != NULL);

  if (g_hash_table_contains (priv->source_table, name))
    {
      g_autoptr (PdSource) source = NULL;
      source = g_hash_table_lookup (priv->source_table, name);
      pd_source_stop (source);
      g_hash_table_remove (priv->source_table, name);
    }
}

/**
 * pd_application_get_source:
 * @self: #PdApplication instance
 * @name: name of the source
 *
 * Retrieve a #PdSource by name.
 *
 * Returns: (transfer none): a #PdSource if one is available with @name.
 */
PdSource *
pd_application_get_source (PdApplication *self,
                           const gchar   *name)
{
  PdApplicationPrivate *priv;

  g_return_val_if_fail (PD_IS_APPLICATION (self), NULL);

  priv = pd_application_get_instance_private (self);

  g_return_val_if_fail (priv->source_table != NULL, NULL);

  if (g_hash_table_contains (priv->source_table, name))
    return g_hash_table_lookup (priv->source_table, name);

  return NULL;
}

/**
 * pd_application_has_source:
 * @self: #PdApplication instance
 * @name: name of the source
 *
 * Check if the #PdSource exists by name.
 *
 * Returns: `True' if a source with @name has been found, `False' otherwise.
 */
gboolean
pd_application_has_source (PdApplication *self,
                           const gchar   *name)
{
  PdApplicationPrivate *priv;

  g_return_val_if_fail (PD_IS_APPLICATION (self), FALSE);

  priv = pd_application_get_instance_private (self);

  g_return_val_if_fail (priv->source_table != NULL, FALSE);

  return g_hash_table_contains (priv->source_table, name);
}

/**
 * pd_application_send_event:
 * @self: #PdApplication instance
 * @event: a #PdEvent
 * @error: return location for a GError, or NULL
 *
 * Send an event message, assumes an existing #PdSource named 'event'.
 */
void
pd_application_send_event (PdApplication  *self,
                           PdEvent        *event,
                           GError        **error)
{
  PdApplicationPrivate *priv;
  PdSource *source;
  g_autofree gchar *msg = NULL;

  g_return_if_fail (PD_IS_APPLICATION (self));
  g_return_if_fail (PD_IS_EVENT (event));
  g_return_if_fail (error == NULL || *error == NULL);

  priv = pd_application_get_instance_private (self);

  if (priv->source_table == NULL)
    {
      g_set_error (error,
                   PD_ERROR,
                   PD_ERROR_NOT_CONNECTED,
                   "The application does not contain any producer connections.");
      return;
    }
  else if (!pd_application_has_source (self, "event"))
    {
      g_set_error (error,
                   PD_ERROR,
                   PD_ERROR_NOT_CONNECTED,
                   "The application does not contain an event bus connection.");
      return;
    }

  source = g_hash_table_lookup (priv->source_table, "event");
  msg = pd_event_serialize (event);
  pd_source_queue_message (source, msg);
}

/**
 * pd_application_send_metric:
 * @self: #PdApplication instance
 * @metric: a #PdMetric
 * @error: return location for a GError, or NULL
 *
 * Send a metric message, assumes an existing #PdSource named 'metric'.
 */
void
pd_application_send_metric (PdApplication  *self,
                            PdMetric       *metric,
                            GError        **error)
{
  PdApplicationPrivate *priv;
  PdSource *source;
  g_autofree gchar *msg = NULL;

  g_return_if_fail (PD_IS_APPLICATION (self));
  g_return_if_fail (PD_IS_METRIC (metric));
  g_return_if_fail (error == NULL || *error == NULL);

  priv = pd_application_get_instance_private (self);

  if (priv->source_table == NULL)
    {
      g_set_error (error,
                   PD_ERROR,
                   PD_ERROR_NOT_CONNECTED,
                   "The application does not contain any producer connections.");
      return;
    }
  else if (!pd_application_has_source (self, "metric"))
    {
      g_set_error (error,
                   PD_ERROR,
                   PD_ERROR_NOT_CONNECTED,
                   "The application does not contain a metric bus connection.");
      return;
    }

  source = g_hash_table_lookup (priv->source_table, "metric");
  msg = pd_metric_serialize (metric);
  pd_source_queue_message (source, msg);
}

/**
 * pd_application_enqueue_job:
 * @self: #PdApplication instance
 * @job: a #PdJob
 *
 * Add a job to the internal job queue.
 */
void
pd_application_enqueue_job (PdApplication *self,
                            PdJob         *job)
{
  PdApplicationPrivate *priv;

  g_return_if_fail (PD_IS_APPLICATION (self));

  priv = pd_application_get_instance_private (self);

  pd_job_queue_enqueue (priv->job_queue, job);
}

/**
 * pd_application_register_job:
 * @self: #PdApplication instance
 * @job: a #PdJob
 *
 * Register a job with the application enabling it to be launched by name.
 * TODO: don't use this
 */
void
pd_application_register_job (PdApplication *self,
                             PdJob         *job)
{
  G_GNUC_UNUSED PdApplicationPrivate *priv;

  // TODO: why are you even putting this here already?
  g_return_if_fail (PD_IS_APPLICATION (self));

  //priv = pd_application_get_instance_private (self);

  //g_hash_table_insert (priv->registered_jobs, name, job);
}

/**
 * pd_application_get_id:
 * @self: #PdApplication instance
 *
 * Gets the application id of @self.
 *
 * Returns: (nullable): the application id, if one is set.
 */
const gchar *
pd_application_get_id (PdApplication *self)
{
  PdApplicationPrivate *priv;

  g_return_val_if_fail (PD_IS_APPLICATION (self), NULL);

  priv = pd_application_get_instance_private (self);
  return priv->id;
}

/**
 * pd_application_set_id:
 * @self: #PdApplication instance
 * @id: (nullable): the application id to use
 *
 * Sets (or unsets) the application id of @self.
 */
void
pd_application_set_id (PdApplication *self,
                       const gchar   *id)
{
  PdApplicationPrivate *priv;

  g_return_if_fail (PD_IS_APPLICATION (self));
  g_return_if_fail (id != NULL);

  priv = pd_application_get_instance_private (self);

  if (g_strcmp0 (id, priv->id) != 0)
    {
      g_free (priv->id);
      priv->id = g_strdup (id);
      g_object_notify_by_pspec (G_OBJECT (self), properties [PROP_ID]);
    }
}

/**
 * pd_application_get_endpoint:
 * @self: #PdApplication instance
 *
 * Gets the application endpoint of @self.
 *
 * Returns: (nullable): the application endpoint, if one is set.
 */
const gchar *
pd_application_get_endpoint (PdApplication *self)
{
  PdApplicationPrivate *priv;

  g_return_val_if_fail (PD_IS_APPLICATION (self), NULL);

  priv = pd_application_get_instance_private (self);
  return priv->endpoint;
}

/**
 * pd_application_set_endpoint:
 * @self: #PdApplication instance
 * @endpoint: (nullable): the broker endpoint to connect to
 *
 * Sets (or unsets) the endpoint of @self.
 */
void
pd_application_set_endpoint (PdApplication *self,
                             const gchar   *endpoint)
{
  PdApplicationPrivate *priv;

  g_return_if_fail (PD_IS_APPLICATION (self));
  g_return_if_fail (endpoint != NULL);

  priv = pd_application_get_instance_private (self);

  if (g_strcmp0 (endpoint, priv->endpoint) != 0)
    {
      g_free (priv->endpoint);
      priv->endpoint = g_strdup (endpoint);
      g_object_notify_by_pspec (G_OBJECT (self), properties [PROP_ENDPOINT]);
    }
}

/**
 * pd_application_get_service:
 * @self: #PdApplication instance
 *
 * Gets the application service of @self.
 *
 * Returns: (nullable): the application service, if one is set.
 */
const gchar *
pd_application_get_service (PdApplication *self)
{
  PdApplicationPrivate *priv;

  g_return_val_if_fail (PD_IS_APPLICATION (self), NULL);

  priv = pd_application_get_instance_private (self);
  return priv->service;
}

/**
 * pd_application_set_service:
 * @self: #PdApplication instance
 * @service: (nullable): the application service to use
 *
 * Sets (or unsets) the application service to @self to register with the broker.
 */
void
pd_application_set_service (PdApplication *self,
                            const gchar   *service)
{
  PdApplicationPrivate *priv;

  g_return_if_fail (PD_IS_APPLICATION (self));
  g_return_if_fail (service != NULL);

  priv = pd_application_get_instance_private (self);

  if (g_strcmp0 (service, priv->service) != 0)
    {
      g_free (priv->service);
      priv->service = g_strdup (service);
      g_object_notify_by_pspec (G_OBJECT (self), properties [PROP_SERVICE]);
    }
}
