/*
 * Copyright © the Plantd contributors. All rights reserved.
 *
 * This file is part of libplantd, distributed under the MIT license.
 * For full terms see the included LICENSE file.
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <json-glib/json-glib.h>

#include "pd-job.h"

/**
 * SECTION:plantd-job
 * @short_description: Job base class
 *
 * An #PdJob is a structure for the Job message type, and also as a runnable
 * object that provides a means for launching asynchronous jobs by an
 * #PdApplication.
 */

/**
 * PdJob:
 *
 * #PdJob is an opaque data structure and can only be accessed using the
 * following functions.
 */

/**
 * PdJobClass:
 * @task: invoked by a #PdApplication as a thread callback.
 *
 * Virtual function table for #PdJob.
 */

typedef struct
{
  GObject            parent;
  gchar             *id;
  // TODO: resolve this, should probably be an actual status type, but it's
  // currently an enum in the API
  /*PdStatus        *status;*/
  /*PdJobStatusType  status;*/
  gint               priority;
} PdJobPrivate;

enum {
  PROP_0,
  PROP_ID,
  /*PROP_STATUS,*/
  PROP_PRIORITY,
  N_PROPS
};

static GParamSpec *properties [N_PROPS];

G_DEFINE_TYPE_WITH_PRIVATE (PdJob, pd_job, G_TYPE_OBJECT)

static void
pd_job_finalize (GObject *object)
{
  PdJob *self = (PdJob *)object;
  PdJobPrivate *priv = pd_job_get_instance_private (self);

  g_clear_pointer (&priv->id, g_free);

  G_OBJECT_CLASS (pd_job_parent_class)->finalize (object);
}

static void
pd_job_get_property (GObject    *object,
                     guint       prop_id,
                     GValue     *value,
                     GParamSpec *pspec)
{
  PdJob *self = PD_JOB (object);

  switch (prop_id)
    {
    case PROP_ID:
      g_value_set_string (value, pd_job_get_id (self));
      break;

    /*
     *case PROP_STATUS:
     *  g_value_set_enum (value, pd_job_get_status (self));
     *  break;
     */

    case PROP_PRIORITY:
      g_value_set_int (value, pd_job_get_priority (self));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
pd_job_set_property (GObject      *object,
                     guint         prop_id,
                     const GValue *value,
                     GParamSpec   *pspec)
{
  PdJob *self = PD_JOB (object);

  switch (prop_id)
    {
    case PROP_ID:
      pd_job_set_id (self, g_value_get_string (value));
      break;

    /*
     *case PROP_STATUS:
     *  pd_job_set_status (self, g_value_get_boxed (value));
     *  break;
     */

    case PROP_PRIORITY:
      pd_job_set_priority(self, g_value_get_int (value));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
pd_job_class_init (PdJobClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->finalize = pd_job_finalize;
  object_class->get_property = pd_job_get_property;
  object_class->set_property = pd_job_set_property;

  properties [PROP_ID] =
    g_param_spec_string ("id",
                         "ID",
                         "The job ID.",
                         NULL,
                         (G_PARAM_READWRITE | G_PARAM_EXPLICIT_NOTIFY | G_PARAM_STATIC_STRINGS));

  properties [PROP_PRIORITY] =
    g_param_spec_int ("priority",
                      "Priority",
                      "The job priority.",
                      G_MININT,
                      G_MAXINT,
                      100,
                      (G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));

  g_object_class_install_properties (object_class, N_PROPS, properties);
}

static void
pd_job_init (PdJob *self)
{
  PdJobPrivate *priv;

  priv = pd_job_get_instance_private (self);

  priv->id = g_uuid_string_random ();
}

PdJob *
pd_job_new (void)
{
  return g_object_new (PD_TYPE_JOB, NULL);
}

gchar *
pd_job_serialize (PdJob *self)
{
  g_return_val_if_fail (PD_IS_JOB (self), NULL);

  return json_gobject_to_data (G_OBJECT (self), NULL);
}

void
pd_job_deserialize (PdJob       *self,
                    const gchar *data)
{
  GError *err = NULL;
  GObject *object = json_gobject_from_data (PD_TYPE_JOB,
                                            data,
                                            -1,
                                            &err);

  if (err != NULL)
    {
      g_critical ("%s", err->message);
      g_error_free (err);
    }

  g_return_if_fail (object != NULL);
  g_return_if_fail (PD_IS_JOB (object));

  pd_job_set_id (self, pd_job_get_id (PD_JOB (object)));
  pd_job_set_priority (self, pd_job_get_priority (PD_JOB (object)));

  g_object_unref (object);
}

/**
 * pd_job_task:
 * @self: a #PdJob
 */
void
pd_job_task (PdJob *self)
{
  PdJobClass *klass;

  g_return_if_fail (PD_IS_JOB (self));

  klass = PD_JOB_GET_CLASS (self);
  g_return_if_fail (klass->task != NULL);

  klass->task (self);
}

const gchar *
pd_job_get_id (PdJob *self)
{
  PdJobPrivate *priv;

  g_return_val_if_fail (PD_IS_JOB (self), NULL);

  priv = pd_job_get_instance_private (self);
  return priv->id;
}

void
pd_job_set_id (PdJob       *self,
               const gchar *id)
{
  PdJobPrivate *priv;

  g_return_if_fail (PD_IS_JOB (self));

  priv = pd_job_get_instance_private (self);

  if (g_strcmp0 (id, priv->id) != 0)
    {
      g_free (priv->id);
      priv->id = g_strdup (id);
      g_object_notify_by_pspec (G_OBJECT (self), properties [PROP_ID]);
    }
}

gint
pd_job_get_priority (PdJob *self)
{
  PdJobPrivate *priv;

  g_return_val_if_fail (PD_IS_JOB (self), -1);

  priv = pd_job_get_instance_private (self);

  return priv->priority;
}

void
pd_job_set_priority (PdJob *self,
                     gint   priority)
{
  PdJobPrivate *priv;

  g_return_if_fail (PD_IS_JOB (self));

  priv = pd_job_get_instance_private (self);

  priv->priority = priority;
}
