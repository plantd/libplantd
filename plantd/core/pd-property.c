/*
 * Copyright © the Plantd contributors. All rights reserved.
 *
 * This file is part of libplantd, distributed under the MIT license.
 * For full terms see the included LICENSE file.
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <json-glib/json-glib.h>

#include "pd-property.h"

struct _PdProperty
{
  GObject  parent;
  gchar   *key;
  gchar   *value;
};

G_DEFINE_TYPE (PdProperty, pd_property, G_TYPE_OBJECT)

enum {
  PROP_0,
  PROP_KEY,
  PROP_VALUE,
  N_PROPS
};

static GParamSpec *properties [N_PROPS];

static void
pd_property_finalize (GObject *object)
{
  PdProperty *self = (PdProperty *)object;

  g_clear_pointer (&self->key, g_free);
  g_clear_pointer (&self->value, g_free);

  G_OBJECT_CLASS (pd_property_parent_class)->finalize (object);
}

static void
pd_property_get_property (GObject    *object,
                          guint       prop_id,
                          GValue     *value,
                          GParamSpec *pspec)
{
  PdProperty *self = PD_PROPERTY (object);

  switch (prop_id)
  {
    case PROP_KEY:
      g_value_set_string (value, pd_property_get_key (self));
      break;

    case PROP_VALUE:
      g_value_set_string (value, pd_property_get_value (self));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
  }
}

static void
pd_property_set_property (GObject      *object,
                          guint         prop_id,
                          const GValue *value,
                          GParamSpec   *pspec)
{
  PdProperty *self = PD_PROPERTY (object);

  switch (prop_id)
  {
    case PROP_KEY:
      pd_property_set_key (self, g_value_get_string (value));
      break;

    case PROP_VALUE:
      pd_property_set_value (self, g_value_get_string (value));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
  }
}

static void
pd_property_class_init (PdPropertyClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->finalize = pd_property_finalize;
  object_class->get_property = pd_property_get_property;
  object_class->set_property = pd_property_set_property;

  properties [PROP_KEY] =
    g_param_spec_string ("key",
                         "Key",
                         "The property key.",
                         NULL,
                         (G_PARAM_READWRITE | G_PARAM_EXPLICIT_NOTIFY | G_PARAM_STATIC_STRINGS));

  properties [PROP_VALUE] =
    g_param_spec_string ("value",
                         "Value",
                         "The property value.",
                         NULL,
                         (G_PARAM_READWRITE | G_PARAM_EXPLICIT_NOTIFY | G_PARAM_STATIC_STRINGS));

  g_object_class_install_properties (object_class, N_PROPS, properties);
}

static void
pd_property_init (PdProperty *self)
{
}

PdProperty *
pd_property_new (const gchar *key,
                 const gchar *value)
{
  return g_object_new (PD_TYPE_PROPERTY,
                       "key", key,
                       "value", value,
                       NULL);
}

gchar *
pd_property_serialize (PdProperty *self)
{
  g_return_val_if_fail (PD_IS_PROPERTY (self), NULL);

  return json_gobject_to_data (G_OBJECT (self), NULL);
}

void
pd_property_deserialize (PdProperty  *self,
                         const gchar *data)
{
  GError *err = NULL;
  GObject *object = json_gobject_from_data (PD_TYPE_PROPERTY,
                                            data,
                                            -1,
                                            &err);

  if (err != NULL)
    {
      g_critical ("%s", err->message);
      g_error_free (err);
    }

  pd_property_set_key (self, pd_property_get_key (PD_PROPERTY (object)));
  pd_property_set_value (self, pd_property_get_value (PD_PROPERTY (object)));

  g_object_unref (object);
}

/**
 * pd_property_get_key:
 * @self: a #PdProperty
 *
 * Returns: (transfer none): The key, or %NULL
 */
const gchar *
pd_property_get_key (PdProperty *self)
{
  g_return_val_if_fail (PD_IS_PROPERTY (self), NULL);

  return self->key;
}

/**
 * pd_property_set_key:
 * @self: a #PdProperty
 * @key: (nullable): a string containing the key, or %NULL
 *
 * Sets the #PdProperty:key property.
 */
void
pd_property_set_key (PdProperty  *self,
                     const gchar *key)
{
  g_return_if_fail (PD_IS_PROPERTY (self));

  if (g_strcmp0 (key, self->key) != 0)
    {
      g_free (self->key);
      self->key = g_strdup (key);
      g_object_notify_by_pspec (G_OBJECT (self), properties [PROP_KEY]);
    }
}

/**
 * pd_property_get_value:
 * @self: a #PdProperty
 *
 * Returns: (transfer none): The value, or %NULL
 */
const gchar *
pd_property_get_value (PdProperty *self)
{
  g_return_val_if_fail (PD_IS_PROPERTY (self), NULL);

  return self->value;
}

/**
 * pd_property_set_value:
 * @self: a #PdProperty
 * @value: (nullable): a string containing the value, or %NULL
 *
 * Sets the #PdProperty:value property.
 */
void
pd_property_set_value (PdProperty  *self,
                       const gchar *value)
{
  g_return_if_fail (PD_IS_PROPERTY (self));

  if (g_strcmp0 (value, self->value) != 0)
    {
      g_free (self->value);
      self->value = g_strdup (value);
      g_object_notify_by_pspec (G_OBJECT (self), properties [PROP_VALUE]);
    }
}
