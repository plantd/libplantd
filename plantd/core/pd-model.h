/*
 * Copyright © the Plantd contributors. All rights reserved.
 *
 * This file is part of libplantd, distributed under the MIT license.
 * For full terms see the included LICENSE file.
 */

#pragma once

#if !defined (PLANTD_INSIDE) && !defined (PLANTD_COMPILATION)
# error "Only <plantd/plantd.h> can be included directly."
#endif

#include <glib-object.h>

#include <plantd/plantd-types.h>

G_BEGIN_DECLS

#define PD_TYPE_MODEL pd_model_get_type ()
G_DECLARE_DERIVABLE_TYPE (PdModel, pd_model, PD, MODEL, GObject)

struct _PdModelClass
{
  /*< private >*/
  GObjectClass parent_class;

  /*< public >*/
  /* signals */
  void (*property_changed) (PdModel   *self,
                            const gchar *key,
                            GValue      *value);

  /*< private >*/
  gpointer padding[12];
};

PdModel     *pd_model_new             (void);

gchar       *pd_model_serialize       (PdModel     *self);
void         pd_model_deserialize     (PdModel     *self,
                                       const gchar *data);

void         pd_model_add             (PdModel     *self,
                                       gchar       *key,
                                       GValue      *value);
void         pd_model_remove          (PdModel     *self,
                                       const gchar *key);

// XXX: add properties?
void         pd_model_add_property    (PdModel     *self,
                                       PdProperty  *property);
void         pd_model_remove_property (PdModel     *self,
                                       PdProperty  *property);

GValue       pd_model_get             (PdModel     *self,
                                       const gchar *key);
void         pd_model_set             (PdModel     *self,
                                       const gchar *key,
                                       GValue      *value);
gint         pd_model_get_int         (PdModel     *self,
                                       const gchar *key);
void         pd_model_set_int         (PdModel     *self,
                                       const gchar *key,
                                       gint        value);
gdouble      pd_model_get_double      (PdModel     *self,
                                       const gchar *key);
void         pd_model_set_double      (PdModel     *self,
                                       const gchar *key,
                                       gdouble      value);
const gchar *pd_model_get_string      (PdModel     *self,
                                       const gchar *key);
void         pd_model_set_string      (PdModel     *self,
                                       const gchar *key,
                                       const gchar *value);

G_END_DECLS
