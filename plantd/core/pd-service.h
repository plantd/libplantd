/*
 * Copyright © the Plantd contributors. All rights reserved.
 *
 * This file is part of libplantd, distributed under the MIT license.
 * For full terms see the included LICENSE file.
 */

#pragma once

#if !defined (PLANTD_INSIDE) && !defined (PLANTD_COMPILATION)
# error "Only <plantd/plantd.h> can be included directly."
#endif

#include <glib-object.h>

#include <plantd/plantd-types.h>

G_BEGIN_DECLS

#define PD_TYPE_SERVICE pd_service_get_type ()
G_DECLARE_DERIVABLE_TYPE (PdService, pd_service, PD, SERVICE, GObject)

struct _PdServiceClass
{
  /*< private >*/
  GObjectClass parent_class;

  /*< public >*/

  /*< private >*/
  gpointer padding[12];
};

PdService *pd_service_new            (const gchar *name);
PdService *pd_service_new_with_model (const gchar *name,
                                      PdModel     *model);

gchar     *pd_service_dup_name       (PdService   *self);
void       pd_service_set_name       (PdService   *self,
                                      const gchar *name);

PdStatus  *pd_service_ref_status     (PdService   *self);
void       pd_service_set_status     (PdService   *self,
                                      PdStatus    *status);

/*
 *PdState   *pd_service_ref_state      (PdService *self);
 *void       pd_service_set_state      (PdService *self,
 *                                      PdState   *state);
 */

G_END_DECLS
