/*
 * Copyright © the Plantd contributors. All rights reserved.
 *
 * This file is part of libplantd, distributed under the MIT license.
 * For full terms see the included LICENSE file.
 */

#pragma once

#if !defined (PLANTD_INSIDE) && !defined (PLANTD_COMPILATION)
# error "Only <plantd/plantd.h> can be included directly."
#endif

#include <glib-object.h>

#include <plantd/plantd-types.h>

G_BEGIN_DECLS

#define PD_TYPE_SYSTEM pd_system_get_type ()
G_DECLARE_FINAL_TYPE (PdSystem, pd_system, PD, SYSTEM, GObject)

PdSystem  *pd_system_new          (void);

gchar     *pd_system_serialize    (PdSystem    *self);
void       pd_system_deserialize  (PdSystem    *self,
                                   const gchar *data);

/*
 *PdService   *pd_system_get_master   (PdSystem  *self);
 *void         pd_system_set_master   (PdSystem  *self,
 *                                     PdService *master);
 *
 *PdService   *pd_system_get_broker   (PdSystem  *self);
 *void         pd_system_set_broker   (PdSystem  *self,
 *                                     PdService *broker);
 *
 *GHashTable  *pd_system_get_services (PdSystem   *self);
 *void         pd_system_set_services (PdSystem   *self,
 *                                     GHashTable *services);
 *
 *GHashTable  *pd_system_get_modules  (PdSystem   *self);
 *void         pd_system_set_modules  (PdSystem   *self,
 *                                     GHashTable *modules);
 */

G_END_DECLS
