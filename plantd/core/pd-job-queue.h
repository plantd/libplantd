/*
 * Copyright © the Plantd contributors. All rights reserved.
 *
 * This file is part of libplantd, distributed under the MIT license.
 * For full terms see the included LICENSE file.
 */

#pragma once

#if !defined (PLANTD_INSIDE) && !defined (PLANTD_COMPILATION)
# error "Only <plantd/plantd.h> can be included directly."
#endif

#include <glib-object.h>
#include <gio/gio.h>

#include <plantd/plantd-types.h>

G_BEGIN_DECLS

#define PD_TYPE_JOB_QUEUE pd_job_queue_get_type ()
G_DECLARE_FINAL_TYPE (PdJobQueue, pd_job_queue, PD, JOB_QUEUE, GObject)

PdJobQueue *pd_job_queue_new     (void);

void        pd_job_queue_enqueue (PdJobQueue  *self,
                                  PdJob       *job);
void        pd_job_queue_dequeue (PdJobQueue  *self,
                                  PdJob       *job);
void        pd_job_queue_run     (PdJobQueue  *self,
                                  GError     **error);
void        pd_job_queue_cancel  (PdJobQueue  *self);

G_END_DECLS
