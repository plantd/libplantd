/*
 * Copyright © the Plantd contributors. All rights reserved.
 *
 * This file is part of libplantd, distributed under the MIT license.
 * For full terms see the included LICENSE file.
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <json-glib/json-glib.h>

#include "pd-system.h"

struct _PdSystem
{
  GObject      parent;
  PdService *master;
  PdService *broker;
  GHashTable  *services;
  GHashTable  *modules;
};

enum {
  PROP_0,
  /*PROP_MASTER,*/
  /*PROP_BROKER,*/
  /*PROP_SERVICES,*/
  /*PROP_MODULES,*/
  N_PROPS
};

static G_GNUC_UNUSED GParamSpec *properties [N_PROPS];

static G_GNUC_UNUSED JsonSerializableIface *serializable_iface = NULL;

static void json_serializable_iface_init (gpointer g_iface);

G_DEFINE_TYPE_WITH_CODE (PdSystem, pd_system, G_TYPE_OBJECT,
                         G_IMPLEMENT_INTERFACE (JSON_TYPE_SERIALIZABLE,
                                                json_serializable_iface_init));

static JsonNode *
pd_system_serialize_property (JsonSerializable *serializable,
                              const gchar      *name,
                              const GValue     *value,
                              GParamSpec       *pspec)
{
  JsonNode *retval = NULL;

  if (g_strcmp0 (name, "services") == 0)
    {
    }
  else if (g_strcmp0 (name, "modules") == 0)
    {
    }
  else
    {
    }

  return retval;
}

static gboolean
pd_system_deserialize_property (JsonSerializable *serializable,
                                const gchar      *name,
                                GValue           *value,
                                GParamSpec       *pspec,
                                JsonNode         *property_node)
{
  gboolean retval = FALSE;

  if (g_strcmp0 (name, "services") == 0)
    {
    }
  else if (g_strcmp0 (name, "modules") == 0)
    {
    }

  return retval;
}

static void
json_serializable_iface_init (gpointer g_iface)
{
  JsonSerializableIface *iface = g_iface;

  iface->serialize_property = pd_system_serialize_property;
  iface->deserialize_property = pd_system_deserialize_property;
}

static void
pd_system_finalize (GObject *object)
{
  G_GNUC_UNUSED PdSystem *self = (PdSystem *)object;

  // TODO: free class data

  G_OBJECT_CLASS (pd_system_parent_class)->finalize (object);
}

static void
pd_system_get_property (GObject    *object,
                        guint       prop_id,
                        GValue     *value,
                        GParamSpec *pspec)
{
  G_GNUC_UNUSED PdSystem *self = PD_SYSTEM (object);

  switch (prop_id)
    {
    //case PROP_:
    //  g_value_set_ (value, pd_system_get_ (self));
    //  break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
pd_system_set_property (GObject      *object,
                        guint         prop_id,
                        const GValue *value,
                        GParamSpec   *pspec)
{
  G_GNUC_UNUSED PdSystem *self = PD_SYSTEM (object);

  switch (prop_id)
    {
    //case PROP_:
    //  pd_system_set_ (self, g_value_get_boolean (value));
    //  break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
pd_system_class_init (PdSystemClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->finalize = pd_system_finalize;
  object_class->get_property = pd_system_get_property;
  object_class->set_property = pd_system_set_property;

  // example
  //properties [PROP_BPROP] =
  //  g_param_spec_boolean ("prop",
  //                        "Prop",
  //                        "The prop...",
  //                        FALSE,
  //                        (G_PARAM_READWRITE | G_PARAM_EXPLICIT_NOTIFY));

  /*g_object_class_install_properties (object_class, N_PROPS, properties);*/
}

static void
pd_system_init (PdSystem *self)
{
}

PdSystem *
pd_system_new (void)
{
  PdSystem *object = g_object_new (PD_TYPE_SYSTEM, NULL);

  // TODO: add setup

  return object;
}

gchar *
pd_system_serialize (PdSystem *self)
{
  g_return_val_if_fail (PD_IS_SYSTEM (self), NULL);

  return json_gobject_to_data (G_OBJECT (self), NULL);
}

  void
pd_system_deserialize (PdSystem    *self,
                       const gchar *data)
{
  GError *err = NULL;
  GObject *object = json_gobject_from_data (PD_TYPE_SYSTEM,
                                            data,
                                            -1,
                                            &err);

  if (err != NULL)
  {
    g_critical ("%s", err->message);
    g_error_free (err);
  }

  g_return_if_fail (object != NULL);
  g_return_if_fail (PD_IS_SYSTEM (object));

  // TODO: add props
  // pd_system_set_...prop... (self, pd_system_get_...prop... (PD_SYSTEM (object)));

  g_object_unref (object);
}
