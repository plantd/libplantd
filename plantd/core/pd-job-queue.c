/*
 * Copyright © the Plantd contributors. All rights reserved.
 *
 * This file is part of libplantd, distributed under the MIT license.
 * For full terms see the included LICENSE file.
 */

#define G_LOG_DOMAIN "plantd-job-queue"

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include "pd-job-queue.h"
#include "pd-job.h"

struct _PdJobQueue
{
  GObject      parent;
  gboolean     running;
  GAsyncQueue *queue;
  GThreadPool *pool;
  //GHashTable  *table;
  gint         max_jobs;
  gchar       *active_job_id;
};

static void s_queue_async  (PdJobQueue  *self,
                            GError       **error);
static void s_queue_cancel (PdJobQueue *self,
                            gpointer      user_data);

G_DEFINE_TYPE (PdJobQueue, pd_job_queue, G_TYPE_OBJECT)

static void
pd_job_queue_finalize (GObject *object)
{
  PdJobQueue *self = (PdJobQueue *)object;

  g_async_queue_unref (self->queue);
  g_clear_pointer (&self->active_job_id, g_free);
  if (self->pool)
    g_thread_pool_free (self->pool, TRUE, FALSE);

  G_OBJECT_CLASS (pd_job_queue_parent_class)->finalize (object);
}

static void
pd_job_queue_class_init (PdJobQueueClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->finalize = pd_job_queue_finalize;
}

static void
pd_job_queue_init (PdJobQueue *self)
{
  /* Job queue initialization */
  self->running = FALSE;
  self->queue = g_async_queue_new ();
  self->max_jobs = 1;
}

PdJobQueue *
pd_job_queue_new (void)
{
  return g_object_new (PD_TYPE_JOB_QUEUE, NULL);
}

/**
 * pd_job_queue_run:
 * @self: a #PdJobQueue
 * @error: return location for a GError, or NULL
 *
 * Starts the thread that monitors the messages received from the parent
 * service.
 */
void
pd_job_queue_run (PdJobQueue  *self,
                  GError     **error)
{
  g_return_if_fail (PD_IS_JOB_QUEUE (self));

  s_queue_async (self, error);
}

/**
 * pd_job_queue_cancel:
 * @self: a #PdJobQueue
 *
 * Stops the thread that monitors the messages received from the parent
 * service.
 */
void
pd_job_queue_cancel (PdJobQueue *self)
{
  g_return_if_fail (PD_IS_JOB_QUEUE (self));

  s_queue_cancel (self, NULL);
}

/**
 * pd_job_queue_enqueue:
 * @self: a #PdJobQueue
 * @job: a #PdJob
 *
 * Add a job to the internal job queue.
 */
void
pd_job_queue_enqueue (PdJobQueue *self,
                      PdJob      *job)
{
  g_return_if_fail (PD_IS_JOB_QUEUE (self));

  g_async_queue_lock (self->queue);
  g_async_queue_push_unlocked (self->queue, job);
  g_async_queue_unlock (self->queue);
}

/**
 * pd_job_queue_dequeue:
 * @self: a #PdJobQueue
 * @job: a #PdJob
 *
 * Remove a job from the internal job queue.
 * TODO: don't use this
 */
void
pd_job_queue_dequeue (PdJobQueue *self,
                      PdJob      *job)
{
  g_return_if_fail (PD_IS_JOB_QUEUE (self));

  // TODO: implement this
}

static void
s_job_func (gpointer data,
            gpointer user_data)
{
  g_return_if_fail (PD_IS_JOB_QUEUE (user_data));
  g_return_if_fail (PD_IS_JOB (data));

  pd_job_task (PD_JOB (data));
}

static void
s_queue_cb (GObject      *source_object,
            GAsyncResult *result,
            gpointer      user_data)
{
  g_debug ("job monitor finished");
}

static void
s_queue_cancel (PdJobQueue *self,
                gpointer    data)
{
  GCancellable *cancellable;

  cancellable = G_CANCELLABLE (data);
  g_cancellable_cancel (cancellable);

  self->running = FALSE;

  g_debug ("job monitor cancelled");
}

static void
s_queue_thread (GTask        *task,
                gpointer      source_object,
                gpointer      task_data,
                GCancellable *cancellable)
{
  g_assert (source_object == g_task_get_source_object (task));
  g_assert (cancellable == g_task_get_cancellable (task));

  PD_JOB_QUEUE(source_object)->running = TRUE;

  while (PD_JOB_QUEUE(source_object)->running)
    {
      PdJob *job;

      if (g_cancellable_is_cancelled (cancellable))
        {
          g_task_return_new_error (task,
                                   G_IO_ERROR, G_IO_ERROR_CANCELLED,
                                   "Task cancelled");
          return;
        }

      g_async_queue_lock (PD_JOB_QUEUE(source_object)->queue);
      job = PD_JOB (g_async_queue_pop_unlocked (PD_JOB_QUEUE(source_object)->queue));
      g_async_queue_unlock (PD_JOB_QUEUE(source_object)->queue);

      g_debug ("Monitor received a new job: %s", pd_job_get_id (job));

      // TODO: add job to thread pool
      g_thread_pool_push (PD_JOB_QUEUE(source_object)->pool, job, NULL);
    }

  g_task_return_boolean (task, TRUE);
}

/**
 * s_queue_async:
 * @self: a #PdApplication
 *
 * Starts the thread that monitors the job queue.
 */
static void
s_queue_async (PdJobQueue  *self,
               GError     **error)
{
  g_autoptr (GTask) task = NULL;
  g_autoptr (GCancellable) cancellable = NULL;

  g_return_if_fail (PD_IS_JOB_QUEUE (self));

  self->pool = g_thread_pool_new (s_job_func, self, self->max_jobs, FALSE, NULL);

  g_return_if_fail (self->pool != NULL);

  g_thread_pool_set_max_unused_threads (1000);

  cancellable = g_cancellable_new ();
  task = g_task_new (self, cancellable, s_queue_cb, NULL);
  /*
   *if (self->max_jobs < 1)
   *  {
   *    g_task_return_new_error (task, JOB_QUEUE_ERROR,
   *                             JOB_QUEUE_ERROR_INVALID_JOB_QUEUE_SIZE,
   *                             "%d is not a valid job queue size",
   *                             self->max_jobs);
   *    g_object_unref (task);
   *    return;
   *  }
   */

  g_task_run_in_thread (task, s_queue_thread);
}
