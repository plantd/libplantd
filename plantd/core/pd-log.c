/*
 * Copyright © the Plantd contributors. All rights reserved.
 *
 * This file is part of libplantd, distributed under the MIT license.
 * For full terms see the included LICENSE file.
 */

#define G_LOG_DOMAIN "plantd-log"

#ifndef _GNU_SOURCE
# define _GNU_SOURCE
#endif

#ifdef __linux__
# include <sys/types.h>
# include <sys/syscall.h>
#endif

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <glib.h>
#include <string.h>
#include <unistd.h>

#include "plantd-debug.h"
#include "pd-log.h"
#include "pd-macros.h"

/**
 * SECTION:plantd-log
 * @title: Logging
 * @short_description: Logging facilities for Pd
 *
 * This provides logging facilities in Pd. It provides filtering
 * based on #GLogLevelFlags.
 */

typedef const gchar *(*PdLogLevelStrFunc) (GLogLevelFlags log_level);

static PdLogLevelStrFunc log_level_str_func;
static int log_verbosity;
static GPtrArray *channels;
static GLogFunc last_handler;
static gchar *domains;
static gboolean has_domains;
static gboolean json;

G_LOCK_DEFINE (channels_lock);

static inline gint
pd_log_get_thread (void)
{
#ifdef __linux__
  return (gint) syscall (SYS_gettid);
#else
  return GPOINTER_TO_INT (g_thread_self ());
#endif /* __linux__ */
}

static const gchar *
pd_log_level_str (GLogLevelFlags log_level)
{
  switch (((gulong)log_level & G_LOG_LEVEL_MASK))
    {
    case G_LOG_LEVEL_ERROR:    return "E";
    case G_LOG_LEVEL_CRITICAL: return "C";
    case G_LOG_LEVEL_WARNING:  return "W";
    case G_LOG_LEVEL_MESSAGE:  return "M";
    case G_LOG_LEVEL_INFO:     return "I";
    case G_LOG_LEVEL_DEBUG:    return "D";
    case PD_LOG_LEVEL_TRACE:   return "T";
    default:                   return "U";
    }
}

static const gchar *
pd_log_level_str_with_color (GLogLevelFlags log_level)
{
  switch (((gulong)log_level & G_LOG_LEVEL_MASK))
    {
    case G_LOG_LEVEL_ERROR:    return "\033[1;31mE\033[0m";
    case G_LOG_LEVEL_CRITICAL: return "\033[1;35mC\033[0m";
    case G_LOG_LEVEL_WARNING:  return "\033[1;33mW\033[0m";
    case G_LOG_LEVEL_MESSAGE:  return "\033[1;32mM\033[0m";
    case G_LOG_LEVEL_INFO:     return "\033[1;32mI\033[0m";
    case G_LOG_LEVEL_DEBUG:    return "\033[1;32mD\033[0m";
    case PD_LOG_LEVEL_TRACE:   return "\033[1;36mT\033[0m";
    default:                   return "\033[1;31mU\033[0m";
    }
}

static void
pd_log_write_to_channel (GIOChannel  *channel,
                         const gchar *message)
{
  g_io_channel_write_chars (channel, message, -1, NULL, NULL);
  g_io_channel_flush (channel, NULL);
}

static void
pd_log_handler (const gchar    *log_domain,
                GLogLevelFlags  log_level,
                const gchar    *message,
                gpointer        user_data)
{
  g_autoptr (GDateTime) dt = NULL;
  g_autofree gchar *ftime = NULL;

  if (G_LIKELY (channels->len))
    {
      const gchar *level;
      gchar *buf;

      if ((log_level == G_LOG_LEVEL_DEBUG || log_level == PD_LOG_LEVEL_TRACE) &&
          has_domains &&
          (log_domain == NULL || strstr (domains, log_domain) == NULL))
        return;

      switch ((int)log_level)
        {
        case G_LOG_LEVEL_MESSAGE:
          if (log_verbosity < 1)
            return;
          break;

        case G_LOG_LEVEL_INFO:
          if (log_verbosity < 2)
            return;
          break;

        case G_LOG_LEVEL_DEBUG:
          if (log_verbosity < 3)
            return;
          break;

        case PD_LOG_LEVEL_TRACE:
          if (log_verbosity < 4)
            return;
          break;

        default:
          break;
        }

      dt = g_date_time_new_now_local ();
      level = log_level_str_func (log_level);
      ftime = g_date_time_format (dt, "%H:%M:%S");

      if (!json)
        {
          buf = g_strdup_printf ("%s.%03d  %20s[% 5d]: %s: %s\n",
                                 ftime,
                                 g_date_time_get_microsecond (dt) / 1000,
                                 log_domain,
                                 pd_log_get_thread (),
                                 level,
                                 message);
        }
      else
        {
          /* TODO: json-ify message content */
          buf = g_strdup_printf ("{ \"timestamp\": \"%s.%03d\", \"domain\": \"%s\", " \
                                 "\"thread\": %d, \"level\": \"%s\", \"message\": \"%s\" }\n",
                                 ftime,
                                 g_date_time_get_microsecond (dt) / 1000,
                                 log_domain,
                                 pd_log_get_thread (),
                                 level,
                                 message);
        }

      G_LOCK (channels_lock);
      g_ptr_array_foreach (channels, (GFunc) pd_log_write_to_channel, buf);
      G_UNLOCK (channels_lock);
      g_free (buf);
    }
}

/**
 * pd_log_init:
 * @_stdout: Indicates logging should be written to stdout.
 * @filename: An optional file to store logs.
 *
 * Initialize logging.
 */
void
pd_log_init (gboolean     _stdout,
             const gchar *filename)
{
  static gsize initialized = FALSE;

  if (g_once_init_enter (&initialized))
    {
      GIOChannel *channel;
      log_level_str_func = pd_log_level_str;
      channels = g_ptr_array_new ();
      domains = g_strdup (g_getenv ("G_MESSAGES_DEBUG"));

      if (g_strcmp0 (g_getenv ("G_MESSAGES_FORMAT"), "json") == 0)
        json = TRUE;

      /* create channel to write to file if a name was given */
      if (filename)
        {
          channel = g_io_channel_new_file (filename, "a", NULL);
          g_ptr_array_add (channels, channel);
        }

      /* write to stdout */
      if (_stdout)
        {
          channel = g_io_channel_unix_new (STDOUT_FILENO);
          g_ptr_array_add (channels, channel);
          if ((filename == NULL) && isatty (STDOUT_FILENO))
            log_level_str_func = pd_log_level_str_with_color;
        }

      if (!pd_str_empty0 (domains) && strcmp (domains, "all") != 0)
        has_domains = TRUE;

      g_log_set_default_handler (pd_log_handler, NULL);
      g_once_init_leave (&initialized, TRUE);
    }
}

/**
 * pd_log_shutdown:
 *
 * Cleans up after the logging subsystem and restores the original
 * log handler.
 */
void
pd_log_shutdown (void)
{
  if (last_handler)
    {
      g_log_set_default_handler (last_handler, NULL);
      last_handler = NULL;
    }

  g_clear_pointer (&domains, g_free);
}

/**
 * pd_log_increase_verbosity:
 *
 * Increase the amount of logging output.
 *
 * Once for %G_LOG_LEVEL_MESSAGE
 * Twice for %G_LOG_LEVEL_INFO
 * Three times for %G_LOG_LEVEL_DEBUG
 * Four times for %PD_LOG_LEVEL_TRACE
 */
void
pd_log_increase_verbosity (void)
{
  log_verbosity++;
}

/**
 * pd_log_decrease_verbosity:
 *
 * Decrease the amount of logging output.
 */
void
pd_log_decrease_verbosity (void)
{
  log_verbosity--;
  log_verbosity = (log_verbosity < 0) ? 0 : log_verbosity;
}

/**
 * pd_log_enable_json:
 *
 * Enable JSON for logging output.
 */
void
pd_log_enable_json (void)
{
  json = TRUE;
}

/**
 * pd_log_disable_json:
 *
 * Disable JSON for logging output.
 */
void
pd_log_disable_json (void)
{
  json = FALSE;
}
