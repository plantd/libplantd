/*
 * Copyright © the Plantd contributors. All rights reserved.
 *
 * This file is part of libplantd, distributed under the MIT license.
 * For full terms see the included LICENSE file.
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <plantd-debug.h>

#include "pd-model.h"

/**
 * SECTION:plantd-model
 * @short_description: Data model base class
 *
 * An #PdModel is used as a base for containing property information.
 */

/**
 * PdModel:
 *
 * #PdModel is an opaque data structure and can only be accessed using the
 * following functions.
 */

/**
 * PdModelClass:
 * @property_changed: invoked after a stored property has been changed
 *
 * Virtual function table for #PdModel.
 */

// TODO: somehow need to maintain a data type table to allow mixed types

typedef struct
{
  /*< public >*/

  /*< private >*/
} PdModelPrivate;

enum {
  PROP_0,
  N_PROPS
};

G_GNUC_UNUSED static GParamSpec *properties [N_PROPS];

enum
{
  SIGNAL_PROPERTY_CHANGED,
  N_SIGNALS
};

static guint signals [N_SIGNALS];

G_DEFINE_TYPE_WITH_PRIVATE (PdModel, pd_model, G_TYPE_OBJECT)

static void
pd_model_finalize (GObject *object)
{
  PdModel *self = (PdModel *)object;
  G_GNUC_UNUSED PdModelPrivate *priv = pd_model_get_instance_private (self);

  // TODO: cleanup...

  G_OBJECT_CLASS (pd_model_parent_class)->finalize (object);
}

static void
pd_model_class_init (PdModelClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->finalize = pd_model_finalize;

  /**
   * PdModel::property_changed:
   * @self: the application
   *
   * The ::property_changed signal is emitted on the primary instance
   * after a stored property has been updated.
   */
  signals [SIGNAL_PROPERTY_CHANGED] =
    g_signal_new ("property-changed",
                  PD_TYPE_MODEL,
                  G_SIGNAL_RUN_LAST | G_SIGNAL_NO_RECURSE | G_SIGNAL_NO_HOOKS,
                  G_STRUCT_OFFSET (PdModelClass, property_changed),
                  NULL,
                  NULL,
                  NULL,
                  G_TYPE_NONE,
                  2,
                  G_TYPE_STRING,
                  G_TYPE_STRING);
}

static void
pd_model_init (PdModel *self)
{
  G_GNUC_UNUSED PdModelPrivate *priv;

  // priv = pd_model_get_instance_private (self);
}

/**
 * pd_model_new:
 *
 * Creates a new #PdModel instance.
 *
 * Returns: a new #PdModel instance
 */
PdModel *
pd_model_new (void)
{
  return g_object_new (PD_TYPE_MODEL, NULL);
}
