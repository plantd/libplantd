/*
 * Copyright © the Plantd contributors. All rights reserved.
 *
 * This file is part of libplantd, distributed under the MIT license.
 * For full terms see the included LICENSE file.
 */

#pragma once

#if !defined (PLANTD_INSIDE) && !defined (PLANTD_COMPILATION)
# error "Only <plantd/plantd.h> can be included directly."
#endif

#include <glib-object.h>

#include <plantd/plantd-types.h>

G_BEGIN_DECLS

#define PD_TYPE_OBJECT pd_object_get_type ()
G_DECLARE_FINAL_TYPE (PdObject, pd_object, PD, OBJECT, GObject)

PdObject    *pd_object_new             (const gchar *id);

void         pd_object_dump            (PdObject    *self);

gchar       *pd_object_serialize       (PdObject    *self);
void         pd_object_deserialize     (PdObject    *self,
                                        const gchar *data);

const gchar *pd_object_get_id          (PdObject    *self);
void         pd_object_set_id          (PdObject    *self,
                                        const gchar *id);

const gchar *pd_object_get_name        (PdObject    *self);
void         pd_object_set_name        (PdObject    *self,
                                        const gchar *name);

void         pd_object_add_object      (PdObject     *self,
                                        PdObject     *object);
void         pd_object_remove_object   (PdObject     *self,
                                        const gchar  *id);
PdObject    *pd_object_lookup_object   (PdObject     *self,
                                        const gchar  *id);
gboolean     pd_object_has_object      (PdObject     *self,
                                        const gchar  *id);

void         pd_object_add_property    (PdObject     *self,
                                        PdProperty   *property);
void         pd_object_remove_property (PdObject     *self,
                                        const gchar  *key);
PdProperty  *pd_object_lookup_property (PdObject     *self,
                                        const gchar  *key);
gboolean     pd_object_has_property    (PdObject     *self,
                                        const gchar  *key);

PdTable     *pd_object_get_objects     (PdObject     *self);
PdTable     *pd_object_ref_objects     (PdObject     *self);
void         pd_object_set_objects     (PdObject     *self,
                                        PdTable      *objects);

PdTable     *pd_object_get_properties  (PdObject     *self);
PdTable     *pd_object_ref_properties  (PdObject     *self);
void         pd_object_set_properties  (PdObject     *self,
                                        PdTable      *properties);

GObject     *pd_gobject_from_data      (const gchar *data);

G_END_DECLS
