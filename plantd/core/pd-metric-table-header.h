/*
 * Copyright © the Plantd contributors. All rights reserved.
 *
 * This file is part of libplantd, distributed under the MIT license.
 * For full terms see the included LICENSE file.
 */

#pragma once

#if !defined (PLANTD_INSIDE) && !defined (PLANTD_COMPILATION)
# error "Only <plantd/plantd.h> can be included directly."
#endif

#include <glib-object.h>

#include <plantd/plantd-types.h>

G_BEGIN_DECLS

#define PD_TYPE_METRIC_TABLE_HEADER pd_metric_table_header_get_type ()
G_DECLARE_FINAL_TYPE (PdMetricTableHeader, pd_metric_table_header, PD, METRIC_TABLE_HEADER, GObject)

PdMetricTableHeader *pd_metric_table_header_new           (const gchar         *name);

gchar               *pd_metric_table_header_serialize     (PdMetricTableHeader *self);
void                 pd_metric_table_header_deserialize   (PdMetricTableHeader *self,
                                                           const gchar         *data);

const gchar         *pd_metric_table_header_get_name      (PdMetricTableHeader *self);
gchar               *pd_metric_table_header_dup_name      (PdMetricTableHeader *self);
void                 pd_metric_table_header_set_name      (PdMetricTableHeader *self,
                                                           const gchar         *name);

PdTable             *pd_metric_table_header_get_columns   (PdMetricTableHeader *self);
PdTable             *pd_metric_table_header_ref_columns   (PdMetricTableHeader *self);
void                 pd_metric_table_header_set_columns   (PdMetricTableHeader *self,
                                                           PdTable             *columns);

gboolean             pd_metric_table_header_add_column    (PdMetricTableHeader *self,
                                                           const gchar         *key,
                                                           gdouble              value);
gboolean             pd_metric_table_header_remove_column (PdMetricTableHeader *self,
                                                           const gchar         *key);
gdouble              pd_metric_table_header_get_column    (PdMetricTableHeader *self,
                                                           const gchar         *key);
gboolean             pd_metric_table_header_has_column    (PdMetricTableHeader *self,
                                                           const gchar         *key);

G_END_DECLS
