/*
 * Copyright © the Plantd contributors. All rights reserved.
 *
 * This file is part of libplantd, distributed under the MIT license.
 * For full terms see the included LICENSE file.
 */

#define G_LOG_DOMAIN "plantd-global"

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include "plantd-debug.h"
#include "pd-global.h"
#include "pd-macros.h"
#include "pd-private.h"

const gchar *
pd_gettext (const gchar *message)
{
  if (message != NULL)
    return g_dgettext (GETTEXT_PACKAGE, message);
  return NULL;
}
