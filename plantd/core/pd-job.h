/*
 * Copyright © the Plantd contributors. All rights reserved.
 *
 * This file is part of libplantd, distributed under the MIT license.
 * For full terms see the included LICENSE file.
 */

#pragma once

#if !defined (PLANTD_INSIDE) && !defined (PLANTD_COMPILATION)
# error "Only <plantd/plantd.h> can be included directly."
#endif

#include <glib-object.h>

#include <plantd/plantd-types.h>

G_BEGIN_DECLS

#define PD_TYPE_JOB pd_job_get_type ()
G_DECLARE_DERIVABLE_TYPE (PdJob, pd_job, PD, JOB, GObject)

struct _PdJobClass
{
  /*< private >*/
  GObjectClass parent_class;

  /* vfuncs */

  void (*task) (PdJob *self);

  /*< private >*/
  gpointer padding[12];
};

PdJob       *pd_job_new          (void);

gchar       *pd_job_serialize    (PdJob       *self);
void         pd_job_deserialize  (PdJob       *self,
                                  const gchar *data);

void         pd_job_task         (PdJob       *self);

const gchar *pd_job_get_id       (PdJob       *self);
void         pd_job_set_id       (PdJob       *self,
                                  const gchar *id);

gint         pd_job_get_priority (PdJob       *self);
void         pd_job_set_priority (PdJob       *self,
                                  gint         priority);

G_END_DECLS
