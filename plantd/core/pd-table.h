/*
 * Copyright © the Plantd contributors. All rights reserved.
 *
 * This file is part of libplantd, distributed under the MIT license.
 * For full terms see the included LICENSE file.
 */

#pragma once

#if !defined (PLANTD_INSIDE) && !defined (PLANTD_COMPILATION)
# error "Only <plantd/plantd.h> can be included directly."
#endif

#include <glib-object.h>
#include <json-glib/json-glib.h>

G_BEGIN_DECLS

#define PD_TYPE_TABLE pd_table_get_type ()
G_DECLARE_DERIVABLE_TYPE (PdTable, pd_table, PD, TABLE, GObject)

struct _PdTableClass
{
  /*< private >*/
  GObjectClass parent_class;

  /*< private >*/
  gpointer padding[12];
};

PdTable  *pd_table_new         (GType         value_type);
void      pd_table_flush       (PdTable      *self);

JsonNode *pd_table_serialize   (PdTable      *self,
                                GType         json_type);
void      pd_table_deserialize (PdTable      *self,
                                JsonNode     *node);

void      pd_table_add         (PdTable      *self,
                                const gchar  *key,
                                const GValue *value);
void      pd_table_remove      (PdTable      *self,
                                const gchar  *key);
GValue   *pd_table_get         (PdTable      *self,
                                const gchar  *key);
gboolean  pd_table_has         (PdTable      *self,
                                const gchar  *key);
GList    *pd_table_get_keys    (PdTable      *self);

G_END_DECLS
