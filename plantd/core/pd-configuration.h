/*
 * Copyright © the Plantd contributors. All rights reserved.
 *
 * This file is part of libplantd, distributed under the MIT license.
 * For full terms see the included LICENSE file.
 */

#pragma once

#if !defined (PLANTD_INSIDE) && !defined (PLANTD_COMPILATION)
# error "Only <plantd/plantd.h> can be included directly."
#endif

#include <glib-object.h>

#include <plantd/plantd-types.h>

G_BEGIN_DECLS

#define PD_TYPE_CONFIGURATION pd_configuration_get_type ()
G_DECLARE_FINAL_TYPE (PdConfiguration, pd_configuration, PD, CONFIGURATION, GObject)

PdConfiguration          *pd_configuration_new             (void);

gchar                    *pd_configuration_serialize       (PdConfiguration           *self);
void                      pd_configuration_deserialize     (PdConfiguration           *self,
                                                            const gchar               *data);

void                      pd_configuration_load            (PdConfiguration           *self,
                                                            const gchar               *filename,
                                                            GError                   **error);
void                      pd_configuration_save            (PdConfiguration           *self,
                                                            const gchar               *filename,
                                                            GError                   **error);

const gchar              *pd_configuration_get_id          (PdConfiguration           *self);
void                      pd_configuration_set_id          (PdConfiguration           *self,
                                                            const gchar               *id);

PdConfigurationNamespace  pd_configuration_get_namespace   (PdConfiguration           *self);
void                      pd_configuration_set_namespace   (PdConfiguration           *self,
                                                            PdConfigurationNamespace   namespace);

void                      pd_configuration_add_object      (PdConfiguration           *self,
                                                            PdObject                  *object);
void                      pd_configuration_remove_object   (PdConfiguration           *self,
                                                            const gchar               *id);

PdObject                 *pd_configuration_lookup_object   (PdConfiguration           *self,
                                                            const gchar               *id);
gboolean                  pd_configuration_has_object      (PdConfiguration           *self,
                                                            const gchar               *id);

void                      pd_configuration_add_property    (PdConfiguration           *self,
                                                            PdProperty                *property);
void                      pd_configuration_remove_property (PdConfiguration           *self,
                                                            const gchar               *key);
PdProperty               *pd_configuration_lookup_property (PdConfiguration           *self,
                                                            const gchar               *key);
gboolean                  pd_configuration_has_property    (PdConfiguration           *self,
                                                            const gchar               *key);

GHashTable               *pd_configuration_get_properties  (PdConfiguration           *self);
PdTable                  *pd_configuration_ref_properties  (PdConfiguration           *self);
void                      pd_configuration_set_properties  (PdConfiguration           *self,
                                                            PdTable                   *properties);

GHashTable               *pd_configuration_get_objects     (PdConfiguration           *self);
PdTable                  *pd_configuration_ref_objects     (PdConfiguration           *self);
void                      pd_configuration_set_objects     (PdConfiguration           *self,
                                                            PdTable                   *objects);

G_END_DECLS
