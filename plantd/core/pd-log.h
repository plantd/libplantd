/*
 * Copyright © the Plantd contributors. All rights reserved.
 *
 * This file is part of libplantd, distributed under the MIT license.
 * For full terms see the included LICENSE file.
 */

#pragma once

#if !defined (PLANTD_INSIDE) && !defined (PLANTD_COMPILATION)
# error "Only <plantd/plantd.h> can be included directly."
#endif

#include <glib.h>

G_BEGIN_DECLS

void pd_log_init               (gboolean     _stdout,
                                const gchar *filename);
void pd_log_shutdown           (void);
void pd_log_increase_verbosity (void);
void pd_log_decrease_verbosity (void);
void pd_log_enable_json        (void);
void pd_log_disable_json       (void);

G_END_DECLS
