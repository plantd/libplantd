/*
 * Copyright © the Plantd contributors. All rights reserved.
 *
 * This file is part of libdcs, distributed under the MIT license.
 * For full terms see the included LICENSE file.
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <json-glib/json-glib.h>

#include "pd-status.h"

/*
 *message Status {
 *  bool enabled = 1;
 *  bool loaded = 2;
 *  bool active = 3;
 *  map<string, string> details = 4;
 *}
 */

struct _PdStatus
{
  GObject     parent;

  gboolean    enabled;
  gboolean    loaded;
  gboolean    active;
  GHashTable *details;
};

enum {
  PROP_0,
  PROP_ENABLED,
  PROP_LOADED,
  PROP_ACTIVE,
  PROP_DETAILS,
  N_PROPS
};

static GParamSpec *properties [N_PROPS];

static void json_serializable_iface_init (gpointer g_iface);

G_DEFINE_TYPE_WITH_CODE (PdStatus, pd_status, G_TYPE_OBJECT,
                         G_IMPLEMENT_INTERFACE (JSON_TYPE_SERIALIZABLE,
                                                json_serializable_iface_init));

static JsonNode *
pd_status_serialize_property (JsonSerializable *serializable,
                              const gchar      *name,
                              const GValue     *value,
                              GParamSpec       *pspec)
{
  JsonNode *retval = NULL;

  if (g_strcmp0 (name, "details") == 0)
    {
      GHashTable *details = NULL;
      GHashTableIter iter;
      JsonObject *obj = NULL;
      gpointer key, val;

      retval = json_node_new (JSON_NODE_OBJECT);

      g_return_val_if_fail (value != NULL, retval);
      g_return_val_if_fail (G_VALUE_HOLDS_POINTER (value), retval);

      details = g_value_get_pointer (value);

      g_return_val_if_fail (details != NULL, retval);

      obj = json_object_new ();

      if (details != NULL)
        {
          g_hash_table_iter_init (&iter, details);
          while(g_hash_table_iter_next (&iter, &key, &val))
            {
              json_object_set_string_member (obj,
                                             (const gchar *) key,
                                             (const gchar *) val);
            }
        }

      json_node_take_object (retval, obj);
    }
  else
    {
      GValue copy = { 0, };

      retval = json_node_new (JSON_NODE_VALUE);

      g_value_init (&copy, G_PARAM_SPEC_VALUE_TYPE (pspec));
      g_value_copy (value, &copy);
      json_node_set_value (retval, &copy);
      g_value_unset (&copy);
    }

  return retval;
}

static gboolean
pd_status_deserialize_property (JsonSerializable *serializable,
                                const gchar      *name,
                                GValue           *value,
                                GParamSpec       *pspec,
                                JsonNode         *property_node)
{
  gboolean retval = FALSE;

  if (g_strcmp0 (name, "details") == 0)
    {
      g_autoptr (GHashTable) details = NULL;
      g_autoptr (JsonObject) obj = NULL;
      JsonObjectIter iter;
      const gchar *key;
      JsonNode *val;

      obj = json_node_get_object (property_node);
      details = g_hash_table_new_full (g_str_hash,
                                       g_str_equal,
                                       g_free,
                                       g_free);

      json_object_ref (obj);

      json_object_iter_init (&iter, obj);
      while (json_object_iter_next (&iter, &key, &val))
        {
          g_hash_table_insert (details,
                               g_strdup (key),
                               g_strdup (json_node_get_string (val)));
        }

      g_value_set_pointer (value, g_hash_table_ref (details));

      retval = TRUE;
    }

  return retval;
}

static void
json_serializable_iface_init (gpointer g_iface)
{
  JsonSerializableIface *iface = g_iface;

  iface->serialize_property = pd_status_serialize_property;
  iface->deserialize_property = pd_status_deserialize_property;
}

static void
pd_status_finalize (GObject *object)
{
  PdStatus *self = (PdStatus *)object;

  g_clear_pointer (&self->details, g_hash_table_unref);

  G_OBJECT_CLASS (pd_status_parent_class)->finalize (object);
}

static void
pd_status_get_property (GObject    *object,
                        guint       prop_id,
                        GValue     *value,
                        GParamSpec *pspec)
{
  PdStatus *self = PD_STATUS (object);

  switch (prop_id)
    {
    case PROP_ENABLED:
      g_value_set_boolean (value, pd_status_get_enabled (self));
      break;

    case PROP_LOADED:
      g_value_set_boolean (value, pd_status_get_loaded (self));
      break;

    case PROP_ACTIVE:
      g_value_set_boolean (value, pd_status_get_active (self));
      break;

    case PROP_DETAILS:
      /*g_value_set_object (value, pd_status_get_details (self));*/
      g_value_set_pointer (value, self->details);
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
pd_status_set_property (GObject      *object,
                        guint         prop_id,
                        const GValue *value,
                        GParamSpec   *pspec)
{
  PdStatus *self = PD_STATUS (object);

  switch (prop_id)
    {
    case PROP_ENABLED:
      pd_status_set_enabled (self, g_value_get_boolean (value));
      break;

    case PROP_LOADED:
      pd_status_set_loaded (self, g_value_get_boolean (value));
      break;

    case PROP_ACTIVE:
      pd_status_set_active (self, g_value_get_boolean (value));
      break;

    case PROP_DETAILS:
      /*pd_status_set_details (self, g_value_get_boxed (value));*/
      /* TODO: Free GHashTable memory with g_hash_table_free */
      pd_status_set_details (self, g_value_get_pointer (value));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
pd_status_class_init (PdStatusClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->finalize = pd_status_finalize;
  object_class->get_property = pd_status_get_property;
  object_class->set_property = pd_status_set_property;

  properties [PROP_ENABLED] =
    g_param_spec_boolean ("enabled",
                          "Enabled",
                          "",
                          FALSE,
                          (G_PARAM_READWRITE | G_PARAM_EXPLICIT_NOTIFY));

  properties [PROP_LOADED] =
    g_param_spec_boolean ("loaded",
                          "Loaded",
                          "",
                          FALSE,
                          (G_PARAM_READWRITE | G_PARAM_EXPLICIT_NOTIFY));

  properties [PROP_ACTIVE] =
    g_param_spec_boolean ("active",
                          "Active",
                          "",
                          FALSE,
                          (G_PARAM_READWRITE | G_PARAM_EXPLICIT_NOTIFY));

  properties [PROP_DETAILS] =
    g_param_spec_pointer ("details",
                          "Details",
                          "",
                          G_PARAM_READWRITE | G_PARAM_EXPLICIT_NOTIFY | G_PARAM_STATIC_STRINGS);

  g_object_class_install_properties (object_class, N_PROPS, properties);
}

static void
pd_status_init (PdStatus *self)
{
}

PdStatus *
pd_status_new (void)
{
  return g_object_new (PD_TYPE_STATUS, NULL);
}

gchar *
pd_status_serialize (PdStatus *self)
{
  g_return_val_if_fail (PD_IS_STATUS (self), NULL);

  return json_gobject_to_data (G_OBJECT (self), NULL);
}

void
pd_status_deserialize (PdStatus    *self,
                       const gchar *data)
{
  g_autoptr (GObject) object = NULL;
  g_autoptr (GHashTable) details = NULL;

  GError *err = NULL;
  object = json_gobject_from_data (PD_TYPE_STATUS,
                                   data,
                                   -1,
                                   &err);

  if (err != NULL)
    {
      g_critical ("%s", err->message);
      g_error_free (err);
    }

  g_return_if_fail (object != NULL);
  g_return_if_fail (PD_IS_STATUS (object));

  details = pd_status_get_details (PD_STATUS (object));

  pd_status_set_enabled (self, pd_status_get_enabled (PD_STATUS (object)));
  pd_status_set_loaded (self, pd_status_get_loaded (PD_STATUS (object)));
  pd_status_set_active (self, pd_status_get_active (PD_STATUS (object)));
  pd_status_set_details (self, details);

  g_clear_object (&object);
}

gboolean
pd_status_get_enabled (PdStatus *self)
{
  g_return_val_if_fail (PD_IS_STATUS (self), FALSE);

  return self->enabled;
}

void
pd_status_set_enabled (PdStatus *self,
                       gboolean  enabled)
{
  g_return_if_fail (PD_IS_STATUS (self));

  self->enabled = enabled;
}

gboolean
pd_status_get_loaded (PdStatus *self)
{
  g_return_val_if_fail (PD_IS_STATUS (self), FALSE);

  return self->loaded;
}

void
pd_status_set_loaded (PdStatus *self,
                      gboolean  loaded)
{
  g_return_if_fail (PD_IS_STATUS (self));

  self->loaded = loaded;
}

gboolean
pd_status_get_active (PdStatus *self)
{
  g_return_val_if_fail (PD_IS_STATUS (self), FALSE);

  return self->active;
}

void
pd_status_set_active (PdStatus *self,
                      gboolean  active)
{
  g_return_if_fail (PD_IS_STATUS (self));

  self->active = active;
}

GHashTable *
pd_status_get_details (PdStatus *self)
{
  GHashTable *details;

  g_return_val_if_fail (PD_IS_STATUS (self), NULL);

  g_object_get (self, "details", &details, NULL);

  return details;
}

void
pd_status_set_details (PdStatus   *self,
                       GHashTable *details)
{
  g_return_if_fail (PD_IS_STATUS (self));

  if (self->details == details)
    return;

  if (details)
    g_hash_table_ref (details);

  if (self->details)
    g_hash_table_unref (self->details);

  self->details = details;

  g_object_notify_by_pspec (G_OBJECT (self), properties [PROP_DETAILS]);
}

void
pd_status_add_detail (PdStatus    *self,
                      const gchar *key,
                      const gchar *value)
{
  g_return_if_fail (PD_IS_STATUS (self));

  if (self->details == NULL)
    self->details = g_hash_table_new_full (g_str_hash, g_str_equal, g_free, g_free);

  if (g_hash_table_contains (self->details, key))
    g_hash_table_replace (self->details, g_strdup (key), g_strdup (value));
  else
    g_hash_table_insert (self->details, g_strdup (key), g_strdup (value));
}

void
pd_status_remove_detail (PdStatus    *self,
                         const gchar *key)
{
  g_return_if_fail (PD_IS_STATUS (self));
  g_return_if_fail (self->details != NULL);

  if (g_hash_table_contains (self->details, key))
    g_hash_table_remove (self->details, key);
}

const gchar *
pd_status_get_detail (PdStatus    *self,
                      const gchar *key)
{
  g_return_val_if_fail (PD_IS_STATUS (self), NULL);
  g_return_val_if_fail (self->details != NULL, NULL);

  if (g_hash_table_contains (self->details, key))
    return g_hash_table_lookup (self->details, key);

  return NULL;
}
