/*
 * Copyright © the Plantd contributors. All rights reserved.
 *
 * This file is part of libplantd, distributed under the MIT license.
 * For full terms see the included LICENSE file.
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <json-glib/json-glib.h>

#include "pd-service.h"

struct _PdService
{
  GObject          parent;
  gchar           *name;
  gchar           *description;
  gchar           *configuration_id;
  PdStatus      *status;
  PdState       *state;
};

enum {
  PROP_0,
  PROP_NAME,
  PROP_DESCRIPTION,
  PROP_CONFIGURATION_ID,
  /*PROP_STATUS,*/
  /*PROP_STATE,*/
  N_PROPS
};

static GParamSpec *properties [N_PROPS];

G_DEFINE_TYPE (PdService, pd_service, G_TYPE_OBJECT)

static void
pd_service_finalize (GObject *object)
{
  PdService *self = (PdService *)object;

  g_clear_pointer (&self->name, g_free);
  g_clear_pointer (&self->description, g_free);
  g_clear_pointer (&self->configuration_id, g_free);

  G_OBJECT_CLASS (pd_service_parent_class)->finalize (object);
}

static void
pd_service_get_property (GObject    *object,
                         guint       prop_id,
                         GValue     *value,
                         GParamSpec *pspec)
{
  PdService *self = PD_SERVICE (object);

  switch (prop_id)
    {
    case PROP_NAME:
      g_value_set_string (value, pd_service_get_name (self));
      break;

    case PROP_DESCRIPTION:
      g_value_set_string (value, pd_service_get_description (self));
      break;

    case PROP_CONFIGURATION_ID:
      g_value_set_string (value, pd_service_get_configuration_id (self));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
pd_service_set_property (GObject      *object,
                         guint         prop_id,
                         const GValue *value,
                         GParamSpec   *pspec)
{
  PdService *self = PD_SERVICE (object);

  switch (prop_id)
    {
    case PROP_NAME:
      pd_service_set_name (self, g_value_get_string (value));
      break;

    case PROP_DESCRIPTION:
      pd_service_set_description (self, g_value_get_string (value));
      break;

    case PROP_CONFIGURATION_ID:
      pd_service_set_configuration_id (self, g_value_get_string (value));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
pd_service_class_init (PdServiceClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->finalize = pd_service_finalize;
  object_class->get_property = pd_service_get_property;
  object_class->set_property = pd_service_set_property;

  properties [PROP_NAME] =
    g_param_spec_string ("name",
                         "Name",
                         "The name of the service.",
                         NULL,
                         (G_PARAM_READWRITE | G_PARAM_EXPLICIT_NOTIFY | G_PARAM_STATIC_STRINGS));

  properties [PROP_DESCRIPTION] =
    g_param_spec_string ("description",
                         "Description",
                         "The service description.",
                         NULL,
                         (G_PARAM_READWRITE | G_PARAM_EXPLICIT_NOTIFY | G_PARAM_STATIC_STRINGS));

  properties [PROP_CONFIGURATION_ID] =
    g_param_spec_string ("configuration-id",
                         "Configuration ID",
                         "The service configuration ID.",
                         NULL,
                         (G_PARAM_READWRITE | G_PARAM_EXPLICIT_NOTIFY | G_PARAM_STATIC_STRINGS));

  g_object_class_install_properties (object_class, N_PROPS, properties);
}

static void
pd_service_init (PdService *self)
{
}

PdService *
pd_service_new (void)
{
  PdService *object = g_object_new (PD_TYPE_SERVICE, NULL);

  // TODO: add setup

  return object;
}

gchar *
pd_service_serialize (PdService *self)
{
  g_return_val_if_fail (PD_IS_SERVICE (self), NULL);

  return json_gobject_to_data (G_OBJECT (self), NULL);
}

void
pd_service_deserialize (PdService   *self,
                        const gchar *data)
{
  GError *err = NULL;
  GObject *object = json_gobject_from_data (PD_TYPE_SERVICE,
                                            data,
                                            -1,
                                            &err);

  if (err != NULL)
  {
    g_critical ("%s", err->message);
    g_error_free (err);
  }

  g_return_if_fail (object != NULL);
  g_return_if_fail (PD_IS_SERVICE (object));

  pd_service_set_name (self, pd_service_get_name (PD_SERVICE (object)));
  pd_service_set_description (self, pd_service_get_description (PD_SERVICE (object)));
  pd_service_set_configuration_id (self, pd_service_get_configuration_id (PD_SERVICE (object)));
  // TODO: add boxed types

  g_object_unref (object);
}

const gchar *
pd_service_get_name (PdService *self)
{
  g_return_val_if_fail (PD_IS_SERVICE (self), NULL);

  return self->name;
}

void
pd_service_set_name (PdService   *self,
                     const gchar *name)
{
  g_return_if_fail (PD_IS_SERVICE (self));

  if (g_strcmp0 (name, self->name) != 0)
    {
      g_free (self->name);
      self->name = g_strdup (name);
      g_object_notify_by_pspec (G_OBJECT (self), properties [PROP_NAME]);
    }
}

const gchar *
pd_service_get_description (PdService *self)
{
  g_return_val_if_fail (PD_IS_SERVICE (self), NULL);

  return self->description;
}

void
pd_service_set_description (PdService   *self,
                            const gchar *description)
{
  g_return_if_fail (PD_IS_SERVICE (self));

  if (g_strcmp0 (description, self->description) != 0)
    {
      g_free (self->description);
      self->description = g_strdup (description);
      g_object_notify_by_pspec (G_OBJECT (self), properties [PROP_DESCRIPTION]);
    }
}

/**
 * pd_service_get_configuration_id:
 * @self: a #PdService
 *
 * Gets the application configuration_id of @self.
 *
 * Returns: (nullable): the application configuration_id, if one is set.
 */
const gchar *
pd_service_get_configuration_id (PdService *self)
{
  g_return_val_if_fail (PD_IS_SERVICE (self), NULL);

  return self->configuration_id;
}

/**
 * pd_service_set_configuration_id:
 * @self: a #PdService
 * @configuration_id: (nullable): the configuration ID the service uses
 *
 * Sets (or unsets) the configuration ID of @self.
 */
void
pd_service_set_configuration_id (PdService   *self,
                                 const gchar *configuration_id)
{
  g_return_if_fail (PD_IS_SERVICE (self));

  if (g_strcmp0 (configuration_id, self->configuration_id) != 0)
    {
      g_free (self->configuration_id);
      self->configuration_id = g_strdup (configuration_id);
      g_object_notify_by_pspec (G_OBJECT (self), properties [PROP_CONFIGURATION_ID]);
    }
}

// XXX: see PdModule for correct way to handle

/**
 * pd_service_get_status:
 * @self: a #PdService
 *
 * Gets the application status of @self.
 *
 * Returns: (transfer none): (nullable): a #PdStatus, if one is set.
 */
PdStatus *
pd_service_get_status (PdService *self)
{
  g_return_val_if_fail (PD_IS_SERVICE (self), NULL);

  return self->status;
}

/**
 * pd_service_set_status:
 * @self: a #PdService
 * @status: (nullable): the module status as a #PdStatus
 *
 * Sets (or unsets) the status of @self.
 */
void
pd_service_set_status (PdService *self,
                       PdStatus  *status)
{
  g_return_if_fail (PD_IS_SERVICE (self));

  self->status = status;
}

/**
 * pd_service_get_state:
 * @self: a #PdService
 *
 * Gets the application state of @self.
 *
 * Returns: (transfer none): (nullable): a #PdState, if one is set.
 */
PdState *
pd_service_get_state (PdService *self)
{
  g_return_val_if_fail (PD_IS_SERVICE (self), NULL);

  return self->state;
}

/**
 * pd_service_set_state:
 * @self: a #PdService
 * @state: (nullable): the service state as a #PdState
 *
 * Sets (or unsets) the state of @self.
 */
void
pd_service_set_state (PdService *self,
                      PdState   *state)
{
  g_return_if_fail (PD_IS_SERVICE (self));

  self->state = state;
}
