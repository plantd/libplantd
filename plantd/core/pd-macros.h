/*
 * Copyright © the Plantd contributors. All rights reserved.
 *
 * This file is part of libplantd, distributed under the MIT license.
 * For full terms see the included LICENSE file.
 */

#pragma once

#if !defined (PLANTD_INSIDE) && !defined (PLANTD_COMPILATION)
# error "Only <plantd/plantd.h> can be included directly."
#endif

#ifndef __GI_SCANNER__

#include <glib.h>

#include "pd-global.h"
#include "pd-object.h"

G_BEGIN_DECLS

#define pd_str_empty0(str)       (!(str) || !*(str))
#define pd_str_equal(str1,str2)  (strcmp(str1,str2)==0)
#define pd_str_equal0(str1,str2) (g_strcmp0(str1,str2)==0)
#define pd_strv_empty0(strv)     (((strv) == NULL) || ((strv)[0] == NULL))
#define pd_set_string(ptr,str)   (pd_take_string((ptr), g_strdup(str)))

static inline void
_g_object_unref0 (gpointer instance)
{
  if (instance)
    g_object_unref (instance);
}

static inline gboolean
pd_take_string (gchar **ptr,
                gchar  *str)
{
  if (*ptr != str)
    {
      g_free (*ptr);
      *ptr = str;
      return TRUE;
    }

  return FALSE;
}

static inline void
pd_clear_string (gchar **ptr)
{
  g_free (*ptr);
  *ptr = NULL;
}

G_END_DECLS

#endif /* __GI_SCANNER__ */
