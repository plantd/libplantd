/*
 * Copyright © the Plantd contributors. All rights reserved.
 *
 * This file is part of libplantd, distributed under the MIT license.
 * For full terms see the included LICENSE file.
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <json-glib/json-glib.h>

#include "pd-event.h"

struct _PdEvent
{
  GObject  parent;
  gint     id;
  gchar   *name;
  gchar   *description;
};

enum {
  PROP_0,
  PROP_ID,
  PROP_NAME,
  PROP_DESCRIPTION,
  N_PROPS
};

static GParamSpec *properties [N_PROPS];

G_DEFINE_TYPE (PdEvent, pd_event, G_TYPE_OBJECT)

static void
pd_event_finalize (GObject *object)
{
  PdEvent *self = (PdEvent *)object;

  g_clear_pointer (&self->name, g_free);
  g_clear_pointer (&self->description, g_free);

  G_OBJECT_CLASS (pd_event_parent_class)->finalize (object);
}

static void
pd_event_get_property (GObject    *object,
                       guint       prop_id,
                       GValue     *value,
                       GParamSpec *pspec)
{
  PdEvent *self = PD_EVENT (object);

  switch (prop_id)
    {
    case PROP_ID:
      g_value_set_int (value, pd_event_get_id (self));
      break;

    case PROP_NAME:
      g_value_take_string (value, pd_event_dup_name (self));
      break;

    case PROP_DESCRIPTION:
      g_value_take_string (value, pd_event_dup_description (self));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
pd_event_set_property (GObject      *object,
                       guint         prop_id,
                       const GValue *value,
                       GParamSpec   *pspec)
{
  PdEvent *self = PD_EVENT (object);

  switch (prop_id)
    {
    case PROP_ID:
      pd_event_set_id (self, g_value_get_int (value));
      break;

    case PROP_NAME:
      pd_event_set_name (self, g_value_get_string (value));
      break;

    case PROP_DESCRIPTION:
      pd_event_set_description (self, g_value_get_string (value));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
pd_event_class_init (PdEventClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->finalize = pd_event_finalize;
  object_class->get_property = pd_event_get_property;
  object_class->set_property = pd_event_set_property;

  properties [PROP_ID] =
    g_param_spec_int ("id",
                      "ID",
                      "The ID of the event.",
                      G_MININT,
                      G_MAXINT,
                      0,
                      (G_PARAM_READWRITE | G_PARAM_EXPLICIT_NOTIFY | G_PARAM_STATIC_STRINGS));

  properties [PROP_NAME] =
    g_param_spec_string ("name",
                         "Name",
                         "The name of the event.",
                         NULL,
                         (G_PARAM_READWRITE | G_PARAM_EXPLICIT_NOTIFY | G_PARAM_STATIC_STRINGS));

  properties [PROP_DESCRIPTION] =
    g_param_spec_string ("description",
                         "Description",
                         "The event description",
                         NULL,
                         (G_PARAM_READWRITE | G_PARAM_EXPLICIT_NOTIFY | G_PARAM_STATIC_STRINGS));

  g_object_class_install_properties (object_class, N_PROPS, properties);
}

static void
pd_event_init (PdEvent *self)
{
}

PdEvent *
pd_event_new (void)
{
  return g_object_new (PD_TYPE_EVENT, NULL);
}

PdEvent *
pd_event_new_full (gint         id,
                   const gchar *name,
                   const gchar *description)
{
  return g_object_new (PD_TYPE_EVENT,
                       "id", id,
                       "name", name,
                       "description", description,
                       NULL);
}

gchar *
pd_event_serialize (PdEvent *self)
{
  g_return_val_if_fail (PD_IS_EVENT (self), NULL);

  return json_gobject_to_data (G_OBJECT (self), NULL);
}

void
pd_event_deserialize (PdEvent     *self,
                      const gchar *data)
{
  g_autoptr (GObject) object = NULL;
  g_autofree gchar *name = NULL;
  g_autofree gchar *description = NULL;

  GError *err = NULL;
  object = json_gobject_from_data (PD_TYPE_EVENT,
                                   data,
                                   -1,
                                   &err);

  if (err != NULL)
    {
      g_critical ("%s", err->message);
      g_error_free (err);
    }

  g_return_if_fail (object != NULL);
  g_return_if_fail (PD_IS_EVENT (object));

  name = pd_event_dup_name (PD_EVENT (object));
  description = pd_event_dup_description (PD_EVENT (object));

  pd_event_set_id (self, pd_event_get_id (PD_EVENT (object)));
  pd_event_set_name (self, name);
  pd_event_set_description (self, description);

  g_clear_object (&object);
}

/**
 * pd_event_to_data:
 *
 * Simple event object serialization.
 *
 * @self: a #PdEvent
 *
 * Returns: (nullable): Serialized data representing the object.
 */
gchar *
pd_event_to_data (PdEvent *self)
{
  gchar *ret;
  const gchar *delimiter = "@@";

  g_return_val_if_fail (PD_IS_EVENT (self), NULL);

  /*pd_object_lock (PD_OBJECT (self));*/
  ret = g_strdup_printf ("%d%s%s%s%s",
                         self->id, delimiter, self->name,
                         delimiter, self->description);
  /*pd_object_unlock (PD_OBJECT (self));*/

  return g_steal_pointer (&ret);
}

/**
 * pd_event_from_data:
 *
 * Simple event object deserialization.
 *
 * @data: Character data to deserialize into a #PdEvent
 *
 * Returns: (transfer full) (nullable): A new #PdEvent
 */
PdEvent *
pd_event_from_data (const gchar *data)
{
  PdEvent *ret = NULL;
  const gchar *delimiter = "@@";
  gint id;
  g_autofree gchar *name;
  g_autofree gchar *description;
  gchar **tokens;

  tokens = g_strsplit (data, delimiter, -1);

  g_return_val_if_fail (g_strv_length (tokens) > 0, NULL);

  if (g_strv_length (tokens) < 2)
    name = g_strdup ("");

  if (g_strv_length (tokens) < 3)
    description = g_strdup ("");

  id = (gint) g_ascii_strtoll (tokens[0], NULL, 0);
  name = g_strdup (tokens[1]);
  description = g_strdup (tokens[2]);
  g_strfreev (tokens);

  ret = pd_event_new_full (id, name, description);

  return g_steal_pointer (&ret);
}

gint
pd_event_get_id (PdEvent *self)
{
  // TODO: define a no-event event for this case
  g_return_val_if_fail (PD_IS_EVENT (self), -1);

  return self->id;
}

void
pd_event_set_id (PdEvent *self,
                 gint     id)
{
  g_return_if_fail (PD_IS_EVENT (self));

  self->id = id;
}

const gchar *
pd_event_get_name (PdEvent *self)
{
  g_return_val_if_fail (PD_IS_EVENT (self), NULL);

  return self->name;
}

/**
 * pd_event_dup_name:
 *
 * Copies the name of the event and returns it to the caller (after
 * locking the object). A copy is used to avoid thread-races.
 */
gchar *
pd_event_dup_name (PdEvent *self)
{
  gchar *ret;

  g_return_val_if_fail (PD_IS_EVENT (self), NULL);

  /*pd_object_lock (PD_OBJECT (self));*/
  ret = g_strdup (self->name);
  /*pd_object_unlock (PD_OBJECT (self));*/

  return g_steal_pointer (&ret);
}

void
pd_event_set_name (PdEvent     *self,
                   const gchar *name)
{
  g_return_if_fail (PD_IS_EVENT (self));

  if (g_strcmp0 (name, self->name) != 0)
    {
      g_free (self->name);
      self->name = g_strdup (name);
      g_object_notify_by_pspec (G_OBJECT (self), properties [PROP_NAME]);
    }
}

const gchar *
pd_event_get_description (PdEvent *self)
{
  g_return_val_if_fail (PD_IS_EVENT (self), NULL);

  return self->description;
}

/**
 * pd_event_dup_description:
 *
 * Copies the description of the event and returns it to the caller (after
 * locking the object). A copy is used to avoid thread-races.
 */
gchar *
pd_event_dup_description (PdEvent *self)
{
  gchar *ret;

  g_return_val_if_fail (PD_IS_EVENT (self), NULL);

  /*pd_object_lock (PD_OBJECT (self));*/
  ret = g_strdup (self->description);
  /*pd_object_unlock (PD_OBJECT (self));*/

  return g_steal_pointer (&ret);
}

void
pd_event_set_description (PdEvent     *self,
                          const gchar *description)
{
  g_return_if_fail (PD_IS_EVENT (self));

  if (g_strcmp0 (description, self->description) != 0)
    {
      g_free (self->description);
      self->description = g_strdup (description);
      g_object_notify_by_pspec (G_OBJECT (self), properties [PROP_DESCRIPTION]);
    }
}
