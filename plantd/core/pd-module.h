/*
 * Copyright © the Plantd contributors. All rights reserved.
 *
 * This file is part of libplantd, distributed under the MIT license.
 * For full terms see the included LICENSE file.
 */

#pragma once

#if !defined (PLANTD_INSIDE) && !defined (PLANTD_COMPILATION)
# error "Only <plantd/plantd.h> can be included directly."
#endif

#include <glib.h>

#include <plantd/plantd-types.h>

/*
 *message Module {
 *  string id = 1;
 *  string name = 2;
 *  string service_name = 3;
 *  string configuration_id = 4;
 *  Status status = 5;
 *  State state = 6;
 *  repeated string services = 7;
 *}
 */

G_BEGIN_DECLS

#define PD_TYPE_MODULE pd_module_get_type ()
G_DECLARE_FINAL_TYPE (PdModule, pd_module, PD, MODULE, GObject)

PdModule    *pd_module_new                  (void);

const gchar *pd_module_get_id               (PdModule    *self);
void         pd_module_set_id               (PdModule    *self,
                                             const gchar *id);

const gchar *pd_module_get_name             (PdModule    *self);
void         pd_module_set_name             (PdModule    *self,
                                             const gchar *name);

const gchar *pd_module_get_service_name     (PdModule    *self);
void         pd_module_set_service_name     (PdModule    *self,
                                             const gchar *service_name);

const gchar *pd_module_get_configuration_id (PdModule    *self);
void         pd_module_set_configuration_id (PdModule    *self,
                                             const gchar *configuration_id);

PdStatus    *pd_module_get_status           (PdModule    *self);
void         pd_module_set_status           (PdModule    *self,
                                             PdStatus    *status);

PdState     *pd_module_get_state            (PdModule    *self);
void         pd_module_set_state            (PdModule    *self,
                                             PdState     *state);

/*
 *GPtrArray   *pd_module_get_services         (PdModule  *self);
 *void         pd_module_set_services         (PdModule  *self,
 *                                               GPtrArray    services);
 */

G_END_DECLS
