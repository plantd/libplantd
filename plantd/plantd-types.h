/*
 * Copyright © the Plantd contributors. All rights reserved.
 *
 * This file is part of libplantd, distributed under the MIT license.
 * For full terms see the included LICENSE file.
 */

#pragma once

#if !defined (PLANTD_INSIDE) && !defined (PLANTD_COMPILATION)
# error "Only <plantd/plantd.h> can be included directly."
#endif

#include <plantd/plantd-enums.h>

G_BEGIN_DECLS

typedef struct _PdApplication             PdApplication;
typedef struct _PdChannel                 PdChannel;
typedef struct _PdConfiguration           PdConfiguration;
typedef struct _PdEvent                   PdEvent;
typedef struct _PdHandler                 PdHandler;
typedef struct _PdHub                     PdHub;
typedef struct _PdJob                     PdJob;
typedef struct _PdJobQueue                PdJobQueue;
typedef struct _PdMdpHandler              PdMdpHandler;
typedef struct _PdMetric                  PdMetric;
typedef struct _PdMetricTable             PdMetricTable;
typedef struct _PdMetricTableHeader       PdMetricTableHeader;
typedef struct _PdModel                   PdModel;
typedef struct _PdModule                  PdModule;
typedef struct _PdObject                  PdObject;
typedef struct _PdProperty                PdProperty;
typedef struct _PdRunner                  PdRunner;
typedef struct _PdService                 PdService;
typedef struct _PdSink                    PdSink;
typedef struct _PdSource                  PdSource;
typedef struct _PdStatus                  PdStatus;
typedef struct _PdSystem                  PdSystem;
typedef struct _PdTable                   PdTable;

typedef struct _PdState                   PdState;
typedef struct _PdStateMachine            PdStateMachine;
typedef struct _PdTransition              PdTransition;
typedef struct _PdTransitionTable         PdTransitionTable;

/**
 * Pd:
 *
 * Represents a ...
 */
typedef struct _PdBroker                  PdBroker;
typedef struct _PdClient                  PdClient;
typedef struct _PdWorker                  PdWorker;

typedef struct _PdChannelRequest          PdChannelRequest;
typedef struct _PdChannelResponse         PdChannelResponse;
typedef struct _PdChannelsResponse        PdChannelsResponse;
typedef struct _PdConfigurationRequest    PdConfigurationRequest;
typedef struct _PdConfigurationResponse   PdConfigurationResponse;
typedef struct _PdConfigurationsResponse  PdConfigurationsResponse;
typedef struct _PdEmpty                   PdEmpty;
typedef struct _PdEventRequest            PdEventRequest;
typedef struct _PdEventResponse           PdEventResponse;
typedef struct _PdJobRequest              PdJobRequest;
typedef struct _PdJobResponse             PdJobResponse;
typedef struct _PdJobStatusResponse       PdJobStatusResponse;
typedef struct _PdJobsResponse            PdJobsResponse;
typedef struct _PdMessageError            PdMessageError;
typedef struct _PdPropertyRequest         PdPropertyRequest;
typedef struct _PdPropertyResponse        PdPropertyResponse;
typedef struct _PdPropertiesRequest       PdPropertiesRequest;
typedef struct _PdPropertiesResponse      PdPropertiesResponse;
typedef struct _PdRequest                 PdRequest;
typedef struct _PdResponse                PdResponse;
typedef struct _PdSettingsRequest         PdSettingsRequest;
typedef struct _PdSettingsResponse        PdSettingsResponse;
typedef struct _PdStatusRequest           PdStatusRequest;
typedef struct _PdStatusResponse          PdStatusResponse;

/**
 * PdJobCallback:
 * @job: a #PdJob
 * @cancellable: optinal #GCancellable object, %NULL to ignore.
 * @user_data: user data passed into the callback.
 *
 * Long-running jobs should occasionally check the @cancellable to see if they
 * have been cancelled.
 */
/*
 *typedef void (*PdJobFunc) (PdJob        *job,
 *                           GCancellable *cancellable,
 *                           gpointer      user_data);
 */

G_END_DECLS
