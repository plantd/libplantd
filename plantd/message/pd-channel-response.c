/*
 * Copyright © the Plantd contributors. All rights reserved.
 *
 * This file is part of libplantd, distributed under the MIT license.
 * For full terms see the included LICENSE file.
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <json-glib/json-glib.h>

#include "pd-response.h"
#include "pd-channel-response.h"
#include "core/pd-channel.h"

/*
 * PdChannelResponse:
 *
 * Represents a response message with a single channel.
 */
struct _PdChannelResponse
{
  PdResponse  parent;
  PdChannel  *channel;
};

enum {
  PROP_0,
  PROP_CHANNEL,
  N_PROPS
};

static GParamSpec *properties [N_PROPS];

G_DEFINE_TYPE (PdChannelResponse, pd_channel_response, PD_TYPE_RESPONSE)

static void
pd_channel_response_finalize (GObject *object)
{
  PdChannelResponse *self = (PdChannelResponse *)object;

  g_clear_object (&self->channel);

  G_OBJECT_CLASS (pd_channel_response_parent_class)->finalize (object);
}

static void
pd_channel_response_get_property (GObject    *object,
                                  guint       prop_id,
                                  GValue     *value,
                                  GParamSpec *pspec)
{
  PdChannelResponse *self = PD_CHANNEL_RESPONSE (object);

  switch (prop_id)
  {
    case PROP_CHANNEL:
      /*g_value_set_object (value, pd_channel_response_get_channel (self));*/
      g_value_set_object (value, self->channel);
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
  }
}

static void
pd_channel_response_set_property (GObject      *object,
                                  guint         prop_id,
                                  const GValue *value,
                                  GParamSpec   *pspec)
{
  PdChannelResponse *self = PD_CHANNEL_RESPONSE (object);

  switch (prop_id)
  {
    case PROP_CHANNEL:
      pd_channel_response_set_channel (self, g_value_get_object (value));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
  }
}

static void
pd_channel_response_class_init (PdChannelResponseClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->finalize = pd_channel_response_finalize;
  object_class->get_property = pd_channel_response_get_property;
  object_class->set_property = pd_channel_response_set_property;

  properties [PROP_CHANNEL] =
    g_param_spec_object ("channel",
                         "Channel",
                         "The channel to response with.",
                         PD_TYPE_CHANNEL,
                         (G_PARAM_READWRITE | G_PARAM_EXPLICIT_NOTIFY));

  g_object_class_install_properties (object_class, N_PROPS, properties);
}

static void
pd_channel_response_init (PdChannelResponse *self)
{
  self->channel = pd_channel_new ();
}

PdChannelResponse *
pd_channel_response_new (void)
{
  return g_object_new (PD_TYPE_CHANNEL_RESPONSE, NULL);
}

/**
 * pd_channel_response_serialize:
 * @self: a #PdChannelResponse
 *
 * Returns the serialized message data.
 *
 * Returns: (transfer full): serialized data that the receiver must free.
 */
gchar *
pd_channel_response_serialize (PdChannelResponse *self)
{
  gchar *ret;

  g_return_val_if_fail (PD_IS_CHANNEL_RESPONSE (self), NULL);

  ret = json_gobject_to_data (G_OBJECT (self), NULL);

  return g_steal_pointer (&ret);
}

void
pd_channel_response_deserialize (PdChannelResponse *self,
                                 const gchar       *data)
{
  GError *err = NULL;
  GObject *object = json_gobject_from_data (PD_TYPE_CHANNEL_RESPONSE,
                                            data,
                                            -1,
                                            &err);

  if (err != NULL)
  {
    g_critical ("%s", err->message);
    g_error_free (err);
  }

  g_return_if_fail (object != NULL);
  g_return_if_fail (PD_IS_CHANNEL_RESPONSE (object));

  pd_channel_response_set_channel (self,
      pd_channel_response_get_channel (PD_CHANNEL_RESPONSE (object)));

  g_object_unref (object);
}

/**
 * pd_channel_response_get_channel:
 * @self: #PdChannelResponse instance
 *
 * Returns: (transfer full): The #PdChannel
 */
PdChannel *
pd_channel_response_get_channel (PdChannelResponse *self)
{
  PdChannel *channel;

  g_return_val_if_fail (PD_IS_CHANNEL_RESPONSE (self), NULL);

  g_object_get (self, "channel", &channel, NULL);

  return channel;
}

void
pd_channel_response_set_channel (PdChannelResponse *self,
                                 PdChannel         *channel)
{
  g_return_if_fail (PD_IS_CHANNEL_RESPONSE (self));
  g_return_if_fail (PD_IS_CHANNEL (channel));

  if (self->channel)
    g_object_unref (self->channel);

  if (channel)
    g_object_ref (channel);

  self->channel = channel;

  g_object_notify_by_pspec (G_OBJECT (self), properties [PROP_CHANNEL]);
}
