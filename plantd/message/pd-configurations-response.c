/*
 * Copyright © the Plantd contributors. All rights reserved.
 *
 * This file is part of libplantd, distributed under the MIT license.
 * For full terms see the included LICENSE file.
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <json-glib/json-glib.h>

#include "pd-response.h"
#include "pd-configurations-response.h"
#include "core/pd-configuration.h"

/*
 * PdConfigurationsResponse:
 *
 * Represents a response message for a list of configurations.
 */
struct _PdConfigurationsResponse
{
  PdResponse parent;
  GPtrArray *configurations;
};

enum {
  PROP_0,
  PROP_CONFIGURATIONS,
  N_PROPS
};

static GParamSpec *properties [N_PROPS];

static void json_serializable_iface_init (gpointer g_iface);

G_DEFINE_TYPE_WITH_CODE (PdConfigurationsResponse, pd_configurations_response, PD_TYPE_RESPONSE,
                         G_IMPLEMENT_INTERFACE (JSON_TYPE_SERIALIZABLE,
                                                json_serializable_iface_init));

static JsonNode *
pd_configurations_response_serialize_property (JsonSerializable *serializable,
                                               const gchar      *name,
                                               const GValue     *value,
                                               GParamSpec       *pspec)
{
  JsonNode *retval = NULL;

  if (g_strcmp0 (name, "configurations") == 0)
    {
      g_autoptr (GPtrArray) configurations = NULL;
      JsonArray *arr = NULL;

      retval = json_node_new (JSON_NODE_ARRAY);
      arr = json_array_new ();

      configurations = g_value_get_boxed (value);

      for (int i = 0; i < configurations->len; i++)
        {
          JsonNode *node = NULL;
          JsonObject *obj = NULL;

          node = json_gobject_serialize (configurations->pdata[i]);

          if (JSON_NODE_HOLDS_OBJECT (node))
            {
              obj = json_node_get_object (node);
              json_array_add_object_element (arr, obj);
            }
        }

      json_node_take_array (retval, arr);
    }
  else
    {
      GValue copy = { 0, };

      retval = json_node_new (JSON_NODE_VALUE);

      g_value_init (&copy, G_PARAM_SPEC_VALUE_TYPE (pspec));
      g_value_copy (value, &copy);
      json_node_set_value (retval, &copy);
      g_value_unset (&copy);
    }

  return retval;
}

static gboolean
pd_configurations_response_deserialize_property (JsonSerializable *serializable,
                                                 const gchar      *name,
                                                 GValue           *value,
                                                 GParamSpec       *pspec,
                                                 JsonNode         *property_node)
{
  gboolean retval = FALSE;
  JsonArray *arr = NULL;

  if (g_strcmp0 (name, "configurations") == 0)
    {
      g_autoptr (GPtrArray) configurations = NULL;
      arr = json_node_get_array (property_node);
      configurations = g_ptr_array_new ();

      for (int i = 0; i < json_array_get_length (arr); i++)
        {
          g_autoptr (PdConfiguration) configuration = NULL;
          JsonNode *node = NULL;

          node = json_array_get_element (arr, i);

          configuration =
            PD_CONFIGURATION (json_gobject_deserialize (
                  PD_TYPE_CONFIGURATION,
                  node));

          g_return_val_if_fail (PD_IS_CONFIGURATION (configuration), FALSE);
          g_ptr_array_add (configurations, configuration);
        }

      g_value_set_boxed (value, configurations);

      retval = TRUE;
    }

  return retval;
}

static void
json_serializable_iface_init (gpointer g_iface)
{
  JsonSerializableIface *iface = g_iface;

  iface->serialize_property = pd_configurations_response_serialize_property;
  iface->deserialize_property = pd_configurations_response_deserialize_property;
}

static void
pd_configurations_response_finalize (GObject *object)
{
  PdConfigurationsResponse *self = (PdConfigurationsResponse *)object;

  g_ptr_array_unref (self->configurations);

  G_OBJECT_CLASS (pd_configurations_response_parent_class)->finalize (object);
}

static void
pd_configurations_response_get_property (GObject    *object,
                                         guint       prop_id,
                                         GValue     *value,
                                         GParamSpec *pspec)
{
  PdConfigurationsResponse *self = PD_CONFIGURATIONS_RESPONSE (object);

  switch (prop_id)
  {
    case PROP_CONFIGURATIONS:
      g_value_set_boxed (value, pd_configurations_response_get_configurations (self));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
  }
}

static void
pd_configurations_response_set_property (GObject      *object,
                                         guint         prop_id,
                                         const GValue *value,
                                         GParamSpec   *pspec)
{
  PdConfigurationsResponse *self = PD_CONFIGURATIONS_RESPONSE (object);

  switch (prop_id)
  {
    case PROP_CONFIGURATIONS:
      pd_configurations_response_set_configurations (self, g_value_get_boxed (value));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
  }
}

static void
pd_configurations_response_class_init (PdConfigurationsResponseClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->finalize = pd_configurations_response_finalize;
  object_class->get_property = pd_configurations_response_get_property;
  object_class->set_property = pd_configurations_response_set_property;

  properties [PROP_CONFIGURATIONS] =
    g_param_spec_boxed ("configurations",
                        "Configurations",
                        "The list of configurations to provide in the response.",
                        G_TYPE_PTR_ARRAY,
                        (G_PARAM_READWRITE | G_PARAM_EXPLICIT_NOTIFY));

  g_object_class_install_properties (object_class, N_PROPS, properties);
}

static void
pd_configurations_response_init (PdConfigurationsResponse *self)
{
}

PdConfigurationsResponse *
pd_configurations_response_new (void)
{
  // TODO: move into init?
  g_autoptr (GPtrArray) configurations = NULL;

  configurations = g_ptr_array_new ();

  return g_object_new (PD_TYPE_CONFIGURATIONS_RESPONSE,
                       "configurations", configurations,
                       NULL);
}

/**
 * pd_configurations_response_serialize:
 * @self: a #PdConfigurationsResponse
 *
 * Returns the serialized message data.
 *
 * Returns: (transfer full): serialized data that the receiver must free.
 */
gchar *
pd_configurations_response_serialize (PdConfigurationsResponse *self)
{
  gchar *ret;

  g_return_val_if_fail (PD_IS_CONFIGURATIONS_RESPONSE (self), NULL);

  ret = json_gobject_to_data (G_OBJECT (self), NULL);

  return g_steal_pointer (&ret);
}

void
pd_configurations_response_deserialize (PdConfigurationsResponse *self,
                                        const gchar              *data)
{
  GError *err = NULL;
  GObject *object = json_gobject_from_data (PD_TYPE_CONFIGURATIONS_RESPONSE,
                                            data,
                                            -1,
                                            &err);

  if (err != NULL)
  {
    g_critical ("%s", err->message);
    g_error_free (err);
  }

  g_return_if_fail (object != NULL);
  g_return_if_fail (PD_IS_CONFIGURATIONS_RESPONSE (object));

  pd_configurations_response_set_configurations (self,
      pd_configurations_response_get_configurations (
        PD_CONFIGURATIONS_RESPONSE (object)));

  g_object_unref (object);
}

void
pd_configurations_response_add_configuration (PdConfigurationsResponse *self,
                                              PdConfiguration          *configuration)
{
  g_return_if_fail (PD_IS_CONFIGURATIONS_RESPONSE (self));
  g_return_if_fail (PD_IS_CONFIGURATION (configuration));

  g_ptr_array_add (self->configurations, configuration);
}

void
pd_configurations_response_remove_configuration (PdConfigurationsResponse *self,
                                                 PdConfiguration          *configuration)
{
  g_return_if_fail (PD_IS_CONFIGURATIONS_RESPONSE (self));
  g_return_if_fail (PD_IS_CONFIGURATION (configuration));

  for (int i = 0; i < self->configurations->len; i++)
    {
      // TODO: add ID to configuration and compare here, endpoint is wrong
      if (g_strcmp0 (pd_configuration_get_id (configuration),
                     pd_configuration_get_id (self->configurations->pdata[i])) == 0)
        self->configurations = g_ptr_array_remove_index (self->configurations, i);
    }
}

/**
 * pd_configurations_response_get_configurations:
 * @self: a #PdConfigurationsResponse
 *
 * Returns: (element-type Pd.Configuration) (transfer full): an array of
 *          #PdConfiguration objects, free the array with g_ptr_array_free
 *          when done.
 */
GPtrArray *
pd_configurations_response_get_configurations (PdConfigurationsResponse *self)
{
  g_return_val_if_fail (PD_IS_CONFIGURATIONS_RESPONSE (self), NULL);

  return self->configurations;
}

/**
 * pd_configurations_response_set_configurations:
 * @self: a #PdConfigurationsResponse
 * @configurations: (element-type Pd.Configuration): an array of
 *                  #PdConfiguration objects to set.
 */
void
pd_configurations_response_set_configurations (PdConfigurationsResponse *self,
                                               GPtrArray                *configurations)
{
  g_return_if_fail (PD_IS_CONFIGURATIONS_RESPONSE (self));

  if (self->configurations == configurations)
    return;

  if (self->configurations != NULL)
    g_ptr_array_unref (self->configurations);

  self->configurations = configurations;

  if (self->configurations != NULL)
    g_ptr_array_ref (self->configurations);

  g_object_notify_by_pspec (G_OBJECT (self), properties [PROP_CONFIGURATIONS]);
}
