/*
 * Copyright © the Plantd contributors. All rights reserved.
 *
 * This file is part of libplantd, distributed under the MIT license.
 * For full terms see the included LICENSE file.
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <json-glib/json-glib.h>

#include "pd-settings-request.h"

/*
 * PdSettingsRequest:
 *
 * Represents a ...
 */
struct _PdSettingsRequest
{
  GObject parent;
};

G_DEFINE_TYPE (PdSettingsRequest, pd_settings_request, G_TYPE_OBJECT)

static void
pd_settings_request_finalize (GObject *object)
{
  G_GNUC_UNUSED PdSettingsRequest *self = (PdSettingsRequest *)object;

  G_OBJECT_CLASS (pd_settings_request_parent_class)->finalize (object);
}

static void
pd_settings_request_class_init (PdSettingsRequestClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->finalize = pd_settings_request_finalize;
}

static void
pd_settings_request_init (PdSettingsRequest *self)
{
}

PdSettingsRequest *
pd_settings_request_new (void)
{
  return g_object_new (PD_TYPE_SETTINGS_REQUEST, NULL);
}

/**
 * pd_settings_request_serialize:
 * @self: a #PdSettingsRequest
 *
 * Returns the serialized message data.
 *
 * Returns: (transfer full): serialized data that the receiver must free.
 */
gchar *
pd_settings_request_serialize (PdSettingsRequest *self)
{
  gchar *ret;

  g_return_val_if_fail (PD_IS_SETTINGS_REQUEST (self), NULL);

  ret = json_gobject_to_data (G_OBJECT (self), NULL);

  return g_steal_pointer (&ret);
}

void
pd_settings_request_deserialize (PdSettingsRequest *self,
                                 const gchar       *data)
{
  GError *err = NULL;
  GObject *object = json_gobject_from_data (PD_TYPE_SETTINGS_REQUEST,
                                            data,
                                            -1,
                                            &err);

  if (err != NULL)
    {
      g_critical ("%s", err->message);
      g_error_free (err);
    }

  g_return_if_fail (object != NULL);
  g_return_if_fail (PD_IS_SETTINGS_REQUEST (object));

  // this doesn't have any properties to set from data

  g_clear_object (&object);
}
