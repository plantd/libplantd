#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <json-glib/json-glib.h>

#include "pd-status-request.h"

/**
 * PdStatusRequest:
 */
struct _PdStatusRequest
{
	GObject parent;
};

G_DEFINE_TYPE (PdStatusRequest, pd_status_request, G_TYPE_OBJECT)

static void
pd_status_request_finalize (GObject *object)
{
  G_OBJECT_CLASS (pd_status_request_parent_class)->finalize (object);
}

static void
pd_status_request_class_init (PdStatusRequestClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->finalize = pd_status_request_finalize;
}

static void
pd_status_request_init (PdStatusRequest *self)
{
}

PdStatusRequest *
pd_status_request_new (void)
{
  PdStatusRequest *request;

  request = g_object_new (PD_TYPE_STATUS_REQUEST, NULL);

  return request;
}

PdStatusRequest *
pd_status_request_new_from_data (const guint8 *data)
{
  PdStatusRequest *request;

  request = g_object_new (PD_TYPE_STATUS_REQUEST, NULL);

  return request;
}

guint8 *
pd_status_request_to_data (PdStatusRequest *self)
{
  g_return_val_if_fail (PD_IS_STATUS_REQUEST (self), NULL);

  return NULL;
}

/**
 * pd_status_request_serialize:
 * @self: a #PdStatusRequest
 *
 * Returns the serialized message data.
 *
 * Returns: (transfer full): serialized data that the receiver must free.
 */
gchar *
pd_status_request_serialize (PdStatusRequest *self)
{
  gchar *ret;

  g_return_val_if_fail (PD_IS_STATUS_REQUEST (self), NULL);

  ret = json_gobject_to_data (G_OBJECT (self), NULL);

  return g_steal_pointer (&ret);
}

void
pd_status_request_deserialize (PdStatusRequest *self,
                               const gchar     *data)
{
  GError *err = NULL;
  GObject *object = json_gobject_from_data (PD_TYPE_STATUS_REQUEST,
                                            data,
                                            -1,
                                            &err);

  if (err != NULL)
    {
      g_critical ("%s", err->message);
      g_error_free (err);
    }

  g_return_if_fail (object != NULL);
  g_return_if_fail (PD_IS_STATUS_REQUEST (object));

  // this doesn't have any properties to set from data

  g_clear_object (&object);
}
