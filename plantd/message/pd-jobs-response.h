/*
 * Copyright © the Plantd contributors. All rights reserved.
 *
 * This file is part of libplantd, distributed under the MIT license.
 * For full terms see the included LICENSE file.
 */

#pragma once

#if !defined (PLANTD_INSIDE) && !defined (PLANTD_COMPILATION)
# error "Only <plantd/plantd.h> can be included directly."
#endif

#include <glib-object.h>

#include <plantd/plantd-types.h>

#include "pd-response.h"

G_BEGIN_DECLS

#define PD_TYPE_JOBS_RESPONSE pd_jobs_response_get_type ()
G_DECLARE_FINAL_TYPE (PdJobsResponse, pd_jobs_response, PD, JOBS_RESPONSE, PdResponse)

PdJobsResponse *pd_jobs_response_new            (void);

gchar          *pd_jobs_response_serialize      (PdJobsResponse *self);
void            pd_jobs_response_deserialize    (PdJobsResponse *self,
                                                 const gchar    *data);

void            pd_jobs_response_add_job        (PdJobsResponse *self,
                                                 PdJob          *job);
void            pd_jobs_response_remove_job     (PdJobsResponse *self,
                                                 PdJob          *job);

GPtrArray      *pd_jobs_response_get_jobs       (PdJobsResponse *self);
void            pd_jobs_response_set_jobs       (PdJobsResponse *self,
                                                 GPtrArray      *jobs);

G_END_DECLS
