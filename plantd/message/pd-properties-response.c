/*
 * Copyright © the Plantd contributors. All rights reserved.
 *
 * This file is part of libplantd, distributed under the MIT license.
 * For full terms see the included LICENSE file.
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <json-glib/json-glib.h>

#include "plantd-error.h"
#include "pd-response.h"
#include "pd-properties-response.h"
#include "core/pd-property.h"

/*
 * PdPropertiesResponse:
 *
 * Represents a response message containting a list of properties.
 */
struct _PdPropertiesResponse
{
  PdResponse  parent;
  GHashTable *properties;
};

enum {
  PROP_0,
  PROP_PROPERTIES,
  N_PROPS
};

static GParamSpec *class_properties [N_PROPS];

static void json_serializable_iface_init (gpointer g_iface);

G_DEFINE_TYPE_WITH_CODE (PdPropertiesResponse, pd_properties_response, PD_TYPE_RESPONSE,
                         G_IMPLEMENT_INTERFACE (JSON_TYPE_SERIALIZABLE,
                                                json_serializable_iface_init));

static JsonNode *
pd_properties_response_serialize_property (JsonSerializable *serializable,
                                           const gchar      *name,
                                           const GValue     *value,
                                           GParamSpec       *pspec)
{
  JsonNode *retval = NULL;

  if (g_strcmp0 (name, "properties") == 0)
    {
      GHashTable *properties = NULL;
      GHashTableIter iter;
      JsonArray *arr = NULL;
      gpointer key, val;

      retval = json_node_new (JSON_NODE_ARRAY);

      g_return_val_if_fail (value != NULL, retval);
      g_return_val_if_fail (G_VALUE_HOLDS_POINTER (value), retval);

      properties = g_value_get_pointer (value);

      g_return_val_if_fail (properties != NULL, retval);

      arr = json_array_new ();

      if (properties != NULL)
        {
          g_hash_table_iter_init (&iter, properties);
          while(g_hash_table_iter_next (&iter, &key, &val))
            {
              JsonNode *node = NULL;
              JsonObject *obj = NULL;
              g_autoptr (PdProperty) property = NULL;

              property = pd_property_new (key, val);
              node = json_gobject_serialize (G_OBJECT (property));

              if (JSON_NODE_HOLDS_OBJECT (node))
                {
                  obj = json_node_dup_object (node);
                  json_array_add_object_element (arr, obj);
                }

              json_node_free (node);
            }
        }

      json_node_take_array (retval, arr);
    }
  else if (g_strcmp0 (name, "error") == 0)
    {
      PdMessageError *error = NULL;

      if (value != NULL)
        {
          error = g_value_get_object (value);
          g_return_val_if_fail (G_IS_OBJECT (error), retval);
          retval = json_gobject_serialize (G_OBJECT (error));
        }
    }
  else
    {
      GValue copy = { 0, };

      retval = json_node_new (JSON_NODE_VALUE);

      g_value_init (&copy, G_PARAM_SPEC_VALUE_TYPE (pspec));
      g_value_copy (value, &copy);
      json_node_set_value (retval, &copy);
      g_value_unset (&copy);
    }

  return retval;
}

static gboolean
pd_properties_response_deserialize_property (JsonSerializable *serializable,
                                             const gchar      *name,
                                             GValue           *value,
                                             GParamSpec       *pspec,
                                             JsonNode         *property_node)
{
  gboolean retval = FALSE;

  if (g_strcmp0 (name, "properties") == 0)
    {
      GHashTable *properties;
      JsonArray *arr;

      arr = json_node_get_array (property_node);
      properties = g_hash_table_new_full (g_str_hash,
                                          g_str_equal,
                                          g_free,
                                          g_free);

      for (gint i = 0; i < json_array_get_length (arr); i++)
        {
          g_autoptr (PdProperty) property = NULL;
          JsonNode *node = NULL;

          node = json_array_get_element (arr, i);

          property = PD_PROPERTY (json_gobject_deserialize (PD_TYPE_PROPERTY, node));
          g_return_val_if_fail (PD_IS_PROPERTY (property), FALSE);
          g_hash_table_insert (properties,
                               g_strdup (pd_property_get_key (property)),
                               g_strdup (pd_property_get_value (property)));
        }

      g_value_set_pointer (value, properties);

      retval = TRUE;
    }

  return retval;
}

static void
json_serializable_iface_init (gpointer g_iface)
{
  JsonSerializableIface *iface = g_iface;

  iface->serialize_property = pd_properties_response_serialize_property;
  iface->deserialize_property = pd_properties_response_deserialize_property;
}

static void
pd_properties_response_finalize (GObject *object)
{
  PdPropertiesResponse *self = (PdPropertiesResponse *)object;

  g_clear_pointer (&self->properties, g_hash_table_unref);

  G_OBJECT_CLASS (pd_properties_response_parent_class)->finalize (object);
}

static void
pd_properties_response_get_property (GObject    *object,
                                     guint       prop_id,
                                     GValue     *value,
                                     GParamSpec *pspec)
{
  PdPropertiesResponse *self = PD_PROPERTIES_RESPONSE (object);

  switch (prop_id)
    {
    case PROP_PROPERTIES:
      g_value_set_pointer (value, self->properties);
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
pd_properties_response_set_property (GObject      *object,
                                     guint         prop_id,
                                     const GValue *value,
                                     GParamSpec   *pspec)
{
  PdPropertiesResponse *self = PD_PROPERTIES_RESPONSE (object);

  switch (prop_id)
    {
    case PROP_PROPERTIES:
      pd_properties_response_set_list (self, g_value_get_pointer (value));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
pd_properties_response_class_init (PdPropertiesResponseClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->finalize = pd_properties_response_finalize;
  object_class->get_property = pd_properties_response_get_property;
  object_class->set_property = pd_properties_response_set_property;

  class_properties [PROP_PROPERTIES] =
    g_param_spec_pointer ("properties",
                          "Properties",
                          "The properties list",
                          (G_PARAM_READWRITE | G_PARAM_EXPLICIT_NOTIFY | G_PARAM_STATIC_STRINGS));

  g_object_class_install_properties (object_class, N_PROPS, class_properties);
}

static void
pd_properties_response_init (PdPropertiesResponse *self)
{
}

PdPropertiesResponse *
pd_properties_response_new (void)
{
  return g_object_new (PD_TYPE_PROPERTIES_RESPONSE, NULL);
}

/**
 * pd_properties_response_serialize:
 * @self: a #PdPropertiesResponse
 *
 * Returns the serialized message data.
 *
 * Returns: (transfer full): serialized data that the receiver must free.
 */
gchar *
pd_properties_response_serialize (PdPropertiesResponse *self)
{
  gchar *ret;

  g_return_val_if_fail (PD_IS_PROPERTIES_RESPONSE (self), NULL);

  ret = json_gobject_to_data (G_OBJECT (self), NULL);

  return g_steal_pointer (&ret);
}

void
pd_properties_response_deserialize (PdPropertiesResponse *self,
                                    const gchar          *data)
{
  g_autoptr (GObject) object = NULL;
  g_autoptr (GHashTable) list = NULL;

  GError *err = NULL;
  object = json_gobject_from_data (PD_TYPE_PROPERTIES_RESPONSE,
                                   data,
                                   -1,
                                   &err);

  if (err != NULL)
    {
      g_critical ("%s", err->message);
      g_error_free (err);
    }

  g_return_if_fail (object != NULL);
  g_return_if_fail (PD_IS_PROPERTIES_RESPONSE (object));

  list = pd_properties_response_get_list (PD_PROPERTIES_RESPONSE (object));
  pd_properties_response_set_list (self, list);

  g_clear_object (&object);
}

void
pd_properties_response_add (PdPropertiesResponse *self,
                            PdProperty           *property)
{
  const gchar *key;
  const gchar *value;

  g_return_if_fail (PD_IS_PROPERTIES_RESPONSE (self));
  g_return_if_fail (PD_IS_PROPERTY (property));

  key = pd_property_get_key (property);
  value = pd_property_get_value (property);

  if (self->properties == NULL)
    self->properties = g_hash_table_new_full (g_str_hash, g_str_equal, g_free, g_free);

  if (g_hash_table_contains (self->properties, key))
    g_hash_table_replace (self->properties, g_strdup (key), g_strdup (value));
  else
    g_hash_table_insert (self->properties, g_strdup (key), g_strdup (value));
}

void
pd_properties_response_remove (PdPropertiesResponse *self,
                               const gchar          *key)
{
  g_return_if_fail (PD_IS_PROPERTIES_RESPONSE (self));
  g_return_if_fail (self->properties != NULL);

  if (g_hash_table_contains (self->properties, key))
    g_hash_table_remove (self->properties, key);
}

/**
 * pd_properties_response_get:
 * @self: a #PdPropertiesResponse
 * @key: the key of the property to look up
 *
 * Returns: (transfer none): the property with the associated key if found,
 *          NULL otherwise.
 */
PdProperty *
pd_properties_response_get (PdPropertiesResponse *self,
                            const gchar          *key)
{
  g_return_val_if_fail (PD_IS_PROPERTIES_RESPONSE (self), NULL);
  g_return_val_if_fail (self->properties != NULL, NULL);

  if (g_hash_table_contains (self->properties, key))
    return pd_property_new (key, g_hash_table_lookup (self->properties, key));

  return NULL;
}

gboolean
pd_properties_response_contains (PdPropertiesResponse *self,
                                 const gchar          *key)
{
  gboolean ret;

  g_return_val_if_fail (PD_IS_PROPERTIES_RESPONSE (self), FALSE);
  g_return_val_if_fail (self->properties != NULL, FALSE);
  g_return_val_if_fail (key != NULL, FALSE);

  ret = g_hash_table_contains (self->properties, key);

  return ret;
}

/**
 * pd_properties_response_get_list:
 * @self: a #PdPropertiesResponse
 *
 * Returns: (element-type utf8 utf8) (transfer full): a hash table of strings
 *          representing #PdProperty objects, free the table with
 *          g_hash_table_destroy when done.
 */
GHashTable *
pd_properties_response_get_list (PdPropertiesResponse *self)
{
  GHashTable *properties;

  g_return_val_if_fail (PD_IS_PROPERTIES_RESPONSE (self), NULL);

  g_object_get (self, "properties", &properties, NULL);

  return properties;
}

void
pd_properties_response_set_list (PdPropertiesResponse *self,
                                 GHashTable           *properties)
{
  g_return_if_fail (PD_IS_PROPERTIES_RESPONSE (self));

  if (self->properties == properties)
    return;

  if (self->properties)
    g_hash_table_unref (self->properties);

  self->properties = properties;

  if (properties)
    g_hash_table_ref (properties);

  g_object_notify_by_pspec (G_OBJECT (self), class_properties [PROP_PROPERTIES]);
}
