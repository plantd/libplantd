/*
 * Copyright © the Plantd contributors. All rights reserved.
 *
 * This file is part of libplantd, distributed under the MIT license.
 * For full terms see the included LICENSE file.
 */

#pragma once

#if !defined (PLANTD_INSIDE) && !defined (PLANTD_COMPILATION)
# error "Only <plantd/plantd.h> can be included directly."
#endif

#include <glib-object.h>

#include "pd-response.h"

G_BEGIN_DECLS

#define PD_TYPE_EVENT_RESPONSE pd_event_response_get_type ()
G_DECLARE_FINAL_TYPE (PdEventResponse, pd_event_response, PD, EVENT_RESPONSE, PdResponse)

// TODO: change this to PdEventsResponse

PdEventResponse *pd_event_response_new         (void);

gchar           *pd_event_response_serialize   (PdEventResponse *self);
void             pd_event_response_deserialize (PdEventResponse *self,
                                                const gchar     *data);

GPtrArray       *pd_event_response_get_events  (PdEventResponse *self);
void             pd_event_response_set_events  (PdEventResponse *self,
                                                GPtrArray       *events);

G_END_DECLS
