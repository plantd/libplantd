/*
 * Copyright © the Plantd contributors. All rights reserved.
 *
 * This file is part of libplantd, distributed under the MIT license.
 * For full terms see the included LICENSE file.
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <json-glib/json-glib.h>

#include "pd-status-response.h"
#include "core/pd-status.h"

/**
 * PdStatusResponse:
 */
struct _PdStatusResponse
{
	GObject     parent;
	PdStatus *status;
};

enum {
  PROP_0,
  PROP_STATUS,
  N_PROPS
};

static GParamSpec *properties [N_PROPS];

G_DEFINE_TYPE (PdStatusResponse, pd_status_response, G_TYPE_OBJECT)

static void
pd_status_response_finalize (GObject *object)
{
  PdStatusResponse *self = (PdStatusResponse *)object;

  g_clear_object (&self->status);

  G_OBJECT_CLASS (pd_status_response_parent_class)->finalize (object);
}

static void
pd_status_response_get_property (GObject    *object,
                                 guint       prop_id,
                                 GValue     *value,
                                 GParamSpec *pspec)
{
  PdStatusResponse *self = PD_STATUS_RESPONSE (object);

  switch (prop_id)
    {
    case PROP_STATUS:
      g_value_take_object (value, pd_status_response_ref_status (self));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
pd_status_response_set_property (GObject      *object,
                                 guint         prop_id,
                                 const GValue *value,
                                 GParamSpec   *pspec)
{
  PdStatusResponse *self = PD_STATUS_RESPONSE (object);

  switch (prop_id)
    {
    case PROP_STATUS:
      pd_status_response_set_status (self, g_value_get_object (value));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
pd_status_response_class_init (PdStatusResponseClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->finalize = pd_status_response_finalize;
  object_class->get_property = pd_status_response_get_property;
  object_class->set_property = pd_status_response_set_property;

  properties [PROP_STATUS] =
      g_param_spec_object ("status",
                           "Status",
                           "The status in the response.",
                           PD_TYPE_STATUS,
                           (G_PARAM_READWRITE | G_PARAM_EXPLICIT_NOTIFY | G_PARAM_STATIC_STRINGS));

  g_object_class_install_properties (object_class, N_PROPS, properties);
}

static void
pd_status_response_init (PdStatusResponse *self)
{
  self->status = pd_status_new ();
}

PdStatusResponse *
pd_status_response_new (void)
{
  PdStatusResponse *response = g_object_new (PD_TYPE_STATUS_RESPONSE, NULL);

  return response;
}

PdStatusResponse *
pd_status_response_new_from_data (const guint8 *data)
{
  PdStatusResponse *response = g_object_new (PD_TYPE_STATUS_RESPONSE, NULL);

  return response;
}

guint8 *
pd_status_response_to_data (PdStatusResponse *self)
{
  guint8 *out = NULL;

  g_return_val_if_fail (PD_IS_STATUS_RESPONSE (self), NULL);

  return out;
}

/**
 * pd_status_response_serialize:
 * @self: a #PdStatusResponse
 *
 * Returns the serialized message data.
 *
 * Returns: (transfer full): serialized data that the receiver must free.
 */
gchar *
pd_status_response_serialize (PdStatusResponse *self)
{
  gchar *ret;

  g_return_val_if_fail (PD_IS_STATUS_RESPONSE (self), NULL);

  ret = json_gobject_to_data (G_OBJECT (self), NULL);

  return g_steal_pointer (&ret);
}

void
pd_status_response_deserialize (PdStatusResponse *self,
                                const gchar      *data)
{
  g_autoptr (GObject) object = NULL;
  g_autoptr (PdStatus) status = NULL;

  GError *err = NULL;
  object = json_gobject_from_data (PD_TYPE_STATUS_RESPONSE,
                                   data,
                                   -1,
                                   &err);

  if (err != NULL)
    {
      g_critical ("%s", err->message);
      g_error_free (err);
    }

  g_return_if_fail (object != NULL);
  g_return_if_fail (PD_IS_STATUS_RESPONSE (object));

  status = pd_status_response_ref_status (PD_STATUS_RESPONSE (object));

  pd_status_response_set_status (self, status);

  g_clear_object (&object);
}

/**
 * pd_status_response_get_status:
 * @self: #PdStatusResponse instance
 *
 * Returns: (transfer full): The #PdStatus
 */
PdStatus *
pd_status_response_get_status (PdStatusResponse *self)
{
  PdStatus *status;

  g_return_val_if_fail (PD_IS_STATUS_RESPONSE (self), NULL);

  g_object_get (self, "status", &status, NULL);

  return status;
}

/**
 * pd_status_response_ref_status:
 * @self: a #PdStatusResponse
 *
 * Gets the status data for the response, and returns a new reference
 *
 * Returns: (transfer full) (nullable): a #PdStatus or %NULL
 */
PdStatus *
pd_status_response_ref_status (PdStatusResponse *self)
{
  PdStatus *ret = NULL;

  g_return_val_if_fail (PD_IS_STATUS_RESPONSE (self), NULL);

  g_set_object (&ret, self->status);

  return g_steal_pointer (&ret);
}

void
pd_status_response_set_status (PdStatusResponse *self,
                               PdStatus         *status)
{
  g_return_if_fail (PD_IS_STATUS_RESPONSE (self));
  g_return_if_fail (PD_IS_STATUS (status));

  if (g_set_object (&self->status, status))
    g_object_notify_by_pspec (G_OBJECT (self), properties [PROP_STATUS]);
}
