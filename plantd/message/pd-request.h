/*
 * Copyright © the Plantd contributors. All rights reserved.
 *
 * This file is part of libplantd, distributed under the MIT license.
 * For full terms see the included LICENSE file.
 */

#pragma once

#if !defined (PLANTD_INSIDE) && !defined (PLANTD_COMPILATION)
# error "Only <plantd/plantd.h> can be included directly."
#endif

#include <glib-object.h>

G_BEGIN_DECLS

#define PD_TYPE_REQUEST pd_request_get_type ()
G_DECLARE_DERIVABLE_TYPE (PdRequest, pd_request, PD, REQUEST, GObject)

struct _PdRequestClass
{
  /*< private >*/
  GObjectClass parent_class;

  /*< private >*/
  gpointer padding[12];
};

PdRequest *pd_request_new         (void);

gchar     *pd_request_serialize   (PdRequest   *self);
void       pd_request_deserialize (PdRequest   *self,
                                   const gchar *data);

G_END_DECLS
