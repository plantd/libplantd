/*
 * Copyright © the Plantd contributors. All rights reserved.
 *
 * This file is part of libplantd, distributed under the MIT license.
 * For full terms see the included LICENSE file.
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <json-glib/json-glib.h>

#include "pd-property-request.h"
#include "core/pd-property.h"

/*
 * PdPropertyRequest:
 *
 * Represents a ...
 */
struct _PdPropertyRequest
{
  GObject  parent;
  gchar   *id;
  gchar   *key;
  gchar   *value;
};

enum {
  PROP_0,
  PROP_ID,
  PROP_KEY,
  PROP_VALUE,
  N_PROPS
};

static GParamSpec *properties [N_PROPS];

G_DEFINE_TYPE (PdPropertyRequest, pd_property_request, G_TYPE_OBJECT)

static void
pd_property_request_finalize (GObject *object)
{
  PdPropertyRequest *self = (PdPropertyRequest *)object;

  g_clear_pointer (&self->id, g_free);
  g_clear_pointer (&self->key, g_free);
  g_clear_pointer (&self->value, g_free);

  G_OBJECT_CLASS (pd_property_request_parent_class)->finalize (object);
}

static void
pd_property_request_get_property (GObject    *object,
                                  guint       prop_id,
                                  GValue     *value,
                                  GParamSpec *pspec)
{
  PdPropertyRequest *self = PD_PROPERTY_REQUEST (object);

  switch (prop_id)
    {
    case PROP_ID:
      g_value_set_string (value, pd_property_request_get_id (self));
      break;

    case PROP_KEY:
      g_value_set_string (value, pd_property_request_get_key (self));
      break;

    case PROP_VALUE:
      g_value_set_string (value, pd_property_request_get_value (self));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
pd_property_request_set_property (GObject      *object,
                                  guint         prop_id,
                                  const GValue *value,
                                  GParamSpec   *pspec)
{
  PdPropertyRequest *self = PD_PROPERTY_REQUEST (object);

  switch (prop_id)
    {
    case PROP_ID:
      pd_property_request_set_id (self, g_value_get_string (value));
      break;

    case PROP_KEY:
      pd_property_request_set_key (self, g_value_get_string (value));
      break;

    case PROP_VALUE:
      pd_property_request_set_value (self, g_value_get_string (value));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
pd_property_request_class_init (PdPropertyRequestClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->finalize = pd_property_request_finalize;
  object_class->get_property = pd_property_request_get_property;
  object_class->set_property = pd_property_request_set_property;

  properties [PROP_ID] =
    g_param_spec_string ("id",
                         "ID",
                         "The id of the service the request is meant for",
                         NULL,
                         (G_PARAM_READWRITE | G_PARAM_EXPLICIT_NOTIFY));

  properties [PROP_KEY] =
    g_param_spec_string ("key",
                         "Key",
                         "The property key being requested",
                         NULL,
                         (G_PARAM_READWRITE | G_PARAM_EXPLICIT_NOTIFY));

  properties [PROP_VALUE] =
    g_param_spec_string ("value",
                         "Value",
                         "The property value being requested",
                         NULL,
                         (G_PARAM_READWRITE | G_PARAM_EXPLICIT_NOTIFY));

  g_object_class_install_properties (object_class, N_PROPS, properties);
}

static void
pd_property_request_init (PdPropertyRequest *self)
{
}

PdPropertyRequest *
pd_property_request_new (const gchar *id,
                         const gchar *key,
                         const gchar *value)
{
  return g_object_new (PD_TYPE_PROPERTY_REQUEST,
                       "id", id,
                       "key", key,
                       "value", value,
                       NULL);
}

/**
 * pd_property_request_serialize:
 * @self: a #PdPropertyRequest
 *
 * Returns the serialized message data.
 *
 * Returns: (transfer full): serialized data that the receiver must free.
 */
gchar *
pd_property_request_serialize (PdPropertyRequest *self)
{
  gchar *ret;

  g_return_val_if_fail (PD_IS_PROPERTY_REQUEST (self), NULL);

  ret = json_gobject_to_data (G_OBJECT (self), NULL);

  return g_steal_pointer (&ret);
}

void
pd_property_request_deserialize (PdPropertyRequest *self,
                                 const gchar       *data)
{
  GError *err = NULL;
  GObject *object = json_gobject_from_data (PD_TYPE_PROPERTY_REQUEST,
                                            data,
                                            -1,
                                            &err);

  if (err != NULL)
    {
      g_critical ("%s", err->message);
      g_error_free (err);
    }

  g_return_if_fail (object != NULL);
  g_return_if_fail (PD_IS_PROPERTY_REQUEST (object));

  pd_property_request_set_id (self,
      pd_property_request_get_id (PD_PROPERTY_REQUEST (object)));

  pd_property_request_set_key (self,
      pd_property_request_get_key (PD_PROPERTY_REQUEST (object)));

  pd_property_request_set_value (self,
      pd_property_request_get_value (PD_PROPERTY_REQUEST (object)));

  g_object_unref (object);
}

const gchar *
pd_property_request_get_id (PdPropertyRequest *self)
{
  g_return_val_if_fail (PD_IS_PROPERTY_REQUEST (self), NULL);

  return self->id;
}

void
pd_property_request_set_id (PdPropertyRequest *self,
                            const gchar       *id)
{
  g_return_if_fail (PD_IS_PROPERTY_REQUEST (self));

  if (g_strcmp0 (id, self->id) != 0)
    {
      g_free (self->id);
      self->id = g_strdup (id);
      g_object_notify_by_pspec (G_OBJECT (self), properties [PROP_ID]);
    }
}

const gchar *
pd_property_request_get_key (PdPropertyRequest *self)
{
  g_return_val_if_fail (PD_IS_PROPERTY_REQUEST (self), NULL);

  return self->key;
}

void
pd_property_request_set_key (PdPropertyRequest *self,
                             const gchar       *key)
{
  g_return_if_fail (PD_IS_PROPERTY_REQUEST (self));

  if (g_strcmp0 (key, self->key) != 0)
    {
      g_free (self->key);
      self->key = g_strdup (key);
      g_object_notify_by_pspec (G_OBJECT (self), properties [PROP_KEY]);
    }
}

const gchar *
pd_property_request_get_value (PdPropertyRequest *self)
{
  g_return_val_if_fail (PD_IS_PROPERTY_REQUEST (self), NULL);

  return self->value;
}

void
pd_property_request_set_value (PdPropertyRequest *self,
                               const gchar       *value)
{
  g_return_if_fail (PD_IS_PROPERTY_REQUEST (self));

  if (g_strcmp0 (value, self->value) != 0)
    {
      g_free (self->value);
      self->value = g_strdup (value);
      g_object_notify_by_pspec (G_OBJECT (self), properties [PROP_VALUE]);
    }
}
