/*
 * Copyright © the Plantd contributors. All rights reserved.
 *
 * This file is part of libplantd, distributed under the MIT license.
 * For full terms see the included LICENSE file.
 */

#pragma once

#if !defined (PLANTD_INSIDE) && !defined (PLANTD_COMPILATION)
# error "Only <plantd/plantd.h> can be included directly."
#endif

#include <glib-object.h>

#include <plantd/plantd-types.h>

#include "pd-response.h"

G_BEGIN_DECLS

#define PD_TYPE_CHANNEL_RESPONSE pd_channel_response_get_type ()
G_DECLARE_FINAL_TYPE (PdChannelResponse, pd_channel_response, PD, CHANNEL_RESPONSE, PdResponse)

PdChannelResponse *pd_channel_response_new         (void);

gchar             *pd_channel_response_serialize   (PdChannelResponse *self);
void               pd_channel_response_deserialize (PdChannelResponse *self,
                                                    const gchar       *data);

PdChannel         *pd_channel_response_get_channel (PdChannelResponse *self);
void               pd_channel_response_set_channel (PdChannelResponse *self,
                                                    PdChannel         *channel);

G_END_DECLS
