/*
 * Copyright © the Plantd contributors. All rights reserved.
 *
 * This file is part of libplantd, distributed under the MIT license.
 * For full terms see the included LICENSE file.
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <json-glib/json-glib.h>

#include "pd-request.h"

/*
 * SECTION:plantd-request
 * @short_description: Request base class
 *
 * An #PdRequest is a structure for all request message types to derive.
 */
typedef struct
{
  GObject parent;
} PdRequestPrivate;

G_DEFINE_TYPE_WITH_PRIVATE (PdRequest, pd_request, G_TYPE_OBJECT)

static void
pd_request_finalize (GObject *object)
{
  G_GNUC_UNUSED PdRequest *self = (PdRequest *)object;

  G_OBJECT_CLASS (pd_request_parent_class)->finalize (object);
}

static void
pd_request_class_init (PdRequestClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->finalize = pd_request_finalize;
}

static void
pd_request_init (PdRequest *self)
{
}

PdRequest *
pd_request_new (void)
{
  return g_object_new (PD_TYPE_REQUEST, NULL);
}

/**
 * pd_request_serialize:
 * @self: a #PdRequest
 *
 * Returns the serialized message data.
 *
 * Returns: (transfer full): serialized data that the receiver must free.
 */
gchar *
pd_request_serialize (PdRequest *self)
{
  gchar *ret;

  g_return_val_if_fail (PD_IS_REQUEST (self), NULL);

  ret = json_gobject_to_data (G_OBJECT (self), NULL);

  return g_steal_pointer (&ret);
}

void
pd_request_deserialize (PdRequest   *self,
                        const gchar *data)
{
  GError *err = NULL;
  GObject *object = json_gobject_from_data (PD_TYPE_REQUEST,
                                            data,
                                            -1,
                                            &err);

  if (err != NULL)
    {
      g_critical ("%s", err->message);
      g_error_free (err);
    }

  g_return_if_fail (object != NULL);
  g_return_if_fail (PD_IS_REQUEST (object));

  g_object_unref (object);
}
