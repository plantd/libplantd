/*
 * Copyright © the Plantd contributors. All rights reserved.
 *
 * This file is part of libplantd, distributed under the MIT license.
 * For full terms see the included LICENSE file.
 */

#pragma once

#if !defined (PLANTD_INSIDE) && !defined (PLANTD_COMPILATION)
# error "Only <plantd/plantd.h> can be included directly."
#endif

#include <glib-object.h>

G_BEGIN_DECLS

#define PD_TYPE_EMPTY pd_empty_get_type ()
G_DECLARE_FINAL_TYPE (PdEmpty, pd_empty, PD, EMPTY, GObject)

PdEmpty *pd_empty_new (void);

G_END_DECLS
