/*
 * Copyright © the Plantd contributors. All rights reserved.
 *
 * This file is part of libplantd, distributed under the MIT license.
 * For full terms see the included LICENSE file.
 */

#pragma once

#if !defined (PLANTD_INSIDE) && !defined (PLANTD_COMPILATION)
# error "Only <plantd/plantd.h> can be included directly."
#endif

#include <glib-object.h>

#include <plantd/plantd-types.h>

#include "pd-request.h"

G_BEGIN_DECLS

#define PD_TYPE_JOB_REQUEST pd_job_request_get_type ()
G_DECLARE_FINAL_TYPE (PdJobRequest, pd_job_request, PD, JOB_REQUEST, PdRequest)

PdJobRequest *pd_job_request_new           (const gchar  *id);

gchar        *pd_job_request_serialize     (PdJobRequest *self);
void          pd_job_request_deserialize   (PdJobRequest *self,
                                            const gchar  *data);

void          pd_job_request_add           (PdJobRequest *self,
                                            PdProperty   *property);
void          pd_job_request_remove        (PdJobRequest *self,
                                            const gchar  *key);
PdProperty   *pd_job_request_get           (PdJobRequest *self,
                                            const gchar  *key);
gboolean      pd_job_request_contains      (PdJobRequest *self,
                                            const gchar  *key);
GHashTable   *pd_job_request_get_list      (PdJobRequest *self);
void          pd_job_request_set_list      (PdJobRequest *self,
                                            GHashTable   *job_properties);

const gchar  *pd_job_request_get_id        (PdJobRequest *self);
void          pd_job_request_set_id        (PdJobRequest *self,
                                            const gchar  *id);

const gchar  *pd_job_request_get_job_id    (PdJobRequest *self);
void          pd_job_request_set_job_id    (PdJobRequest *self,
                                            const gchar  *job_id);

const gchar  *pd_job_request_get_job_value (PdJobRequest *self);
void          pd_job_request_set_job_value (PdJobRequest *self,
                                            const gchar  *job_value);

G_END_DECLS
