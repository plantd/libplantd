/*
 * Copyright © the Plantd contributors. All rights reserved.
 *
 * This file is part of libplantd, distributed under the MIT license.
 * For full terms see the included LICENSE file.
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <json-glib/json-glib.h>

#include "pd-settings-response.h"

/*
 * PdSettingsResponse:
 *
 * Represents a ...
 */
struct _PdSettingsResponse
{
  GObject     parent;

  gchar      *id;
  GHashTable *settings;
};

enum {
  PROP_0,
  PROP_ID,
  PROP_SETTINGS,
  N_PROPS
};

static GParamSpec *properties [N_PROPS];

static void json_serializable_iface_init (gpointer g_iface);

G_DEFINE_TYPE_WITH_CODE (PdSettingsResponse, pd_settings_response, G_TYPE_OBJECT,
                         G_IMPLEMENT_INTERFACE (JSON_TYPE_SERIALIZABLE,
                                                json_serializable_iface_init));

static JsonNode *
pd_settings_response_serialize_property (JsonSerializable *serializable,
                                         const gchar      *name,
                                         const GValue     *value,
                                         GParamSpec       *pspec)
{
  JsonNode *retval = NULL;

  if (g_strcmp0 (name, "settings") == 0)
    {
      GHashTable *settings = NULL;
      GHashTableIter iter;
      JsonObject *obj = NULL;
      gpointer key, val;

      retval = json_node_new (JSON_NODE_OBJECT);

      g_return_val_if_fail (value != NULL, retval);
      g_return_val_if_fail (G_VALUE_HOLDS_POINTER (value), retval);

      settings = g_value_get_pointer (value);

      g_return_val_if_fail (settings != NULL, retval);

      obj = json_object_new ();

      if (settings != NULL)
        {
          g_hash_table_iter_init (&iter, settings);
          while(g_hash_table_iter_next (&iter, &key, &val))
            {
              json_object_set_string_member (obj,
                                             (const gchar *) key,
                                             (const gchar *) val);
            }
        }

      json_node_take_object (retval, obj);
    }
  else
    {
      GValue copy = { 0, };

      retval = json_node_new (JSON_NODE_VALUE);

      g_value_init (&copy, G_PARAM_SPEC_VALUE_TYPE (pspec));
      g_value_copy (value, &copy);
      json_node_set_value (retval, &copy);
      g_value_unset (&copy);
    }

  return retval;
}

static gboolean
pd_settings_response_deserialize_property (JsonSerializable *serializable,
                                           const gchar      *name,
                                           GValue           *value,
                                           GParamSpec       *pspec,
                                           JsonNode         *property_node)
{
  gboolean retval = FALSE;

  if (g_strcmp0 (name, "settings") == 0)
    {
      g_autoptr (GHashTable) settings = NULL;
      g_autoptr (JsonObject) obj = NULL;
      JsonObjectIter iter;
      const gchar *key;
      JsonNode *val;

      obj = json_node_get_object (property_node);
      settings = g_hash_table_new_full (g_str_hash,
                                        g_str_equal,
                                        g_free,
                                        g_free);

      json_object_ref (obj);

      json_object_iter_init (&iter, obj);
      while (json_object_iter_next (&iter, &key, &val))
        {
          g_hash_table_insert (settings,
                               g_strdup (key),
                               g_strdup (json_node_get_string (val)));
        }

      g_value_set_pointer (value, g_hash_table_ref (settings));

      retval = TRUE;
    }

  return retval;
}

static void
json_serializable_iface_init (gpointer g_iface)
{
  JsonSerializableIface *iface = g_iface;

  iface->serialize_property = pd_settings_response_serialize_property;
  iface->deserialize_property = pd_settings_response_deserialize_property;
}

static void
pd_settings_response_finalize (GObject *object)
{
  PdSettingsResponse *self = (PdSettingsResponse *)object;

  g_clear_pointer (&self->id, g_free);
  g_clear_pointer (&self->settings, g_hash_table_unref);

  G_OBJECT_CLASS (pd_settings_response_parent_class)->finalize (object);
}

static void
pd_settings_response_get_property (GObject    *object,
                                   guint       prop_id,
                                   GValue     *value,
                                   GParamSpec *pspec)
{
  PdSettingsResponse *self = PD_SETTINGS_RESPONSE (object);

  switch (prop_id)
    {
    case PROP_ID:
      g_value_take_string (value, pd_settings_response_dup_id (self));
      break;

    case PROP_SETTINGS:
      g_value_set_pointer (value, self->settings);
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
pd_settings_response_set_property (GObject      *object,
                                   guint         prop_id,
                                   const GValue *value,
                                   GParamSpec   *pspec)
{
  PdSettingsResponse *self = PD_SETTINGS_RESPONSE (object);

  switch (prop_id)
    {
    case PROP_ID:
      pd_settings_response_set_id (self, g_value_get_string (value));
      break;

    case PROP_SETTINGS:
      pd_settings_response_set_settings (self, g_value_get_pointer (value));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
pd_settings_response_class_init (PdSettingsResponseClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->finalize = pd_settings_response_finalize;
  object_class->get_property = pd_settings_response_get_property;
  object_class->set_property = pd_settings_response_set_property;

  properties [PROP_ID] =
    g_param_spec_string ("id",
                         "ID",
                         "The ID of the response",
                         NULL,
                         (G_PARAM_READWRITE | G_PARAM_EXPLICIT_NOTIFY | G_PARAM_STATIC_STRINGS));

  properties [PROP_SETTINGS] =
    g_param_spec_pointer ("settings",
                          "Settings",
                          "",
                          G_PARAM_READWRITE | G_PARAM_EXPLICIT_NOTIFY | G_PARAM_STATIC_STRINGS);

  g_object_class_install_properties (object_class, N_PROPS, properties);
}

static void
pd_settings_response_init (PdSettingsResponse *self)
{
}

PdSettingsResponse *
pd_settings_response_new (void)
{
  return g_object_new (PD_TYPE_SETTINGS_RESPONSE, NULL);
}

/**
 * pd_settings_response_serialize:
 *
 * Returns the serialized message data.
 *
 * @self: a #PdSettingsResponse
 *
 * Returns: (transfer full): serialized data that the receiver must free.
 */
gchar *
pd_settings_response_serialize (PdSettingsResponse *self)
{
  gchar *ret;

  g_return_val_if_fail (PD_IS_SETTINGS_RESPONSE (self), NULL);

  ret = json_gobject_to_data (G_OBJECT (self), NULL);

  return g_steal_pointer (&ret);
}

void
pd_settings_response_deserialize (PdSettingsResponse *self,
                                  const gchar        *data)
{
  g_autoptr (GObject) object = NULL;
  g_autoptr (GHashTable) settings = NULL;
  g_autofree gchar *id = NULL;

  GError *err = NULL;
  object = json_gobject_from_data (PD_TYPE_SETTINGS_RESPONSE,
                                   data,
                                   -1,
                                   &err);

  if (err != NULL)
    {
      g_critical ("%s", err->message);
      g_error_free (err);
    }

  g_return_if_fail (object != NULL);
  g_return_if_fail (PD_IS_SETTINGS_RESPONSE (object));

  id = pd_settings_response_dup_id (PD_SETTINGS_RESPONSE (object));
  settings = pd_settings_response_get_settings (PD_SETTINGS_RESPONSE (object));

  pd_settings_response_set_id (self, id);
  pd_settings_response_set_settings (self, settings);

  g_clear_object (&object);
}

const gchar *
pd_settings_response_get_id (PdSettingsResponse *self)
{
  g_return_val_if_fail (PD_IS_SETTINGS_RESPONSE (self), NULL);

  return self->id;
}

/**
 * pd_settings_response_dup_id:
 *
 * Copies the id of the settings response and returns it to the caller (after
 * locking the object). A copy is used to avoid thread-races.
 */
gchar *
pd_settings_response_dup_id (PdSettingsResponse *self)
{
  gchar *ret;

  g_return_val_if_fail (PD_IS_SETTINGS_RESPONSE (self), NULL);

  /*pd_object_lock (PD_OBJECT (self));*/
  ret = g_strdup (self->id);
  /*pd_object_unlock (PD_OBJECT (self));*/

  return g_steal_pointer (&ret);
}

void
pd_settings_response_set_id (PdSettingsResponse *self,
                             const gchar        *id)
{
  g_return_if_fail (PD_IS_SETTINGS_RESPONSE (self));

  if (g_strcmp0 (id, self->id) != 0)
    {
      g_free (self->id);
      self->id = g_strdup (id);
      g_object_notify_by_pspec (G_OBJECT (self), properties [PROP_ID]);
    }
}

/**
 * pd_settings_response_get_settings:
 * @self: #PdSettingsResponse instance
 *
 * Returns the settings response object
 *
 * Returns: (element-type utf8 Pd.Settings) (transfer full): The plantd settings
 * response GHashTable
 */
GHashTable *
pd_settings_response_get_settings (PdSettingsResponse *self)
{
  GHashTable *settings;

  g_return_val_if_fail (PD_IS_SETTINGS_RESPONSE (self), NULL);

  g_object_get (self, "settings", &settings, NULL);

  return settings;
}

void
pd_settings_response_set_settings (PdSettingsResponse *self,
                                   GHashTable         *settings)
{
  g_return_if_fail (PD_IS_SETTINGS_RESPONSE (self));

  if (self->settings == settings)
    return;

  if (settings)
    g_hash_table_ref (settings);

  if (self->settings)
    g_hash_table_unref (self->settings);

  self->settings = settings;

  g_object_notify_by_pspec (G_OBJECT (self), properties [PROP_SETTINGS]);
}

void
pd_settings_response_add_setting (PdSettingsResponse *self,
                                  const gchar        *key,
                                  const gchar        *value)
{
  g_return_if_fail (PD_IS_SETTINGS_RESPONSE (self));

  if (self->settings == NULL)
    self->settings = g_hash_table_new_full (g_str_hash,
                                            g_str_equal,
                                            g_free,
                                            g_free);

  if (g_hash_table_contains (self->settings, key))
    g_hash_table_replace (self->settings,
                          g_strdup (key),
                          g_strdup (value));
  else
    g_hash_table_insert (self->settings,
                         g_strdup (key),
                         g_strdup (value));

  g_object_notify_by_pspec (G_OBJECT (self), properties [PROP_SETTINGS]);
}

void
pd_settings_response_remove_setting (PdSettingsResponse *self,
                                     const gchar        *key)
{
  g_return_if_fail (PD_IS_SETTINGS_RESPONSE (self));
  g_return_if_fail (self->settings != NULL);

  if (g_hash_table_contains (self->settings, key))
    g_hash_table_remove (self->settings, key);

  g_object_notify_by_pspec (G_OBJECT (self), properties [PROP_SETTINGS]);
}

const gchar *
pd_settings_response_get_setting (PdSettingsResponse *self,
                                  const gchar        *key)
{
  g_return_val_if_fail (PD_IS_SETTINGS_RESPONSE (self), NULL);
  g_return_val_if_fail (self->settings != NULL, NULL);

  if (g_hash_table_contains (self->settings, key))
    return g_hash_table_lookup (self->settings, key);

  return NULL;
}
