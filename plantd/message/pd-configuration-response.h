/*
 * Copyright © the Plantd contributors. All rights reserved.
 *
 * This file is part of libplantd, distributed under the MIT license.
 * For full terms see the included LICENSE file.
 */

#pragma once

#if !defined (PLANTD_INSIDE) && !defined (PLANTD_COMPILATION)
# error "Only <plantd/plantd.h> can be included directly."
#endif

#include <glib-object.h>

#include <plantd/plantd-types.h>

#include "pd-response.h"

G_BEGIN_DECLS

#define PD_TYPE_CONFIGURATION_RESPONSE pd_configuration_response_get_type ()
G_DECLARE_FINAL_TYPE (PdConfigurationResponse, pd_configuration_response, PD, CONFIGURATION_RESPONSE, PdResponse)

PdConfigurationResponse *pd_configuration_response_new               (void);

gchar                   *pd_configuration_response_serialize         (PdConfigurationResponse *self);
void                     pd_configuration_response_deserialize       (PdConfigurationResponse *self,
                                                                      const gchar             *data);

PdConfiguration         *pd_configuration_response_get_configuration (PdConfigurationResponse *self);
PdConfiguration         *pd_configuration_response_ref_configuration (PdConfigurationResponse *self);
void                     pd_configuration_response_set_configuration (PdConfigurationResponse *self,
                                                                      PdConfiguration         *configuration);

G_END_DECLS
