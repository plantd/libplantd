/*
 * Copyright © the Plantd contributors. All rights reserved.
 *
 * This file is part of libplantd, distributed under the MIT license.
 * For full terms see the included LICENSE file.
 */

#pragma once

#if !defined (PLANTD_INSIDE) && !defined (PLANTD_COMPILATION)
# error "Only <plantd/plantd.h> can be included directly."
#endif

#include <glib-object.h>

#include <plantd/plantd-types.h>

#include "pd-response.h"

G_BEGIN_DECLS

#define PD_TYPE_JOB_RESPONSE pd_job_response_get_type ()
G_DECLARE_FINAL_TYPE (PdJobResponse, pd_job_response, PD, JOB_RESPONSE, PdResponse)

PdJobResponse *pd_job_response_new         (void);

gchar         *pd_job_response_serialize   (PdJobResponse *self);
void           pd_job_response_deserialize (PdJobResponse *self,
                                            const gchar   *data);

PdJob         *pd_job_response_get_job     (PdJobResponse *self);
void           pd_job_response_set_job     (PdJobResponse *self,
                                            PdJob         *job);

G_END_DECLS
