/*
 * Copyright © the Plantd contributors. All rights reserved.
 *
 * This file is part of libplantd, distributed under the MIT license.
 * For full terms see the included LICENSE file.
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <json-glib/json-glib.h>

#include <plantd/plantd-enum-types.h>

#include "pd-request.h"
#include "pd-configuration-request.h"
#include "core/pd-configuration.h"

/*
 * PdConfigurationRequest:
 *
 * Represents a request message to read configuration data.
 */
struct _PdConfigurationRequest
{
  PdRequest                 parent;
  gchar                    *id;
  PdConfigurationNamespace  namespace;
};

enum {
  PROP_0,
  PROP_ID,
  PROP_NAMESPACE,
  N_PROPS
};

static GParamSpec *properties [N_PROPS];

G_DEFINE_TYPE (PdConfigurationRequest, pd_configuration_request, PD_TYPE_REQUEST)

static void
pd_configuration_request_finalize (GObject *object)
{
  PdConfigurationRequest *self = (PdConfigurationRequest *)object;

  g_clear_pointer (&self->id, g_free);

  G_OBJECT_CLASS (pd_configuration_request_parent_class)->finalize (object);
}

static void
pd_configuration_request_get_property (GObject    *object,
                                       guint       prop_id,
                                       GValue     *value,
                                       GParamSpec *pspec)
{
  PdConfigurationRequest *self = PD_CONFIGURATION_REQUEST (object);

  switch (prop_id)
    {
    case PROP_ID:
      g_value_set_string (value, pd_configuration_request_get_id (self));
      break;

    case PROP_NAMESPACE:
      g_value_set_enum (value, pd_configuration_request_get_namespace (self));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
pd_configuration_request_set_property (GObject      *object,
                                       guint         prop_id,
                                       const GValue *value,
                                       GParamSpec   *pspec)
{
  PdConfigurationRequest *self = PD_CONFIGURATION_REQUEST (object);

  switch (prop_id)
    {
    case PROP_ID:
      pd_configuration_request_set_id (self, g_value_get_string (value));
      break;

    case PROP_NAMESPACE:
      pd_configuration_request_set_namespace (self, g_value_get_enum (value));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
pd_configuration_request_class_init (PdConfigurationRequestClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->finalize = pd_configuration_request_finalize;
  object_class->get_property = pd_configuration_request_get_property;
  object_class->set_property = pd_configuration_request_set_property;

  properties [PROP_ID] =
    g_param_spec_string ("id",
                         "ID",
                         "The ID of the configuration being requested.",
                         NULL,
                         (G_PARAM_READWRITE | G_PARAM_EXPLICIT_NOTIFY | G_PARAM_STATIC_STRINGS));

  properties [PROP_NAMESPACE] =
    g_param_spec_enum ("namespace",
                       "Namespace",
                       "The namespace of the configuration being requested.",
                       PD_TYPE_CONFIGURATION_NAMESPACE,
                       PD_CONFIGURATION_NAMESPACE_ACQUIRE,
                       (G_PARAM_READWRITE | G_PARAM_EXPLICIT_NOTIFY | G_PARAM_STATIC_STRINGS));

  g_object_class_install_properties (object_class, N_PROPS, properties);
}

static void
pd_configuration_request_init (PdConfigurationRequest *self)
{
}

PdConfigurationRequest *
pd_configuration_request_new (void)
{
  return g_object_new (PD_TYPE_CONFIGURATION_REQUEST, NULL);
}

/**
 * pd_configuration_request_serialize:
 * @self: a #PdConfigurationRequest
 *
 * Returns the serialized message data.
 *
 * Returns: (transfer full): serialized data that the receiver must free.
 */
gchar *
pd_configuration_request_serialize (PdConfigurationRequest *self)
{
  gchar *ret;

  g_return_val_if_fail (PD_IS_CONFIGURATION_REQUEST (self), NULL);

  ret = json_gobject_to_data (G_OBJECT (self), NULL);

  return g_steal_pointer (&ret);
}

void
pd_configuration_request_deserialize (PdConfigurationRequest *self,
                                      const gchar            *data)
{
  GError *err = NULL;
  GObject *object = json_gobject_from_data (PD_TYPE_CONFIGURATION_REQUEST,
                                            data,
                                            -1,
                                            &err);

  if (err != NULL)
    {
      g_critical ("%s", err->message);
      g_error_free (err);
    }

  g_return_if_fail (object != NULL);
  g_return_if_fail (PD_IS_CONFIGURATION_REQUEST (object));

  pd_configuration_request_set_id (
      self,
      pd_configuration_request_get_id (PD_CONFIGURATION_REQUEST (object)));

  pd_configuration_request_set_namespace (
      self,
      pd_configuration_request_get_namespace (PD_CONFIGURATION_REQUEST (object)));

  g_clear_object (&object);
}

const gchar *
pd_configuration_request_get_id (PdConfigurationRequest *self)
{
  g_return_val_if_fail (PD_IS_CONFIGURATION_REQUEST (self), NULL);

  return self->id;
}

void
pd_configuration_request_set_id (PdConfigurationRequest *self,
                                 const gchar            *id)
{
  g_return_if_fail (PD_IS_CONFIGURATION_REQUEST (self));

  if (g_strcmp0 (id, self->id) != 0)
    {
      g_free (self->id);
      self->id = g_strdup (id);
      g_object_notify_by_pspec (G_OBJECT (self), properties [PROP_ID]);
    }
}

PdConfigurationNamespace
pd_configuration_request_get_namespace (PdConfigurationRequest *self)
{
  g_return_val_if_fail (PD_IS_CONFIGURATION_REQUEST (self),
                        PD_CONFIGURATION_NAMESPACE_ACQUIRE);

  return self->namespace;
}

void
pd_configuration_request_set_namespace (PdConfigurationRequest   *self,
                                        PdConfigurationNamespace  namespace)
{
  g_return_if_fail (PD_IS_CONFIGURATION_REQUEST (self));

  self->namespace = namespace;
}
