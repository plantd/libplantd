/*
 * Copyright © the Plantd contributors. All rights reserved.
 *
 * This file is part of libplantd, distributed under the MIT license.
 * For full terms see the included LICENSE file.
 */

#pragma once

#if !defined (PLANTD_INSIDE) && !defined (PLANTD_COMPILATION)
# error "Only <plantd/plantd.h> can be included directly."
#endif

#include <glib-object.h>

G_BEGIN_DECLS

#define PD_TYPE_PROPERTY_REQUEST pd_property_request_get_type ()
G_DECLARE_FINAL_TYPE (PdPropertyRequest, pd_property_request, PD, PROPERTY_REQUEST, GObject)

PdPropertyRequest *pd_property_request_new         (const gchar       *id,
                                                    const gchar       *key,
                                                    const gchar       *value);

gchar             *pd_property_request_serialize   (PdPropertyRequest *self);
void               pd_property_request_deserialize (PdPropertyRequest *self,
                                                    const gchar       *data);

const gchar       *pd_property_request_get_id      (PdPropertyRequest *self);
void               pd_property_request_set_id      (PdPropertyRequest *self,
                                                    const gchar       *id);

const gchar       *pd_property_request_get_key     (PdPropertyRequest *self);
void               pd_property_request_set_key     (PdPropertyRequest *self,
                                                    const gchar       *key);

const gchar       *pd_property_request_get_value   (PdPropertyRequest *self);
void               pd_property_request_set_value   (PdPropertyRequest *self,
                                                    const gchar       *value);

G_END_DECLS
