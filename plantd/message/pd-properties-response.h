/*
 * Copyright © the Plantd contributors. All rights reserved.
 *
 * This file is part of libplantd, distributed under the MIT license.
 * For full terms see the included LICENSE file.
 */

#pragma once

#if !defined (PLANTD_INSIDE) && !defined (PLANTD_COMPILATION)
# error "Only <plantd/plantd.h> can be included directly."
#endif

#include <glib-object.h>

#include <plantd/plantd-types.h>

#include "pd-response.h"

G_BEGIN_DECLS

#define PD_TYPE_PROPERTIES_RESPONSE pd_properties_response_get_type ()
G_DECLARE_FINAL_TYPE (PdPropertiesResponse, pd_properties_response, PD, PROPERTIES_RESPONSE, PdResponse)

PdPropertiesResponse *pd_properties_response_new         (void);

gchar                *pd_properties_response_serialize   (PdPropertiesResponse *self);
void                  pd_properties_response_deserialize (PdPropertiesResponse *self,
                                                          const gchar          *data);

void                  pd_properties_response_add         (PdPropertiesResponse *self,
                                                          PdProperty           *property);
void                  pd_properties_response_remove      (PdPropertiesResponse *self,
                                                          const gchar          *key);
PdProperty           *pd_properties_response_get         (PdPropertiesResponse *self,
                                                          const gchar          *key);
gboolean              pd_properties_response_contains    (PdPropertiesResponse *self,
                                                          const gchar          *key);
GHashTable           *pd_properties_response_get_list    (PdPropertiesResponse *self);
void                  pd_properties_response_set_list    (PdPropertiesResponse *self,
                                                          GHashTable           *properties);

G_END_DECLS
