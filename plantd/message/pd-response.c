/*
 * Copyright © the Plantd contributors. All rights reserved.
 *
 * This file is part of libplantd, distributed under the MIT license.
 * For full terms see the included LICENSE file.
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <json-glib/json-glib.h>

#include "pd-response.h"
#include "pd-message-error.h"

/*
 * SECTION:plantd-response
 * @short_description: Response base class
 *
 * An #PdResponse is a structure for all response message types to derive.
 */
typedef struct
{
  PdMessageError *error;
} PdResponsePrivate;

enum {
  PROP_0,
  PROP_ERROR,
  N_PROPS
};

static GParamSpec *properties [N_PROPS];

G_DEFINE_TYPE_WITH_PRIVATE (PdResponse, pd_response, G_TYPE_OBJECT)

static void
pd_response_finalize (GObject *object)
{
  PdResponse *self = (PdResponse *)object;
  PdResponsePrivate *priv = pd_response_get_instance_private (self);

  g_clear_object (&priv->error);

  G_OBJECT_CLASS (pd_response_parent_class)->finalize (object);
}

static void
pd_response_get_property (GObject    *object,
                          guint       prop_id,
                          GValue     *value,
                          GParamSpec *pspec)
{
  PdResponse *self = PD_RESPONSE (object);
  G_GNUC_UNUSED PdResponsePrivate *priv = pd_response_get_instance_private (self);

  switch (prop_id)
  {
    case PROP_ERROR:
      g_value_take_object (value, pd_response_ref_error (self));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
  }
}

static void
pd_response_set_property (GObject      *object,
                          guint         prop_id,
                          const GValue *value,
                          GParamSpec   *pspec)
{
  PdResponse *self = PD_RESPONSE (object);

  switch (prop_id)
  {
    case PROP_ERROR:
      pd_response_set_error (self, g_value_get_object (value));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
  }
}

static void
pd_response_class_init (PdResponseClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->finalize = pd_response_finalize;
  object_class->get_property = pd_response_get_property;
  object_class->set_property = pd_response_set_property;

  properties [PROP_ERROR] =
    g_param_spec_object ("error",
                         "Error",
                         "The error body of the response",
                         PD_TYPE_MESSAGE_ERROR,
                         (G_PARAM_READWRITE | G_PARAM_EXPLICIT_NOTIFY | G_PARAM_STATIC_STRINGS));

  g_object_class_install_properties (object_class, N_PROPS, properties);
}

static void
pd_response_init (PdResponse *self)
{
}

PdResponse *
pd_response_new (void)
{
  return g_object_new (PD_TYPE_RESPONSE, NULL);
}

/**
 * pd_response_serialize:
 * @self: a #PdResponse
 *
 * Returns the serialized message data.
 *
 * Returns: (transfer full): serialized data that the receiver must free.
 */
gchar *
pd_response_serialize (PdResponse *self)
{
  gchar *ret;

  g_return_val_if_fail (PD_IS_RESPONSE (self), NULL);

  ret = json_gobject_to_data (G_OBJECT (self), NULL);

  return g_steal_pointer (&ret);
}

void pd_response_deserialize (PdResponse *self,
                                const gchar  *data)
{
  g_autoptr (GObject) object = NULL;
  g_autoptr (PdMessageError) error = NULL;

  GError *err = NULL;
  object = json_gobject_from_data (PD_TYPE_RESPONSE,
                                   data,
                                   -1,
                                   &err);

  if (err != NULL)
    {
      g_critical ("%s", err->message);
      g_error_free (err);
    }

  g_return_if_fail (object != NULL);
  g_return_if_fail (PD_IS_RESPONSE (object));

  error = pd_response_ref_error (PD_RESPONSE (object));
  pd_response_set_error (self, error);

  g_clear_object (&object);
}

/**
 * pd_response_get_error:
 * @self: a #PdResponse
 *
 * Retrieve the #PdMessageError contained by the response message.
 *
 * Returns: (transfer none): a #PdMessageError if one is set.
 */
PdMessageError *
pd_response_get_error (PdResponse *self)
{
  PdMessageError *error;

  g_return_val_if_fail (PD_IS_RESPONSE (self), NULL);

  g_object_get (self, "error", &error, NULL);

  return error;
}

/**
 * pd_response_ref_error:
 * @self: a #PdResponse
 *
 * Gets the error for the response message, and returns a new reference
 * to the #PdMessageError.
 *
 * Returns: (transfer full) (nullable): a #PdMessageError or %NULL
 */
PdMessageError *
pd_response_ref_error (PdResponse *self)
{
  PdResponsePrivate *priv;
  PdMessageError *ret = NULL;

  g_return_val_if_fail (PD_IS_RESPONSE (self), NULL);

  priv = pd_response_get_instance_private (self);

  g_set_object (&ret, priv->error);

  return g_steal_pointer (&ret);
}

void
pd_response_set_error (PdResponse     *self,
                       PdMessageError *error)
{
  PdResponsePrivate *priv;

  g_return_if_fail (PD_IS_RESPONSE (self));
  g_return_if_fail (!error || PD_IS_MESSAGE_ERROR (error));

  priv = pd_response_get_instance_private (self);

  if (g_set_object (&priv->error, error))
    g_object_notify_by_pspec (G_OBJECT (self), properties [PROP_ERROR]);
}
