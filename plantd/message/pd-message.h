/*
 * Copyright © the Plantd contributors. All rights reserved.
 *
 * This file is part of libplantd, distributed under the MIT license.
 * For full terms see the included LICENSE file.
 */

#pragma once

#include <glib.h>

#define PLANTD_INSIDE

G_BEGIN_DECLS

// Messages
#include <plantd/message/pd-channel-request.h>
#include <plantd/message/pd-channel-response.h>
#include <plantd/message/pd-channels-response.h>
#include <plantd/message/pd-configuration-request.h>
#include <plantd/message/pd-configuration-response.h>
#include <plantd/message/pd-configurations-response.h>
#include <plantd/message/pd-empty.h>
#include <plantd/message/pd-event-request.h>
#include <plantd/message/pd-event-response.h>
#include <plantd/message/pd-job-request.h>
#include <plantd/message/pd-job-response.h>
#include <plantd/message/pd-jobs-response.h>
#include <plantd/message/pd-job-status-response.h>
#include <plantd/message/pd-message-error.h>
#include <plantd/message/pd-property-request.h>
#include <plantd/message/pd-property-response.h>
#include <plantd/message/pd-properties-request.h>
#include <plantd/message/pd-properties-response.h>
#include <plantd/message/pd-request.h>
#include <plantd/message/pd-response.h>
#include <plantd/message/pd-settings-request.h>
#include <plantd/message/pd-settings-response.h>
#include <plantd/message/pd-status-request.h>
#include <plantd/message/pd-status-response.h>

G_END_DECLS

#undef PLANTD_INSIDE
