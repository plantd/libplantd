/*
 * Copyright © the Plantd contributors. All rights reserved.
 *
 * This file is part of libplantd, distributed under the MIT license.
 * For full terms see the included LICENSE file.
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <json-glib/json-glib.h>

#include "pd-request.h"
#include "pd-event-request.h"
#include "core/pd-event.h"

/*
 * PdEventRequest:
 *
 * Represents a request message for a single event.
 */
struct _PdEventRequest
{
  PdRequest  parent;
  PdEvent   *event;
};

enum {
  PROP_0,
  PROP_EVENT,
  N_PROPS
};

static GParamSpec *properties [N_PROPS];

G_DEFINE_TYPE (PdEventRequest, pd_event_request, PD_TYPE_REQUEST)

static void
pd_event_request_finalize (GObject *object)
{
  PdEventRequest *self = (PdEventRequest *)object;

  g_clear_object (&self->event);

  G_OBJECT_CLASS (pd_event_request_parent_class)->finalize (object);
}

static void
pd_event_request_get_property (GObject    *object,
                               guint       prop_id,
                               GValue     *value,
                               GParamSpec *pspec)
{
  PdEventRequest *self = PD_EVENT_REQUEST (object);

  switch (prop_id)
    {
    case PROP_EVENT:
      /*g_value_set_object (value, pd_event_request_get_event (self));*/
      g_value_set_object (value, self->event);
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
pd_event_request_set_property (GObject      *object,
                               guint         prop_id,
                               const GValue *value,
                               GParamSpec   *pspec)
{
  PdEventRequest *self = PD_EVENT_REQUEST (object);

  switch (prop_id)
    {
    case PROP_EVENT:
      pd_event_request_set_event (self, g_value_get_object (value));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
pd_event_request_class_init (PdEventRequestClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->finalize = pd_event_request_finalize;
  object_class->get_property = pd_event_request_get_property;
  object_class->set_property = pd_event_request_set_property;

  properties [PROP_EVENT] =
    g_param_spec_object ("event",
                         "Event",
                         "The event to use in the request.",
                         PD_TYPE_EVENT,
                         (G_PARAM_READWRITE | G_PARAM_EXPLICIT_NOTIFY));

  g_object_class_install_properties (object_class, N_PROPS, properties);
}

static void
pd_event_request_init (PdEventRequest *self)
{
}

PdEventRequest *
pd_event_request_new (void)
{
  return g_object_new (PD_TYPE_EVENT_REQUEST, NULL);
}

/**
 * pd_event_request_serialize:
 * @self: a #PdEventRequest
 *
 * Returns the serialized message data.
 *
 * Returns: (transfer full): serialized data that the receiver must free.
 */
gchar *
pd_event_request_serialize (PdEventRequest *self)
{
  gchar *ret;

  g_return_val_if_fail (PD_IS_EVENT_REQUEST (self), NULL);

  ret = json_gobject_to_data (G_OBJECT (self), NULL);

  return g_steal_pointer (&ret);
}

void
pd_event_request_deserialize (PdEventRequest *self,
                              const gchar    *data)
{
  GError *err = NULL;
  GObject *object = json_gobject_from_data (PD_TYPE_EVENT_REQUEST,
                                            data,
                                            -1,
                                            &err);

  if (err != NULL)
    {
      g_critical ("%s", err->message);
      g_error_free (err);
    }

  g_return_if_fail (object != NULL);
  g_return_if_fail (PD_IS_EVENT_REQUEST (object));

  pd_event_request_set_event (self,
      pd_event_request_get_event (PD_EVENT_REQUEST (object)));

  g_object_unref (object);
}

/**
 * pd_event_request_get_event:
 * @self: #PdEventRequest instance
 * 
 * Returns the PdEvent object
 *
 * Returns: (transfer full): The #PdEvent
 */
PdEvent *
pd_event_request_get_event (PdEventRequest *self)
{
  PdEvent *event;

  g_return_val_if_fail (PD_IS_EVENT_REQUEST (self), NULL);

  g_object_get (self, "event", &event, NULL);

  return event;
}

void
pd_event_request_set_event (PdEventRequest *self,
                            PdEvent        *event)
{
  g_return_if_fail (PD_IS_EVENT_REQUEST (self));
  g_return_if_fail (PD_IS_EVENT (event));

  if (self->event)
    g_object_unref (self->event);

  if (event)
    g_object_ref (event);

  self->event = event;

  g_object_notify_by_pspec (G_OBJECT (self), properties [PROP_EVENT]);
}
