/*
 * Copyright © the Plantd contributors. All rights reserved.
 *
 * This file is part of libplantd, distributed under the MIT license.
 * For full terms see the included LICENSE file.
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <json-glib/json-glib.h>

#include "pd-response.h"
#include "pd-configuration-response.h"
#include "core/pd-configuration.h"

/*
 * PdConfigurationResponse:
 *
 * Represents a response to a request for an application configuration.
 */
struct _PdConfigurationResponse
{
  PdResponse       parent;
  PdConfiguration *configuration;
};

enum {
  PROP_0,
  PROP_CONFIGURATION,
  N_PROPS
};

static GParamSpec *properties [N_PROPS];

G_DEFINE_TYPE (PdConfigurationResponse, pd_configuration_response, PD_TYPE_RESPONSE)

static void
pd_configuration_response_finalize (GObject *object)
{
  PdConfigurationResponse *self = (PdConfigurationResponse *)object;

  g_clear_object (&self->configuration);

  G_OBJECT_CLASS (pd_configuration_response_parent_class)->finalize (object);
}

static void
pd_configuration_response_get_property (GObject    *object,
                                        guint       prop_id,
                                        GValue     *value,
                                        GParamSpec *pspec)
{
  PdConfigurationResponse *self = PD_CONFIGURATION_RESPONSE (object);

  switch (prop_id)
    {
    case PROP_CONFIGURATION:
      g_value_take_object (value, pd_configuration_response_ref_configuration (self));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
pd_configuration_response_set_property (GObject      *object,
                                        guint         prop_id,
                                        const GValue *value,
                                        GParamSpec   *pspec)
{
  PdConfigurationResponse *self = PD_CONFIGURATION_RESPONSE (object);

  switch (prop_id)
    {
    case PROP_CONFIGURATION:
      pd_configuration_response_set_configuration (self, g_value_get_object (value));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
pd_configuration_response_class_init (PdConfigurationResponseClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->finalize = pd_configuration_response_finalize;
  object_class->get_property = pd_configuration_response_get_property;
  object_class->set_property = pd_configuration_response_set_property;

  properties [PROP_CONFIGURATION] =
    g_param_spec_object ("configuration",
                         "Configuration",
                         "The configuration in the response.",
                         PD_TYPE_CONFIGURATION,
                         (G_PARAM_READWRITE | G_PARAM_EXPLICIT_NOTIFY | G_PARAM_STATIC_STRINGS));

  g_object_class_install_properties (object_class, N_PROPS, properties);
}

static void
pd_configuration_response_init (PdConfigurationResponse *self)
{
  self->configuration = pd_configuration_new ();
}

PdConfigurationResponse *
pd_configuration_response_new (void)
{
  return g_object_new (PD_TYPE_CONFIGURATION_RESPONSE, NULL);
}

/**
 * pd_configuration_response_serialize:
 * @self: a #PdConfigurationResponse
 *
 * Returns the serialized message data.
 *
 * Returns: (transfer full): serialized data that the receiver must free.
 */
gchar *
pd_configuration_response_serialize (PdConfigurationResponse *self)
{
  gchar *ret;

  g_return_val_if_fail (PD_IS_CONFIGURATION_RESPONSE (self), NULL);

  ret = json_gobject_to_data (G_OBJECT (self), NULL);

  return g_steal_pointer (&ret);
}

void
pd_configuration_response_deserialize (PdConfigurationResponse *self,
                                       const gchar             *data)
{
  g_autoptr (GObject) object = NULL;
  g_autoptr (PdConfiguration) configuration = NULL;

  GError *err = NULL;
  object = json_gobject_from_data (PD_TYPE_CONFIGURATION_RESPONSE,
                                   data,
                                   -1,
                                   &err);

  if (err != NULL)
    {
      g_critical ("%s", err->message);
      g_error_free (err);
    }

  g_return_if_fail (object != NULL);
  g_return_if_fail (PD_IS_CONFIGURATION_RESPONSE (object));

  configuration = pd_configuration_response_ref_configuration (
      PD_CONFIGURATION_RESPONSE (object));

  pd_configuration_response_set_configuration (self, configuration);

  g_clear_object (&object);
}

/**
 * pd_configuration_response_get_configuration:
 * @self: #PdConfigurationResponse instance
 *
 * Returns: (transfer full): The #PdConfiguration
 */
PdConfiguration *
pd_configuration_response_get_configuration (PdConfigurationResponse *self)
{
  PdConfiguration *configuration;

  g_return_val_if_fail (PD_IS_CONFIGURATION_RESPONSE (self), NULL);

  g_object_get (self, "configuration", &configuration, NULL);

  return configuration;
}

/**
 * pd_configuration_response_ref_configuration:
 * @self: a #PdConfigurationResponse
 *
 * Gets the configuration data for the response, and returns a new reference
 *
 * Returns: (transfer full) (nullable): a #PdConfiguration or %NULL
 */
PdConfiguration *
pd_configuration_response_ref_configuration (PdConfigurationResponse *self)
{
  PdConfiguration *ret = NULL;

  g_return_val_if_fail (PD_IS_CONFIGURATION_RESPONSE (self), NULL);

  g_set_object (&ret, self->configuration);

  return g_steal_pointer (&ret);
}

void
pd_configuration_response_set_configuration (PdConfigurationResponse *self,
                                             PdConfiguration         *configuration)
{
  g_return_if_fail (PD_IS_CONFIGURATION_RESPONSE (self));
  g_return_if_fail (PD_IS_CONFIGURATION (configuration));

  if (g_set_object (&self->configuration, configuration))
    g_object_notify_by_pspec (G_OBJECT (self), properties [PROP_CONFIGURATION]);
}
