/*
 * Copyright © the Plantd contributors. All rights reserved.
 *
 * This file is part of libplantd, distributed under the MIT license.
 * For full terms see the included LICENSE file.
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <json-glib/json-glib.h>

#include "pd-response.h"
#include "pd-channels-response.h"
#include "core/pd-channel.h"

/*
 * PdChannelsResponse:
 *
 * Represents a response message containing a list of channels.
 */
struct _PdChannelsResponse
{
  PdResponse  parent;
  GPtrArray  *channels;
};

enum {
  PROP_0,
  PROP_CHANNELS,
  N_PROPS
};

static GParamSpec *properties [N_PROPS];

static void json_serializable_iface_init (gpointer g_iface);

G_DEFINE_TYPE_WITH_CODE (PdChannelsResponse, pd_channels_response, PD_TYPE_RESPONSE,
                         G_IMPLEMENT_INTERFACE (JSON_TYPE_SERIALIZABLE,
                                                json_serializable_iface_init));

static JsonNode *
pd_channels_response_serialize_property (JsonSerializable *serializable,
                                         const gchar      *name,
                                         const GValue     *value,
                                         GParamSpec       *pspec)
{
  JsonNode *retval = NULL;

  if (g_strcmp0 (name, "channels") == 0)
    {
      g_autoptr (GPtrArray) channels = NULL;
      JsonArray *arr = NULL;

      retval = json_node_new (JSON_NODE_ARRAY);
      arr = json_array_new ();

      channels = g_value_get_boxed (value);

      for (int i = 0; i < channels->len; i++)
        {
          JsonNode *node = NULL;
          JsonObject *obj = NULL;

          node = json_gobject_serialize (channels->pdata[i]);

          if (JSON_NODE_HOLDS_OBJECT (node))
            {
              obj = json_node_get_object (node);
              json_array_add_object_element (arr, obj);
            }
        }

      json_node_take_array (retval, arr);
    }
  else
    {
      GValue copy = { 0, };

      retval = json_node_new (JSON_NODE_VALUE);

      g_value_init (&copy, G_PARAM_SPEC_VALUE_TYPE (pspec));
      g_value_copy (value, &copy);
      json_node_set_value (retval, &copy);
      g_value_unset (&copy);
    }

  return retval;
}

static gboolean
pd_channels_response_deserialize_property (JsonSerializable *serializable,
                                           const gchar      *name,
                                           GValue           *value,
                                           GParamSpec       *pspec,
                                           JsonNode         *property_node)
{
  gboolean retval = FALSE;
  JsonArray *arr = NULL;

  if (g_strcmp0 (name, "channels") == 0)
    {
      g_autoptr (GPtrArray) channels = NULL;
      arr = json_node_get_array (property_node);
      channels = g_ptr_array_new ();

      for (gint i = 0; i < json_array_get_length (arr); i++)
        {
          g_autoptr (PdChannel) channel = NULL;
          JsonNode *node = NULL;

          node = json_array_get_element (arr, i);

          channel = PD_CHANNEL (json_gobject_deserialize (PD_TYPE_CHANNEL, node));
          g_return_val_if_fail (PD_IS_CHANNEL (channel), FALSE);
          g_ptr_array_add (channels, channel);
        }

      g_value_set_boxed (value, channels);

      retval = TRUE;
    }

  return retval;
}

static void
json_serializable_iface_init (gpointer g_iface)
{
  JsonSerializableIface *iface = g_iface;

  iface->serialize_property = pd_channels_response_serialize_property;
  iface->deserialize_property = pd_channels_response_deserialize_property;
}

static void
pd_channels_response_finalize (GObject *object)
{
  PdChannelsResponse *self = (PdChannelsResponse *)object;

  g_ptr_array_unref (self->channels);

  G_OBJECT_CLASS (pd_channels_response_parent_class)->finalize (object);
}

static void
pd_channels_response_get_property (GObject    *object,
                                   guint       prop_id,
                                   GValue     *value,
                                   GParamSpec *pspec)
{
  PdChannelsResponse *self = PD_CHANNELS_RESPONSE (object);

  switch (prop_id)
  {
    case PROP_CHANNELS:
      g_value_set_boxed (value, pd_channels_response_get_channels (self));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
  }
}

static void
pd_channels_response_set_property (GObject      *object,
                                   guint         prop_id,
                                   const GValue *value,
                                   GParamSpec   *pspec)
{
  PdChannelsResponse *self = PD_CHANNELS_RESPONSE (object);

  switch (prop_id)
  {
    case PROP_CHANNELS:
      pd_channels_response_set_channels (self, g_value_get_boxed (value));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
  }
}

static void
pd_channels_response_class_init (PdChannelsResponseClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->finalize = pd_channels_response_finalize;
  object_class->get_property = pd_channels_response_get_property;
  object_class->set_property = pd_channels_response_set_property;

  properties [PROP_CHANNELS] =
    g_param_spec_boxed ("channels",
                        "Channels",
                        "The list of channels to provide in the response.",
                        G_TYPE_PTR_ARRAY,
                        (G_PARAM_READWRITE | G_PARAM_EXPLICIT_NOTIFY));

  g_object_class_install_properties (object_class, N_PROPS, properties);
}

static void
pd_channels_response_init (PdChannelsResponse *self)
{
}

PdChannelsResponse *
pd_channels_response_new (void)
{
  g_autoptr (GPtrArray) channels = NULL;

  channels = g_ptr_array_new ();

  return g_object_new (PD_TYPE_CHANNELS_RESPONSE,
                       "channels", channels,
                       NULL);
}

/**
 * pd_channels_response_serialize:
 * @self: a #PdChannelsResponse
 *
 * Returns the serialized message data.
 *
 * Returns: (transfer full): serialized data that the receiver must free.
 */
gchar *
pd_channels_response_serialize (PdChannelsResponse *self)
{
  gchar *ret;

  g_return_val_if_fail (PD_IS_CHANNELS_RESPONSE (self), NULL);

  ret = json_gobject_to_data (G_OBJECT (self), NULL);

  return g_steal_pointer (&ret);
}

void
pd_channels_response_deserialize (PdChannelsResponse *self,
                                  const gchar        *data)
{
  GError *err = NULL;
  GObject *object = json_gobject_from_data (PD_TYPE_CHANNELS_RESPONSE,
                                            data,
                                            -1,
                                            &err);

  if (err != NULL)
    {
      g_critical ("%s", err->message);
      g_error_free (err);
    }

  g_return_if_fail (object != NULL);
  g_return_if_fail (PD_IS_CHANNELS_RESPONSE (object));

  pd_channels_response_set_channels (self,
                                     pd_channels_response_get_channels (
                                       PD_CHANNELS_RESPONSE (object)));

  g_object_unref (object);
}

/**
 * pd_channels_response_add_channel:
 * @self: a #PdChannelsResponse
 * @channel: a #PdChannel to add
 *
 * Adds a channel to the list.
 */
void
pd_channels_response_add_channel (PdChannelsResponse *self,
                                  PdChannel          *channel)
{
  g_return_if_fail (PD_IS_CHANNELS_RESPONSE (self));
  g_return_if_fail (PD_IS_CHANNEL (channel));

  g_ptr_array_add (self->channels, channel);
}

/**
 * pd_channels_response_remove_channel:
 * @self: a #PdChannelsResponse
 * @channel: a #PdChannel to remove if it exists
 *
 * Checks if the channel exists in the list and removes it if found.
 */
void
pd_channels_response_remove_channel (PdChannelsResponse *self,
                                     PdChannel          *channel)
{
  g_return_if_fail (PD_IS_CHANNELS_RESPONSE (self));
  g_return_if_fail (PD_IS_CHANNEL (channel));

  for (int i = 0; i < self->channels->len; i++)
    {
      // TODO: add ID to channel and compare here, endpoint is wrong
      if (g_strcmp0 (pd_channel_get_endpoint (channel),
                     pd_channel_get_endpoint (self->channels->pdata[i])) == 0)
        self->channels = g_ptr_array_remove_index (self->channels, i);
    }
}

/**
 * pd_channels_response_get_channels:
 * @self: a #PdChannelsResponse
 *
 * Retrieves the list of channels from the response.
 *
 * Returns: (element-type Pd.Channel) (transfer full): an array of
 *          #PdChannel objects, free the array with g_ptr_array_free when
 *          done.
 */
GPtrArray *
pd_channels_response_get_channels (PdChannelsResponse *self)
{
  g_return_val_if_fail (PD_IS_CHANNELS_RESPONSE (self), NULL);

  return self->channels;
}

/**
 * pd_channels_response_set_channels:
 * @self: a #PdChannelsResponse
 * @channels: (element-type Pd.Channel): an array of #PdChannel objects
 *            to set.
 *
 * Assigns a list of channels to the response.
 */
void
pd_channels_response_set_channels (PdChannelsResponse *self,
                                   GPtrArray          *channels)
{
  g_return_if_fail (PD_IS_CHANNELS_RESPONSE (self));

  if (self->channels == channels)
    return;

  if (self->channels != NULL)
    g_ptr_array_unref (self->channels);

  self->channels = channels;

  if (self->channels != NULL)
    g_ptr_array_ref (self->channels);

  g_object_notify_by_pspec (G_OBJECT (self), properties [PROP_CHANNELS]);
}
