/*
 * Copyright © the Plantd contributors. All rights reserved.
 *
 * This file is part of libplantd, distributed under the MIT license.
 * For full terms see the included LICENSE file.
 */

#pragma once

#if !defined (PLANTD_INSIDE) && !defined (PLANTD_COMPILATION)
# error "Only <plantd/plantd.h> can be included directly."
#endif

#include <glib-object.h>

#include <plantd/plantd-types.h>

#include "pd-response.h"

G_BEGIN_DECLS

#define PD_TYPE_JOB_STATUS_RESPONSE pd_job_status_response_get_type ()
G_DECLARE_FINAL_TYPE (PdJobStatusResponse, pd_job_status_response, PD, JOB_STATUS_RESPONSE, PdResponse)

PdJobStatusResponse *pd_job_status_response_new         (void);

gchar               *pd_job_status_response_serialize   (PdJobStatusResponse *self);
void                 pd_job_status_response_deserialize (PdJobStatusResponse *self,
                                                         const gchar         *data);

PdJob               *pd_job_status_response_get_job     (PdJobStatusResponse *self);
void                 pd_job_status_response_set_job     (PdJobStatusResponse *self,
                                                         PdJob               *job);

G_END_DECLS
