/*
 * Copyright © the Plantd contributors. All rights reserved.
 *
 * This file is part of libplantd, distributed under the MIT license.
 * For full terms see the included LICENSE file.
 */

#pragma once

#if !defined (PLANTD_INSIDE) && !defined (PLANTD_COMPILATION)
# error "Only <plantd/plantd.h> can be included directly."
#endif

#include <glib-object.h>

G_BEGIN_DECLS

#define PD_TYPE_SETTINGS_RESPONSE pd_settings_response_get_type ()
G_DECLARE_FINAL_TYPE (PdSettingsResponse, pd_settings_response, PD, SETTINGS_RESPONSE, GObject)

PdSettingsResponse *pd_settings_response_new            (void);

gchar              *pd_settings_response_serialize      (PdSettingsResponse *self);
void                pd_settings_response_deserialize    (PdSettingsResponse *self,
                                                         const gchar        *data);

// XXX: why is there an ID in this? doesn't make sense
const gchar        *pd_settings_response_get_id         (PdSettingsResponse *self);
gchar              *pd_settings_response_dup_id         (PdSettingsResponse *self);
void                pd_settings_response_set_id         (PdSettingsResponse *self,
                                                         const gchar        *id);

GHashTable         *pd_settings_response_get_settings   (PdSettingsResponse *self);
void                pd_settings_response_set_settings   (PdSettingsResponse *self,
                                                         GHashTable         *settings);

void                pd_settings_response_add_setting    (PdSettingsResponse *self,
                                                         const gchar        *key,
                                                         const gchar        *value);
void                pd_settings_response_remove_setting (PdSettingsResponse *self,
                                                         const gchar        *key);
const gchar        *pd_settings_response_get_setting    (PdSettingsResponse *self,
                                                         const gchar        *key);

G_END_DECLS
