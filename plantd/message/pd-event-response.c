/*
 * Copyright © the Plantd contributors. All rights reserved.
 *
 * This file is part of libplantd, distributed under the MIT license.
 * For full terms see the included LICENSE file.
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include "pd-response.h"
#include "pd-event-response.h"

/*
 * PdEventResponse:
 *
 * Represents a response message for a single event.
 */
struct _PdEventResponse
{
  PdResponse parent;
};

G_DEFINE_TYPE (PdEventResponse, pd_event_response, PD_TYPE_RESPONSE)

static void
pd_event_response_finalize (GObject *object)
{
  G_GNUC_UNUSED PdEventResponse *self = (PdEventResponse *)object;

  G_OBJECT_CLASS (pd_event_response_parent_class)->finalize (object);
}

static void
pd_event_response_class_init (PdEventResponseClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->finalize = pd_event_response_finalize;
}

static void
pd_event_response_init (PdEventResponse *self)
{
}

PdEventResponse *
pd_event_response_new (void)
{
  PdEventResponse *object = g_object_new (PD_TYPE_EVENT_RESPONSE, NULL);

  // TODO: add setup

  return object;
}
