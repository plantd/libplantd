/*
 * Copyright © the Plantd contributors. All rights reserved.
 *
 * This file is part of libplantd, distributed under the MIT license.
 * For full terms see the included LICENSE file.
 */

#pragma once

#if !defined (PLANTD_INSIDE) && !defined (PLANTD_COMPILATION)
# error "Only <plantd/plantd.h> can be included directly."
#endif

#include <glib-object.h>

#include <plantd/plantd-types.h>

G_BEGIN_DECLS

#define PD_TYPE_STATUS_RESPONSE pd_status_response_get_type ()
G_DECLARE_FINAL_TYPE (PdStatusResponse, pd_status_response, PD, STATUS_RESPONSE, GObject)

PdStatusResponse *pd_status_response_new           (void);
PdStatusResponse *pd_status_response_new_from_data (const guint8     *data);

guint8           *pd_status_response_to_data       (PdStatusResponse *self);

gchar            *pd_status_response_serialize     (PdStatusResponse *self);
void              pd_status_response_deserialize   (PdStatusResponse *self,
                                                    const gchar      *data);

PdStatus         *pd_status_response_get_status    (PdStatusResponse *self);
PdStatus         *pd_status_response_ref_status    (PdStatusResponse *self);
void              pd_status_response_set_status    (PdStatusResponse *self,
                                                    PdStatus         *status);

G_END_DECLS
