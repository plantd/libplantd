/*
 * Copyright © the Plantd contributors. All rights reserved.
 *
 * This file is part of libplantd, distributed under the MIT license.
 * For full terms see the included LICENSE file.
 */

#pragma once

#if !defined (PLANTD_INSIDE) && !defined (PLANTD_COMPILATION)
# error "Only <plantd/plantd.h> can be included directly."
#endif

#include <glib-object.h>

#include <plantd/plantd-types.h>

#include "pd-request.h"

G_BEGIN_DECLS

#define PD_TYPE_EVENT_REQUEST pd_event_request_get_type ()
G_DECLARE_FINAL_TYPE (PdEventRequest, pd_event_request, PD, EVENT_REQUEST, PdRequest)

PdEventRequest *pd_event_request_new         (void);

gchar          *pd_event_request_serialize   (PdEventRequest *self);
void            pd_event_request_deserialize (PdEventRequest *self,
                                              const gchar    *data);

PdEvent        *pd_event_request_get_event   (PdEventRequest *self);
void            pd_event_request_set_event   (PdEventRequest *self,
                                              PdEvent        *event);

G_END_DECLS
