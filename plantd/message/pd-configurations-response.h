/*
 * Copyright © the Plantd contributors. All rights reserved.
 *
 * This file is part of libplantd, distributed under the MIT license.
 * For full terms see the included LICENSE file.
 */

#pragma once

#if !defined (PLANTD_INSIDE) && !defined (PLANTD_COMPILATION)
# error "Only <plantd/plantd.h> can be included directly."
#endif

#include <glib-object.h>

#include <plantd/plantd-types.h>

#include "pd-response.h"

G_BEGIN_DECLS

#define PD_TYPE_CONFIGURATIONS_RESPONSE pd_configurations_response_get_type ()
G_DECLARE_FINAL_TYPE (PdConfigurationsResponse, pd_configurations_response, PD, CONFIGURATIONS_RESPONSE, PdResponse)

PdConfigurationsResponse *pd_configurations_response_new                    (void);

gchar                      *pd_configurations_response_serialize            (PdConfigurationsResponse *self);
void                        pd_configurations_response_deserialize          (PdConfigurationsResponse *self,
                                                                             const gchar              *data);

void                        pd_configurations_response_add_configuration    (PdConfigurationsResponse *self,
                                                                             PdConfiguration          *configuration);
void                        pd_configurations_response_remove_configuration (PdConfigurationsResponse *self,
                                                                             PdConfiguration          *configuration);

GPtrArray                  *pd_configurations_response_get_configurations   (PdConfigurationsResponse *self);
void                        pd_configurations_response_set_configurations   (PdConfigurationsResponse *self,
                                                                             GPtrArray                *configurations);

G_END_DECLS
