/*
 * Copyright © the Plantd contributors. All rights reserved.
 *
 * This file is part of libplantd, distributed under the MIT license.
 * For full terms see the included LICENSE file.
 */

#pragma once

#if !defined (PLANTD_INSIDE) && !defined (PLANTD_COMPILATION)
# error "Only <plantd/plantd.h> can be included directly."
#endif

#include <glib-object.h>

G_BEGIN_DECLS

#define PD_TYPE_STATUS_REQUEST pd_status_request_get_type ()
G_DECLARE_FINAL_TYPE (PdStatusRequest, pd_status_request, PD, STATUS_REQUEST, GObject)

PdStatusRequest *pd_status_request_new           (void);
PdStatusRequest *pd_status_request_new_from_data (const guint8    *data);

guint8          *pd_status_request_to_data       (PdStatusRequest *self);

gchar           *pd_status_request_serialize     (PdStatusRequest *self);
void             pd_status_request_deserialize   (PdStatusRequest *self,
                                                  const gchar     *data);

G_END_DECLS
