/*
 * Copyright © the Plantd contributors. All rights reserved.
 *
 * This file is part of libplantd, distributed under the MIT license.
 * For full terms see the included LICENSE file.
 */

#pragma once

#if !defined (PLANTD_INSIDE) && !defined (PLANTD_COMPILATION)
# error "Only <plantd/plantd.h> can be included directly."
#endif

#include <glib-object.h>

G_BEGIN_DECLS

#define PD_TYPE_SETTINGS_REQUEST pd_settings_request_get_type ()
G_DECLARE_FINAL_TYPE (PdSettingsRequest, pd_settings_request, PD, SETTINGS_REQUEST, GObject)

PdSettingsRequest *pd_settings_request_new         (void);

gchar             *pd_settings_request_serialize   (PdSettingsRequest *self);
void               pd_settings_request_deserialize (PdSettingsRequest *self,
                                                    const gchar       *data);

G_END_DECLS
