/*
 * Copyright © the Plantd contributors. All rights reserved.
 *
 * This file is part of libplantd, distributed under the MIT license.
 * For full terms see the included LICENSE file.
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <json-glib/json-glib.h>

#include "pd-property-response.h"
#include "pd-response.h"
#include "core/pd-property.h"

/*
 * PdPropertyResponse:
 *
 * Represents a ...
 */
struct _PdPropertyResponse
{
  PdResponse  parent;
  PdProperty *field;
};

enum {
  PROP_0,
  PROP_FIELD,
  N_PROPS
};

static GParamSpec *properties [N_PROPS];

G_DEFINE_TYPE (PdPropertyResponse, pd_property_response, PD_TYPE_RESPONSE)

static void
pd_property_response_finalize (GObject *object)
{
  PdPropertyResponse *self = (PdPropertyResponse *)object;

  g_clear_object (&self->field);

  G_OBJECT_CLASS (pd_property_response_parent_class)->finalize (object);
}

static void
pd_property_response_get_property (GObject    *object,
                                   guint       prop_id,
                                   GValue     *value,
                                   GParamSpec *pspec)
{
  PdPropertyResponse *self = PD_PROPERTY_RESPONSE (object);

  switch (prop_id)
    {
    case PROP_FIELD:
      g_value_take_object (value, pd_property_response_ref (self));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
pd_property_response_set_property (GObject      *object,
                                   guint         prop_id,
                                   const GValue *value,
                                   GParamSpec   *pspec)
{
  PdPropertyResponse *self = PD_PROPERTY_RESPONSE (object);

  switch (prop_id)
    {
    case PROP_FIELD:
      pd_property_response_set (self, g_value_get_object (value));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
pd_property_response_class_init (PdPropertyResponseClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->finalize = pd_property_response_finalize;
  object_class->get_property = pd_property_response_get_property;
  object_class->set_property = pd_property_response_set_property;

  properties [PROP_FIELD] =
    g_param_spec_object ("property",
                         "Property",
                         "The property field of the response",
                         PD_TYPE_PROPERTY,
                         (G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));

  g_object_class_install_properties (object_class, N_PROPS, properties);
}

static void
pd_property_response_init (PdPropertyResponse *self)
{
}

PdPropertyResponse *
pd_property_response_new (void)
{
  return g_object_new (PD_TYPE_PROPERTY_RESPONSE, NULL);
}

/**
 * pd_property_response_serialize:
 * @self: a #PdPropertyResponse
 *
 * Returns the serialized message data.
 *
 * Returns: (transfer full): serialized data that the receiver must free.
 */
gchar *
pd_property_response_serialize (PdPropertyResponse *self)
{
  gchar *ret = NULL;

  g_return_val_if_fail (PD_IS_PROPERTY_RESPONSE (self), NULL);

  ret = json_gobject_to_data (G_OBJECT (self), NULL);

  return g_steal_pointer (&ret);
}

void
pd_property_response_deserialize (PdPropertyResponse *self,
                                  const gchar        *data)
{
  g_autoptr (GObject) object = NULL;
  g_autoptr (PdProperty) field = NULL;

  GError *err = NULL;
  object = json_gobject_from_data (PD_TYPE_PROPERTY_RESPONSE,
                                   data,
                                   -1,
                                   &err);

  if (err != NULL)
    {
      g_critical ("%s", err->message);
      g_error_free (err);
    }

  g_return_if_fail (object != NULL);
  g_return_if_fail (PD_IS_PROPERTY_RESPONSE (object));

  field = pd_property_response_ref (PD_PROPERTY_RESPONSE (object));
  pd_property_response_set (self, field);

  g_clear_object (&object);
}

/**
 * pd_property_response_get:
 * @self: a #PdPropertyResponse
 *
 * Retrieve the #PdProperty contained by the response message.
 *
 * Returns: (transfer none): a #PdProperty if one is set.
 */
PdProperty *
pd_property_response_get (PdPropertyResponse *self)
{
  PdProperty *field;

  g_return_val_if_fail (PD_IS_PROPERTY_RESPONSE (self), NULL);

  g_object_get (self, "property", &field, NULL);

  return field;
}

/**
 * pd_property_response_ref:
 * @self: a #PdPropertyResponse
 *
 * Gets the property field for the response, and returns a new reference
 * to the #PdProperty.
 *
 * Returns: (transfer full) (nullable): a #PdProperty or %NULL
 */
PdProperty *
pd_property_response_ref (PdPropertyResponse *self)
{
  PdProperty *ret = NULL;

  g_return_val_if_fail (PD_IS_PROPERTY_RESPONSE (self), NULL);

  g_set_object (&ret, self->field);

  return g_steal_pointer (&ret);
}

void
pd_property_response_set (PdPropertyResponse *self,
                          PdProperty         *field)
{
  g_return_if_fail (PD_IS_PROPERTY_RESPONSE (self));
  g_return_if_fail (PD_IS_PROPERTY (field));

  if (self->field)
    g_object_unref (self->field);

  if (field)
    g_object_ref (field);

  self->field = field;
}
