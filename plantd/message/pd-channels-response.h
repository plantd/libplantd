/*
 * Copyright © the Plantd contributors. All rights reserved.
 *
 * This file is part of libplantd, distributed under the MIT license.
 * For full terms see the included LICENSE file.
 */

#pragma once

#if !defined (PLANTD_INSIDE) && !defined (PLANTD_COMPILATION)
# error "Only <plantd/plantd.h> can be included directly."
#endif

#include <glib-object.h>

#include <plantd/plantd-types.h>

#include "pd-response.h"

G_BEGIN_DECLS

#define PD_TYPE_CHANNELS_RESPONSE pd_channels_response_get_type ()
G_DECLARE_FINAL_TYPE (PdChannelsResponse, pd_channels_response, PD, CHANNELS_RESPONSE, PdResponse)

PdChannelsResponse *pd_channels_response_new            (void);

gchar              *pd_channels_response_serialize      (PdChannelsResponse *self);
void                pd_channels_response_deserialize    (PdChannelsResponse *self,
                                                         const gchar        *data);

void                pd_channels_response_add_channel    (PdChannelsResponse *self,
                                                         PdChannel          *channel);
void                pd_channels_response_remove_channel (PdChannelsResponse *self,
                                                         PdChannel          *channel);

GPtrArray          *pd_channels_response_get_channels   (PdChannelsResponse *self);
void                pd_channels_response_set_channels   (PdChannelsResponse *self,
                                                         GPtrArray          *channels);

G_END_DECLS
