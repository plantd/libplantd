/*
 * Copyright © the Plantd contributors. All rights reserved.
 *
 * This file is part of libplantd, distributed under the MIT license.
 * For full terms see the included LICENSE file.
 */

#pragma once

#if !defined (PLANTD_INSIDE) && !defined (PLANTD_COMPILATION)
# error "Only <plantd/plantd.h> can be included directly."
#endif

#include <glib-object.h>

#include "pd-request.h"

G_BEGIN_DECLS

#define PD_TYPE_CHANNEL_REQUEST pd_channel_request_get_type ()
G_DECLARE_FINAL_TYPE (PdChannelRequest, pd_channel_request, PD, CHANNEL_REQUEST, PdRequest)

PdChannelRequest *pd_channel_request_new         (void);

gchar            *pd_channel_request_serialize   (PdChannelRequest *self);
void              pd_channel_request_deserialize (PdChannelRequest *self,
                                                  const gchar      *data);

const gchar      *pd_channel_request_get_id      (PdChannelRequest *self);
void              pd_channel_request_set_id      (PdChannelRequest *self,
                                                  const gchar      *id);

G_END_DECLS
