/*
 * Copyright © the Plantd contributors. All rights reserved.
 *
 * This file is part of libplantd, distributed under the MIT license.
 * For full terms see the included LICENSE file.
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <json-glib/json-glib.h>

#include "pd-message-error.h"

/*
 * PdMessageError:
 *
 * Represents a ...
 */
struct _PdMessageError
{
  GObject  parent;
  gchar   *message;
  gint     code;
};

enum {
  PROP_0,
  PROP_MESSAGE,
  PROP_CODE,
  N_PROPS
};

static GParamSpec *properties [N_PROPS];

G_DEFINE_TYPE (PdMessageError, pd_message_error, G_TYPE_OBJECT)

static void
pd_message_error_finalize (GObject *object)
{
  PdMessageError *self = (PdMessageError *)object;

  g_clear_pointer (&self->message, g_free);

  G_OBJECT_CLASS (pd_message_error_parent_class)->finalize (object);
}

static void
pd_message_error_get_property (GObject    *object,
                               guint       prop_id,
                               GValue     *value,
                               GParamSpec *pspec)
{
  PdMessageError *self = PD_MESSAGE_ERROR (object);

  switch (prop_id)
  {
    case PROP_MESSAGE:
      g_value_set_string (value, pd_message_error_get_message (self));
      break;

    case PROP_CODE:
      g_value_set_int (value, pd_message_error_get_code (self));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
  }
}

static void
pd_message_error_set_property (GObject      *object,
                               guint         prop_id,
                               const GValue *value,
                               GParamSpec   *pspec)
{
  PdMessageError *self = PD_MESSAGE_ERROR (object);

  switch (prop_id)
  {
    case PROP_MESSAGE:
      pd_message_error_set_message (self, g_value_get_string (value));
      break;

    case PROP_CODE:
      pd_message_error_set_code (self, g_value_get_int (value));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
  }
}

static void
pd_message_error_class_init (PdMessageErrorClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->finalize = pd_message_error_finalize;
  object_class->get_property = pd_message_error_get_property;
  object_class->set_property = pd_message_error_set_property;

  properties [PROP_MESSAGE] =
    g_param_spec_string ("message",
                         "Message",
                         "The error message string.",
                         NULL,
                         (G_PARAM_READWRITE | G_PARAM_EXPLICIT_NOTIFY | G_PARAM_STATIC_STRINGS));

  properties [PROP_CODE] =
    g_param_spec_int ("code",
                      "Code",
                      "The error code.",
                      G_MININT,
                      G_MAXINT,
                      0,
                      (G_PARAM_READWRITE | G_PARAM_EXPLICIT_NOTIFY));

  g_object_class_install_properties (object_class, N_PROPS, properties);
}

static void
pd_message_error_init (PdMessageError *self)
{
}

PdMessageError *
pd_message_error_new (void)
{
  return g_object_new (PD_TYPE_MESSAGE_ERROR, NULL);
}

/**
 * pd_message_error_serialize:
 * @self: a #PdMessageError
 *
 * Returns the serialized message data.
 *
 * Returns: (transfer full): serialized data that the receiver must free.
 */
gchar *
pd_message_error_serialize (PdMessageError *self)
{
  gchar *ret;

  g_return_val_if_fail (PD_IS_MESSAGE_ERROR (self), NULL);

  ret = json_gobject_to_data (G_OBJECT (self), NULL);

  return g_steal_pointer (&ret);
}

void
pd_message_error_deserialize (PdMessageError *self,
                              const gchar    *data)
{
  GError *err = NULL;
  GObject *object = json_gobject_from_data (PD_TYPE_MESSAGE_ERROR,
                                            data,
                                            -1,
                                            &err);

  if (err != NULL)
    {
      g_critical ("%s", err->message);
      g_error_free (err);
    }

  g_return_if_fail (object != NULL);
  g_return_if_fail (PD_IS_MESSAGE_ERROR (object));

  pd_message_error_set_message (self,
      pd_message_error_get_message (PD_MESSAGE_ERROR (object)));

  pd_message_error_set_code (self,
      pd_message_error_get_code (PD_MESSAGE_ERROR (object)));

  g_object_unref (object);
}

const gchar *
pd_message_error_get_message (PdMessageError *self)
{
  g_return_val_if_fail (PD_IS_MESSAGE_ERROR (self), NULL);

  return self->message;
}

void
pd_message_error_set_message (PdMessageError *self,
                              const gchar    *message)
{
  g_return_if_fail (PD_IS_MESSAGE_ERROR (self));

  if (g_strcmp0 (message, self->message) != 0)
    {
      g_free (self->message);
      self->message = g_strdup (message);
      g_object_notify_by_pspec (G_OBJECT (self), properties [PROP_MESSAGE]);
    }
}

gint
pd_message_error_get_code (PdMessageError *self)
{
  g_return_val_if_fail (PD_IS_MESSAGE_ERROR (self), -1);

  return self->code;
}

void
pd_message_error_set_code (PdMessageError *self,
                           gint            code)
{
  g_return_if_fail (PD_IS_MESSAGE_ERROR (self));

  self->code = code;
}
