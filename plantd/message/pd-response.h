/*
 * Copyright © the Plantd contributors. All rights reserved.
 *
 * This file is part of libplantd, distributed under the MIT license.
 * For full terms see the included LICENSE file.
 */

#pragma once

#if !defined (PLANTD_INSIDE) && !defined (PLANTD_COMPILATION)
# error "Only <plantd/plantd.h> can be included directly."
#endif

#include <glib-object.h>

#include <plantd/plantd-types.h>

G_BEGIN_DECLS

#define PD_TYPE_RESPONSE pd_response_get_type ()
G_DECLARE_DERIVABLE_TYPE (PdResponse, pd_response, PD, RESPONSE, GObject)

struct _PdResponseClass
{
  /*< private >*/
  GObjectClass parent_class;

  /*< private >*/
  gpointer padding[12];
};

PdResponse     *pd_response_new         (void);

gchar          *pd_response_serialize   (PdResponse     *self);
void            pd_response_deserialize (PdResponse     *self,
                                         const gchar    *data);

PdMessageError *pd_response_get_error   (PdResponse     *self);
PdMessageError *pd_response_ref_error   (PdResponse     *self);
void            pd_response_set_error   (PdResponse     *self,
                                         PdMessageError *error);

G_END_DECLS
