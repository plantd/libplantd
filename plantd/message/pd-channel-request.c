/*
 * Copyright © the Plantd contributors. All rights reserved.
 *
 * This file is part of libplantd, distributed under the MIT license.
 * For full terms see the included LICENSE file.
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <json-glib/json-glib.h>

#include "pd-request.h"
#include "pd-channel-request.h"

/*
 * PdChannelRequest:
 *
 * Represents a request message for a single channel with the given ID.
 */
struct _PdChannelRequest
{
  PdRequest  parent;
  gchar     *id;
};

enum {
  PROP_0,
  PROP_ID,
  N_PROPS
};

static GParamSpec *properties [N_PROPS];

G_DEFINE_TYPE (PdChannelRequest, pd_channel_request, PD_TYPE_REQUEST)

static void
pd_channel_request_finalize (GObject *object)
{
  PdChannelRequest *self = (PdChannelRequest *)object;

  g_clear_pointer (&self->id, g_free);

  G_OBJECT_CLASS (pd_channel_request_parent_class)->finalize (object);
}

static void
pd_channel_request_get_property (GObject    *object,
                                 guint       prop_id,
                                 GValue     *value,
                                 GParamSpec *pspec)
{
  PdChannelRequest *self = PD_CHANNEL_REQUEST (object);

  switch (prop_id)
  {
    case PROP_ID:
      g_value_set_string (value, pd_channel_request_get_id (self));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
  }
}

static void
pd_channel_request_set_property (GObject      *object,
                                 guint         prop_id,
                                 const GValue *value,
                                 GParamSpec   *pspec)
{
  PdChannelRequest *self = PD_CHANNEL_REQUEST (object);

  switch (prop_id)
  {
    case PROP_ID:
      pd_channel_request_set_id (self, g_value_get_string (value));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
  }
}

static void
pd_channel_request_class_init (PdChannelRequestClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->finalize = pd_channel_request_finalize;
  object_class->get_property = pd_channel_request_get_property;
  object_class->set_property = pd_channel_request_set_property;

  properties [PROP_ID] =
    g_param_spec_string ("id",
                         "ID",
                         "The ID of the channel being requested.",
                         NULL,
                         (G_PARAM_READWRITE | G_PARAM_EXPLICIT_NOTIFY | G_PARAM_STATIC_STRINGS));

  g_object_class_install_properties (object_class, N_PROPS, properties);
}

static void
pd_channel_request_init (PdChannelRequest *self)
{
}

PdChannelRequest *
pd_channel_request_new (void)
{
  return g_object_new (PD_TYPE_CHANNEL_REQUEST, NULL);
}

/**
 * pd_channel_request_serialize:
 * @self: a #PdChannelRequest
 *
 * Returns the serialized message data.
 *
 * Returns: (transfer full): serialized data that the receiver must free.
 */
gchar *
pd_channel_request_serialize (PdChannelRequest *self)
{
  gchar *ret;

  g_return_val_if_fail (PD_IS_CHANNEL_REQUEST (self), NULL);

  ret = json_gobject_to_data (G_OBJECT (self), NULL);

  return g_steal_pointer (&ret);
}

void
pd_channel_request_deserialize (PdChannelRequest *self,
                                const gchar      *data)
{
  g_autoptr (GObject) object = NULL;
  g_autofree gchar *id = NULL;
  GError *err = NULL;

  object = json_gobject_from_data (PD_TYPE_CHANNEL_REQUEST,
                                   data,
                                   -1,
                                   &err);

  if (err != NULL)
  {
    g_critical ("%s", err->message);
    g_error_free (err);
  }

  g_return_if_fail (object != NULL);
  g_return_if_fail (PD_IS_CHANNEL_REQUEST (object));

  id = g_strdup (pd_channel_request_get_id (PD_CHANNEL_REQUEST (object)));

  pd_channel_request_set_id (self, id);

  g_clear_object (&object);
}

const gchar *
pd_channel_request_get_id (PdChannelRequest *self)
{
  g_return_val_if_fail (PD_IS_CHANNEL_REQUEST (self), NULL);

  return self->id;
}

void
pd_channel_request_set_id (PdChannelRequest *self,
                           const gchar      *id)
{
  g_return_if_fail (PD_IS_CHANNEL_REQUEST (self));

  if (g_strcmp0 (id, self->id) != 0)
    {
      g_free (self->id);
      self->id = g_strdup (id);
      g_object_notify_by_pspec (G_OBJECT (self), properties [PROP_ID]);
    }
}
