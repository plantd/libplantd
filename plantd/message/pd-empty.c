/*
 * Copyright © the Plantd contributors. All rights reserved.
 *
 * This file is part of libplantd, distributed under the MIT license.
 * For full terms see the included LICENSE file.
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include "pd-empty.h"

struct _PdEmpty
{
  GObject parent;
};

G_DEFINE_TYPE (PdEmpty, pd_empty, G_TYPE_OBJECT)

static void
pd_empty_finalize (GObject *object)
{
  G_GNUC_UNUSED PdEmpty *self = (PdEmpty *)object;

  G_OBJECT_CLASS (pd_empty_parent_class)->finalize (object);
}

static void
pd_empty_class_init (PdEmptyClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->finalize = pd_empty_finalize;
}

static void
pd_empty_init (PdEmpty *self)
{
}

PdEmpty *
pd_empty_new (void)
{
  return g_object_new (PD_TYPE_EMPTY, NULL);
}
