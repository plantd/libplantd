/*
 * Copyright © the Plantd contributors. All rights reserved.
 *
 * This file is part of libplantd, distributed under the MIT license.
 * For full terms see the included LICENSE file.
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <json-glib/json-glib.h>

#include "pd-properties-request.h"
#include "core/pd-property.h"

/*
 * PdPropertiesRequest:
 *
 * Represents a ...
 */
struct _PdPropertiesRequest
{
  GObject     parent;
  gchar      *id;
  GHashTable *properties;
};

enum {
  PROP_0,
  PROP_ID,
  PROP_PROPERTIES,
  N_PROPS
};

static GParamSpec *class_properties [N_PROPS];

static void json_serializable_iface_init (gpointer g_iface);

G_DEFINE_TYPE_WITH_CODE (PdPropertiesRequest, pd_properties_request, G_TYPE_OBJECT,
                         G_IMPLEMENT_INTERFACE (JSON_TYPE_SERIALIZABLE,
                                                json_serializable_iface_init));

static JsonNode *
pd_properties_request_serialize_property (JsonSerializable *serializable,
                                          const gchar      *name,
                                          const GValue     *value,
                                          GParamSpec       *pspec)
{
  JsonNode *retval = NULL;

  if (g_strcmp0 (name, "properties") == 0)
    {
      GHashTable *properties = NULL;
      GHashTableIter iter;
      JsonArray *arr = NULL;
      gpointer key, val;

      retval = json_node_new (JSON_NODE_ARRAY);

      g_return_val_if_fail (value != NULL, retval);
      g_return_val_if_fail (G_VALUE_HOLDS_POINTER (value), retval);

      properties = g_value_get_pointer (value);

      g_return_val_if_fail (properties != NULL, retval);

      arr = json_array_new ();

      if (properties != NULL)
        {
          g_hash_table_iter_init (&iter, properties);
          while(g_hash_table_iter_next (&iter, &key, &val))
            {
              JsonNode *node = NULL;
              JsonObject *obj = NULL;
              g_autoptr (PdProperty) property = NULL;

              property = pd_property_new (key, val);
              node = json_gobject_serialize (G_OBJECT (property));

              if (JSON_NODE_HOLDS_OBJECT (node))
                {
                  obj = json_node_dup_object (node);
                  json_array_add_object_element (arr, obj);
                }

              json_node_free (node);
            }
        }

      json_node_take_array (retval, arr);
    }
  else
    {
      GValue copy = { 0, };

      retval = json_node_new (JSON_NODE_VALUE);

      g_value_init (&copy, G_PARAM_SPEC_VALUE_TYPE (pspec));
      g_value_copy (value, &copy);
      json_node_set_value (retval, &copy);
      g_value_unset (&copy);
    }

  return retval;
}

static gboolean
pd_properties_request_deserialize_property (JsonSerializable *serializable,
                                            const gchar      *name,
                                            GValue           *value,
                                            GParamSpec       *pspec,
                                            JsonNode         *property_node)
{
  gboolean retval = FALSE;

  if (g_strcmp0 (name, "properties") == 0)
    {
      GHashTable *properties;
      JsonArray *arr;

      arr = json_node_get_array (property_node);
      properties = g_hash_table_new_full (g_str_hash,
                                          g_str_equal,
                                          g_free,
                                          g_free);

      for (gint i = 0; i < json_array_get_length (arr); i++)
        {
          g_autoptr (PdProperty) property = NULL;
          JsonNode *node = NULL;

          node = json_array_get_element (arr, i);

          property = PD_PROPERTY (json_gobject_deserialize (PD_TYPE_PROPERTY, node));
          g_return_val_if_fail (PD_IS_PROPERTY (property), FALSE);
          g_hash_table_insert (properties,
                               g_strdup (pd_property_get_key (property)),
                               g_strdup (pd_property_get_value (property)));
        }

      g_value_set_pointer (value, properties);

      retval = TRUE;
    }

  return retval;
}

static void
json_serializable_iface_init (gpointer g_iface)
{
  JsonSerializableIface *iface = g_iface;

  iface->serialize_property = pd_properties_request_serialize_property;
  iface->deserialize_property = pd_properties_request_deserialize_property;
}

static void
pd_properties_request_finalize (GObject *object)
{
  PdPropertiesRequest *self = (PdPropertiesRequest *)object;

  g_clear_pointer (&self->id, g_free);
  g_clear_pointer (&self->properties, g_hash_table_unref);

  G_OBJECT_CLASS (pd_properties_request_parent_class)->finalize (object);
}

static void
pd_properties_request_get_property (GObject    *object,
                                    guint       prop_id,
                                    GValue     *value,
                                    GParamSpec *pspec)
{
  PdPropertiesRequest *self = PD_PROPERTIES_REQUEST (object);

  switch (prop_id)
    {
    case PROP_ID:
      g_value_set_string (value, pd_properties_request_get_id (self));
      break;

    case PROP_PROPERTIES:
      g_value_set_pointer (value, self->properties);
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
pd_properties_request_set_property (GObject      *object,
                                    guint         prop_id,
                                    const GValue *value,
                                    GParamSpec   *pspec)
{
  PdPropertiesRequest *self = PD_PROPERTIES_REQUEST (object);

  switch (prop_id)
    {
    case PROP_ID:
      pd_properties_request_set_id (self, g_value_get_string (value));
      break;

    case PROP_PROPERTIES:
      pd_properties_request_set_list (self, g_value_get_pointer (value));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
pd_properties_request_class_init (PdPropertiesRequestClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->finalize = pd_properties_request_finalize;
  object_class->get_property = pd_properties_request_get_property;
  object_class->set_property = pd_properties_request_set_property;

  class_properties [PROP_ID] =
    g_param_spec_string ("id",
                         "ID",
                         "The id of the service the request is meant for",
                         NULL,
                         (G_PARAM_READWRITE | G_PARAM_EXPLICIT_NOTIFY | G_PARAM_STATIC_STRINGS));

  class_properties [PROP_PROPERTIES] =
    g_param_spec_pointer ("properties",
                          "Properties",
                          "The properties list",
                          (G_PARAM_READWRITE | G_PARAM_EXPLICIT_NOTIFY | G_PARAM_STATIC_STRINGS));

  g_object_class_install_properties (object_class, N_PROPS, class_properties);
}

static void
pd_properties_request_init (PdPropertiesRequest *self)
{
}

PdPropertiesRequest *
pd_properties_request_new (const gchar *id)
{
  return g_object_new (PD_TYPE_PROPERTIES_REQUEST,
                       "id", id,
                       NULL);
}

/**
 * pd_properties_request_serialize:
 * @self: a #PdPropertiesRequest
 *
 * Returns the serialized message data.
 *
 * Returns: (transfer full): serialized data that the receiver must free.
 */
gchar *
pd_properties_request_serialize (PdPropertiesRequest *self)
{
  gchar *ret;

  g_return_val_if_fail (PD_IS_PROPERTIES_REQUEST (self), NULL);

  ret = json_gobject_to_data (G_OBJECT (self), NULL);

  return g_steal_pointer (&ret);
}

void
pd_properties_request_deserialize (PdPropertiesRequest *self,
                                   const gchar         *data)
{
  g_autoptr (GObject) object = NULL;
  g_autoptr (GHashTable) list = NULL;
  g_autofree gchar *id = NULL;

  GError *err = NULL;
  object = json_gobject_from_data (PD_TYPE_PROPERTIES_REQUEST,
                                   data,
                                   -1,
                                   &err);

  if (err != NULL)
    {
      g_critical ("%s", err->message);
      g_error_free (err);
    }

  g_return_if_fail (object != NULL);
  g_return_if_fail (PD_IS_PROPERTIES_REQUEST (object));

  id = g_strdup (pd_properties_request_get_id (PD_PROPERTIES_REQUEST (object)));
  list = pd_properties_request_get_list (PD_PROPERTIES_REQUEST (object));

  pd_properties_request_set_id (self, id);
  pd_properties_request_set_list (self, list);

  g_clear_object (&object);
}

void
pd_properties_request_add (PdPropertiesRequest *self,
                           PdProperty          *property)
{
  const gchar *key;
  const gchar *value;

  g_return_if_fail (PD_IS_PROPERTIES_REQUEST (self));
  g_return_if_fail (PD_IS_PROPERTY (property));

  key = pd_property_get_key (property);
  value = pd_property_get_value (property);

  if (self->properties == NULL)
    self->properties = g_hash_table_new_full (g_str_hash, g_str_equal, g_free, g_free);

  if (g_hash_table_contains (self->properties, key))
    g_hash_table_replace (self->properties, g_strdup (key), g_strdup (value));
  else
    g_hash_table_insert (self->properties, g_strdup (key), g_strdup (value));
}

void
pd_properties_request_remove (PdPropertiesRequest *self,
                              const gchar         *key)
{
  g_return_if_fail (PD_IS_PROPERTIES_REQUEST (self));
  g_return_if_fail (self->properties != NULL);

  if (g_hash_table_contains (self->properties, key))
    g_hash_table_remove (self->properties, key);
}

/**
 * pd_properties_request_get:
 * @self: a #PdPropertiesRequest
 * @key: the key of the property to look up
 *
 * Returns: (transfer none): the property with the associated key if found,
 *          NULL otherwise.
 */
PdProperty *
pd_properties_request_get (PdPropertiesRequest *self,
                           const gchar         *key)
{
  g_return_val_if_fail (PD_IS_PROPERTIES_REQUEST (self), NULL);
  g_return_val_if_fail (self->properties != NULL, NULL);

  if (g_hash_table_contains (self->properties, key))
    return pd_property_new (key, g_hash_table_lookup (self->properties, key));

  return NULL;
}

gboolean
pd_properties_request_contains (PdPropertiesRequest *self,
                                const gchar         *key)
{
  gboolean ret;

  g_return_val_if_fail (PD_IS_PROPERTIES_REQUEST (self), FALSE);
  g_return_val_if_fail (self->properties != NULL, FALSE);
  g_return_val_if_fail (key != NULL, FALSE);

  ret = g_hash_table_contains (self->properties, key);

  return ret;
}

/**
 * pd_properties_request_get_list:
 * @self: a #PdPropertiesRequest
 *
 * Returns: (element-type utf8 utf8) (transfer full): a hash table of strings
 *          representing #PdProperty objects, free the table with
 *          g_hash_table_destroy when done.
 */
GHashTable *
pd_properties_request_get_list (PdPropertiesRequest *self)
{
  GHashTable *properties;

  g_return_val_if_fail (PD_IS_PROPERTIES_REQUEST (self), NULL);

  g_object_get (self, "properties", &properties, NULL);

  return properties;
}

void
pd_properties_request_set_list (PdPropertiesRequest *self,
                                GHashTable          *properties)
{
  g_return_if_fail (PD_IS_PROPERTIES_REQUEST (self));

  if (self->properties == properties)
    return;

  if (self->properties)
    g_hash_table_unref (self->properties);

  if (properties)
    g_hash_table_ref (properties);

  self->properties = properties;

  g_object_notify_by_pspec (G_OBJECT (self), class_properties [PROP_PROPERTIES]);
}

const gchar *
pd_properties_request_get_id (PdPropertiesRequest *self)
{
  g_return_val_if_fail (PD_IS_PROPERTIES_REQUEST (self), NULL);

  return self->id;
}

void
pd_properties_request_set_id (PdPropertiesRequest *self,
                              const gchar         *id)
{
  g_return_if_fail (PD_IS_PROPERTIES_REQUEST (self));

  if (g_strcmp0 (id, self->id) != 0)
    {
      g_free (self->id);
      self->id = g_strdup (id);
      g_object_notify_by_pspec (G_OBJECT (self), class_properties [PROP_ID]);
    }
}
