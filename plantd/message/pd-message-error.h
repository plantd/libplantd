/*
 * Copyright © the Plantd contributors. All rights reserved.
 *
 * This file is part of libplantd, distributed under the MIT license.
 * For full terms see the included LICENSE file.
 */

#pragma once

#if !defined (PLANTD_INSIDE) && !defined (PLANTD_COMPILATION)
# error "Only <plantd/plantd.h> can be included directly."
#endif

#include <glib-object.h>

G_BEGIN_DECLS

#define PD_TYPE_MESSAGE_ERROR pd_message_error_get_type ()
G_DECLARE_FINAL_TYPE (PdMessageError, pd_message_error, PD, MESSAGE_ERROR, GObject)

PdMessageError *pd_message_error_new         (void);

gchar          *pd_message_error_serialize   (PdMessageError *self);
void            pd_message_error_deserialize (PdMessageError *self,
                                              const gchar    *data);

const gchar    *pd_message_error_get_message (PdMessageError *self);
void            pd_message_error_set_message (PdMessageError *self,
                                              const gchar    *message);

gint            pd_message_error_get_code    (PdMessageError *self);
void            pd_message_error_set_code    (PdMessageError *self,
                                              gint            code);

G_END_DECLS
