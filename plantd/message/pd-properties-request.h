/*
 * Copyright © the Plantd contributors. All rights reserved.
 *
 * This file is part of libplantd, distributed under the MIT license.
 * For full terms see the included LICENSE file.
 */

#pragma once

#if !defined (PLANTD_INSIDE) && !defined (PLANTD_COMPILATION)
# error "Only <plantd/plantd.h> can be included directly."
#endif

#include <glib-object.h>

#include <plantd/plantd-types.h>

G_BEGIN_DECLS

#define PD_TYPE_PROPERTIES_REQUEST pd_properties_request_get_type ()
G_DECLARE_FINAL_TYPE (PdPropertiesRequest, pd_properties_request, PD, PROPERTIES_REQUEST, GObject)

PdPropertiesRequest *pd_properties_request_new         (const gchar         *id);

gchar               *pd_properties_request_serialize   (PdPropertiesRequest *self);
void                 pd_properties_request_deserialize (PdPropertiesRequest *self,
                                                        const gchar         *data);

void                 pd_properties_request_add         (PdPropertiesRequest *self,
                                                        PdProperty          *property);
void                 pd_properties_request_remove      (PdPropertiesRequest *self,
                                                        const gchar         *key);
PdProperty          *pd_properties_request_get         (PdPropertiesRequest *self,
                                                        const gchar         *key);
gboolean             pd_properties_request_contains    (PdPropertiesRequest *self,
                                                        const gchar         *key);
GHashTable          *pd_properties_request_get_list    (PdPropertiesRequest *self);
void                 pd_properties_request_set_list    (PdPropertiesRequest *self,
                                                        GHashTable          *properties);

const gchar         *pd_properties_request_get_id      (PdPropertiesRequest *self);
void                 pd_properties_request_set_id      (PdPropertiesRequest *self,
                                                        const gchar         *id);

G_END_DECLS
