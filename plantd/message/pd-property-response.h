/*
 * Copyright © the Plantd contributors. All rights reserved.
 *
 * This file is part of libplantd, distributed under the MIT license.
 * For full terms see the included LICENSE file.
 */

#pragma once

#if !defined (PLANTD_INSIDE) && !defined (PLANTD_COMPILATION)
# error "Only <plantd/plantd.h> can be included directly."
#endif

#include <glib-object.h>

#include <plantd/plantd-types.h>

#include "pd-response.h"

G_BEGIN_DECLS

#define PD_TYPE_PROPERTY_RESPONSE pd_property_response_get_type ()
G_DECLARE_FINAL_TYPE (PdPropertyResponse, pd_property_response, PD, PROPERTY_RESPONSE, PdResponse)

PdPropertyResponse *pd_property_response_new         (void);

gchar              *pd_property_response_serialize   (PdPropertyResponse *self);
void                pd_property_response_deserialize (PdPropertyResponse *self,
                                                      const gchar        *data);

PdProperty         *pd_property_response_get         (PdPropertyResponse *self);
PdProperty         *pd_property_response_ref         (PdPropertyResponse *self);
void                pd_property_response_set         (PdPropertyResponse *self,
                                                      PdProperty         *property);

G_END_DECLS
