/*
 * Copyright © the Plantd contributors. All rights reserved.
 *
 * This file is part of libplantd, distributed under the MIT license.
 * For full terms see the included LICENSE file.
 */

#define G_LOG_DOMAIN "plantd-job-response"

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <json-glib/json-glib.h>

#include <plantd/plantd.h>

#include "pd-response.h"

/*
 * PdJobResponse:
 *
 * Represents a response message for the request to enqueue an asynchronous job.
 */
struct _PdJobResponse
{
  PdResponse  parent;
  PdJob      *job;
};

enum {
  PROP_0,
  PROP_JOB,
  N_PROPS
};

static GParamSpec *properties [N_PROPS];

G_DEFINE_TYPE (PdJobResponse, pd_job_response, PD_TYPE_RESPONSE)

static void
pd_job_response_finalize (GObject *object)
{
  PdJobResponse *self = (PdJobResponse *)object;

  // TODO: really need to determine if things like PdJob should be a boxed type with
  // a _ref and _unref, see DzlRing as example
  g_object_unref (self->job);

  G_OBJECT_CLASS (pd_job_response_parent_class)->finalize (object);
}

static void
pd_job_response_get_property (GObject    *object,
                              guint       prop_id,
                              GValue     *value,
                              GParamSpec *pspec)
{
  PdJobResponse *self = PD_JOB_RESPONSE (object);

  switch (prop_id)
    {
    case PROP_JOB:
      /*g_value_set_object (value, pd_job_response_get_job (self));*/
      g_value_set_object (value, self->job);
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
pd_job_response_set_property (GObject      *object,
                              guint         prop_id,
                              const GValue *value,
                              GParamSpec   *pspec)
{
  PdJobResponse *self = PD_JOB_RESPONSE (object);

  switch (prop_id)
    {
    case PROP_JOB:
      pd_job_response_set_job (self, g_value_get_object (value));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
pd_job_response_class_init (PdJobResponseClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->finalize = pd_job_response_finalize;
  object_class->get_property = pd_job_response_get_property;
  object_class->set_property = pd_job_response_set_property;

  properties [PROP_JOB] =
    g_param_spec_object ("job",
                         "Job",
                         "The job to use in the response.",
                         PD_TYPE_JOB,
                         (G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));

  g_object_class_install_properties (object_class, N_PROPS, properties);
}

static void
pd_job_response_init (PdJobResponse *self)
{
  self->job = pd_job_new ();
}

PdJobResponse *
pd_job_response_new (void)
{
  return g_object_new (PD_TYPE_JOB_RESPONSE, NULL);
}

/**
 * pd_job_response_serialize:
 * @self: a #PdJobResponse
 *
 * Returns the serialized message data.
 *
 * Returns: (transfer full): serialized data that the receiver must free.
 */
gchar *
pd_job_response_serialize (PdJobResponse *self)
{
  gchar *ret;

  g_return_val_if_fail (PD_IS_JOB_RESPONSE (self), NULL);

  ret = json_gobject_to_data (G_OBJECT (self), NULL);

  return g_steal_pointer (&ret);
}

void
pd_job_response_deserialize (PdJobResponse *self,
                             const gchar   *data)
{
  GError *err = NULL;
  GObject *object = json_gobject_from_data (PD_TYPE_JOB_RESPONSE,
                                            data,
                                            -1,
                                            &err);

  if (err != NULL)
    {
      g_critical ("%s", err->message);
      g_error_free (err);
    }

  g_return_if_fail (object != NULL);
  g_return_if_fail (PD_IS_JOB_RESPONSE (object));

  pd_job_response_set_job (
      self,
      pd_job_response_get_job (PD_JOB_RESPONSE (object)));

  g_object_unref (object);
}

/**
 * pd_job_response_get_job:
 * @self: #PdJobResponse instance
 *
 * Returns: (transfer full): The #PdJob
 */
PdJob *
pd_job_response_get_job (PdJobResponse *self)
{
  PdJob *job;

  g_return_val_if_fail (PD_IS_JOB_RESPONSE (self), NULL);

  g_object_get (self, "job", &job, NULL);

  return job;
}

void
pd_job_response_set_job (PdJobResponse *self,
                         PdJob         *job)
{
  g_return_if_fail (PD_IS_JOB_RESPONSE (self));
  g_return_if_fail (PD_IS_JOB (job));

  if (self->job)
    g_object_unref (self->job);

  if (job)
    g_object_ref (job);

  self->job = job;
}
