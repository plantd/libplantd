/*
 * Copyright © the Plantd contributors. All rights reserved.
 *
 * This file is part of libplantd, distributed under the MIT license.
 * For full terms see the included LICENSE file.
 */

#pragma once

#if !defined (PLANTD_INSIDE) && !defined (PLANTD_COMPILATION)
# error "Only <plantd/plantd.h> can be included directly."
#endif

#include <glib-object.h>

#include <plantd/plantd-types.h>

#include "pd-response.h"

G_BEGIN_DECLS

#define PD_TYPE_CONFIGURATION_REQUEST pd_configuration_request_get_type ()
G_DECLARE_FINAL_TYPE (PdConfigurationRequest, pd_configuration_request, PD, CONFIGURATION_REQUEST, PdResponse)

PdConfigurationRequest   *pd_configuration_request_new           (void);

gchar                    *pd_configuration_request_serialize     (PdConfigurationRequest   *self);
void                      pd_configuration_request_deserialize   (PdConfigurationRequest   *self,
                                                                  const gchar              *data);

const gchar              *pd_configuration_request_get_id        (PdConfigurationRequest   *self);
void                      pd_configuration_request_set_id        (PdConfigurationRequest   *self,
                                                                  const gchar              *id);

PdConfigurationNamespace  pd_configuration_request_get_namespace (PdConfigurationRequest   *self);
void                      pd_configuration_request_set_namespace (PdConfigurationRequest   *self,
                                                                  PdConfigurationNamespace  namespace);

G_END_DECLS
