/*
 * Copyright © the Plantd contributors. All rights reserved.
 *
 * This file is part of libplantd, distributed under the MIT license.
 * For full terms see the included LICENSE file.
 */

#pragma once

#if !defined (PLANTD_INSIDE) && !defined (PLANTD_COMPILATION)
# error "Only <plantd/plantd.h> can be included directly."
#endif

#include <glib-object.h>

G_BEGIN_DECLS

/**
 * PdConfigurationNamespace:
 * @PD_CONFIGURATION_NAMESPACE_ACQUIRE:
 * @PD_CONFIGURATION_NAMESPACE_ANALYZE:
 * @PD_CONFIGURATION_NAMESPACE_CONTROL:
 * @PD_CONFIGURATION_NAMESPACE_EXPERIMENT:
 * @PD_CONFIGURATION_NAMESPACE_MONITOR:
 * @PD_CONFIGURATION_NAMESPACE_PRESENT:
 * @PD_CONFIGURATION_NAMESPACE_RECORD:
 * @PD_CONFIGURATION_NAMESPACE_STATE:
 *
 * The configuration namespace type.
 */
typedef enum
{
  PD_CONFIGURATION_NAMESPACE_ACQUIRE = 0,
  PD_CONFIGURATION_NAMESPACE_ANALYZE,
  PD_CONFIGURATION_NAMESPACE_CONTROL,
  PD_CONFIGURATION_NAMESPACE_EXPERIMENT,
  PD_CONFIGURATION_NAMESPACE_MONITOR,
  PD_CONFIGURATION_NAMESPACE_PRESENT,
  PD_CONFIGURATION_NAMESPACE_RECORD,
  PD_CONFIGURATION_NAMESPACE_STATE
} PdConfigurationNamespace;

/**
 * PdJobStatusType:
 * @PD_JOB_STATUS_TYPE_SUBMITTED: specifies a submitted job.
 * @PD_JOB_STATUS_TYPE_READY: specifies a ready job.
 * @PD_JOB_STATUS_TYPE_STARTED: specifies a started job.
 * @PD_JOB_STATUS_TYPE_RUNNING: specifies a running job.
 * @PD_JOB_STATUS_TYPE_PAUSED: specifies a paused job.
 * @PD_JOB_STATUS_TYPE_RESUMED: specifies a resumed job.
 * @PD_JOB_STATUS_TYPE_STOPPED: specifies a stopped job.
 * @PD_JOB_STATUS_TYPE_FAILED: specifies a failed job.
 *
 * The job status value.
 */
typedef enum
{
  PD_JOB_STATUS_TYPE_SUBMITTED = 0,
  PD_JOB_STATUS_TYPE_READY,
  PD_JOB_STATUS_TYPE_STARTED,
  PD_JOB_STATUS_TYPE_RUNNING,
  PD_JOB_STATUS_TYPE_PAUSED,
  PD_JOB_STATUS_TYPE_RESUMED,
  PD_JOB_STATUS_TYPE_STOPPED,
  PD_JOB_STATUS_TYPE_FAILED
} PdJobStatusType;

/**
 * PdChannelType:
 * @PD_CHANNEL_TYPE_SINK:
 * @PD_CHANNEL_TYPE_SOURCE:
 *
 * The type of channel.
 */
typedef enum
{
  PD_CHANNEL_TYPE_SINK = 0,
  PD_CHANNEL_TYPE_SOURCE
} PdChannelType;

/**
 * PdServiceType:
 * @PD_SERVICE_TYPE_ACQUIRE:
 * @PD_SERVICE_TYPE_ANALYZE:
 * @PD_SERVICE_TYPE_CONTROL:
 * @PD_SERVICE_TYPE_EXPERIMENT:
 * @PD_SERVICE_TYPE_MONITOR:
 * @PD_SERVICE_TYPE_PRESENT:
 * @PD_SERVICE_TYPE_RECORD:
 * @PD_SERVICE_TYPE_STATE:
 *
 * The service type.
 */
typedef enum
{
  PD_SERVICE_TYPE_ACQUIRE = 0,
  PD_SERVICE_TYPE_ANALYZE,
  PD_SERVICE_TYPE_CONTROL,
  PD_SERVICE_TYPE_EXPERIMENT,
  PD_SERVICE_TYPE_MONITOR,
  PD_SERVICE_TYPE_PRESENT,
  PD_SERVICE_TYPE_RECORD,
  PD_SERVICE_TYPE_STATE
} PdServiceType;

/**
 * PdErrorEnum:
 * @PD_ERROR_FAILED: Generic error condition for when an operation fails.
 * @PD_ERROR_NOT_CONNECTED: Transport endpoint is not connected.
 *
 * Error codes returned by Plantd functions.
 **/
typedef enum {
  PD_ERROR_FAILED,
  PD_ERROR_NOT_CONNECTED,
  PD_ERROR_NOT_FOUND
} PdErrorEnum;

G_END_DECLS
