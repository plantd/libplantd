#!/usr/bin/env python3

from gi.repository import GLib

from ..importer import modules

Pd = modules["Pd"]._introspection_module
__all__ = []

#
# GLib logging wrappers
#


def _modname() -> str:
    import inspect

    frm = inspect.stack()[2]
    mod = inspect.getmodule(frm[0])
    return mod.__name__


def _log(domain, level, *messages) -> None:
    message = " ".join(messages)
    v = GLib.Variant("a{sv}", {"MESSAGE": GLib.Variant.new_string(message)})
    GLib.log_variant(domain, level, v)


def critical(*messages) -> None:
    _log(_modname(), GLib.LogLevelFlags.LEVEL_CRITICAL, *messages)


def warning(*messages) -> None:
    _log(_modname(), GLib.LogLevelFlags.LEVEL_WARNING, *messages)


def debug(*messages) -> None:
    _log(_modname(), GLib.LogLevelFlags.LEVEL_DEBUG, *messages)


def message(*messages) -> None:
    _log(_modname(), GLib.LogLevelFlags.LEVEL_MESSAGE, *messages)


def info(*messages) -> None:
    _log(_modname(), GLib.LogLevelFlags.LEVEL_INFO, *messages)


Pd.critical = critical
Pd.debug = debug
Pd.info = info
Pd.message = message
Pd.warning = warning
