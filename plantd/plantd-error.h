/*
 * Copyright © the Plantd contributors. All rights reserved.
 *
 * This file is part of libplantd, distributed under the MIT license.
 * For full terms see the included LICENSE file.
 */

#pragma once

#if !defined (PLANTD_INSIDE) && !defined (PLANTD_COMPILATION)
# error "Only <plantd/plantd.h> can be included directly."
#endif

#include <glib.h>
#include <plantd/plantd-enums.h>

G_BEGIN_DECLS

/**
 * PD_ERROR:
 *
 * Error domain for Plantd. Errors in this domain will be from the #PdErrorEnum enumeration.
 * See #GError for more information on error domains.
 **/
#define PD_ERROR pd_error_quark()

//PD_AVAILABLE_IN_ALL
GQuark      pd_error_quark      (void);
//PD_AVAILABLE_IN_ALL
PdErrorEnum pd_error_from_errno (gint err_no);

G_END_DECLS
