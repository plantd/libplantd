/*
 * plantd.h
 * This file is part of libplantd
 * Copyright © 2019 - Geoff Johnson
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included
 * in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
 * OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#pragma once

#include <glib.h>

#define PLANTD_INSIDE

G_BEGIN_DECLS

#if !GLIB_CHECK_VERSION(2, 48, 0)
# error "libplantd requires glib-2.0 >= 2.48.0"
#endif

#include <plantd/plantd-types.h>
#include <plantd/plantd-debug.h>
#include <plantd/plantd-error.h>
#include <plantd/plantd-enums.h>
#include <plantd/plantd-enum-types.h>

// MDP API
#include <plantd/mdp/pd-broker.h>
#include <plantd/mdp/pd-client.h>
#include <plantd/mdp/pd-worker.h>

// Core types
#include <plantd/core/pd-application.h>
#include <plantd/core/pd-channel.h>
#include <plantd/core/pd-configuration.h>
#include <plantd/core/pd-event.h>
#include <plantd/core/pd-handler.h>
#include <plantd/core/pd-hub.h>
#include <plantd/core/pd-job.h>
#include <plantd/core/pd-log.h>
#include <plantd/core/pd-metric.h>
#include <plantd/core/pd-metric-table.h>
#include <plantd/core/pd-metric-table-header.h>
#include <plantd/core/pd-module.h>
#include <plantd/core/pd-object.h>
#include <plantd/core/pd-property.h>
#include <plantd/core/pd-service.h>
#include <plantd/core/pd-sink.h>
#include <plantd/core/pd-source.h>
#include <plantd/core/pd-status.h>
#include <plantd/core/pd-system.h>

// State related types
#include <plantd/state/pd-state.h>
#include <plantd/state/pd-state-machine.h>
#include <plantd/state/pd-transition.h>
#include <plantd/state/pd-transition-table.h>

// Messages
#include <plantd/message/pd-channel-request.h>
#include <plantd/message/pd-channel-response.h>
#include <plantd/message/pd-channels-response.h>
#include <plantd/message/pd-configuration-request.h>
#include <plantd/message/pd-configuration-response.h>
#include <plantd/message/pd-configurations-response.h>
#include <plantd/message/pd-empty.h>
#include <plantd/message/pd-event-request.h>
#include <plantd/message/pd-event-response.h>
#include <plantd/message/pd-job-request.h>
#include <plantd/message/pd-job-response.h>
#include <plantd/message/pd-jobs-response.h>
#include <plantd/message/pd-job-status-response.h>
#include <plantd/message/pd-message-error.h>
#include <plantd/message/pd-property-request.h>
#include <plantd/message/pd-property-response.h>
#include <plantd/message/pd-properties-request.h>
#include <plantd/message/pd-properties-response.h>
#include <plantd/message/pd-request.h>
#include <plantd/message/pd-response.h>
#include <plantd/message/pd-settings-request.h>
#include <plantd/message/pd-settings-response.h>
#include <plantd/message/pd-status-request.h>
#include <plantd/message/pd-status-response.h>

G_END_DECLS

#undef PLANTD_INSIDE
