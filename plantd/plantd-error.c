#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <errno.h>
#include "plantd-error.h"

/**
 * SECTION:plantd-error
 * @short_description: Error helper functions
 * @include: plantd/plantd.h
 *
 * Contains helper functions for reporting errors to the user.
 **/

/**
 * pd_error_quark:
 *
 * Gets the Plantd Error Quark.
 *
 * Returns: a #GQuark.
 **/
G_DEFINE_QUARK (pd-error-quark, pd_error)

/**
 * pd_error_from_errno:
 * @err_no: Error number as defined in errno.h.
 *
 * Converts errno.h error codes into Plantd error codes.
 *
 * Returns: #PdErrorEnum value for the given errno.h error number.
 **/
PdErrorEnum
pd_error_from_errno (gint err_no)
{
  switch (err_no)
    {
#ifdef ENOTCONN
    case ENOTCONN:
      return PD_ERROR_NOT_CONNECTED;
#endif

    default:
      return PD_ERROR_FAILED;
    }
}
