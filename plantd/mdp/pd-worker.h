/*
 * Copyright © the Plantd contributors. All rights reserved.
 *
 * This file is part of libplantd, distributed under the MIT license.
 * For full terms see the included LICENSE file.
 */

#pragma once

#if !defined (PLANTD_INSIDE) && !defined (PLANTD_COMPILATION)
# error "Only <plantd/plantd.h> can be included directly."
#endif

#include <glib-object.h>
#include <czmq.h>

G_BEGIN_DECLS

/*
 * Type declaration.
 */
#define PD_TYPE_WORKER pd_worker_get_type ()
G_DECLARE_FINAL_TYPE (PdWorker, pd_worker, PD, WORKER, GObject)

/*
 * Method definitions.
 */
PdWorker    *pd_worker_new           (const gchar  *broker,
                                      const gchar  *service);

void         pd_worker_connect       (PdWorker     *self);
void         pd_worker_disconnect    (PdWorker     *self);

const gchar *pd_worker_get_broker    (PdWorker     *self);
void         pd_worker_set_broker    (PdWorker     *self,
                                      const gchar  *broker);

const gchar *pd_worker_get_service   (PdWorker     *self);
void         pd_worker_set_service   (PdWorker     *self,
                                      const gchar  *service);

void         pd_worker_set_liveness  (PdWorker     *self,
                                      gint          liveness);

void         pd_worker_set_heartbeat (PdWorker     *self,
                                      gint          heartbeat);

void         pd_worker_set_reconnect (PdWorker     *self,
                                      gint          reconnect);

zmsg_t      *pd_worker_recv          (PdWorker     *self,
                                      zframe_t    **reply_p);

void         pd_worker_send          (PdWorker     *self,
                                      zmsg_t      **report_p,
                                      zframe_t     *reply_to);

G_END_DECLS
