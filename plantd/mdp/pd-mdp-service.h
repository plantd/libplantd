/*
 * Copyright © the Plantd contributors. All rights reserved.
 *
 * This file is part of libplantd, distributed under the MIT license.
 * For full terms see the included LICENSE file.
 */

#pragma once

#if !defined (PLANTD_INSIDE) && !defined (PLANTD_COMPILATION)
# error "Only <plantd/plantd.h> can be included directly."
#endif

#include <glib-object.h>

#include <plantd/plantd-types.h>

G_BEGIN_DECLS

#define PD_TYPE_MDP_SERVICE pd_mdp_service_get_type ()
G_DECLARE_FINAL_TYPE (PdMdpService, pd_mdp_service, PD, MDP_SERVICE, GObject)

PdMdpService *pd_mdp_service_new             (void);

void          pd_mdp_service_dispatch        (PdMdpService *self);
void          pd_mdp_service_enable_command  (PdMdpService *self,
                                              const gchar  *name);
void          pd_mdp_service_disable_command (PdMdpService *self,
                                              const gchar  *name);
gboolean      pd_mdp_service_command_enabled (PdMdpService *self,
                                              const gchar  *name);

void          pd_mdp_service_push_request    (PdMdpService *self,
                                              gpointer      request);
gpointer      pd_mdp_service_pop_request     (PdMdpService *self);
gpointer      pd_mdp_service_peek_request    (PdMdpService *self);
gint          pd_mdp_service_requests_length (PdMdpService *self);
gint          pd_mdp_service_worker_count    (PdMdpService *self);
void          pd_mdp_service_add_waiting     (PdMdpService *self,
                                              gpointer      waiting);
void          pd_mdp_service_remove_waiting  (PdMdpService *self,
                                              gpointer      waiting);

gchar        *pd_mdp_service_dup_name        (PdMdpService *self);
void          pd_mdp_service_set_name        (PdMdpService *self,
                                              const gchar  *name);

PdBroker     *pd_mdp_service_ref_broker      (PdMdpService *self);
void          pd_mdp_service_set_broker      (PdMdpService *self,
                                              PdBroker     *broker);

G_END_DECLS
