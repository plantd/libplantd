/*
 * Copyright © the Plantd contributors. All rights reserved.
 *
 * This file is part of libplantd, distributed under the MIT license.
 * For full terms see the included LICENSE file.
 */

#define G_LOG_DOMAIN "plantd-mdp"

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include "pd-mdp.h"

void
pd_mdp_dump (zmsg_t *msg)
{
  for (zframe_t *iter = zmsg_first (msg);
       iter != NULL;
       iter = zmsg_next (msg))
    {
      PD_TRACE_MSG ("[%03d] %s",
                    (gint)zframe_size (iter),
                    zframe_strdup (iter));
    }
}
