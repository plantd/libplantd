/*
 * Copyright © the Plantd contributors. All rights reserved.
 *
 * This file is part of libplantd, distributed under the MIT license.
 * For full terms see the included LICENSE file.
 */

#define G_LOG_DOMAIN "plantd-mdp-service"

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <czmq.h>
#include <plantd/plantd.h>

#include "pd-mdp-service.h"
/*#include "plantd-mdp-worker.h"*/

struct _PdMdpService
{
  GObject     parent;
  gchar      *name;
  gsize       workers;
  zlist_t    *requests;
  zlist_t    *waiting;
  zlist_t    *blacklist;
  PdBroker *broker;
};

G_DEFINE_TYPE (PdMdpService, pd_mdp_service, G_TYPE_OBJECT)

static void
pd_mdp_service_finalize (GObject *object)
{
  PdMdpService *self;
  g_autofree gchar *command = NULL;

  self = (PdMdpService *)object;

  while (zlist_size (self->requests))
    {
      zmsg_t *msg = (zmsg_t *) zlist_pop (self->requests);
      zmsg_destroy (&msg);
    }

  /* Free memory keeping blacklisted commands */
  command = (gchar *) zlist_first (self->blacklist);
  while (command)
    zlist_remove (self->blacklist, command);

  zlist_destroy (&self->requests);
  zlist_destroy (&self->waiting);
  zlist_destroy (&self->blacklist);

  g_clear_pointer (&self->name, g_free);

  G_OBJECT_CLASS (pd_mdp_service_parent_class)->finalize (object);
}

static void
pd_mdp_service_class_init (PdMdpServiceClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->finalize = pd_mdp_service_finalize;
}

static void
pd_mdp_service_init (PdMdpService *self)
{
  /* TODO: switch to GList or GPtrArray */
  self->requests = zlist_new ();
  self->waiting = zlist_new ();
  self->blacklist = zlist_new ();
}

PdMdpService *
pd_mdp_service_new (void)
{
  return g_object_new (PD_TYPE_MDP_SERVICE, NULL);
}

void
pd_mdp_service_dispatch (PdMdpService *self)
{
}

void
pd_mdp_service_enable_command (PdMdpService *self,
                               const gchar  *name)
{
}

void
pd_mdp_service_disable_command (PdMdpService *self,
                                const gchar  *name)
{
}

gboolean
pd_mdp_service_command_enabled (PdMdpService *self,
                                const gchar  *name)
{
  return FALSE;
}

void
pd_mdp_service_push_request (PdMdpService *self,
                             gpointer      request)
{
}

gpointer
pd_mdp_service_pop_request (PdMdpService *self)
{
  return NULL;
}

gpointer
pd_mdp_service_peek_request (PdMdpService *self)
{
  return NULL;
}

gint
pd_mdp_service_requests_length (PdMdpService *self)
{
  return 0;
}

gint
pd_mdp_service_worker_count (PdMdpService *self)
{
  return 0;
}

void
pd_mdp_service_add_waiting (PdMdpService *self,
                            gpointer      waiting)
{
}

void
pd_mdp_service_remove_waiting (PdMdpService *self,
                               gpointer      waiting)
{
}

gchar *
pd_mdp_service_dup_name (PdMdpService *self)
{
  return NULL;
}

void
pd_mdp_service_set_name (PdMdpService *self,
                         const gchar  *name)
{
}

PdBroker *
pd_mdp_service_ref_broker (PdMdpService *self)
{
  return NULL;
}

void
pd_mdp_service_set_broker (PdMdpService *self,
                           PdBroker     *broker)
{
}
