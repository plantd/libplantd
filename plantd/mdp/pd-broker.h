/*
 * Copyright © the Plantd contributors. All rights reserved.
 *
 * This file is part of libplantd, distributed under the MIT license.
 * For full terms see the included LICENSE file.
 */

#pragma once

#if !defined (PLANTD_INSIDE) && !defined (PLANTD_COMPILATION)
# error "Only <plantd/plantd.h> can be included directly."
#endif

#include <glib-object.h>

G_BEGIN_DECLS

/*
 * Type declaration.
 */
#define PD_TYPE_BROKER pd_broker_get_type ()
G_DECLARE_FINAL_TYPE (PdBroker, pd_broker, PD, BROKER, GObject)

/*
 * Method definitions.
 */
PdBroker    *pd_broker_new           (const gchar *endpoint);

void         pd_broker_bind          (PdBroker    *self);
void         pd_broker_run           (PdBroker    *self);
void         pd_broker_stop          (PdBroker    *self);

const gchar *pd_broker_get_endpoint  (PdBroker    *self);
void         pd_broker_set_endpoint  (PdBroker    *self,
                                      const gchar *endpoint);

gint         pd_broker_get_liveness  (PdBroker    *self);
void         pd_broker_set_liveness  (PdBroker    *self,
                                      gint         liveness);

gint         pd_broker_get_interval  (PdBroker    *self);
void         pd_broker_set_interval  (PdBroker    *self,
                                      gint         interval);

gint         pd_broker_get_expiry    (PdBroker    *self);
void         pd_broker_set_expiry    (PdBroker    *self,
                                      gint         expiry);

G_END_DECLS
