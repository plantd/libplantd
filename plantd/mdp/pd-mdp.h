/*
 * Copyright © the Plantd contributors. All rights reserved.
 *
 * This file is part of libplantd, distributed under the MIT license.
 * For full terms see the included LICENSE file.
 */

#include <glib-unix.h>
#include <czmq.h>

#include "plantd-debug.h"

#pragma once

#if !defined (PLANTD_INSIDE) && !defined (PLANTD_COMPILATION)
# error "Only <plantd/plantd.h> can be included directly."
#endif

// This is the version of MDP/Client we implement
#define PD_MDP_CLIENT             "MDPC01"

// MDP/Client commands, as strings
#define PD_MDP_CLIENT_REQUEST     "\001"
#define PD_MDP_CLIENT_REPLY       "\002"
#define PD_MDP_CLIENT_NAK         "\003"

G_GNUC_UNUSED static const gchar *mdpc_commands [] = {
  NULL,
  "REQUEST",
  "REPLY",
  "NAK"
};

// This is the version of MDP/Worker we implement
#define PD_MDP_WORKER             "MDPW01"

// MDP/Server commands, as strings
#define PD_MDP_WORKER_READY       "\001"
#define PD_MDP_WORKER_REQUEST     "\002"
#define PD_MDP_WORKER_REPLY       "\003"
#define PD_MDP_WORKER_HEARTBEAT   "\004"
#define PD_MDP_WORKER_DISCONNECT  "\005"

static const gchar *mdpw_commands [] = {
  NULL,
  "READY",
  "REQUEST",
  "REPLY",
  "HEARTBEAT",
  "DISCONNECT"
};

void pd_mdp_dump (zmsg_t *msg);
