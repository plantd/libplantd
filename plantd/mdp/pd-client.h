/*
 * Copyright © the Plantd contributors. All rights reserved.
 *
 * This file is part of libplantd, distributed under the MIT license.
 * For full terms see the included LICENSE file.
 */

#pragma once

#if !defined (PLANTD_INSIDE) && !defined (PLANTD_COMPILATION)
# error "Only <plantd/plantd.h> can be included directly."
#endif

#include <glib-object.h>
#include <czmq.h>

G_BEGIN_DECLS

/*
 * Type declaration.
 */
#define PD_TYPE_CLIENT pd_client_get_type ()
G_DECLARE_FINAL_TYPE (PdClient, pd_client, PD, CLIENT, GObject)

/*
 * Method definitions.
 */
PdClient    *pd_client_new           (const gchar  *broker);

gint         pd_client_send          (PdClient     *self,
                                      const gchar  *service,
                                      zmsg_t      **request_p);
gint         pd_client_send_request  (PdClient     *self,
                                      const gchar  *service,
                                      const gchar  *rpc,
                                      const gchar  *request);
zmsg_t      *pd_client_recv          (PdClient     *self,
                                      char        **service);
gchar       *pd_client_recv_response (PdClient     *self);

const gchar *pd_client_get_broker    (PdClient     *self);
void         pd_client_set_broker    (PdClient     *self,
                                      const gchar  *broker);

void         pd_client_set_timeout   (PdClient     *self,
                                      gint          timeout);

G_END_DECLS
