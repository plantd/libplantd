/*
 * Copyright © the Plantd contributors. All rights reserved.
 *
 * This file is part of libplantd, distributed under the MIT license.
 * For full terms see the included LICENSE file.
 */

#pragma once

#if !defined (PLANTD_INSIDE) && !defined (PLANTD_COMPILATION)
# error "Only <plantd/plantd.h> can be included directly."
#endif

#include <glib-object.h>

#include <plantd/plantd-types.h>

G_BEGIN_DECLS

#define PD_TYPE_TRANSITION_TABLE pd_transition_table_get_type ()
G_DECLARE_FINAL_TYPE (PdTransitionTable, pd_transition_table, PD, TRANSITION_TABLE, GObject)

PdTransitionTable *pd_transition_table_new      (void);

gboolean           pd_transition_table_add      (PdTransitionTable *self,
                                                 PdTransition      *transition);
gboolean           pd_transition_table_remove   (PdTransitionTable *self,
                                                 gint               row);
gint               pd_transition_table_length   (PdTransitionTable *self);
PdTransition      *pd_transition_table_get      (PdTransitionTable *self,
                                                 gint               row);
gboolean           pd_transition_table_contains (PdTransitionTable *self,
                                                 PdTransition      *transition);

G_END_DECLS
