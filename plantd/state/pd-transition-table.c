/*
 * Copyright © the Plantd contributors. All rights reserved.
 *
 * This file is part of libplantd, distributed under the MIT license.
 * For full terms see the included LICENSE file.
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include "pd-transition-table.h"

#include "plantd/plantd.h"

/*
 * PdTransitionTable:
 *
 * Represents a ...
 */
struct _PdTransitionTable
{
  GObject    parent;
  GPtrArray *transitions;
};

G_DEFINE_TYPE (PdTransitionTable, pd_transition_table, G_TYPE_OBJECT)

static void
pd_transition_table_finalize (GObject *object)
{
  PdTransitionTable *self = (PdTransitionTable *)object;

  if (self->transitions)
    g_ptr_array_free (self->transitions, TRUE);

  G_OBJECT_CLASS (pd_transition_table_parent_class)->finalize (object);
}

static void
pd_transition_table_class_init (PdTransitionTableClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->finalize = pd_transition_table_finalize;
}

static void
pd_transition_table_init (PdTransitionTable *self)
{
}

PdTransitionTable *
pd_transition_table_new (void)
{
  return g_object_new (PD_TYPE_TRANSITION_TABLE, NULL);
}

/*
 *const gchar *
 *pd_transition_table_to_string (PdTransitionTable *self)
 *{
 *  g_autoptr (GString) str = NULL;
 *
 *  g_return_val_if_fail (PD_IS_TRANSITION_TABLE (self), NULL);
 *
 *  str = g_string_new (" # | start |  end |");
 *
 *  for (gint i = 0; i < self->transitions->len; i++)
 *    {
 *      PdTransition *el;
 *      PdState *start;
 *      PdState *end;
 *
 *      el = g_ptr_array_index (self->transitions, i);
 *
 *      g_return_val_if_fail (PD_IS_TRANSITION (el), NULL);
 *
 *      g_string_append_printf (str, "", );
 *
 *    }
 *
 *  return g_strdup (str->str);
 *}
 */

gboolean
pd_transition_table_add (PdTransitionTable *self,
                         PdTransition      *transition)
{
  g_return_val_if_fail (PD_IS_TRANSITION_TABLE (self), FALSE);
  g_return_val_if_fail (PD_IS_TRANSITION (transition), FALSE);

  if (self->transitions == NULL)
    self->transitions = g_ptr_array_new ();

  g_ptr_array_add (self->transitions, transition);

  return TRUE;
}

/**
 * pd_transition_table_remove:
 * @self: a #PdTransitionTable
 * @row: the row number to remove, not the row index
 *
 * returns: TRUE if the row was removed, FALSE otherwise
 */
gboolean
pd_transition_table_remove (PdTransitionTable *self,
                            gint               row)
{
  g_return_val_if_fail (PD_IS_TRANSITION_TABLE (self), FALSE);
  g_return_val_if_fail (self->transitions != NULL, FALSE);

  // Array is not zero terminated so there's no extra element to -1
  if (row <= self->transitions->len)
    {
      g_ptr_array_remove_index (self->transitions, row - 1);
      return TRUE;
    }

  return FALSE;
}

gint
pd_transition_table_length (PdTransitionTable *self)
{
  g_return_val_if_fail (PD_IS_TRANSITION_TABLE (self), 0);
  g_return_val_if_fail (self->transitions != NULL, 0);

  return self->transitions->len;
}

/**
 * pd_transition_table_get:
 * @self: a #PdTransitionTable
 *
 * Retrieve the #PdTransition located at the given row index.
 *
 * Returns: (transfer none): a #PdTransition if one is set at the index given.
 */
PdTransition *
pd_transition_table_get (PdTransitionTable *self,
                         gint               row)
{
  g_return_val_if_fail (PD_IS_TRANSITION_TABLE (self), NULL);
  g_return_val_if_fail (self->transitions != NULL, NULL);

  return g_ptr_array_index (self->transitions, row);
}

gboolean
pd_transition_table_contains (PdTransitionTable *self,
                              PdTransition      *transition)
{
  g_return_val_if_fail (PD_IS_TRANSITION_TABLE (self), FALSE);
  g_return_val_if_fail (PD_IS_TRANSITION (transition), FALSE);

  for (gint i = 0; i < self->transitions->len; i++)
    {
      PdTransition *el;

      el = g_ptr_array_index (self->transitions, i);

      g_return_val_if_fail (PD_IS_TRANSITION (el), FALSE);

      if (pd_transition_equal (transition, el))
        return TRUE;
    }

  return FALSE;
}
