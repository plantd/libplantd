/*
 * Copyright © the Plantd contributors. All rights reserved.
 *
 * This file is part of libplantd, distributed under the MIT license.
 * For full terms see the included LICENSE file.
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <json-glib/json-glib.h>

#include "pd-state.h"

struct _PdState
{
  GObject  parent;
  gint     id;
  gchar   *name;
  gchar   *description;
};

enum {
  PROP_0,
  PROP_ID,
  PROP_NAME,
  PROP_DESCRIPTION,
  N_PROPS
};

static GParamSpec *properties [N_PROPS];

G_DEFINE_TYPE (PdState, pd_state, G_TYPE_OBJECT)

static void
pd_state_finalize (GObject *object)
{
  PdState *self = (PdState *)object;

  g_clear_pointer (&self->name, g_free);
  g_clear_pointer (&self->description, g_free);

  G_OBJECT_CLASS (pd_state_parent_class)->finalize (object);
}

static void
pd_state_get_property (GObject    *object,
                       guint       prop_id,
                       GValue     *value,
                       GParamSpec *pspec)
{
  PdState *self = PD_STATE (object);

  switch (prop_id)
    {
    case PROP_ID:
      g_value_set_int (value, pd_state_get_id (self));
      break;

    case PROP_NAME:
      g_value_take_string (value, pd_state_dup_name (self));
      break;

    case PROP_DESCRIPTION:
      g_value_take_string (value, pd_state_dup_description (self));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
pd_state_set_property (GObject      *object,
                       guint         prop_id,
                       const GValue *value,
                       GParamSpec   *pspec)
{
  PdState *self = PD_STATE (object);

  switch (prop_id)
    {
    case PROP_ID:
      pd_state_set_id (self, g_value_get_int (value));
      break;

    case PROP_NAME:
      pd_state_set_name (self, g_value_get_string (value));
      break;

    case PROP_DESCRIPTION:
      pd_state_set_description (self, g_value_get_string (value));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
pd_state_class_init (PdStateClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->finalize = pd_state_finalize;
  object_class->get_property = pd_state_get_property;
  object_class->set_property = pd_state_set_property;

  properties [PROP_ID] =
    g_param_spec_int ("id",
                      "ID",
                      "The ID of the state.",
                      G_MININT,
                      G_MAXINT,
                      0,
                      (G_PARAM_READWRITE | G_PARAM_EXPLICIT_NOTIFY));

  properties [PROP_NAME] =
    g_param_spec_string ("name",
                         "Name",
                         "The name of the state.",
                         NULL,
                         (G_PARAM_READWRITE | G_PARAM_EXPLICIT_NOTIFY | G_PARAM_STATIC_STRINGS));

  properties [PROP_DESCRIPTION] =
    g_param_spec_string ("description",
                         "Description",
                         "The state description",
                         NULL,
                         (G_PARAM_READWRITE | G_PARAM_EXPLICIT_NOTIFY | G_PARAM_STATIC_STRINGS));

  g_object_class_install_properties (object_class, N_PROPS, properties);
}

static void
pd_state_init (PdState *self)
{
}

PdState *
pd_state_new (void)
{
  return g_object_new (PD_TYPE_STATE, NULL);
}

PdState *
pd_state_new_full (gint         id,
                   const gchar *name,
                   const gchar *description)
{
  return g_object_new (PD_TYPE_STATE,
                       "id", id,
                       "name", name,
                       "description", description,
                       NULL);
}

PdState *
pd_state_copy (PdState *self)
{
  g_return_val_if_fail (PD_IS_STATE (self), NULL);

  return g_object_new (PD_TYPE_STATE,
                       "id", pd_state_get_id (self),
                       "name", pd_state_get_name (self),
                       "description", pd_state_get_description (self),
                       NULL);
}

gboolean
pd_state_equal (PdState *a,
                PdState *b)
{
  g_return_val_if_fail (PD_IS_STATE (a), FALSE);
  g_return_val_if_fail (PD_IS_STATE (b), FALSE);

  return (pd_state_get_id (a) == pd_state_get_id (b));
}

/**
 * pd_state_serialize:
 * @self: a #PdState
 *
 * Returns the serialized json data.
 *
 * Returns: (transfer full): serialized data that the receiver must free.
 */
gchar *
pd_state_serialize (PdState *self)
{
  gchar *ret;

  g_return_val_if_fail (PD_IS_STATE (self), NULL);

  ret = json_gobject_to_data (G_OBJECT (self), NULL);

  return g_steal_pointer (&ret);
}

void
pd_state_deserialize (PdState     *self,
                      const gchar *data)
{
  g_autoptr (GObject) object = NULL;
  g_autofree gchar *name = NULL;
  g_autofree gchar *description = NULL;

  GError *err = NULL;
  object = json_gobject_from_data (PD_TYPE_STATE,
                                   data,
                                   -1,
                                   &err);

  if (err != NULL)
    {
      g_critical ("%s", err->message);
      g_error_free (err);
    }

  g_return_if_fail (object != NULL);
  g_return_if_fail (PD_IS_STATE (object));

  name = pd_state_dup_name (PD_STATE (object));
  description = pd_state_dup_description (PD_STATE (object));

  pd_state_set_id (self, pd_state_get_id (PD_STATE (object)));
  pd_state_set_name (self, name);
  pd_state_set_description (self, description);

  g_clear_object (&object);
}

gint
pd_state_get_id (PdState *self)
{
  // TODO: define a no-state state for this
  g_return_val_if_fail (PD_IS_STATE (self), -1);

  return self->id;
}

void
pd_state_set_id (PdState *self,
                 gint     id)
{
  g_return_if_fail (PD_IS_STATE (self));

  self->id = id;
}

const gchar *
pd_state_get_name (PdState *self)
{
  g_return_val_if_fail (PD_IS_STATE (self), NULL);

  return self->name;
}

/**
 * pd_state_dup_name:
 *
 * Copies the name of the state and returns it to the caller (after
 * locking the object). A copy is used to avoid thread-races.
 *
 * Returns: (transfer full): a copy of the state name.
 */
gchar *
pd_state_dup_name (PdState *self)
{
  gchar *ret;

  g_return_val_if_fail (PD_IS_STATE (self), NULL);

  /*pd_object_lock (PD_OBJECT (self));*/
  ret = g_strdup (self->name);
  /*pd_object_unlock (PD_OBJECT (self));*/

  return g_steal_pointer (&ret);
}

void
pd_state_set_name (PdState     *self,
                   const gchar *name)
{
  g_return_if_fail (PD_IS_STATE (self));

  if (g_strcmp0 (name, self->name) != 0)
    {
      g_free (self->name);
      self->name = g_strdup (name);
      g_object_notify_by_pspec (G_OBJECT (self), properties [PROP_NAME]);
    }
}

const gchar *
pd_state_get_description (PdState *self)
{
  g_return_val_if_fail (PD_IS_STATE (self), NULL);

  return self->description;
}

/**
 * pd_state_dup_description:
 *
 * Copies the description of the state and returns it to the caller (after
 * locking the object). A copy is used to avoid thread-races.
 *
 * Returns: (transfer full): a copy of the state description.
 */
gchar *
pd_state_dup_description (PdState *self)
{
  gchar *ret;

  g_return_val_if_fail (PD_IS_STATE (self), NULL);

  /*pd_object_lock (PD_OBJECT (self));*/
  ret = g_strdup (self->description);
  /*pd_object_unlock (PD_OBJECT (self));*/

  return g_steal_pointer (&ret);
}

void
pd_state_set_description (PdState     *self,
                          const gchar *description)
{
  g_return_if_fail (PD_IS_STATE (self));

  if (g_strcmp0 (description, self->description) != 0)
    {
      g_free (self->description);
      self->description = g_strdup (description);
      g_object_notify_by_pspec (G_OBJECT (self), properties [PROP_DESCRIPTION]);
    }
}
