/*
 * Copyright © the Plantd contributors. All rights reserved.
 *
 * This file is part of libplantd, distributed under the MIT license.
 * For full terms see the included LICENSE file.
 */

#pragma once

#if !defined (PLANTD_INSIDE) && !defined (PLANTD_COMPILATION)
# error "Only <plantd/plantd.h> can be included directly."
#endif

#include <glib-object.h>

G_BEGIN_DECLS

#define PD_TYPE_STATE pd_state_get_type ()
G_DECLARE_FINAL_TYPE (PdState, pd_state, PD, STATE, GObject)

PdState     *pd_state_new             (void);
PdState     *pd_state_new_full        (gint         id,
                                       const gchar *name,
                                       const gchar *description);

PdState     *pd_state_copy            (PdState     *self);
void         pd_state_free            (PdState     *self);
gboolean     pd_state_equal           (PdState     *a,
                                       PdState     *b);

gchar       *pd_state_serialize       (PdState     *self);
void         pd_state_deserialize     (PdState     *self,
                                       const gchar *data);

gint         pd_state_get_id          (PdState     *self);
void         pd_state_set_id          (PdState     *self,
                                       gint         id);

const gchar *pd_state_get_name        (PdState     *self);
gchar       *pd_state_dup_name        (PdState     *self);
void         pd_state_set_name        (PdState     *self,
                                       const gchar *name);

const gchar *pd_state_get_description (PdState     *self);
gchar       *pd_state_dup_description (PdState     *self);
void         pd_state_set_description (PdState     *self,
                                       const gchar *description);

G_END_DECLS
