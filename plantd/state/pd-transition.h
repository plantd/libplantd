/*
 * Copyright © the Plantd contributors. All rights reserved.
 *
 * This file is part of libplantd, distributed under the MIT license.
 * For full terms see the included LICENSE file.
 */

#pragma once

#if !defined (PLANTD_INSIDE) && !defined (PLANTD_COMPILATION)
# error "Only <plantd/plantd.h> can be included directly."
#endif

#include <glib-object.h>

#include <plantd/plantd-types.h>

G_BEGIN_DECLS

#define PD_TYPE_TRANSITION pd_transition_get_type ()
G_DECLARE_DERIVABLE_TYPE (PdTransition, pd_transition, PD, TRANSITION, GObject)

struct _PdTransitionClass
{
  /*< private >*/
  GObjectClass parent_class;

  /* vfuncs */

  gboolean (*evaluate) (PdTransition *self,
                        PdEvent      *event);
  void     (*execute)  (PdTransition *self,
                        gpointer      data);

  /*< private >*/
  gpointer padding[12];
};

PdTransition *pd_transition_new             (gint          id);

gboolean      pd_transition_equal           (PdTransition *a,
                                             PdTransition *b);

gboolean      pd_transition_evaluate        (PdTransition *self,
                                             PdEvent      *event);
void          pd_transition_execute         (PdTransition *self,
                                             gpointer      data);

gint          pd_transition_get_id          (PdTransition *self);
void          pd_transition_set_id          (PdTransition *self,
                                             gint          id);

PdState      *pd_transition_get_start_state (PdTransition *self);
PdState      *pd_transition_ref_start_state (PdTransition *self);
void          pd_transition_set_start_state (PdTransition *self,
                                             PdState      *start);

PdState      *pd_transition_get_end_state   (PdTransition *self);
PdState      *pd_transition_ref_end_state   (PdTransition *self);
void          pd_transition_set_end_state   (PdTransition *self,
                                             PdState      *end);

G_END_DECLS
