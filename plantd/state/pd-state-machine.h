/*
 * Copyright © the Plantd contributors. All rights reserved.
 *
 * This file is part of libplantd, distributed under the MIT license.
 * For full terms see the included LICENSE file.
 */

#pragma once

#if !defined (PLANTD_INSIDE) && !defined (PLANTD_COMPILATION)
# error "Only <plantd/plantd.h> can be included directly."
#endif

#include <glib-object.h>

#include <plantd/plantd-types.h>

G_BEGIN_DECLS

#define PD_TYPE_STATE_MACHINE pd_state_machine_get_type ()
G_DECLARE_DERIVABLE_TYPE (PdStateMachine, pd_state_machine, PD, STATE_MACHINE, GObject)

/* TODO: remove if private class isn't needed for signal function */
struct _PdStateMachineClass
{
  /*< private >*/
  GObjectClass parent_class;

  /*< public >*/
  /* signals */
  void (*state_changed) (PdApplication *self,
                         PdState       *state);

  /*< private >*/
  gpointer padding[12];
};

PdStateMachine *pd_state_machine_new                  (void);

void            pd_state_machine_start                (PdStateMachine    *self);
void            pd_state_machine_stop                 (PdStateMachine    *self);
gboolean        pd_state_machine_running              (PdStateMachine    *self);
void            pd_state_machine_submit_event         (PdStateMachine    *self,
                                                       PdEvent           *event);
void            pd_state_machine_set_transition_table (PdStateMachine    *self,
                                                       PdTransitionTable *table);
PdState        *pd_state_machine_get_current_state    (PdStateMachine    *self);
gboolean        pd_state_machine_set_initial_state    (PdStateMachine    *self,
                                                       PdState           *state);

G_END_DECLS
