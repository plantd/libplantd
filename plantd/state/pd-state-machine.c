/*
 * Copyright © the Plantd contributors. All rights reserved.
 *
 * This file is part of libplantd, distributed under the MIT license.
 * For full terms see the included LICENSE file.
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <gio/gio.h>

#include "pd-state-machine.h"
#include "pd-state.h"
#include "pd-transition.h"
#include "pd-transition-table.h"
#include "core/pd-event.h"

// TODO: implement add_sub_machine for forks

/**
 * SECTION:plantd-state-machine
 * @short_description: State machine class
 *
 * An #PdStateMachine is used by applications that control program flow by
 * altering state using events.
 */

/*
 * PdStateMachine:
 *
 * #PdStateMachine is an implementation of a finite state machine.
 */

typedef struct
{
  /*< private >*/
  PdTransitionTable *table;
  gboolean             running;
  PdState           *initial_state;
  PdState           *current_state;
  GAsyncQueue         *queue;
} PdStateMachinePrivate;

enum {
  SIGNAL_STATE_CHANGED,
  N_SIGNALS
};

static guint signals [N_SIGNALS];

G_DEFINE_TYPE_WITH_PRIVATE (PdStateMachine, pd_state_machine, G_TYPE_OBJECT)

static void
pd_state_machine_finalize (GObject *object)
{
  PdStateMachine *self = (PdStateMachine *)object;
  PdStateMachinePrivate *priv;

  priv = pd_state_machine_get_instance_private (self);

  pd_state_machine_stop (self);

  g_clear_object (&priv->table);
  g_clear_object (&priv->initial_state);
  g_clear_object (&priv->current_state);
  g_clear_pointer (&priv->queue, g_async_queue_unref);

  G_OBJECT_CLASS (pd_state_machine_parent_class)->finalize (object);
}

static void
pd_state_machine_class_init (PdStateMachineClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->finalize = pd_state_machine_finalize;

  /**
   * PdStateMachine::state_changed:
   * @self: the state machine
   *
   * The ::state_changed signal is emitted on the primary instance
   * after it has transitioned into a new state.
   */
  signals [SIGNAL_STATE_CHANGED] =
    g_signal_new ("state-changed", PD_TYPE_STATE_MACHINE, G_SIGNAL_RUN_FIRST,
                  G_STRUCT_OFFSET (PdStateMachineClass, state_changed),
                  NULL, NULL, NULL,
                  G_TYPE_NONE, 1, PD_TYPE_STATE);
}

static void
pd_state_machine_init (PdStateMachine *self)
{
  PdStateMachinePrivate *priv;

  priv = pd_state_machine_get_instance_private (self);

  priv->running = FALSE;
  priv->queue = g_async_queue_new ();
}

PdStateMachine *
pd_state_machine_new (void)
{
  return g_object_new (PD_TYPE_STATE_MACHINE, NULL);
}

static void
_update_state (PdStateMachine *self,
               PdTransition   *transition)
{
  PdStateMachinePrivate *priv;
  PdState *new_state;

  priv = pd_state_machine_get_instance_private (self);

  new_state = pd_transition_get_end_state (transition);
  g_set_object (&priv->current_state, new_state);

  // XXX: not totally positive that passing the state machine down is wise
  pd_transition_execute (transition, self);
}

static gboolean
_process_event (PdStateMachine *self,
                PdEvent        *event)
{
  PdStateMachinePrivate *priv;
  g_autoptr (PdState) frozen = NULL;

  priv = pd_state_machine_get_instance_private (self);

  frozen = pd_state_copy (priv->current_state);

  for (gint i = 0; i < pd_transition_table_length (priv->table); i++)
    {
      PdTransition *transition;
      PdState *start;
      PdState *end;

      transition = pd_transition_table_get (priv->table, i);
      start = pd_transition_get_start_state (transition);
      end = pd_transition_get_end_state (transition);

      g_debug (" * i:%2d/%2d | t:%5d | f:%5d | s:%5d | e:%5d",
               i, pd_transition_table_length (priv->table),
               pd_transition_get_id (transition),
               pd_state_get_id (frozen),
               pd_state_get_id (start),
               pd_state_get_id (end));

      if (pd_state_equal (start, frozen))
        {
          g_debug (" > i:%2d/%2d | t:%5d | f:%5d = s:%5d | e:%5d | evt:%5d",
                   i, pd_transition_table_length (priv->table),
                   pd_transition_get_id (transition),
                   pd_state_get_id (frozen),
                   pd_state_get_id (start),
                   pd_state_get_id (end),
                   pd_event_get_id (event));

          if (pd_transition_evaluate (transition, event))
            {
              _update_state (self, transition);
              g_signal_emit (self, signals [SIGNAL_STATE_CHANGED], 0, priv->current_state);

              return TRUE;
            }
        }
    }

  return FALSE;
}

static void
_run_state_machine_cb (GObject      *source_object,
                       GAsyncResult *result,
                       gpointer      user_data)
{
  g_debug ("state machine finished");
}

static void
_run_state_machine_cancel (PdStateMachine *self,
                           gpointer        data)
{
  PdStateMachinePrivate *priv;
  GCancellable *cancellable G_GNUC_UNUSED;

  priv = pd_state_machine_get_instance_private (self);

  /*cancellable = G_CANCELLABLE (cancellable);*/

  priv->running = FALSE;

  g_debug ("state machine cancelled");
}

static void
_run_state_machine_thread (GTask        *task,
                           gpointer      source_object,
                           gpointer      task_data,
                           GCancellable *cancellable)
{
  PdStateMachinePrivate *priv;

  g_assert (source_object == g_task_get_source_object (task));
  g_assert (cancellable == g_task_get_cancellable (task));

  priv = pd_state_machine_get_instance_private (source_object);

  g_debug ("starting state machine");

  if (priv->table == NULL)
    g_task_return_boolean (task, FALSE);

  if (pd_transition_table_length (priv->table) == 0)
    g_task_return_boolean (task, FALSE);

  /*
   *if (priv->initial_state != NULL)
   *  priv->current_state = priv->initial_state;
   *else
   *  priv->current_state = pd_transition_get_start_state (
   *      pd_transition_table_get (priv->table, 0));
   */

  priv->current_state = pd_transition_get_start_state (
      pd_transition_table_get (priv->table, 0));

  /*
   *g_debug ("starting at %s (%d): %s",
   *         pd_state_get_id (priv->current_state),
   *         pd_state_get_name (priv->current_state),
   *         pd_state_get_description (priv->current_state));
   */

  priv->running = TRUE;

  while (priv->running)
    {
      g_autoptr (PdEvent) event = NULL;

      if (g_cancellable_is_cancelled (cancellable))
        {
          break;
        }

      g_async_queue_lock (priv->queue);

      // block and wait for next event
      event = g_async_queue_pop_unlocked (priv->queue);

      if (!_process_event (PD_STATE_MACHINE (source_object), PD_EVENT (event)))
        g_debug ("skip state %d in %d",
                 pd_event_get_id (event),
                 pd_state_get_id (priv->current_state));

      g_async_queue_unlock (priv->queue);
    }

  priv->running = FALSE;

  g_task_return_boolean (task, TRUE);
}

static void
_run_state_machine_async (PdStateMachine *self)
{
  g_autoptr (GTask) task = NULL;
  g_autoptr (GCancellable) cancellable = NULL;

  g_return_if_fail (PD_IS_STATE_MACHINE (self));

  cancellable = g_cancellable_new ();
  task = g_task_new (self, cancellable, _run_state_machine_cb, NULL);

  g_task_run_in_thread (task, _run_state_machine_thread);
}

void
pd_state_machine_start (PdStateMachine *self)
{
  g_return_if_fail (PD_IS_STATE_MACHINE (self));

  _run_state_machine_async (self);
}

void
pd_state_machine_stop (PdStateMachine *self)
{
  g_return_if_fail (PD_IS_STATE_MACHINE (self));

  _run_state_machine_cancel (self, NULL);
}

gboolean
pd_state_machine_running (PdStateMachine *self)
{
  PdStateMachinePrivate *priv;

  g_return_val_if_fail (PD_IS_STATE_MACHINE (self), FALSE);

  priv = pd_state_machine_get_instance_private (self);

  return priv->running;
}

void
pd_state_machine_submit_event (PdStateMachine *self,
                               PdEvent        *event)
{
  PdStateMachinePrivate *priv;
  PdEvent *copy;

  g_return_if_fail (PD_IS_STATE_MACHINE (self));
  g_return_if_fail (PD_IS_EVENT (event));

  priv = pd_state_machine_get_instance_private (self);

  copy = pd_event_new_full (pd_event_get_id (event),
                              pd_event_get_name (event),
                              pd_event_get_description (event));

  g_async_queue_lock (priv->queue);
  g_async_queue_push_unlocked (priv->queue, copy);
  g_async_queue_unlock (priv->queue);
}

void
pd_state_machine_set_transition_table (PdStateMachine    *self,
                                       PdTransitionTable *table)
{
  PdStateMachinePrivate *priv;

  g_return_if_fail (PD_IS_STATE_MACHINE (self));
  g_return_if_fail (PD_IS_TRANSITION_TABLE (table));

  priv = pd_state_machine_get_instance_private (self);

  g_set_object (&priv->table, table);
}

/**
 * pd_state_machine_get_current_state:
 * @self: a #PdStateMachine
 *
 * Retrieve the #PdState held as the current state.
 *
 * Returns: (transfer none): a #PdState if one is set as the current state.
 */
PdState *
pd_state_machine_get_current_state (PdStateMachine *self)
{
  PdStateMachinePrivate *priv;

  g_return_val_if_fail (PD_IS_STATE_MACHINE (self), NULL);

  priv = pd_state_machine_get_instance_private (self);

  return priv->current_state;
}

gboolean
pd_state_machine_set_initial_state (PdStateMachine *self,
                                    PdState        *state)
{
  PdStateMachinePrivate *priv;

  g_return_val_if_fail (PD_IS_STATE_MACHINE (self), FALSE);

  priv = pd_state_machine_get_instance_private (self);

  if (priv->running)
    return FALSE;

  if (pd_state_equal (priv->initial_state, state))
    return TRUE;

  if (g_set_object (&priv->initial_state, state))
    return TRUE;

  return FALSE;
}
