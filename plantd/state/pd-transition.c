/*
 * Copyright © the Plantd contributors. All rights reserved.
 *
 * This file is part of libplantd, distributed under the MIT license.
 * For full terms see the included LICENSE file.
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include "pd-transition.h"
#include "pd-state.h"
#include "core/pd-event.h"

/**
 * SECTION:plantd-transition
 * @short_description: Transition base class
 *
 * An #PdTransition is used by applications that connect to message buses.
 */

/**
 * PdTransition:
 *
 * #PdTransition is used with #PdStateMachine instances and meant to
 * control program flow.
 */

/**
 * PdTransitionClass:
 * @evaluate: invoked to determine if the transition should occur.
 * @execute: invoked by the state machine when the transition takes place.
 *
 * Virtual function table for #PdTransition.
 */

typedef struct
{
  /*< public >*/
  gint       id;
  PdState *start;
  PdState *end;
} PdTransitionPrivate;

enum {
  PROP_0,
  PROP_ID,
  PROP_START,
  PROP_END,
  N_PROPS
};

static GParamSpec *properties [N_PROPS];

G_DEFINE_TYPE_WITH_PRIVATE (PdTransition, pd_transition, G_TYPE_OBJECT)

static void
pd_transition_finalize (GObject *object)
{
  PdTransitionPrivate *priv;

  priv = pd_transition_get_instance_private (PD_TRANSITION (object));

  g_clear_object (&priv->start);
  g_clear_object (&priv->end);

  G_OBJECT_CLASS (pd_transition_parent_class)->finalize (object);
}

static void
pd_transition_get_property (GObject    *object,
                            guint       prop_id,
                            GValue     *value,
                            GParamSpec *pspec)
{
  PdTransition *self;
  G_GNUC_UNUSED PdTransitionPrivate *priv;

  self = PD_TRANSITION (object);
  priv = pd_transition_get_instance_private (self);

  switch (prop_id)
    {
    case PROP_ID:
      g_value_set_int (value, pd_transition_get_id (self));
      break;

    case PROP_START:
      g_value_take_object (value, pd_transition_ref_start_state (self));
      break;

    case PROP_END:
      g_value_take_object (value, pd_transition_ref_end_state (self));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
pd_transition_set_property (GObject      *object,
                            guint         prop_id,
                            const GValue *value,
                            GParamSpec   *pspec)
{
  PdTransition *self;

  self = PD_TRANSITION (object);

  switch (prop_id)
    {
    case PROP_ID:
      pd_transition_set_id (self, g_value_get_int (value));
      break;

    case PROP_START:
      pd_transition_set_start_state (self, g_value_get_object (value));
      break;

    case PROP_END:
      pd_transition_set_end_state (self, g_value_get_object (value));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static gboolean
pd_transition_real_evaluate (PdTransition *self,
                             PdEvent      *event)
{
  PdTransitionPrivate *priv;

  g_return_val_if_fail (PD_IS_TRANSITION (self), FALSE);

  priv = pd_transition_get_instance_private (self);

  if (priv->id == pd_event_get_id (event))
    return TRUE;

  return FALSE;
}

static void
pd_transition_real_execute (PdTransition *self,
                            gpointer      data)
{
  PdTransitionPrivate *priv;

  g_return_if_fail (PD_IS_TRANSITION (self));

  priv = pd_transition_get_instance_private (self);

  g_info ("Transition ID: %d\n", priv->id);
}

static void
pd_transition_class_init (PdTransitionClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->finalize = pd_transition_finalize;

  klass->evaluate = pd_transition_real_evaluate;
  klass->execute = pd_transition_real_execute;

  object_class->get_property = pd_transition_get_property;
  object_class->set_property = pd_transition_set_property;

  properties [PROP_ID] =
    g_param_spec_int ("id",
                      "ID",
                      "The ID of the transition.",
                      G_MININT,
                      G_MAXINT,
                      -1,
                      G_PARAM_READWRITE);

  properties [PROP_START] =
    g_param_spec_object ("start-state",
                         "Start state",
                         "The starting state of the transition.",
                         PD_TYPE_STATE,
                         (G_PARAM_READWRITE | G_PARAM_EXPLICIT_NOTIFY | G_PARAM_STATIC_STRINGS));

  properties [PROP_END] =
    g_param_spec_object ("end-state",
                         "End state",
                         "The ending state of the transition.",
                         PD_TYPE_STATE,
                         (G_PARAM_READWRITE | G_PARAM_EXPLICIT_NOTIFY | G_PARAM_STATIC_STRINGS));

  g_object_class_install_properties (object_class, N_PROPS, properties);
}

static void
pd_transition_init (PdTransition *self)
{
}

PdTransition *
pd_transition_new (gint id)
{
  return g_object_new (PD_TYPE_TRANSITION,
                       "id", id,
                       NULL);
}

gboolean
pd_transition_equal (PdTransition *a,
                     PdTransition *b)
{
  g_return_val_if_fail (PD_IS_TRANSITION (a), FALSE);
  g_return_val_if_fail (PD_IS_TRANSITION (b), FALSE);

  return (pd_transition_get_id (a) == pd_transition_get_id (b));
}

gboolean
pd_transition_evaluate (PdTransition *self,
                        PdEvent      *event)
{
  PdTransitionClass *klass;

  g_return_val_if_fail (PD_IS_TRANSITION (self), FALSE);
  g_return_val_if_fail (PD_IS_EVENT (event), FALSE);

  klass = PD_TRANSITION_GET_CLASS (self);
  g_return_val_if_fail (klass->evaluate != NULL, FALSE);

  return klass->evaluate (self, event);
}

void
pd_transition_execute (PdTransition *self,
                       gpointer      data)
{
  PdTransitionClass *klass;

  g_return_if_fail (PD_IS_TRANSITION (self));

  klass = PD_TRANSITION_GET_CLASS (self);
  g_return_if_fail (klass->execute != NULL);

  return klass->execute (self, data);
}

gint
pd_transition_get_id (PdTransition *self)
{
  PdTransitionPrivate *priv;

  g_return_val_if_fail (PD_IS_TRANSITION (self), G_MAXINT);

  priv = pd_transition_get_instance_private (self);

  return priv->id;
}

void
pd_transition_set_id (PdTransition *self,
                      gint          id)
{
  PdTransitionPrivate *priv;

  g_return_if_fail (PD_IS_TRANSITION (self));

  priv = pd_transition_get_instance_private (self);

  priv->id = id;
}

/**
 * pd_transition_get_start_state:
 * @self: a #PdTransition
 *
 * Retrieve the #PdState held as the start state.
 *
 * Returns: (transfer full): a #PdState if one is set as the start state.
 */
PdState *
pd_transition_get_start_state (PdTransition *self)
{
  PdState *state;

  g_return_val_if_fail (PD_IS_TRANSITION (self), NULL);

  g_object_get (self, "start-state", &state, NULL);

  return state;
}

/**
 * pd_transition_ref_start_state:
 * @self: a #PdTransition
 *
 * Gets the start state for the transition, and returns a new reference
 * to the #PdState.
 *
 * Returns: (transfer full) (nullable): a #PdState or %NULL
 */
PdState *
pd_transition_ref_start_state (PdTransition *self)
{
  PdTransitionPrivate *priv;
  PdState *ret = NULL;

  g_return_val_if_fail (PD_IS_TRANSITION (self), NULL);

  priv = pd_transition_get_instance_private (self);

  /*pd_object_lock (G_OBJECT (self));*/
  g_set_object (&ret, priv->start);
  /*pd_object_unlock (G_OBJECT (self));*/

  return g_steal_pointer (&ret);
}

void
pd_transition_set_start_state (PdTransition *self,
                               PdState      *start)
{
  PdTransitionPrivate *priv;

  g_return_if_fail (PD_IS_TRANSITION (self));
  g_return_if_fail (!start || PD_IS_STATE (start));

  priv = pd_transition_get_instance_private (self);

  if (g_set_object (&priv->start, start))
    g_object_notify_by_pspec (G_OBJECT (self), properties [PROP_START]);
}

/**
 * pd_transition_get_end_state:
 * @self: a #PdTransition
 *
 * Retrieve the #PdState held as the end state.
 *
 * Returns: (transfer full): a #PdState if one is set as the end state.
 */
PdState *
pd_transition_get_end_state (PdTransition *self)
{
  PdState *state;

  g_return_val_if_fail (PD_IS_TRANSITION (self), NULL);

  g_object_get (self, "end-state", &state, NULL);

  return state;
}

/**
 * pd_transition_ref_end_state:
 * @self: a #PdTransition
 *
 * Gets the start state for the transition, and returns a new reference
 * to the #PdState.
 *
 * Returns: (transfer full) (nullable): a #PdState or %NULL
 */
PdState *
pd_transition_ref_end_state (PdTransition *self)
{
  PdTransitionPrivate *priv;
  PdState *ret = NULL;

  g_return_val_if_fail (PD_IS_TRANSITION (self), NULL);

  priv = pd_transition_get_instance_private (self);

  /*pd_object_lock (G_OBJECT (self));*/
  g_set_object (&ret, priv->end);
  /*pd_object_unlock (G_OBJECT (self));*/

  return g_steal_pointer (&ret);
}

void
pd_transition_set_end_state (PdTransition *self,
                             PdState      *end)
{
  PdTransitionPrivate *priv;

  g_return_if_fail (PD_IS_TRANSITION (self));
  g_return_if_fail (!end || PD_IS_STATE (end));

  priv = pd_transition_get_instance_private (self);

  if (g_set_object (&priv->end, end))
    g_object_notify_by_pspec (G_OBJECT (self), properties [PROP_END]);
}
