# Plantd GLib Library

[![Build Status](https://gitlab.com/plantd/libplantd/badges/staging/pipeline.svg)](https://gitlab.com/plantd/libplantd/-/commits/staging)
[![codecov](https://codecov.io/gl/plantd/libplantd/branch/staging/graph/badge.svg?token=4WwpbJt5vS)](https://codecov.io/gl/plantd/libplantd)
[![FOSSA Status](https://app.fossa.com/api/projects/git%2Bgitlab.com%2Fplantd%2Flibplantd.svg?type=shield)](https://app.fossa.com/projects/git%2Bgitlab.com%2Fplantd%2Flibplantd?ref=badge_shield)
[![License MIT](https://img.shields.io/badge/License-MIT-brightgreen.svg)](https://img.shields.io/badge/License-MIT-brightgreen.svg)

---

This library is still in early stages. `libplantd` can be used to create basic
functionality for distributed devices, but it is subject to change.

## Dependencies

* `ninja`
* `meson`
* `glib-2.0`
* `libczmq`
* `gobject-introspection`

## Related Projects

Other `plantd` projects in varying states of completeness and usefulness.

* [Message Broker](https://gitlab.com/plantd/broker)
* GraphQL API gateway
  * [Alpha](https://gitlab.com/plantd/master)
  * [Beta](https://gitlab.com/plantd/plantd-django)
* Utilities
  * [Control](https://gitlab.com/plantd/plantctl)
  * [Messenger](https://gitlab.com/plantd/messenger)

## Developer Documentation

Additional documentation is available that's useful during development.

* [API](doc/dev/api.md)
* [Code Coverage](doc/dev/coverage.md)
* [Documentation](doc/dev/documentation.md)
* [Testing](doc/dev/testing.md)
  * [Memory Leaks](doc/dev/testing.md#memory-leaks)
  * [Troubleshooting CI](doc/dev/testing.md#troubleshooting-ci)
* [Setup](doc/dev/setup.md)
  * [Build](doc/dev/setup.md#build)
  
