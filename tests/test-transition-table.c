#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <plantd/plantd.h>

static void
test_transition_table_construct (void)
{
  g_autoptr (PdTransitionTable) object = NULL;

  object = pd_transition_table_new ();

  g_assert_nonnull (object);
}

static void
test_transition_table_operations (void)
{
  g_autoptr (PdTransitionTable) table = NULL;
  g_autoptr (PdTransition) a = NULL;
  g_autoptr (PdTransition) b = NULL;
  g_autoptr (PdTransition) c = NULL;

  table = pd_transition_table_new ();
  a = pd_transition_new (1);
  b = pd_transition_new (2);
  c = pd_transition_new (3);

  g_assert_nonnull (table);
  g_assert_nonnull (a);
  g_assert_nonnull (b);
  g_assert_nonnull (c);

  g_assert_true (pd_transition_table_add (table, a));
  g_assert_true (pd_transition_table_add (table, b));
  g_assert_true (pd_transition_table_add (table, c));

  g_assert_true (pd_transition_table_contains (table, a));
  g_assert_true (pd_transition_table_contains (table, b));
  g_assert_true (pd_transition_table_contains (table, c));

  g_assert_cmpint (pd_transition_table_length (table), ==, 3);

  g_assert_true (!pd_transition_table_remove (table, 4));
  g_assert_true (pd_transition_table_remove (table, 3));

  g_assert_cmpint (pd_transition_table_length (table), ==, 2);

  g_assert_false (pd_transition_table_contains (table, c));

  for (gint i = 0; i < pd_transition_table_length (table); i++)
    {
      PdTransition *transition;

      transition = pd_transition_table_get (table, i);
      g_assert_cmpint (pd_transition_get_id (transition), ==, i + 1);
    }
}

gint
main (gint argc, gchar *argv[])
{
  g_test_init (&argc, &argv, NULL);

  g_test_add_func ("/transition-table/construct", test_transition_table_construct);
  g_test_add_func ("/transition-table/operations", test_transition_table_operations);

  return g_test_run ();
}
