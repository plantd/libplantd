#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <plantd/plantd.h>

#include "plantd/plantd-utils.h"

static void
test_object_construct (void)
{
  g_autoptr (PdObject) object = NULL;

  object = pd_object_new ("obj0");

  g_assert_nonnull (object);
  g_assert_cmpstr ("obj0", ==, pd_object_get_id (object));
}

static const gchar *json = "{ \
  \"id\": \"obj0\", \
  \"name\": \"TestObject\", \
  \"properties\": [ \
    { \"key\": \"key_b\", \"value\": \"value_b\" }, \
    { \"key\": \"key_a\", \"value\": \"value_a\" } \
  ], \
  \"objects\": [ \
    { \
      \"id\": \"obj1\", \
      \"name\": \"TestChildObject\", \
      \"properties\": [ \
        { \"key\": \"key_c\", \"value\": \"value_c\" } \
      ], \
      \"objects\": [ \
      ] \
    } \
  ] \
}";

static void
test_object_json_serialize (void)
{
  g_autoptr (GError) err = NULL;
  g_autoptr (GMatchInfo) match_info = NULL;
  g_autoptr (GRegex) regex = NULL;
  g_autoptr (PdObject) object = NULL;
  g_autoptr (PdObject) child = NULL;
  g_autoptr (PdProperty) prop_a = NULL;
  g_autoptr (PdProperty) prop_b = NULL;
  g_autoptr (PdProperty) prop_c = NULL;
  g_autofree gchar *data = NULL;
  g_autofree gchar *data1 = NULL;

  object = pd_object_new ("obj0");
  g_assert_nonnull (object);
  pd_object_set_name (object, "TestObject");

  prop_a = pd_property_new ("key_a", NULL);
  prop_b = pd_property_new ("key_b", NULL);
  prop_c = pd_property_new ("key_c", NULL);
  g_assert_nonnull (prop_a);
  g_assert_nonnull (prop_b);
  g_assert_nonnull (prop_c);

  pd_property_set_value (prop_a, "value_a");
  pd_property_set_value (prop_b, "value_b");
  pd_property_set_value (prop_c, "value_c");

  pd_object_add_property (object, g_object_ref (prop_a));
  pd_object_add_property (object, g_object_ref (prop_b));

  child = pd_object_new ("obj1");
  g_assert_nonnull (child);
  pd_object_set_name (child, "TestChildObject");
  pd_object_add_property (child, g_object_ref (prop_c));

  pd_object_add_object (object, g_object_ref (child));

  data = pd_object_serialize (object);
  data1 = remove_whitespace (data);

  {
    regex = g_regex_new (".id.:.obj0.", 0, 0, &err);
    g_regex_match (regex, data1, 0, &match_info);
    g_assert_true (g_match_info_matches (match_info));
  }
  {
    regex = g_regex_new (".name.:.TestObject.", 0, 0, &err);
    g_regex_match (regex, data1, 0, &match_info);
    g_assert_true (g_match_info_matches (match_info));
  }
  {
    regex = g_regex_new (".value.:.value_a.", 0, 0, &err);
    g_regex_match (regex, data1, 0, &match_info);
    g_assert_true (g_match_info_matches (match_info));
  }
}

static void
test_object_json_deserialize (void)
{
  g_autoptr (PdObject) object = NULL;
  g_autofree gchar *id = NULL;
  g_autofree gchar *name = NULL;

  object = pd_object_new ("test");
  g_assert_nonnull (object);
  pd_object_deserialize (object, json);

  /*object = pd_gobject_from_data (json);*/

  id = g_strdup (pd_object_get_id (object));
  name = g_strdup (pd_object_get_name (object));

  g_assert_cmpstr (id, ==, "obj0");
  g_assert_cmpstr (name, ==, "TestObject");
  g_assert_true (pd_object_has_property (object, "key_a"));
  g_assert_true (pd_object_has_property (object, "key_b"));
  g_assert_true (pd_object_has_object (object, "obj1"));
}

gint
main (gint argc, gchar *argv[])
{
  g_test_init (&argc, &argv, NULL);

  g_test_add_func ("/object/construct", test_object_construct);
  g_test_add_func ("/object/json-serialize", test_object_json_serialize);
  g_test_add_func ("/object/json-deserialize", test_object_json_deserialize);

  return g_test_run ();
}
