#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <plantd/plantd.h>

static void
test_source_construct (void)
{
  g_autoptr (PdSource) object = NULL;

  object = pd_source_new ("tcp://localhost:5555", "");

  g_assert_nonnull (object);
}

/*
 *static const gchar *json = "{ \
 *  \"\": \"\", \
 *}";
 */

static void
test_source_serialize (void)
{
  g_autoptr (PdSource) object = NULL;
}

static void
test_source_deserialize (void)
{
  g_autoptr (PdSource) object = NULL;
}

gint
main (gint argc, gchar *argv[])
{
  g_test_init (&argc, &argv, NULL);

  g_test_add_func ("/source/construct", test_source_construct);
  g_test_add_func ("/source/serialize", test_source_serialize);
  g_test_add_func ("/source/deserialize", test_source_deserialize);

  return g_test_run ();
}
