#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <plantd/plantd.h>

#include "lib/test-application.h"
#include "lib/test-sink.h"

static void
test_message_shutdown (PdClient *client)
{
  pd_client_send_request (client,
                            "test-app",
                            "shutdown",
                            "{}");
}

static gboolean
test_events_cb (gpointer data)
{
  g_autoptr (PdClient) client = NULL;
  PdApplication *app;

  if (g_test_verbose ())
    g_print ("/test/events\n");

  app = PD_APPLICATION (data);

  g_usleep (200000);

  for (gint i = 0; i < 100; i++)
    {
      g_autoptr (PdEvent) event = NULL;

      event = pd_event_new_full (i, "test", "test-event");
      pd_application_send_event (app, event, NULL);
      g_usleep (100);
    }

  g_usleep (200000);
  g_assert_cmpint (test_sink_get_message_count (), ==, 100);

  client = pd_client_new ("tcp://localhost:19999");
  g_usleep (100000);
  test_message_shutdown (client);

  return FALSE;
}

static void
test_events_activate_cb (GApplication *application)
{
  PdApplication *app;
  PdSource *source;
  PdSink *sink;

  if (g_test_verbose ())
    g_print ("/test/events/activate\n");

  app = PD_APPLICATION (application);

  source = pd_application_get_source (app, "event");
  pd_source_start (source);

  sink = pd_application_get_sink (app, "event");
  pd_sink_start (sink);
}

static void
test_application_events (void)
{
  gchar *binpath = g_test_build_filename (G_TEST_BUILT, "unimportant", NULL);
  gchar *argv[] = { binpath, NULL };

  g_autoptr (PdApplication) app = NULL;
  g_autoptr (PdSource) source = NULL;
  g_autoptr (PdSink) sink = NULL;
  g_autoptr (PdHub) hub = NULL;

  if (g_test_verbose ())
    g_print ("/test/application/events\n");

  app = test_application_new ();
  pd_application_set_id (app, "org.plantd.TestApplication");
  pd_application_set_service (app, "test-app");

  source = pd_source_new ("tcp://localhost:20998", "");
  pd_application_add_source (PD_APPLICATION (app), "event", source);
  g_assert_true (pd_application_has_source (PD_APPLICATION (app), "event"));

  sink = test_sink_new ("tcp://localhost:20999", "");
  test_sink_set_message_type (0);
  pd_application_add_sink (PD_APPLICATION (app), "event", sink);

  hub = pd_hub_new ("tcp://*:20998", "tcp://*:20999");
  pd_hub_start (hub);

  g_timeout_add (1, test_events_cb, app);
  g_signal_connect (app, "activate", G_CALLBACK (test_events_activate_cb), NULL);

  g_application_run (G_APPLICATION (app), G_N_ELEMENTS (argv) - 1, argv);

  pd_hub_stop (hub);

  g_free (binpath);
}

gint
main (gint argc, gchar *argv[])
{
  g_test_init (&argc, &argv, NULL);

  g_test_add_func ("/application/events", test_application_events);

  return g_test_run ();
}
