#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <glib.h>
#include <gio/gio.h>

#include "plantd/plantd.h"
#include "plantd/plantd-utils.h"

#ifdef G_LOG_DOMAIN
#undef G_LOG_DOMAIN
#endif
#define G_LOG_DOMAIN "integration-test"

static void
test_mdp (void)
{
  g_autoptr (PdBroker) broker = NULL;
  g_autoptr (PdClient) client = NULL;
  g_autoptr (PdWorker) worker = NULL;

  const gchar *endpoint_broker = "tcp://*:5555";
  const gchar *endpoint = "tcp://localhost:5555";
  const gchar *service = "echo";
  const gchar *msg = "integration-test";

  zmsg_t *request;
  zframe_t *reply_to;
  zmsg_t *request_recv;
  zmsg_t *request_recv_dup;
  zmsg_t *reply_from_worker;
  gchar *msg_recv;
  gchar *msg_from_worker;

  pd_log_init (true, NULL);
  pd_log_increase_verbosity ();
  pd_log_increase_verbosity ();
  pd_log_increase_verbosity ();
  pd_log_increase_verbosity ();

  // Create broker, bind to endpoint and start running
  broker = pd_broker_new (endpoint_broker);
  g_assert_nonnull (broker);
  pd_broker_bind (broker);
  pd_broker_run (broker);

  // Create client
  client = pd_client_new (endpoint);
  g_assert_nonnull (client);

  // Create worker
  worker = pd_worker_new (endpoint, service);
  pd_worker_connect (worker);
  g_usleep (100000); // needed because it takes time to register the worker

  // Send request using client
  request = zmsg_new ();
  g_assert_nonnull (request);
  zmsg_pushstr (request, msg);
  pd_client_send (client, service, &request);

  // Receive request using worker
  request_recv = pd_worker_recv (worker, &reply_to);
  g_assert_nonnull (request_recv);
  g_assert_nonnull (reply_to);

  // Duplicate the received message in order to read it
  // without changing the original message
  request_recv_dup = zmsg_dup (request_recv);
  msg_recv = zmsg_popstr(request_recv_dup);
  g_assert_cmpstr (msg_recv, ==, msg);
  zmsg_destroy (&request_recv_dup);
  g_free (msg_recv);

  // Echo received message using worker
  pd_worker_send (worker, &request_recv, reply_to);
  zframe_destroy (&reply_to);

  // Receive echo using client
  reply_from_worker = pd_client_recv (client, NULL);

  // The first frame contains an address. We want the second frame.
  g_assert_nonnull(reply_from_worker);
  zmsg_next (reply_from_worker);
  msg_from_worker = zframe_strdup (zmsg_next (reply_from_worker));
  g_assert_cmpstr (msg_from_worker, ==, msg);
  zmsg_destroy (&reply_from_worker);
  g_free (msg_from_worker);
}

static void
test_socket_cleanup (void){

  PdBroker *broker = NULL;

  const gchar *endpoint_broker = "tcp://*:5555";
  broker = pd_broker_new (endpoint_broker);

  g_assert_nonnull (broker);

  pd_broker_bind (broker);
  pd_broker_run (broker);

  g_usleep (100000);

  g_object_unref (broker);
}

gint
main (gint argc, gchar *argv[])
{
  g_test_init (&argc, &argv, NULL);

  g_test_add_func ("/integration/mdp", test_mdp);
  g_test_add_func ("/integration/socket", test_socket_cleanup);

  return g_test_run ();
}
