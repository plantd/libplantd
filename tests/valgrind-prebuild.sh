#!/bin/bash

G_SLICE=always-malloc G_DEBUG=gc-friendly \
  meson test 
    --wrap='valgrind --tool=memcheck --leak-check=full --show-leak-kinds=definite --errors-for-leak-kinds=definite --leak-resolution=high --num-callers=20 --suppressions=../tests/glib.supp --suppressions=../tests/zeromq.supp' \
    -C _build
