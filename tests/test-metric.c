#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <plantd/plantd.h>

#include "plantd/plantd-utils.h"

static void
test_metric_construct (void)
{
  {
    g_autoptr (PdMetric) object = NULL;
    g_autoptr (PdMetricTable) table = NULL;

    object = pd_metric_new ();
    table = pd_metric_table_new ();
    g_assert_nonnull (object);
    g_assert_nonnull (table);
    pd_metric_set_id (object, "foo");
    pd_metric_set_name (object, "bar");
    pd_metric_set_data (object, table);
    g_assert_cmpstr (pd_metric_get_id (object), ==, "foo");
    g_assert_cmpstr (pd_metric_get_name (object), ==, "bar");
    g_assert_nonnull (pd_metric_get_data (object));
    g_object_unref (table);
  }

  {
    g_autoptr (PdMetric) object = NULL;
    g_autoptr (PdMetricTable) table = NULL;

    table = pd_metric_table_new ();
    g_assert_nonnull (table);
    object = pd_metric_new_full ("foo", "bar", table);
    g_assert_nonnull (object);
    g_object_unref (table);

    {
      g_autofree gchar *id = NULL;
      g_autofree gchar *name = NULL;

      id = pd_metric_dup_id (object);
      name = pd_metric_dup_name (object);
      g_assert_cmpstr (id, ==, "foo");
      g_assert_cmpstr (name, ==, "bar");
      g_assert_nonnull (pd_metric_get_data (object));
    }

    {
      g_autofree gchar *id = NULL;
      g_autofree gchar *name = NULL;

      pd_metric_set_id (object, "baz");
      pd_metric_set_name (object, "meh");
      id = pd_metric_dup_id (object);
      name = pd_metric_dup_name (object);
      g_assert_cmpstr (id, ==, "baz");
      g_assert_cmpstr (name, ==, "meh");
    }
  }
}

static void
test_metric_get_data_behavior (void)
{
  g_autoptr (PdMetric) metric = NULL;
  g_autoptr (PdMetricTable) table1 = NULL;
  g_autoptr (PdMetricTable) table2 = NULL;
  g_autoptr (PdMetricTable) table3 = NULL;
  g_autoptr (PdMetricTableHeader) header = NULL;
  
  // Construct the metric (incudes constructig the header and table)
  table1 = pd_metric_table_new ();
  header = pd_metric_table_header_new ("calibration");
  pd_metric_table_header_add_column (header, "column_1", 1.0);
  pd_metric_table_set_name (table1, "baz");
  pd_metric_table_set_timestamp (table1, "2019-04-09 14:00:0.00-00");
  pd_metric_table_set_header (table1, header);
  pd_metric_table_add_entry (table1, "value_1", 10.0);
  metric = pd_metric_new_full ("foo", "bar", table1);

  // use get_data
  table2 = pd_metric_get_data (metric);

  // add an entry to table2
  pd_metric_table_add_entry (table2, "value_2", 20.0);

  // see if this changed the attribute of the metric
  table3 = pd_metric_get_data (metric);
  g_assert_cmpfloat (pd_metric_table_get_entry (table2, "value_1"),
                     ==,
                     pd_metric_table_get_entry (table3, "value_1"));
  g_assert_cmpfloat (pd_metric_table_get_entry (table2, "value_2"),
                     ==,
                     pd_metric_table_get_entry (table3, "value_2"));

  // See if table1 was changed. 
  g_assert_cmpfloat (
    pd_metric_table_get_entry (table2, "value_2"),
    ==,
    pd_metric_table_get_entry (table1, "value_2"));

  // Check that the pointers are the same
  g_assert_true (table1 == table2);
  g_assert_true (table1 == table3);
}

static void
test_metric_ref_data_behavior (void)
{
  g_autoptr (PdMetric) metric = NULL;
  g_autoptr (PdMetricTable) table1 = NULL;
  g_autoptr (PdMetricTable) table2 = NULL;
  g_autoptr (PdMetricTable) table3 = NULL;
  g_autoptr (PdMetricTableHeader) header = NULL;
  
  // Construct the metric (incudes constructig the header and table)
  table1 = pd_metric_table_new ();
  header = pd_metric_table_header_new ("calibration");
  pd_metric_table_header_add_column (header, "column_1", 1.0);
  pd_metric_table_set_name (table1, "baz");
  pd_metric_table_set_timestamp (table1, "2019-04-09 14:00:0.00-00");
  pd_metric_table_set_header (table1, header);
  pd_metric_table_add_entry (table1, "value_1", 10.0);
  metric = pd_metric_new_full ("foo", "bar", table1);

  // use ref_data
  table2 = pd_metric_ref_data (metric);

  // add an entry to table2
  pd_metric_table_add_entry (table2, "value_2", 20.0);

  // see if this changed the attribute of the metric
  table3 = pd_metric_ref_data (metric);
  g_assert_cmpfloat (pd_metric_table_get_entry (table2, "value_1"),
                     ==,
                     pd_metric_table_get_entry (table3, "value_1"));
  g_assert_cmpfloat (pd_metric_table_get_entry (table2, "value_2"),
                     ==,
                     pd_metric_table_get_entry (table3, "value_2"));
  
  // See if table1 was changed. 
  g_assert_cmpfloat (pd_metric_table_get_entry (table2, "value_2"),
                     ==,
                     pd_metric_table_get_entry (table1, "value_2"));

  // Check that the pointers are the same
  g_assert_true (table1 == table2);
  g_assert_true (table1 == table3);

}

static void
test_metric_serialize_deserialize (void)
{
  g_autoptr (PdMetric) metric1 = NULL;
  g_autoptr (PdMetric) metric2 = NULL;
  g_autoptr (PdMetricTable) table1 = NULL;
  g_autoptr (PdMetricTable) table2 = NULL;
  g_autoptr (PdMetricTableHeader) header1 = NULL;
  g_autoptr (PdMetricTableHeader) header2 = NULL;
  g_autofree gchar *data1 = NULL;

  // Construct table1
  table1 = pd_metric_table_new ();
  header1 = pd_metric_table_header_new ("calibration");

  g_assert_nonnull (table1);
  g_assert_nonnull (header1);

  pd_metric_table_header_add_column (header1, "column_1", 1.0);
  pd_metric_table_header_add_column (header1, "column_2", 2.0);
  pd_metric_table_header_add_column (header1, "column_3", 3.0);
  pd_metric_table_header_add_column (header1, "column_4", 4.0);
  pd_metric_table_header_add_column (header1, "column_5", 5.0);
  pd_metric_table_header_add_column (header1, "column_6", 6.0);

  pd_metric_table_set_name (table1, "baz");
  pd_metric_table_set_timestamp (table1, "2019-04-09 14:00:0.00-00");
  pd_metric_table_set_header (table1, header1);
  pd_metric_table_add_entry (table1, "value_1", 10.0);
  pd_metric_table_add_entry (table1, "value_2", 20.0);
  pd_metric_table_add_entry (table1, "value_3", 30.0);

  metric1 = pd_metric_new_full ("foo", "bar", table1);
  g_assert_nonnull (metric1);

  // Serialize metric1
  data1 = pd_metric_serialize (metric1);

  // Create empty metric2
  metric2 = pd_metric_new ();
  g_assert_nonnull (metric2);

  // Deserialize the json string into metric2
  pd_metric_deserialize (metric2, data1);

  // Compare the metrics
  g_assert_cmpstr (pd_metric_get_id (metric1), ==, pd_metric_get_id (metric2));
  g_assert_cmpstr (pd_metric_get_name (metric1), ==, pd_metric_get_name (metric2));

  table2 = pd_metric_get_data (metric2);
  g_assert_cmpstr (pd_metric_table_get_name (table1), ==, pd_metric_table_get_name (table2));
  g_assert_cmpstr (pd_metric_table_get_timestamp (table1), ==, pd_metric_table_get_timestamp (table2));

  g_assert_true (pd_metric_table_has_entry (table2, "value_1"));
  g_assert_true (pd_metric_table_has_entry (table2, "value_2"));
  g_assert_true (pd_metric_table_has_entry (table2, "value_3"));

  g_assert_cmpfloat (pd_metric_table_get_entry (table1, "value_1"), ==, pd_metric_table_get_entry (table2, "value_1"));
  g_assert_cmpfloat (pd_metric_table_get_entry (table1, "value_2"), ==, pd_metric_table_get_entry (table2, "value_2"));
  g_assert_cmpfloat (pd_metric_table_get_entry (table1, "value_3"), ==, pd_metric_table_get_entry (table2, "value_3"));

  header2 = pd_metric_table_get_header (table2);

  g_assert_cmpstr (pd_metric_table_header_get_name (header1), ==, pd_metric_table_header_get_name (header2));
 
  g_assert_true (pd_metric_table_header_has_column (header2, "column_1"));
  g_assert_true (pd_metric_table_header_has_column (header2, "column_2"));
  g_assert_true (pd_metric_table_header_has_column (header2, "column_3"));
  g_assert_true (pd_metric_table_header_has_column (header2, "column_4"));
  g_assert_true (pd_metric_table_header_has_column (header2, "column_5"));
  g_assert_true (pd_metric_table_header_has_column (header2, "column_6"));

  g_assert_cmpfloat (pd_metric_table_header_get_column (header1, "column_1"), ==, pd_metric_table_header_get_column (header2, "column_1"));
  g_assert_cmpfloat (pd_metric_table_header_get_column (header1, "column_2"), ==, pd_metric_table_header_get_column (header2, "column_2"));
  g_assert_cmpfloat (pd_metric_table_header_get_column (header1, "column_3"), ==, pd_metric_table_header_get_column (header2, "column_3"));
  g_assert_cmpfloat (pd_metric_table_header_get_column (header1, "column_4"), ==, pd_metric_table_header_get_column (header2, "column_4"));
  g_assert_cmpfloat (pd_metric_table_header_get_column (header1, "column_5"), ==, pd_metric_table_header_get_column (header2, "column_5"));
  g_assert_cmpfloat (pd_metric_table_header_get_column (header1, "column_6"), ==, pd_metric_table_header_get_column (header2, "column_6"));

}

gint
main (gint argc, gchar *argv[])
{
  g_test_init (&argc, &argv, NULL);

  g_test_add_func ("/metric/construct", test_metric_construct);
  g_test_add_func ("/metric/serialize_deserialize", test_metric_serialize_deserialize);
  g_test_add_func ("/metric/get_data_behavior", test_metric_get_data_behavior);
  g_test_add_func ("/metric/ref_data_behavior", test_metric_ref_data_behavior);

  return g_test_run ();
}
