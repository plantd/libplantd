#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <plantd/plantd.h>

static void
test_state_machine_construct (void)
{
  g_autoptr (PdStateMachine) object = NULL;

  object = pd_state_machine_new ();

  g_assert_nonnull (object);
}

static void
test_state_machine_running (void)
{
  g_autoptr (PdStateMachine) fsm = NULL;
  g_autoptr (PdTransitionTable) table = NULL;

  table = pd_transition_table_new ();

  // Add transitions to table
  // #1
  {
    g_autoptr (PdTransition) transition = NULL;
    g_autoptr (PdState) start = NULL;
    g_autoptr (PdState) end = NULL;

    transition = pd_transition_new (0);
    start = pd_state_new_full (0, "start", "transition 1 start state");
    end = pd_state_new_full (1, "end", "transition 1 end state");

    g_assert_nonnull (transition);
    g_assert_nonnull (start);
    g_assert_nonnull (end);

    pd_transition_set_start_state (transition, g_steal_pointer (&start));
    pd_transition_set_end_state (transition, g_steal_pointer (&end));

    pd_transition_table_add (table, g_steal_pointer (&transition));

    g_assert_cmpint (pd_transition_table_length (table), ==, 1);
  }
  // #2
  {
    g_autoptr (PdTransition) transition = NULL;
    g_autoptr (PdState) start = NULL;
    g_autoptr (PdState) end = NULL;

    transition = pd_transition_new (1);
    start = pd_state_new_full (1, "start", "transition 2 start state");
    end = pd_state_new_full (2, "end", "transition 2 end state");

    g_assert_nonnull (transition);
    g_assert_nonnull (start);
    g_assert_nonnull (end);

    pd_transition_set_start_state (transition, g_steal_pointer (&start));
    pd_transition_set_end_state (transition, g_steal_pointer (&end));

    pd_transition_table_add (table, g_steal_pointer (&transition));

    g_assert_cmpint (pd_transition_table_length (table), ==, 2);
  }
  // #3
  {
    g_autoptr (PdTransition) transition = NULL;
    g_autoptr (PdState) start = NULL;
    g_autoptr (PdState) end = NULL;

    transition = pd_transition_new (2);
    start = pd_state_new_full (2, "start", "transition 3 start state");
    end = pd_state_new_full (0, "end", "transition 3 end state");

    g_assert_nonnull (transition);
    g_assert_nonnull (start);
    g_assert_nonnull (end);

    pd_transition_set_start_state (transition, g_steal_pointer (&start));
    pd_transition_set_end_state (transition, g_steal_pointer (&end));

    pd_transition_table_add (table, g_steal_pointer (&transition));

    g_assert_cmpint (pd_transition_table_length (table), ==, 3);
  }

  fsm = pd_state_machine_new ();

  g_assert_nonnull (fsm);

  pd_state_machine_set_transition_table (fsm, table);
  pd_state_machine_start (fsm);
  g_usleep (100000);
  g_assert_true (pd_state_machine_running (fsm));

  // Test transitions
  {
    g_autoptr (PdEvent) event = NULL;

    event = pd_event_new_full (1, "one", "event 1");
    pd_state_machine_submit_event (fsm, event);
    //g_assert_true (pd_state_equal (pd_state_machine_current_state (fsm),
    //                                 pd_transition_table_get (table, 1));
  }
  {
    g_autoptr (PdEvent) event = NULL;

    event = pd_event_new_full (2, "two", "event 2");
    pd_state_machine_submit_event (fsm, event);
    //g_assert_true (pd_state_equal (pd_state_machine_current_state (fsm),
    //                                 pd_transition_table_get (table, 2));
  }
  {
    g_autoptr (PdEvent) event = NULL;

    event = pd_event_new_full (0, "zero", "event 0");
    pd_state_machine_submit_event (fsm, event);
    //g_assert_true (pd_state_equal (pd_state_machine_current_state (fsm),
    //                                 pd_transition_table_get (table, 1));
  }

  pd_state_machine_stop (fsm);
  g_assert_true (!pd_state_machine_running (fsm));
}

gint
main (gint argc, gchar *argv[])
{
  g_test_init (&argc, &argv, NULL);

  g_test_add_func ("/state-machine/construct", test_state_machine_construct);
  g_test_add_func ("/state-machine/running", test_state_machine_running);

  return g_test_run ();
}
