#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <plantd/plantd.h>

#include "plantd/plantd-utils.h"

static void
test_metric_table_header_construct (void)
{
  g_autoptr (PdMetricTableHeader) object = NULL;

  object = pd_metric_table_header_new (NULL);
  g_assert_nonnull (object);

  pd_metric_table_header_set_name (object, "foo");
  g_assert_cmpstr (pd_metric_table_header_get_name (object), ==, "foo");
}

static void
test_metric_table_header_serialize_deserialize (void)
{
  g_autoptr (PdMetricTableHeader) header1 = NULL;
  g_autoptr (PdMetricTableHeader) header2 = NULL;
  g_autofree gchar *data = NULL;

  // create header1 and fill it with values 
  header1 = pd_metric_table_header_new ("foo");
  g_assert_nonnull (header1);
  pd_metric_table_header_add_column (header1, "column_1", 1.0);
  pd_metric_table_header_add_column (header1, "column_2", 0.0);

  // serialize header1
  data = pd_metric_table_header_serialize (header1);

  // create header2 and deserialize data into it
  header2 = pd_metric_table_header_new ("bar");
  g_assert_nonnull (header2);
  pd_metric_table_header_deserialize (header2, data);

  // assert that the data is the same for header1 and header2
  g_assert_cmpstr (pd_metric_table_header_get_name (header1),
                   ==,
                   pd_metric_table_header_get_name (header2));
  g_assert_cmpfloat (pd_metric_table_header_get_column (header1, "column_1"),
                     ==,
                     pd_metric_table_header_get_column (header2, "column_1"));
  g_assert_cmpfloat (pd_metric_table_header_get_column (header1, "column_2"),
                     ==,
                     pd_metric_table_header_get_column (header2, "column_2"));

}

gint
main (gint argc, gchar *argv[])
{
  g_test_init (&argc, &argv, NULL);

  g_test_add_func ("/metric-table-header/construct", test_metric_table_header_construct);
  g_test_add_func ("/metric-table-header/serialize_deserialize", test_metric_table_header_serialize_deserialize);

  return g_test_run ();
}
