#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <plantd/plantd.h>

#include "plantd/plantd-utils.h"

static void
test_metric_table_construct (void)
{
  g_autoptr (PdMetricTable) object = NULL;

  object = pd_metric_table_new ();
  g_assert_nonnull (object);

  pd_metric_table_set_name (object, "foo");
  g_assert_cmpstr (pd_metric_table_get_name (object), ==, "foo");
}

static void
test_metric_table_serialize_deserialize (void)
{
  g_autoptr (PdMetricTable) table1 = NULL;
  g_autoptr (PdMetricTable) table2 = NULL;
  g_autoptr (PdMetricTableHeader) header1 = NULL;
  g_autoptr (PdMetricTableHeader) header2 = NULL;
  g_autofree gchar *data1 = NULL;

  // Construct the original table1
  table1 = pd_metric_table_new ();
  g_assert_nonnull (table1);

  header1 = pd_metric_table_header_new ("bar");
  pd_metric_table_header_add_column (header1, "column_1", 1.0);
  pd_metric_table_header_add_column (header1, "column_2", 2.0);
  pd_metric_table_header_add_column (header1, "column_3", 3.0);
  pd_metric_table_header_add_column (header1, "column_4", 4.0);
  pd_metric_table_header_add_column (header1, "column_5", 5.0);
  pd_metric_table_header_add_column (header1, "column_6", 6.0);

  pd_metric_table_set_name (table1, "foo");
  pd_metric_table_set_timestamp (table1, "2019-04-09 14:00:0.00-00");
  pd_metric_table_set_header (table1, header1);
  pd_metric_table_add_entry (table1, "value_1", 10.0);
  pd_metric_table_add_entry (table1, "value_2", 20.0);
  pd_metric_table_add_entry (table1, "value_3", 30.0);

  // Serialize table1 
  data1 = pd_metric_table_serialize (table1);

  // Create empty table2
  table2 = pd_metric_table_new ();
  g_assert_nonnull (table2);

  // deserialize the json string from table1 into table2
  pd_metric_table_deserialize (table2, data1);

  // compare the tables
  g_assert_cmpstr (pd_metric_table_get_name (table1),
                   ==,
                   pd_metric_table_get_name (table2));
  g_assert_cmpstr (pd_metric_table_get_timestamp (table1),
                   ==,
                   pd_metric_table_get_timestamp (table2));

  g_assert_true (pd_metric_table_has_entry (table2, "value_1"));
  g_assert_true (pd_metric_table_has_entry (table2, "value_2"));
  g_assert_true (pd_metric_table_has_entry (table2, "value_3"));

  g_assert_cmpfloat (pd_metric_table_get_entry (table1, "value_1"),
                     ==,
                     pd_metric_table_get_entry (table2, "value_1"));
  g_assert_cmpfloat (pd_metric_table_get_entry (table1, "value_2"),
                     ==,
                     pd_metric_table_get_entry (table2, "value_2"));
  g_assert_cmpfloat (pd_metric_table_get_entry (table1, "value_3"),
                     ==,
                     pd_metric_table_get_entry (table2, "value_3"));

  header2 = pd_metric_table_get_header (table2);

  g_assert_cmpstr (pd_metric_table_header_get_name (header1),
                  ==,
                  pd_metric_table_header_get_name (header2));
 
  g_assert_true (pd_metric_table_header_has_column (header2, "column_1"));
  g_assert_true (pd_metric_table_header_has_column (header2, "column_2"));
  g_assert_true (pd_metric_table_header_has_column (header2, "column_3"));
  g_assert_true (pd_metric_table_header_has_column (header2, "column_4"));
  g_assert_true (pd_metric_table_header_has_column (header2, "column_5"));
  g_assert_true (pd_metric_table_header_has_column (header2, "column_6"));

  g_assert_cmpfloat (pd_metric_table_header_get_column (header1, "column_1"),
                     ==,
                     pd_metric_table_header_get_column (header2, "column_1"));
  g_assert_cmpfloat (pd_metric_table_header_get_column (header1, "column_2"),
                     ==,
                     pd_metric_table_header_get_column (header2, "column_2"));
  g_assert_cmpfloat (pd_metric_table_header_get_column (header1, "column_3"),
                     ==,
                     pd_metric_table_header_get_column (header2, "column_3"));
  g_assert_cmpfloat (pd_metric_table_header_get_column (header1, "column_4"),
                     ==,
                     pd_metric_table_header_get_column (header2, "column_4"));
  g_assert_cmpfloat (pd_metric_table_header_get_column (header1, "column_5"),
                     ==,
                     pd_metric_table_header_get_column (header2, "column_5"));
  g_assert_cmpfloat (pd_metric_table_header_get_column (header1, "column_6"),
                     ==,
                     pd_metric_table_header_get_column (header2, "column_6"));

}

gint
main (gint argc, gchar *argv[])
{
  g_test_init (&argc, &argv, NULL);

  g_test_add_func ("/metric-table/construct", test_metric_table_construct);
  g_test_add_func ("/metric-table/serialize-deserialize", test_metric_table_serialize_deserialize);

  return g_test_run ();
}
