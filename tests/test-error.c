#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <errno.h>
#include <plantd/plantd.h>

static void
test_error_errno (void)
{
  PdErrorEnum error;

  error = pd_error_from_errno (ENOTCONN);
  g_assert_cmpint (error, ==, PD_ERROR_NOT_CONNECTED);

  error = pd_error_from_errno (ENOTSUP);
  g_assert_cmpint (error, ==, PD_ERROR_FAILED);
}

gint
main (gint argc, gchar *argv[])
{
  g_test_init (&argc, &argv, NULL);

  g_test_add_func ("/error/errno", test_error_errno);

  return g_test_run ();
}
