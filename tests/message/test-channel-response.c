#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <plantd/plantd.h>

#include "plantd/plantd-utils.h"

static void
test_channel_response_construct (void)
{
  g_autoptr (PdChannelResponse) channel_response = NULL;

  channel_response = pd_channel_response_new ();

  g_assert_nonnull (channel_response);
}

static void
test_channel_response_deserialize (void)
{
  g_autoptr (PdChannelResponse) channel_response = NULL;

  channel_response = pd_channel_response_new ();

  g_assert_nonnull (channel_response);
}

static void
test_channel_response_serialize (void)
{
  g_autoptr (PdChannelResponse) channel_response = NULL;

  channel_response = pd_channel_response_new ();

  g_assert_nonnull (channel_response);
}


gint
main (gint argc, gchar *argv[])
{
  g_test_init (&argc, &argv, NULL);

  g_test_add_func ("/ChannelResponse/construct", test_channel_response_construct);
  g_test_add_func ("/ChannelResponse/deserialize", test_channel_response_deserialize);
  g_test_add_func ("/ChannelResponse/serialize", test_channel_response_serialize);

  return g_test_run ();
}
