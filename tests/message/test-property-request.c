#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <plantd/plantd.h>

#include "plantd/plantd-utils.h"

static void
test_property_request_construct (void)
{
  g_autoptr (PdPropertyRequest) object = NULL;

  object = pd_property_request_new ("service-id", "prop-key", "prop-value");

  g_assert_nonnull (object);
  g_assert_cmpstr (pd_property_request_get_id (object), ==, "service-id");
  g_assert_cmpstr (pd_property_request_get_key (object), ==, "prop-key");
  g_assert_cmpstr (pd_property_request_get_value (object), ==, "prop-value");

  pd_property_request_set_id (object, "new-id");
  pd_property_request_set_key (object, "new-key");
  pd_property_request_set_value (object, "new-value");

  g_assert_cmpstr (pd_property_request_get_id (object), ==, "new-id");
  g_assert_cmpstr (pd_property_request_get_key (object), ==, "new-key");
  g_assert_cmpstr (pd_property_request_get_value (object), ==, "new-value");
}

static const gchar *json = "{" \
  "\"id\": \"service-id\"," \
  "\"key\": \"prop-key\"," \
  "\"value\": \"prop-value\"" \
"}";

static void
test_property_request_json_serialize (void)
{
  g_autoptr (GError) err = NULL;
  g_autoptr (GMatchInfo) match_info = NULL;
  g_autoptr (GRegex) regex = NULL;
  g_autoptr (PdPropertyRequest) request = NULL;
  g_autofree gchar *data = NULL;
  g_autofree gchar *data1 = NULL;

  request = pd_property_request_new ("service-id", "prop-key", "prop-value");

  data = pd_property_request_serialize (request);
  data1 = remove_whitespace (data);

  {
    regex = g_regex_new (".id.:.service-id.", 0, 0, &err);
    g_regex_match (regex, data1, 0, &match_info);
    g_assert_true (g_match_info_matches (match_info));
  }
  {
    regex = g_regex_new (".key.:.prop-key.", 0, 0, &err);
    g_regex_match (regex, data1, 0, &match_info);
    g_assert_true (g_match_info_matches (match_info));
  }
  {
    regex = g_regex_new (".value.:.prop-value.", 0, 0, &err);
    g_regex_match (regex, data1, 0, &match_info);
    g_assert_true (g_match_info_matches (match_info));
  }
}

static void
test_property_request_json_deserialize (void)
{
  g_autoptr (PdPropertyRequest) request = NULL;

  request = pd_property_request_new (NULL, NULL, NULL);

  pd_property_request_deserialize (request, json);

  g_assert_cmpstr (pd_property_request_get_id (request), ==, "service-id");
  g_assert_cmpstr (pd_property_request_get_key (request), ==, "prop-key");
  g_assert_cmpstr (pd_property_request_get_value (request), ==, "prop-value");
}

gint
main (gint argc, gchar *argv[])
{
  g_test_init (&argc, &argv, NULL);

  g_test_add_func ("/PropertyRequest/construct", test_property_request_construct);
  g_test_add_func ("/PropertyRequest/json-serialize", test_property_request_json_serialize);
  g_test_add_func ("/PropertyRequest/json-deserialize", test_property_request_json_deserialize);

  return g_test_run ();
}
