#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <plantd/plantd.h>

#include "plantd/plantd-utils.h"

static void
test_event_request_construct (void)
{
  g_autoptr (PdEventRequest) event_request = NULL;

  event_request = pd_event_request_new ();

  g_assert_nonnull (event_request);
}

static void
test_event_request_deserialize (void)
{
  g_autoptr (PdEventRequest) event_request = NULL;

  event_request = pd_event_request_new ();

  g_assert_nonnull (event_request);
}

static void
test_event_request_serialize (void)
{
  g_autoptr (PdEventRequest) event_request = NULL;

  event_request = pd_event_request_new ();

  g_assert_nonnull (event_request);
}


gint
main (gint argc, gchar *argv[])
{
  g_test_init (&argc, &argv, NULL);

  g_test_add_func ("/EventRequest/construct", test_event_request_construct);
  g_test_add_func ("/EventRequest/deserialize", test_event_request_deserialize);
  g_test_add_func ("/EventRequest/serialize", test_event_request_serialize);

  return g_test_run ();
}
