#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <plantd/plantd.h>

#include "plantd/plantd-utils.h"

static void
test_configuration_response_construct (void)
{
  g_autoptr (PdConfigurationResponse) configuration_response = NULL;

  configuration_response = pd_configuration_response_new ();

  g_assert_nonnull (configuration_response);
}

static void
test_configuration_response_deserialize (void)
{
  g_autoptr (PdConfigurationResponse) configuration_response = NULL;

  configuration_response = pd_configuration_response_new ();

  g_assert_nonnull (configuration_response);
}

static void
test_configuration_response_serialize (void)
{
  g_autoptr (PdConfigurationResponse) configuration_response = NULL;

  configuration_response = pd_configuration_response_new ();

  g_assert_nonnull (configuration_response);
}


gint
main (gint argc, gchar *argv[])
{
  g_test_init (&argc, &argv, NULL);

  g_test_add_func ("/ConfigurationResponse/construct", test_configuration_response_construct);
  g_test_add_func ("/ConfigurationResponse/deserialize", test_configuration_response_deserialize);
  g_test_add_func ("/ConfigurationResponse/serialize", test_configuration_response_serialize);

  return g_test_run ();
}
