#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <plantd/plantd.h>

#include "plantd/plantd-utils.h"

static void
test_configuration_request_construct (void)
{
  g_autoptr (PdConfigurationRequest) configuration_request = NULL;

  configuration_request = pd_configuration_request_new ();

  g_assert_nonnull (configuration_request);
}

static void
test_configuration_request_deserialize (void)
{
  g_autoptr (PdConfigurationRequest) configuration_request = NULL;

  configuration_request = pd_configuration_request_new ();

  g_assert_nonnull (configuration_request);
}

static void
test_configuration_request_serialize (void)
{
  g_autoptr (PdConfigurationRequest) configuration_request = NULL;

  configuration_request = pd_configuration_request_new ();

  g_assert_nonnull (configuration_request);
}


gint
main (gint argc, gchar *argv[])
{
  g_test_init (&argc, &argv, NULL);

  g_test_add_func ("/ConfigurationRequest/construct", test_configuration_request_construct);
  g_test_add_func ("/ConfigurationRequest/deserialize", test_configuration_request_deserialize);
  g_test_add_func ("/ConfigurationRequest/serialize", test_configuration_request_serialize);

  return g_test_run ();
}
