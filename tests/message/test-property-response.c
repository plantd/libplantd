#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <plantd/plantd.h>

#include "plantd/plantd-utils.h"

static void
test_property_response_construct (void)
{
  g_autoptr (PdPropertyResponse) object = NULL;
  g_autoptr (PdProperty) prop = NULL;

  object = pd_property_response_new ();
  prop = pd_property_new ("prop-key", "prop-val");

  g_assert_nonnull (object);
  g_assert_nonnull (prop);

  pd_property_response_set (object, prop);

  {
    g_autoptr (PdProperty) p = NULL;

    p = pd_property_response_get (object);

    g_assert_nonnull (p);
    g_assert_cmpstr (pd_property_get_key (p), ==, "prop-key");
    g_assert_cmpstr (pd_property_get_value (p), ==, "prop-val");
  }
}

static const gchar *json = "{" \
  "\"property\": { \"key\": \"prop-key\", \"value\": \"prop-val\" }" \
"}";

static void
test_property_response_json_serialize (void)
{
  {
    g_autoptr (GError) err = NULL;
    g_autoptr (GMatchInfo) match_info = NULL;
    g_autoptr (GRegex) regex = NULL;
    g_autoptr (PdPropertyResponse) response = NULL;
    g_autoptr (PdProperty) prop = NULL;
    g_autofree gchar *data = NULL;
    g_autofree gchar *data1 = NULL;

    response = pd_property_response_new ();
    prop = pd_property_new ("prop-key", "prop-val");

    pd_property_response_set (response, prop);

    data = pd_property_response_serialize (response);
    data1 = remove_whitespace (data);

    {
      regex = g_regex_new (".key.:.prop-key.", 0, 0, &err);
      g_regex_match (regex, data1, 0, &match_info);
      g_assert_true (g_match_info_matches (match_info));
    }
    {
      regex = g_regex_new (".value.:.prop-val.", 0, 0, &err);
      g_regex_match (regex, data1, 0, &match_info);
      g_assert_true (g_match_info_matches (match_info));
    }
  }

  {
    g_autoptr (PdPropertyResponse) response = NULL;
    g_autoptr (PdMessageError) error = NULL;
    g_autofree gchar *data = NULL;

    response = pd_property_response_new ();
    error = pd_message_error_new ();
    pd_message_error_set_code (error, 404);
    pd_message_error_set_message (error, "not found");
    pd_response_set_error (PD_RESPONSE (response), error);
    data = pd_property_response_serialize (response);
    if (g_test_verbose ())
      g_print ("\n%s\n", data);
  }
}

static void
test_property_response_json_deserialize (void)
{
  g_autoptr (PdPropertyResponse) response = NULL;
  g_autoptr (PdProperty) prop = NULL;

  response = pd_property_response_new ();
  pd_property_response_deserialize (response, json);

  prop = pd_property_response_get (response);

  g_assert_nonnull (prop);

  g_assert_cmpstr (pd_property_get_key (prop), ==, "prop-key");
  g_assert_cmpstr (pd_property_get_value (prop), ==, "prop-val");
}

gint
main (gint argc, gchar *argv[])
{
  g_test_init (&argc, &argv, NULL);

  g_test_add_func ("/PropertyResponse/construct", test_property_response_construct);
  g_test_add_func ("/PropertyResponse/json-serialize", test_property_response_json_serialize);
  g_test_add_func ("/PropertyResponse/json-deserialize", test_property_response_json_deserialize);

  return g_test_run ();
}
