#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <plantd/plantd.h>

#include "plantd/plantd-utils.h"

static void
test_channels_response_construct (void)
{
  g_autoptr (PdChannelsResponse) channels_response = NULL;

  channels_response = pd_channels_response_new ();

  g_assert_nonnull (channels_response);
}

static void
test_channels_response_deserialize (void)
{
  g_autoptr (PdChannelsResponse) channels_response = NULL;

  channels_response = pd_channels_response_new ();

  g_assert_nonnull (channels_response);
}

static void
test_channels_response_serialize (void)
{
  g_autoptr (PdChannelsResponse) channels_response = NULL;

  channels_response = pd_channels_response_new ();

  g_assert_nonnull (channels_response);
}


gint
main (gint argc, gchar *argv[])
{
  g_test_init (&argc, &argv, NULL);

  g_test_add_func ("/ChannelsResponse/construct", test_channels_response_construct);
  g_test_add_func ("/ChannelsResponse/deserialize", test_channels_response_deserialize);
  g_test_add_func ("/ChannelsResponse/serialize", test_channels_response_serialize);

  return g_test_run ();
}
