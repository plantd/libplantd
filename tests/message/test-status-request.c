#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <plantd/plantd.h>

#include "plantd/plantd-utils.h"

static void
test_status_request_construct (void)
{
  g_autoptr (PdStatusRequest) status_request = NULL;

  status_request = pd_status_request_new ();

  g_assert_nonnull (status_request);
}

static void
test_status_request_deserialize (void)
{
  g_autoptr (PdStatusRequest) status_request = NULL;

  status_request = pd_status_request_new ();

  g_assert_nonnull (status_request);
}

static void
test_status_request_serialize (void)
{
  g_autoptr (PdStatusRequest) status_request = NULL;

  status_request = pd_status_request_new ();

  g_assert_nonnull (status_request);
}


gint
main (gint argc, gchar *argv[])
{
  g_test_init (&argc, &argv, NULL);

  g_test_add_func ("/StatusRequest/construct", test_status_request_construct);
  g_test_add_func ("/StatusRequest/deserialize", test_status_request_deserialize);
  g_test_add_func ("/StatusRequest/serialize", test_status_request_serialize);

  return g_test_run ();
}
