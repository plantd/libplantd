#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <plantd/plantd.h>

#include "plantd/plantd-utils.h"

static void
test_event_response_construct (void)
{
  g_autoptr (PdEventResponse) event_response = NULL;

  event_response = pd_event_response_new ();

  g_assert_nonnull (event_response);
}

static void
test_event_response_deserialize (void)
{
  g_autoptr (PdEventResponse) event_response = NULL;

  event_response = pd_event_response_new ();

  g_assert_nonnull (event_response);
}

static void
test_event_response_serialize (void)
{
  g_autoptr (PdEventResponse) event_response = NULL;

  event_response = pd_event_response_new ();

  g_assert_nonnull (event_response);
}


gint
main (gint argc, gchar *argv[])
{
  g_test_init (&argc, &argv, NULL);

  g_test_add_func ("/EventResponse/construct", test_event_response_construct);
  g_test_add_func ("/EventResponse/deserialize", test_event_response_deserialize);
  g_test_add_func ("/EventResponse/serialize", test_event_response_serialize);

  return g_test_run ();
}
