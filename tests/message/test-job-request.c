#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <plantd/plantd.h>

#include "plantd/plantd-utils.h"

static void
test_job_request_construct (void)
{
  g_autoptr (PdJobRequest) object = NULL;
  g_autoptr (PdProperty) prop1 = NULL;
  g_autoptr (PdProperty) prop2 = NULL;

  object = pd_job_request_new ("41ea2a26-e968-4960-accd-5ff5baa1e219");

  g_assert_nonnull (object);
  g_assert_cmpstr (pd_job_request_get_id (object), ==,
                   "41ea2a26-e968-4960-accd-5ff5baa1e219");

  prop1 = pd_property_new ("prop-key1", "prop-val1");
  prop2 = pd_property_new ("prop-key2", "prop-val2");

  g_assert_nonnull (object);

  pd_job_request_set_id (object, "req-id");
  pd_job_request_set_job_id (object, "job-id");
  pd_job_request_set_job_value (object, "job-value");

  g_assert_cmpstr (pd_job_request_get_id (object), ==, "req-id");
  g_assert_cmpstr (pd_job_request_get_job_id (object), ==, "job-id");
  g_assert_cmpstr (pd_job_request_get_job_value (object), ==, "job-value");

  pd_job_request_set_id (object, "new-id");
  pd_job_request_set_job_id (object, "new-job-id");
  pd_job_request_set_job_value (object, "new-value");

  g_assert_cmpstr (pd_job_request_get_id (object), ==, "new-id");
  g_assert_cmpstr (pd_job_request_get_job_id (object), ==, "new-job-id");
  g_assert_cmpstr (pd_job_request_get_job_value (object), ==, "new-value");

  g_assert_nonnull (prop1);
  g_assert_nonnull (prop2);

  pd_job_request_add (object, prop1);
  pd_job_request_add (object, prop2);

  g_assert_true (pd_job_request_contains (object, "prop-key1"));
  g_assert_true (pd_job_request_contains (object, "prop-key2"));

  pd_job_request_remove (object, "prop-key2");

  g_assert_true (!pd_job_request_contains (object, "prop-key2"));

  {
    g_autoptr (PdProperty) prop = NULL;

    prop = pd_job_request_get (object, "prop-key1");

    g_assert_cmpstr (pd_property_get_key (prop), ==, "prop-key1");
    g_assert_cmpstr (pd_property_get_value (prop), ==, "prop-val1");
  }
}

static const gchar *json = "{ \
  \"id\": \"req-id\", \
  \"jobId\": \"job-id\", \
  \"jobValue\": \"job-value\", \
  \"jobProperties\": [ \
    { \"key\": \"baz\", \"value\": \"meh\" }, \
    { \"key\": \"foo\", \"value\": \"bar\" } \
  ] \
}";

static void
test_job_request_json_serialize (void)
{
  g_autoptr (PdJobRequest) object = NULL;
  g_autoptr (PdProperty) prop1 = NULL;
  g_autoptr (PdProperty) prop2 = NULL;
  g_autofree gchar *data = NULL;
  g_autofree gchar *data1 = NULL;
  g_autofree gchar *data2 = NULL;

  object = pd_job_request_new (NULL);
  g_assert_nonnull (object);

  pd_job_request_set_id (object, "req-id");
  pd_job_request_set_job_id (object, "job-id");
  pd_job_request_set_job_value (object, "job-value");
  prop1 = pd_property_new ("foo", "bar");
  prop2 = pd_property_new ("baz", "meh");
  pd_job_request_add (object, prop1);
  pd_job_request_add (object, prop2);

  data = pd_job_request_serialize (object);
  data1 = remove_whitespace (json);
  data2 = remove_whitespace (data);

  g_assert_cmpstr (data1, ==, data2);
}

static void
test_job_request_json_deserialize (void)
{
  g_autoptr (PdJobRequest) object = NULL;

  object = pd_job_request_new ("41ea2a26-e968-4960-accd-5ff5baa1e219");
  g_assert_nonnull (object);

  pd_job_request_deserialize (object, json);

  g_assert_cmpstr (pd_job_request_get_id (object), ==, "req-id");
  g_assert_cmpstr (pd_job_request_get_job_id (object), ==, "job-id");
  g_assert_cmpstr (pd_job_request_get_job_value (object), ==, "job-value");
  // TODO: test properties
}

gint
main (gint argc, gchar *argv[])
{
  g_test_init (&argc, &argv, NULL);

  g_test_add_func ("/JobRequest/construct", test_job_request_construct);
  g_test_add_func ("/JobRequest/json_serialize", test_job_request_json_serialize);
  g_test_add_func ("/JobRequest/json_deserialize", test_job_request_json_deserialize);

  return g_test_run ();
}
