#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <plantd/plantd.h>

#include "plantd/plantd-utils.h"

static void
test_job_response_construct (void)
{
  g_autoptr (PdJobResponse) job_response = NULL;

  job_response = pd_job_response_new ();

  g_assert_nonnull (job_response);
}

static void
test_job_response_deserialize (void)
{
  g_autoptr (PdJobResponse) job_response = NULL;

  job_response = pd_job_response_new ();

  g_assert_nonnull (job_response);
}

static void
test_job_response_serialize (void)
{
  g_autoptr (PdJobResponse) job_response = NULL;

  job_response = pd_job_response_new ();

  g_assert_nonnull (job_response);
}


gint
main (gint argc, gchar *argv[])
{
  g_test_init (&argc, &argv, NULL);

  g_test_add_func ("/JobResponse/construct", test_job_response_construct);
  g_test_add_func ("/JobResponse/deserialize", test_job_response_deserialize);
  g_test_add_func ("/JobResponse/serialize", test_job_response_serialize);

  return g_test_run ();
}
