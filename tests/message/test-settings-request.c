#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <plantd/plantd.h>

#include "plantd/plantd-utils.h"

static void
test_settings_request_construct (void)
{
  g_autoptr (PdSettingsRequest) settings_request = NULL;

  settings_request = pd_settings_request_new ();

  g_assert_nonnull (settings_request);
}

static void
test_settings_request_deserialize (void)
{
  g_autoptr (PdSettingsRequest) settings_request = NULL;

  settings_request = pd_settings_request_new ();

  g_assert_nonnull (settings_request);
}

static void
test_settings_request_serialize (void)
{
  g_autoptr (PdSettingsRequest) settings_request = NULL;

  settings_request = pd_settings_request_new ();

  g_assert_nonnull (settings_request);
}


gint
main (gint argc, gchar *argv[])
{
  g_test_init (&argc, &argv, NULL);

  g_test_add_func ("/SettingsRequest/construct", test_settings_request_construct);
  g_test_add_func ("/SettingsRequest/deserialize", test_settings_request_deserialize);
  g_test_add_func ("/SettingsRequest/serialize", test_settings_request_serialize);

  return g_test_run ();
}
