#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <plantd/plantd.h>

#include "plantd/plantd-utils.h"

static void
test_properties_response_construct (void)
{
  g_autoptr (PdPropertiesResponse) object = NULL;
  g_autoptr (PdProperty) prop1 = NULL;
  g_autoptr (PdProperty) prop2 = NULL;

  object = pd_properties_response_new ();
  prop1 = pd_property_new ("prop-key1", "prop-val1");
  prop2 = pd_property_new ("prop-key2", "prop-val2");

  g_assert_nonnull (object);
  g_assert_nonnull (prop1);
  g_assert_nonnull (prop2);

  pd_properties_response_add (object, prop1);
  pd_properties_response_add (object, prop2);

  g_assert_true (pd_properties_response_contains (object, "prop-key1"));
  g_assert_true (pd_properties_response_contains (object, "prop-key2"));

  pd_properties_response_remove (object, "prop-key2");

  g_assert_true (!pd_properties_response_contains (object, "prop-key2"));

  {
    g_autoptr (PdProperty) prop = NULL;

    prop = pd_properties_response_get (object, "prop-key1");

    g_assert_cmpstr (pd_property_get_key (prop), ==, "prop-key1");
    g_assert_cmpstr (pd_property_get_value (prop), ==, "prop-val1");
  }
}

static const gchar *json = "{" \
  "\"error\":{\"message\":\"thingsareborked\",\"code\":200}," \
  "\"properties\": [" \
    "{ \"key\": \"prop-key2\", \"value\": \"prop-val2\" }," \
    "{ \"key\": \"prop-key1\", \"value\": \"prop-val1\" }" \
  "]" \
"}";


static void
test_properties_response_json_serialize (void)
{
  g_autoptr (GError) err = NULL;
  g_autoptr (GMatchInfo) match_info = NULL;
  g_autoptr (GRegex) regex = NULL;
  g_autoptr (PdPropertiesResponse) response = NULL;
  g_autoptr (PdProperty) prop1 = NULL;
  g_autoptr (PdProperty) prop2 = NULL;
  g_autoptr (PdMessageError) error = NULL;
  g_autofree gchar *data = NULL;
  g_autofree gchar *data1 = NULL;

  response = pd_properties_response_new ();
  prop1 = pd_property_new ("prop-key1", "prop-val1");
  prop2 = pd_property_new ("prop-key2", "prop-val2");
  error = pd_message_error_new ();

  g_assert_nonnull (error);

  pd_properties_response_add (response, prop1);
  pd_properties_response_add (response, prop2);

  pd_message_error_set_code (error, 200);
  pd_message_error_set_message (error, "things are borked");
  pd_response_set_error (PD_RESPONSE (response), error);

  data = pd_properties_response_serialize (response);
  data1 = remove_whitespace (data);

  if (g_test_verbose ())
    g_print ("\n%s\n", data);

  {
    regex = g_regex_new (".code.:200", 0, 0, &err);
    g_regex_match (regex, data1, 0, &match_info);
    g_assert_true (g_match_info_matches (match_info));
  }
  {
    regex = g_regex_new (".properties.:[[].*[]]", 0, 0, &err);
    g_regex_match (regex, data1, 0, &match_info);
    g_assert_true (g_match_info_matches (match_info));
  }
  {
    regex = g_regex_new (".key.:.prop-key2.", 0, 0, &err);
    g_regex_match (regex, data1, 0, &match_info);
    g_assert_true (g_match_info_matches (match_info));
  }
  {
    regex = g_regex_new (".value.:.prop-val2.", 0, 0, &err);
    g_regex_match (regex, data1, 0, &match_info);
    g_assert_true (g_match_info_matches (match_info));
  }
}

static void
test_properties_response_json_deserialize (void)
{
  g_autoptr (PdPropertiesResponse) response = NULL;
  g_autoptr (PdProperty) prop1 = NULL;
  g_autoptr (PdProperty) prop2 = NULL;

  response = pd_properties_response_new ();
  pd_properties_response_deserialize (response, json);

  prop1 = pd_properties_response_get (response, "prop-key1");
  prop2 = pd_properties_response_get (response, "prop-key2");

  g_assert_nonnull (prop1);
  g_assert_nonnull (prop2);

  g_assert_cmpstr (pd_property_get_key (prop1), ==, "prop-key1");
  g_assert_cmpstr (pd_property_get_value (prop1), ==, "prop-val1");
  g_assert_cmpstr (pd_property_get_key (prop2), ==, "prop-key2");
  g_assert_cmpstr (pd_property_get_value (prop2), ==, "prop-val2");
}

gint
main (gint argc, gchar *argv[])
{
  g_test_init (&argc, &argv, NULL);

  g_test_add_func ("/PropertiesResponse/construct", test_properties_response_construct);
  g_test_add_func ("/PropertiesResponse/json-serialize", test_properties_response_json_serialize);
  g_test_add_func ("/PropertiesResponse/json-deserialize", test_properties_response_json_deserialize);

  return g_test_run ();
}
