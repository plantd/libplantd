#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <plantd/plantd.h>

#include "plantd/plantd-utils.h"

static void
test_configurations_response_construct (void)
{
  g_autoptr (PdConfigurationsResponse) configurations_response = NULL;

  configurations_response = pd_configurations_response_new ();

  g_assert_nonnull (configurations_response);
}

static void
test_configurations_response_deserialize (void)
{
  g_autoptr (PdConfigurationsResponse) configurations_response = NULL;

  configurations_response = pd_configurations_response_new ();

  g_assert_nonnull (configurations_response);
}

static void
test_configurations_response_serialize (void)
{
  g_autoptr (PdConfigurationsResponse) configurations_response = NULL;

  configurations_response = pd_configurations_response_new ();

  g_assert_nonnull (configurations_response);
}


gint
main (gint argc, gchar *argv[])
{
  g_test_init (&argc, &argv, NULL);

  g_test_add_func ("/ConfigurationsResponse/construct", test_configurations_response_construct);
  g_test_add_func ("/ConfigurationsResponse/deserialize", test_configurations_response_deserialize);
  g_test_add_func ("/ConfigurationsResponse/serialize", test_configurations_response_serialize);

  return g_test_run ();
}
