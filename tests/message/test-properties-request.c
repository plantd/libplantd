#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <plantd/plantd.h>

#include "plantd/plantd-utils.h"

static void
test_properties_request_construct (void)
{
  g_autoptr (PdPropertiesRequest) object = NULL;
  g_autoptr (PdProperty) prop1 = NULL;
  g_autoptr (PdProperty) prop2 = NULL;

  object = pd_properties_request_new ("service-id");
  prop1 = pd_property_new ("prop-key1", "prop-val1");
  prop2 = pd_property_new ("prop-key2", "prop-val2");

  g_assert_nonnull (object);

  g_assert_cmpstr (pd_properties_request_get_id (object), ==, "service-id");

  pd_properties_request_set_id (object, "new-id");

  g_assert_cmpstr (pd_properties_request_get_id (object), ==, "new-id");

  g_assert_nonnull (prop1);
  g_assert_nonnull (prop2);

  pd_properties_request_add (object, prop1);
  pd_properties_request_add (object, prop2);

  g_assert_true (pd_properties_request_contains (object, "prop-key1"));
  g_assert_true (pd_properties_request_contains (object, "prop-key2"));

  pd_properties_request_remove (object, "prop-key2");

  g_assert_true (!pd_properties_request_contains (object, "prop-key2"));

  {
    g_autoptr (PdProperty) prop;

    prop = pd_properties_request_get (object, "prop-key1");

    g_assert_cmpstr (pd_property_get_key (prop), ==, "prop-key1");
    g_assert_cmpstr (pd_property_get_value (prop), ==, "prop-val1");
  }
}

static const gchar *json = "{" \
  "\"id\": \"service-id\"," \
  "\"properties\": [" \
    "{ \"key\": \"prop-key2\", \"value\": \"prop-val2\" }," \
    "{ \"key\": \"prop-key1\", \"value\": \"prop-val1\" }" \
  "]" \
"}";

static void
test_properties_request_json_serialize (void)
{
  g_autoptr (GError) err = NULL;
  g_autoptr (GMatchInfo) match_info = NULL;
  g_autoptr (GRegex) regex = NULL;
  g_autoptr (PdPropertiesRequest) request = NULL;
  g_autoptr (PdProperty) prop1 = NULL;
  g_autoptr (PdProperty) prop2 = NULL;
  g_autofree gchar *data = NULL;
  g_autofree gchar *data1 = NULL;

  request = pd_properties_request_new ("service-id");
  prop1 = pd_property_new ("prop-key1", "prop-val1");
  prop2 = pd_property_new ("prop-key2", "prop-val2");

  pd_properties_request_add (request, prop1);
  pd_properties_request_add (request, prop2);

  data = pd_properties_request_serialize (request);
  data1 = remove_whitespace (data);

  {
    regex = g_regex_new (".id.:.service-id.", 0, 0, &err);
    g_regex_match (regex, data1, 0, &match_info);
    g_assert_true (g_match_info_matches (match_info));
  }
  {
    regex = g_regex_new (".properties.:[[].*[]]", 0, 0, &err);
    g_regex_match (regex, data1, 0, &match_info);
    g_assert_true (g_match_info_matches (match_info));
  }
  {
    regex = g_regex_new (".key.:.prop-key2.", 0, 0, &err);
    g_regex_match (regex, data1, 0, &match_info);
    g_assert_true (g_match_info_matches (match_info));
  }
  {
    regex = g_regex_new (".value.:.prop-val2.", 0, 0, &err);
    g_regex_match (regex, data1, 0, &match_info);
    g_assert_true (g_match_info_matches (match_info));
  }
}

static void
test_properties_request_json_deserialize (void)
{
  g_autoptr (PdPropertiesRequest) request = NULL;
  g_autoptr (PdProperty) prop1 = NULL;
  g_autoptr (PdProperty) prop2 = NULL;

  request = pd_properties_request_new (NULL);

  pd_properties_request_deserialize (request, json);

  g_assert_cmpstr (pd_properties_request_get_id (request), ==, "service-id");

  prop1 = pd_properties_request_get (request, "prop-key1");
  prop2 = pd_properties_request_get (request, "prop-key2");

  g_assert_nonnull (prop1);
  g_assert_nonnull (prop2);

  g_assert_cmpstr (pd_property_get_key (prop1), ==, "prop-key1");
  g_assert_cmpstr (pd_property_get_value (prop1), ==, "prop-val1");
  g_assert_cmpstr (pd_property_get_key (prop2), ==, "prop-key2");
  g_assert_cmpstr (pd_property_get_value (prop2), ==, "prop-val2");
}

gint
main (gint argc, gchar *argv[])
{
  g_test_init (&argc, &argv, NULL);

  g_test_add_func ("/PropertiesRequest/construct", test_properties_request_construct);
  g_test_add_func ("/PropertiesRequest/json-serialize", test_properties_request_json_serialize);
  g_test_add_func ("/PropertiesRequest/json-deserialize", test_properties_request_json_deserialize);

  return g_test_run ();
}
