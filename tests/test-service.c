#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <plantd/plantd.h>

#include "plantd/plantd-utils.h"

static void
test_service_construct (void)
{
  g_autoptr (PdService) object = NULL;

  object = pd_service_new ("org.plantd.Test");
  g_assert_nonnull (object);

/*
 *  g_autoptr (PdStatus) status = NULL;
 *  g_autoptr (PdState) state = NULL;
 *
 *  status = pd_status_new ();
 *  state = pd_state_new ();
 *
 *  pd_service_set_name (object, "test-service");
 *  pd_service_set_description (object, "A test service");
 *  pd_service_set_configuration_id (object,
 *                                     "41ea2a26-e968-4960-accd-5ff5baa1e219");
 *  pd_service_set_status (object, status);
 *  pd_service_set_state (object, state);
 *
 *  g_assert_cmpstr (pd_service_get_name (object), ==,
 *                   "test-service");
 *  g_assert_cmpstr (pd_service_get_description (object), ==,
 *                   "A test service");
 *  g_assert_cmpstr (pd_service_get_configuration_id (object),
 *                   ==, "41ea2a26-e968-4960-accd-5ff5baa1e219");
 */
}

static const gchar *json = "{ \
  \"name\": \"test-service\", \
  \"description\": \"A test service\", \
  \"configuration-id\": \"41ea2a26-e968-4960-accd-5ff5baa1e219\" \
}";

static void
test_service_json_serialize (void)
{
  g_autoptr (PdService) object = NULL;
  g_autoptr (PdStatus) status = NULL;
  g_autoptr (PdState) state = NULL;
  g_autofree gchar *data = NULL;
  g_autofree gchar *data1 = NULL;
  g_autofree gchar *data2 = NULL;

  object = pd_service_new ("org.plantd.Test");
  g_assert_nonnull (object);

/*
 *  pd_service_set_name (object, "test-service");
 *  pd_service_set_description (object, "A test service");
 *  pd_service_set_configuration_id (object,
 *                                     "41ea2a26-e968-4960-accd-5ff5baa1e219");
 *  pd_service_set_status (object, status);
 *  pd_service_set_state (object, state);
 *
 *  data = pd_service_serialize (object);
 *  data1 = remove_whitespace (json);
 *  data2 = remove_whitespace (data);
 *
 *  g_assert_cmpstr (data1, ==, data2);
 */
}

static void
test_service_json_deserialize (void)
{
  g_autoptr (PdService) object = NULL;

  object = pd_service_new ("org.plantd.Test");
  g_assert_nonnull (object);

/*
 *  pd_service_deserialize (object, json);
 *
 *  g_assert_cmpstr (pd_service_get_name (object), ==,
 *                   "test-service");
 *  g_assert_cmpstr (pd_service_get_description (object), ==,
 *                   "A test service");
 *  g_assert_cmpstr (pd_service_get_configuration_id (object),
 *                   ==, "41ea2a26-e968-4960-accd-5ff5baa1e219");
 */
}

gint
main (gint argc, gchar *argv[])
{
  g_test_init (&argc, &argv, NULL);

  g_test_add_func ("/service/construct", test_service_construct);
  g_test_add_func ("/service/json-serialize", test_service_json_serialize);
  g_test_add_func ("/service/json-deserialize", test_service_json_deserialize);

  return g_test_run ();
}
