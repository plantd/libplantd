#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <json-glib/json-glib.h>
#include <plantd/plantd.h>

#include "plantd/plantd-utils.h"
#include "plantd/core/pd-table.h"

static void
test_table_double_value (void)
{
  g_autoptr (PdTable) table = NULL;

  GValue v1 = G_VALUE_INIT;
  GValue v2 = G_VALUE_INIT;
  GValue v3 = G_VALUE_INIT;

  table = pd_table_new (G_TYPE_DOUBLE);
  g_assert_nonnull (table);

  g_value_init (&v1, G_TYPE_DOUBLE);
  g_value_init (&v2, G_TYPE_DOUBLE);
  g_value_init (&v3, G_TYPE_DOUBLE);

  g_value_set_double (&v1, 1.0);
  g_value_set_double (&v2, 2.0);
  g_value_set_double (&v3, 3.0);

  pd_table_add (table, "value_1", &v1);
  pd_table_add (table, "value_2", &v2);
  pd_table_add (table, "value_3", &v3);

  g_assert_true (pd_table_has (table, "value_1"));
  g_assert_true (pd_table_has (table, "value_2"));
  g_assert_true (pd_table_has (table, "value_3"));
  g_assert_cmpfloat (g_value_get_double (pd_table_get (table, "value_1")), ==, 1.0);
  g_assert_cmpfloat (g_value_get_double (pd_table_get (table, "value_2")), ==, 2.0);
  g_assert_cmpfloat (g_value_get_double (pd_table_get (table, "value_3")), ==, 3.0);

  pd_table_remove (table, "value_1");
  pd_table_remove (table, "value_2");
  pd_table_remove (table, "value_3");
  g_assert_false (pd_table_has (table, "value_1"));
  g_assert_false (pd_table_has (table, "value_2"));
  g_assert_false (pd_table_has (table, "value_3"));
}

static void
test_table_string_value (void)
{
  g_autoptr (PdTable) table = NULL;

  GValue v1 = G_VALUE_INIT;
  GValue v2 = G_VALUE_INIT;
  GValue v3 = G_VALUE_INIT;

  table = pd_table_new (G_TYPE_STRING);
  g_assert_nonnull (table);

  g_value_init (&v1, G_TYPE_STRING);
  g_value_init (&v2, G_TYPE_STRING);
  g_value_init (&v3, G_TYPE_STRING);

  g_value_set_static_string (&v1, "a");
  g_value_set_static_string (&v2, "b");
  g_value_set_static_string (&v3, "c");

  pd_table_add (table, "value_1", &v1);
  pd_table_add (table, "value_2", &v2);
  pd_table_add (table, "value_3", &v3);

  g_assert_true (pd_table_has (table, "value_1"));
  g_assert_true (pd_table_has (table, "value_2"));
  g_assert_true (pd_table_has (table, "value_3"));
  g_assert_cmpstr (g_value_get_string (pd_table_get (table, "value_1")), ==, "a");
  g_assert_cmpstr (g_value_get_string (pd_table_get (table, "value_2")), ==, "b");
  g_assert_cmpstr (g_value_get_string (pd_table_get (table, "value_3")), ==, "c");

  pd_table_remove (table, "value_1");
  pd_table_remove (table, "value_2");
  pd_table_remove (table, "value_3");
  g_assert_false (pd_table_has (table, "value_1"));
  g_assert_false (pd_table_has (table, "value_2"));
  g_assert_false (pd_table_has (table, "value_3"));
}

static void
test_table_object_value (void)
{
  g_autoptr (PdTable) table = NULL;
  g_autoptr (PdObject) obj1 = NULL;
  g_autoptr (PdObject) obj2 = NULL;
  g_autoptr (PdObject) obj3 = NULL;

  GValue v1 = G_VALUE_INIT;
  GValue v2 = G_VALUE_INIT;
  GValue v3 = G_VALUE_INIT;

  obj1 = pd_object_new ("obj1");
  obj2 = pd_object_new ("obj2");
  obj3 = pd_object_new ("obj3");

  table = pd_table_new (G_TYPE_OBJECT);
  g_assert_nonnull (table);

  g_value_init (&v1, G_TYPE_OBJECT);
  g_value_init (&v2, G_TYPE_OBJECT);
  g_value_init (&v3, G_TYPE_OBJECT);

  g_value_set_object (&v1, obj1);
  g_value_set_object (&v2, obj2);
  g_value_set_object (&v3, obj3);

  pd_table_add (table, "value_1", &v1);
  pd_table_add (table, "value_2", &v2);
  pd_table_add (table, "value_3", &v3);

  g_value_unset (&v1);
  g_value_unset (&v2);
  g_value_unset (&v3);

  g_assert_true (pd_table_has (table, "value_1"));
  g_assert_true (pd_table_has (table, "value_2"));
  g_assert_true (pd_table_has (table, "value_3"));

  {
    g_autofree gchar *id1 = NULL;
    g_autofree gchar *id2 = NULL;
    g_autofree gchar *id3 = NULL;

    id1 = g_strdup (pd_object_get_id (g_value_get_object (pd_table_get (table, "value_1"))));
    id2 = g_strdup (pd_object_get_id (g_value_get_object (pd_table_get (table, "value_2"))));
    id3 = g_strdup (pd_object_get_id (g_value_get_object (pd_table_get (table, "value_3"))));

    g_assert_cmpstr (id1, ==, "obj1");
    g_assert_cmpstr (id2, ==, "obj2");
    g_assert_cmpstr (id3, ==, "obj3");
  }

  pd_table_remove (table, "value_1");
  pd_table_remove (table, "value_2");
  pd_table_remove (table, "value_3");
  g_assert_false (pd_table_has (table, "value_1"));
  g_assert_false (pd_table_has (table, "value_2"));
  g_assert_false (pd_table_has (table, "value_3"));
}

static const gchar *double_json = "{ \
  \"value_1\": 1.0, \
  \"value_2\": 2.0, \
  \"value_3\": 3.0 \
}";

static const gchar *string_json = "{ \
  \"value_1\": \"a\", \
  \"value_2\": \"b\", \
  \"value_3\": \"c\" \
}";

static const gchar *object_json = "[ \
  { \"key\": \"a\", \"value\": \"value_a\" }, \
  { \"key\": \"b\", \"value\": \"value_b\" }, \
  { \"key\": \"c\", \"value\": \"value_c\" } \
]";

static void
test_table_double_serialize (void)
{
  g_autoptr (PdTable) table = NULL;
  g_autoptr (JsonGenerator) generator = NULL;
  g_autoptr (JsonNode) node = NULL;
  g_autofree gchar *data = NULL;
  g_autofree gchar *data1 = NULL;

  GValue v1 = G_VALUE_INIT;
  GValue v2 = G_VALUE_INIT;
  GValue v3 = G_VALUE_INIT;

  table = pd_table_new (G_TYPE_DOUBLE);

  g_assert_nonnull (table);

  g_value_init (&v1, G_TYPE_DOUBLE);
  g_value_init (&v2, G_TYPE_DOUBLE);
  g_value_init (&v3, G_TYPE_DOUBLE);

  g_value_set_double (&v1, 1.0);
  g_value_set_double (&v2, 2.0);
  g_value_set_double (&v3, 3.0);

  pd_table_add (table, "value_1", &v1);
  pd_table_add (table, "value_2", &v2);
  pd_table_add (table, "value_3", &v3);

  generator = json_generator_new ();
  node = pd_table_serialize (table, JSON_TYPE_OBJECT);
  json_generator_set_root (generator, node);
  data = json_generator_to_data (generator, NULL);

  data1 = remove_whitespace (data);

  for (gint i = 1; i <= 3; i++)
    {
      g_autoptr (GMatchInfo) match_info = NULL;
      g_autoptr (GRegex) regex = NULL;
      g_autofree gchar *pattern = NULL;

      pattern = g_strdup_printf ("\"value_%d\":%.1f", i, (gfloat) i);
      regex = g_regex_new (pattern, 0, 0, NULL);
      g_regex_match (regex, data1, 0, &match_info);
      g_assert_true (g_match_info_matches (match_info));
    }
}

static void
test_table_string_serialize (void)
{
  g_autoptr (PdTable) table = NULL;
  g_autoptr (JsonGenerator) generator = NULL;
  g_autoptr (JsonNode) node = NULL;
  g_autofree gchar *data = NULL;
  g_autofree gchar *data1 = NULL;

  GValue v1 = G_VALUE_INIT;
  GValue v2 = G_VALUE_INIT;
  GValue v3 = G_VALUE_INIT;

  table = pd_table_new (G_TYPE_STRING);

  g_assert_nonnull (table);

  g_value_init (&v1, G_TYPE_STRING);
  g_value_init (&v2, G_TYPE_STRING);
  g_value_init (&v3, G_TYPE_STRING);

  g_value_set_static_string (&v1, "a");
  g_value_set_static_string (&v2, "b");
  g_value_set_static_string (&v3, "c");

  pd_table_add (table, "value_1", &v1);
  pd_table_add (table, "value_2", &v2);
  pd_table_add (table, "value_3", &v3);

  generator = json_generator_new ();
  node = pd_table_serialize (table, JSON_TYPE_OBJECT);
  json_generator_set_root (generator, node);
  data = json_generator_to_data (generator, NULL);

  data1 = remove_whitespace (data);

  for (gint i = 0; i < 3; i++)
    {
      g_autoptr (GMatchInfo) match_info = NULL;
      g_autoptr (GRegex) regex = NULL;
      g_autofree gchar *pattern = NULL;
      gchar values[3] = {'a', 'b', 'c'};

      pattern = g_strdup_printf ("\"value_%d\":\"%c\"", i + 1, values[i]);
      regex = g_regex_new (pattern, 0, 0, NULL);
      g_regex_match (regex, data1, 0, &match_info);
      g_assert_true (g_match_info_matches (match_info));
    }
}

static void
test_table_object_serialize (void)
{
  g_autoptr (PdTable) table = NULL;
  g_autoptr (PdProperty) prop1 = NULL;
  g_autoptr (PdProperty) prop2 = NULL;
  g_autoptr (PdProperty) prop3 = NULL;
  g_autoptr (JsonGenerator) generator = NULL;
  g_autoptr (JsonNode) node = NULL;
  g_autofree gchar *data = NULL;
  g_autofree gchar *data1 = NULL;

  GValue v1 = G_VALUE_INIT;
  GValue v2 = G_VALUE_INIT;
  GValue v3 = G_VALUE_INIT;

  table = pd_table_new (G_TYPE_OBJECT);

  g_assert_nonnull (table);

  prop1 = pd_property_new ("a", "value_a");
  prop2 = pd_property_new ("b", "value_b");
  prop3 = pd_property_new ("c", "value_c");

  g_value_init (&v1, G_TYPE_OBJECT);
  g_value_init (&v2, G_TYPE_OBJECT);
  g_value_init (&v3, G_TYPE_OBJECT);

  g_value_set_object (&v1, prop1);
  g_value_set_object (&v2, prop2);
  g_value_set_object (&v3, prop3);

  pd_table_add (table, "value_1", &v1);
  pd_table_add (table, "value_2", &v2);
  pd_table_add (table, "value_3", &v3);

  g_value_unset (&v1);
  g_value_unset (&v2);
  g_value_unset (&v3);

  generator = json_generator_new ();
  node = pd_table_serialize (table, JSON_TYPE_ARRAY);
  json_generator_set_root (generator, node);
  data = json_generator_to_data (generator, NULL);

  data1 = remove_whitespace (data);

  for (gint i = 0; i < 3; i++)
    {
      g_autoptr (GMatchInfo) match_info = NULL;
      g_autoptr (GRegex) regex = NULL;
      g_autofree gchar *pattern = NULL;
      gchar vals[3] = {'a', 'b', 'c'};

      pattern = g_strdup_printf ("\"key\":\"%c\",\"value\":\"value_%c\"", vals[i], vals[i]);
      regex = g_regex_new (pattern, 0, 0, NULL);
      g_regex_match (regex, data1, 0, &match_info);
      g_assert_true (g_match_info_matches (match_info));
    }
}

static void
test_table_double_deserialize (void)
{
  g_autoptr (PdTable) table = NULL;
  g_autoptr (JsonNode) node = NULL;

  table = pd_table_new (G_TYPE_DOUBLE);

  g_assert_nonnull (table);

  node = json_from_string (double_json, NULL);
  pd_table_deserialize (table, node);

  g_assert_true (pd_table_has (table, "value_1"));
  g_assert_true (pd_table_has (table, "value_2"));
  g_assert_true (pd_table_has (table, "value_3"));
  g_assert_cmpfloat (g_value_get_double (pd_table_get (table, "value_1")), ==, 1.0);
  g_assert_cmpfloat (g_value_get_double (pd_table_get (table, "value_2")), ==, 2.0);
  g_assert_cmpfloat (g_value_get_double (pd_table_get (table, "value_3")), ==, 3.0);
}

static void
test_table_string_deserialize (void)
{
  g_autoptr (PdTable) table = NULL;
  g_autoptr (JsonNode) node = NULL;

  table = pd_table_new (G_TYPE_STRING);

  g_assert_nonnull (table);

  node = json_from_string (string_json, NULL);
  pd_table_deserialize (table, node);

  g_assert_true (pd_table_has (table, "value_1"));
  g_assert_true (pd_table_has (table, "value_2"));
  g_assert_true (pd_table_has (table, "value_3"));
  g_assert_cmpstr (g_value_get_string (pd_table_get (table, "value_1")), ==, "a");
  g_assert_cmpstr (g_value_get_string (pd_table_get (table, "value_2")), ==, "b");
  g_assert_cmpstr (g_value_get_string (pd_table_get (table, "value_3")), ==, "c");
}

static void
test_table_object_deserialize (void)
{
  g_autoptr (PdTable) table = NULL;
  g_autoptr (JsonNode) node = NULL;

  table = pd_table_new (PD_TYPE_PROPERTY);

  g_assert_nonnull (table);

  node = json_from_string (object_json, NULL);
  pd_table_deserialize (table, node);

  g_assert_true (pd_table_has (table, "a"));
  g_assert_true (pd_table_has (table, "b"));
  g_assert_true (pd_table_has (table, "c"));

  {
    g_autoptr (PdProperty) prop1 = NULL;
    g_autofree gchar *key1 = NULL;
    g_autofree gchar *key2 = NULL;
    g_autofree gchar *key3 = NULL;

    key1 = g_strdup (pd_property_get_key (g_value_get_object (pd_table_get (table, "a"))));
    key2 = g_strdup (pd_property_get_key (g_value_get_object (pd_table_get (table, "b"))));
    key3 = g_strdup (pd_property_get_key (g_value_get_object (pd_table_get (table, "c"))));

    g_assert_cmpstr (key1, ==, "a");
    g_assert_cmpstr (key2, ==, "b");
    g_assert_cmpstr (key3, ==, "c");
  }
}

gint
main (gint argc, gchar *argv[])
{
  g_test_init (&argc, &argv, NULL);

  g_test_add_func ("/table/double-value", test_table_double_value);
  g_test_add_func ("/table/string-value", test_table_string_value);
  g_test_add_func ("/table/object-value", test_table_object_value);
  g_test_add_func ("/table/double-serialize", test_table_double_serialize);
  g_test_add_func ("/table/string-serialize", test_table_string_serialize);
  g_test_add_func ("/table/object-serialize", test_table_object_serialize);
  g_test_add_func ("/table/double-deserialize", test_table_double_deserialize);
  g_test_add_func ("/table/string-deserialize", test_table_string_deserialize);
  g_test_add_func ("/table/object-deserialize", test_table_object_deserialize);

  return g_test_run ();
}
