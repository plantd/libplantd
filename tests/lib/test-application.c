#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <plantd/plantd.h>

#include "test-application.h"

typedef PdApplication TestApplication;
typedef PdApplicationClass TestApplicationClass;

G_DEFINE_TYPE (TestApplication, test_application, PD_TYPE_APPLICATION)

static void
test_application_finalize (GObject *object)
{
  G_OBJECT_CLASS (test_application_parent_class)->finalize (object);
}

static PdConfigurationResponse *
test_application_get_configuration (PdApplication  *self,
                                    GError        **error)
{
  g_autoptr (PdConfiguration) configuration = NULL;
  PdConfigurationResponse *response = NULL;
  g_autofree gchar *uuid = NULL;

  uuid = g_uuid_string_random ();
  configuration = pd_configuration_new ();
  pd_configuration_set_id (configuration, uuid);
  pd_configuration_set_namespace (configuration, PD_CONFIGURATION_NAMESPACE_ACQUIRE);

  response = pd_configuration_response_new ();
  pd_configuration_response_set_configuration (response, configuration);

  return response;
}

static PdStatusResponse *
test_application_get_status (PdApplication  *self,
                             GError        **error)
{
  g_autoptr (PdStatus) status = NULL;
  PdStatusResponse *response = NULL;

  status = pd_status_new ();
  pd_status_set_active (status, TRUE);
  pd_status_set_loaded (status, FALSE);
  pd_status_set_enabled (status, TRUE);
  pd_status_add_detail (status, "foo", "bar");
  pd_status_add_detail (status, "baz", "qux");

  response = pd_status_response_new ();
  pd_status_response_set_status (response, status);

  return response;
}

static PdSettingsResponse *
test_application_get_settings (PdApplication  *self,
                               GError        **error)
{
  PdSettingsResponse *response = NULL;

  response = pd_settings_response_new ();

  pd_settings_response_set_id (response, pd_application_get_id (PD_APPLICATION (self)));
  pd_settings_response_add_setting (response, "foo", "bar");
  pd_settings_response_add_setting (response, "baz", "qux");

  return response;
}

static void
test_application_class_init (TestApplicationClass *klass)
{
  G_OBJECT_CLASS (klass)->finalize = test_application_finalize;
  PD_APPLICATION_CLASS (klass)->get_configuration = test_application_get_configuration;
  PD_APPLICATION_CLASS (klass)->get_status = test_application_get_status;
  PD_APPLICATION_CLASS (klass)->get_settings = test_application_get_settings;

  if (g_test_verbose ())
    g_print ("setting environment");

  /* Run the application in standalone mode */
  g_setenv ("PD_MODULE_STANDALONE", "true", TRUE);
  g_setenv ("PD_MODULE_ENDPOINT", "tcp://localhost:19999", TRUE);
  g_setenv ("PD_MODULE_BROKER", "tcp://*:19999", TRUE);

  g_assert_true (PD_APPLICATION_CLASS (klass)->get_configuration != NULL);
  g_assert_true (PD_APPLICATION_CLASS (klass)->get_status != NULL);
}

static void
test_application_init (TestApplication *self)
{
}

static gboolean
test_application_quit (gpointer user_data)
{
  GApplication *app;

  app = G_APPLICATION (user_data);

  g_application_quit (app);

  return FALSE;
}

PdApplication *
test_application_new (void)
{
  return g_object_new (TEST_TYPE_APPLICATION, NULL);
}
