#include <glib.h>
#include <gio/gio.h>

#include <plantd/plantd.h>

/**
 * TestSink
 *
 * An #PdSink implementation for testing.
 */

G_BEGIN_DECLS

static guint message_count = 0;
static guint message_type = 0;

#define TEST_TYPE_SINK test_sink_get_type ()

void      test_sink_set_message_type  (guint type);
guint     test_sink_get_message_count (void);
PdSink *test_sink_new               (const gchar *endpoint, const gchar *filter);

G_END_DECLS
