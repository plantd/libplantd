#include <glib.h>
#include <gio/gio.h>

#include <plantd/plantd.h>

/**
 * TestApplication
 *
 * An #PdApplication implementation for testing.
 */

G_BEGIN_DECLS

#define TEST_TYPE_APPLICATION test_application_get_type ()

PdApplication *test_application_new (void);

G_END_DECLS
