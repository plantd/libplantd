#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <plantd/plantd.h>

#include "test-sink.h"

typedef PdSink TestSink;
typedef PdSinkClass TestSinkClass;

G_DEFINE_TYPE (TestSink, test_sink, PD_TYPE_SINK);

void
test_sink_set_message_type (guint type)
{
  message_type = type;
}

guint
test_sink_get_message_count (void)
{
  return message_count;
}

static void
test_sink_handle_message (PdSink      *self,
                          const gchar *msg)
{
  if (g_test_verbose ())
    g_print ("saw a message: %s\n", msg);

  if (message_type == 0)
    {
      g_autoptr (PdEvent) event = NULL;

      event = pd_event_new ();
      pd_event_deserialize (event, msg);
    }
  else
    {
      g_autoptr (PdMetric) metric = NULL;

      metric = pd_metric_new ();
      pd_metric_deserialize (metric, msg);
    }

  message_count++;
}

static void
test_sink_finalize (GObject *object)
{
  G_OBJECT_CLASS (test_sink_parent_class)->finalize (object);
}

static void
test_sink_class_init (TestSinkClass *klass)
{
  G_OBJECT_CLASS (klass)->finalize = test_sink_finalize;
  PD_SINK_CLASS (klass)->handle_message = test_sink_handle_message;
}

static void
test_sink_init (TestSink *self)
{
  message_count = 0;
}

PdSink *
test_sink_new (const gchar *endpoint,
               const gchar *filter)
{
  return g_object_new (TEST_TYPE_SINK,
                       "endpoint", endpoint,
                       "filter", filter,
                       NULL);
}
