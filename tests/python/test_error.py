# -*- Mode: Python -*-

from __future__ import absolute_import

import gi
import unittest

gi.require_version("Pd", "1.0")

# pylint: disable=wrong-import-position

from gi.repository import Pd

# pylint: enable=wrong-import-position


class TestError(unittest.TestCase):
    pass
