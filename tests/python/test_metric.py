#!/usr/bin/env python3
# -*- Mode: Python -*-

from __future__ import absolute_import

import gi
import unittest

gi.require_version("Pd", "1.0")

# pylint: disable=wrong-import-position

from gi.repository import Pd

# pylint: enable=wrong-import-position


class TestMetric(unittest.TestCase):
    def test_memory_leak(self):
        metric = Pd.Metric.new()
        table = Pd.MetricTable.new()

        metric.set_data(table)
        metric.set_id("baz")
        _ = metric.get_data()

    def test_construction_1(self):
        metric = Pd.Metric.new()
        table = Pd.MetricTable.new()

        metric.set_id("foo")
        metric.set_name("bar")
        metric.set_data(table)

        with self.subTest("id"):
            self.assertEqual(metric.dup_id(), "foo")

        with self.subTest("name"):
            self.assertEqual(metric.dup_name(), "bar")

        with self.subTest("data type"):
            self.assertTrue(isinstance(metric.get_data(), Pd.MetricTable))

    def test_construction_2(self):
        table = Pd.MetricTable.new()
        metric = Pd.Metric.new_full("foo", "bar", table)

        with self.subTest("id"):
            self.assertEqual(metric.dup_id(), "foo")

        with self.subTest("name"):
            self.assertEqual(metric.dup_name(), "bar")

        with self.subTest("data type"):
            self.assertTrue(isinstance(metric.get_data(), Pd.MetricTable))

        metric.set_id("baz")
        metric.set_name("meh")

        with self.subTest("id renamed"):
            self.assertEqual(metric.dup_id(), "baz")

        with self.subTest("name renamed"):
            self.assertEqual(metric.dup_name(), "meh")

    def test_get_data_behavior(self):

        # Construct a metric
        table1 = Pd.MetricTable.new()
        header = Pd.MetricTableHeader.new("calibration")
        header.add_column("column_1", 1.0)
        table1.set_name("baz")
        table1.set_timestamp("2019-04-09 14:00:0.00-00")
        table1.set_header(header)
        table1.add_entry("value_1", 10.0)
        metric = Pd.Metric.new_full("foo", "bar", table1)

        # use get_data
        table2 = metric.get_data()

        # add an entry to table2
        table2.add_entry("value_2", 20.0)

        # see if this changed the attribute of metric
        table3 = metric.get_data()
        with self.subTest("Compare value_1 of table3 and table2"):
            self.assertEqual(table3.get_entry("value_1"), table2.get_entry("value_1"))
        with self.subTest("Compare value_2 of table3 and table2"):
            self.assertEqual(table3.get_entry("value_2"), table2.get_entry("value_2"))

        # see if table1 was changed
        with self.subTest("Compare value_2 of table3 and table1"):
            self.assertEqual(table3.get_entry("value_2"), table1.get_entry("value_2"))

        # see if the objects are identical
        with self.subTest("table3 == table2"):
            self.assertTrue(table3 == table2)
        with self.subTest("table3 == table1"):
            self.assertTrue(table3 == table1)

    def test_ref_data_behavior(self):

        # Construct a metric
        table1 = Pd.MetricTable.new()
        header = Pd.MetricTableHeader.new("calibration")
        header.add_column("column_1", 1.0)
        table1.set_name("baz")
        table1.set_timestamp("2019-04-09 14:00:0.00-00")
        table1.set_header(header)
        table1.add_entry("value_1", 10.0)
        metric = Pd.Metric.new_full("foo", "bar", table1)

        # use ref_data
        table2 = metric.ref_data()

        # add an entry to table2
        table2.add_entry("value_2", 20.0)

        # see if this changed the attribute of metric
        table3 = metric.ref_data()
        with self.subTest("Compare value_1 of table3 and table2"):
            self.assertEqual(table3.get_entry("value_1"), table2.get_entry("value_1"))
        with self.subTest("Compare value_2 of table3 and table2"):
            self.assertEqual(table3.get_entry("value_2"), table2.get_entry("value_2"))

        # see if table1 was changed
        with self.subTest("Compare value_2 of table3 and table1"):
            self.assertEqual(table3.get_entry("value_2"), table1.get_entry("value_2"))

        # see if the objects are identical
        with self.subTest("table3 == table2"):
            self.assertTrue(table3 == table2)
        with self.subTest("table3 == table1"):
            self.assertTrue(table3 == table1)

    def test_serialize_deserialize(self):
        # Create metric1 and fill it with values
        table1 = Pd.MetricTable.new()
        header1 = Pd.MetricTableHeader.new("calibration")

        header1.add_column("column_1", 1.0)
        header1.add_column("column_2", 2.0)
        header1.add_column("column_3", 3.0)
        header1.add_column("column_4", 4.0)
        header1.add_column("column_5", 5.0)
        header1.add_column("column_6", 6.0)

        table1.set_name("baz")
        table1.set_timestamp("2019-04-09 14:00:0.00-00")
        table1.set_header(header1)
        table1.add_entry("value_1", 10.0)
        table1.add_entry("value_2", 20.0)
        table1.add_entry("value_3", 30.0)

        metric1 = Pd.Metric.new_full("foo", "bar", table1)

        # Serialize metric1 into json
        data1 = metric1.serialize()

        # Create empty metric2
        metric2 = Pd.Metric.new()

        # Deserialize data1 into metric2
        metric2.deserialize(data1)

        # Compare the metrics
        with self.subTest("compare ids"):
            self.assertEqual(metric1.dup_id(), metric2.dup_id())

        with self.subTest("compare names"):
            self.assertEqual(metric1.dup_name(), metric2.dup_name())

        table2 = metric2.get_data()
        with self.subTest("compare table names"):
            self.assertEqual(table1.get_name(), table2.get_name())
        with self.subTest("compare table timestamps"):
            self.assertEqual(table1.get_timestamp(), table2.get_timestamp())
        with self.subTest("compare tables 1"):
            self.assertEqual(table1.get_entry("value_1"), table2.get_entry("value_1"))
        with self.subTest("compare tables 2"):
            self.assertEqual(table1.get_entry("value_2"), table2.get_entry("value_2"))
        with self.subTest("compare tables 3"):
            self.assertEqual(table1.get_entry("value_3"), table2.get_entry("value_3"))

        header2 = table2.get_header()
        with self.subTest("compare header names"):
            self.assertEqual(header1.get_name(), header2.get_name())
        with self.subTest("compare headers 1"):
            self.assertEqual(
                header1.get_column("column_1"), header2.get_column("column_1")
            )
        with self.subTest("compare headers 2"):
            self.assertEqual(
                header1.get_column("column_2"), header2.get_column("column_2")
            )
        with self.subTest("compare headers 3"):
            self.assertEqual(
                header1.get_column("column_3"), header2.get_column("column_3")
            )
        with self.subTest("compare headers 4"):
            self.assertEqual(
                header1.get_column("column_4"), header2.get_column("column_4")
            )
        with self.subTest("compare headers 5"):
            self.assertEqual(
                header1.get_column("column_5"), header2.get_column("column_5")
            )
        with self.subTest("compare headers 6"):
            self.assertEqual(
                header1.get_column("column_6"), header2.get_column("column_6")
            )


class TestMetricTable(unittest.TestCase):
    def test_construction(self):
        table = Pd.MetricTable.new()
        table.set_name("foo")
        table.set_timestamp("2019-04-09 14:00:0.00-00")
        table.add_entry("value_1", 10.0)

        with self.subTest("name"):
            self.assertEqual(table.dup_name(), "foo")
        with self.subTest("timestamp"):
            self.assertEqual(table.get_timestamp(), "2019-04-09 14:00:0.00-00")
        with self.subTest("entry"):
            self.assertEqual(table.get_entry("value_1"), 10.0)

    def test_serialize_deserialize(self):

        # Create table1 and fill it with values
        table1 = Pd.MetricTable.new()
        header1 = Pd.MetricTableHeader.new("calibration")

        header1.add_column("column_1", 1.0)
        header1.add_column("column_2", 2.0)
        header1.add_column("column_3", 3.0)
        header1.add_column("column_4", 4.0)
        header1.add_column("column_5", 5.0)
        header1.add_column("column_6", 6.0)

        table1.set_name("baz")
        table1.set_timestamp("2019-04-09 14:00:0.00-00")
        table1.set_header(header1)
        table1.add_entry("value_1", 10.0)
        table1.add_entry("value_2", 20.0)
        table1.add_entry("value_3", 30.0)

        # Serialize table1 into json
        data1 = table1.serialize()

        # Create empty table2
        table2 = Pd.MetricTable.new()

        # Deserialize data1 into table2
        table2.deserialize(data1)

        # Compare the tables
        with self.subTest("compare table names"):
            self.assertEqual(table1.get_name(), table2.get_name())
        with self.subTest("compare table timestamps"):
            self.assertEqual(table1.get_timestamp(), table2.get_timestamp())
        with self.subTest("compare tables 1"):
            self.assertEqual(table1.get_entry("value_1"), table2.get_entry("value_1"))
        with self.subTest("compare tables 2"):
            self.assertEqual(table1.get_entry("value_2"), table2.get_entry("value_2"))
        with self.subTest("compare tables 3"):
            self.assertEqual(table1.get_entry("value_3"), table2.get_entry("value_3"))

        header2 = table2.get_header()
        with self.subTest("compare header names"):
            self.assertEqual(header1.get_name(), header2.get_name())
        with self.subTest("compare headers 1"):
            self.assertEqual(
                header1.get_column("column_1"), header2.get_column("column_1")
            )
        with self.subTest("compare headers 2"):
            self.assertEqual(
                header1.get_column("column_2"), header2.get_column("column_2")
            )
        with self.subTest("compare headers 3"):
            self.assertEqual(
                header1.get_column("column_3"), header2.get_column("column_3")
            )
        with self.subTest("compare headers 4"):
            self.assertEqual(
                header1.get_column("column_4"), header2.get_column("column_4")
            )
        with self.subTest("compare headers 5"):
            self.assertEqual(
                header1.get_column("column_5"), header2.get_column("column_5")
            )
        with self.subTest("compare headers 6"):
            self.assertEqual(
                header1.get_column("column_6"), header2.get_column("column_6")
            )


class TestMetricTableHeader(unittest.TestCase):
    def test_construction(self):
        header = Pd.MetricTableHeader()
        header.set_name("foo")
        self.assertEqual(header.get_name(), "foo")

    def test_serialize_deserialize(self):

        # Create a new header
        header1 = Pd.MetricTableHeader(name="foo")
        header1.add_column("column_1", 1.0)
        header1.add_column("column_2", 0.0)

        # Serialize the header
        data = header1.serialize()

        # Create a second header
        header2 = Pd.MetricTableHeader(name="bar")

        # Deserialize the data into the second header
        header2.deserialize(data)

        # Check equality
        with self.subTest("name"):
            self.assertEqual(header1.get_name(), header2.get_name())
        with self.subTest("column_1"):
            self.assertEqual(
                header1.get_column("column_1"), header2.get_column("column_1")
            )
        with self.subTest("column_2"):
            self.assertEqual(
                header1.get_column("column_2"), header2.get_column("column_2")
            )


if __name__ == "__main__":
    unittest.main()
