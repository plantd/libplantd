# -*- Mode: Python -*-

import os
import sys
import time
import unittest
import gi

gi.require_version("Pd", "1.0")

# pylint: disable=wrong-import-position

from gi.repository import GLib, Pd

# pylint: enable=wrong-import-position


class ExampleApplication(Pd.Application):
    __gtype_name__ = "ExampleApplication"

    def __init__(self, *args, **kwargs):
        super().__init__(*args, application_id="org.plantd.TestApplication", flags=0, **kwargs)
        # Run the test in standalone mode
        os.environ["PLANTD_MODULE_STANDALONE"] = "true"
        os.environ["PLANTD_MODULE_ENDPOINT"] = "tcp://localhost:17999"
        os.environ["PLANTD_MODULE_BROKER"] = "tcp://*:17999"
        # Named service for messaging
        self.set_service("test-app")
        self.set_inactivity_timeout(1000)

    def do_get_configuration(self):
        configuration = Pd.Configuration.new()
        configuration.set_id(GLib.uuid_string_random())
        configuration.set_namespace(Pd.ConfigurationNamespace.ACQUIRE)

        response = Pd.ConfigurationResponse.new()
        response.set_configuration(configuration)

        return response


class ExampleSink(Pd.Sink):
    __gtype_name__ = "TestSink"

    def __init__(self, *args, **kwargs):
        super().__init__(*args, endpoint="tcp://localhost:18999", filter="", **kwargs)
        self.message_count = 0
        self.message_type = 0

    def do_handle_message(self, msg):
        if self.message_type == 0:
            event = Pd.Event.new()
            event.deserialize(msg)
        else:
            metric = Pd.Metric.new()
            metric.deserialize(msg)
        self.message_count = self.message_count + 1


class TestApplication(unittest.TestCase):
    def setUp(self) -> None:
        log_file = os.getenv("TEST_APPLICATION_LOG", "/dev/null")
        Pd.log_init(True, log_file)
        if os.getenv("TEST_APPLICATION_LOG_VERBOSE", False):
            Pd.log_increase_verbosity()
            Pd.log_increase_verbosity()
            Pd.log_increase_verbosity()
            Pd.log_increase_verbosity()
        self.app_id = "test-app"
        self.app = ExampleApplication()
        self.app.add_property(Pd.Property.new("foo", "bar"))
        self.app.add_property(Pd.Property.new("baz", "qux"))
        self.client = Pd.Client.new("tcp://localhost:17999")

    def tearDown(self) -> None:
        Pd.log_shutdown()

    def _send_shutdown(self):
        self.client.send_request(self.app_id, "shutdown", "{}")

    def on_test_events_cb(self, user_data):
        time.sleep(0.2)
        for i in range(0, 100):
            event = Pd.Event.new_full(i, "test", "test-event")
            self.app.send_event(event)
            time.sleep(0.0001)
        time.sleep(0.2)
        self.assertEqual(self.sink.message_count, 100)
        time.sleep(0.1)
        self._send_shutdown()
        return False

    def test_events(self):
        source = Pd.Source.new("tcp://localhost:18998", "")
        self.app.add_source("event", source)
        self.sink = ExampleSink()
        self.app.add_sink("event", self.sink)
        hub = Pd.Hub.new("tcp://*:18998", "tcp://*:18999")
        hub.start()
        source.start()
        self.sink.start()
        GLib.timeout_add_seconds(1, self.on_test_events_cb, None)
        self.app.run(sys.argv)

    def _test_get_configuration(self):
        request = Pd.ConfigurationRequest.new()
        request.set_id(self.app_id)
        message = request.serialize()
        self.client.send_request(self.app_id, "get-configuration", message)
        data = self.client.recv_response()
        print(data)
        self.assertIsNotNone(data)

        response = Pd.ConfigurationResponse.new()
        response.deserialize(data)

        configuration = response.get_configuration()
        self.assertIsNotNone(configuration.get_id())
        self.assertEqual(configuration.get_namespace(), Pd.ConfigurationNamespace.ACQUIRE)

    def on_test_messages_cb(self, user_data):
        self._test_get_configuration()
        self._send_shutdown()

    def test_get_configuration(self):
        GLib.timeout_add_seconds(1, self.on_test_messages_cb, None)
        self.app.run(sys.argv)

    def on_test_properties_cb(self, user_data):
        prop = self.app.lookup_property("foo")
        self.assertEqual(prop.get_value(), "bar")
        prop = self.app.lookup_property("foo")
        self.assertEqual(prop.get_value(), "bar")
        prop = self.app.lookup_property("baz")
        self.assertEqual(prop.get_value(), "qux")
        self._send_shutdown()

    def test_properties(self):
        GLib.timeout_add_seconds(1, self.on_test_properties_cb, None)
        self.app.run(sys.argv)


if __name__ == "__main__":
    unittest.main()
