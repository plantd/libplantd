# -*- Mode: Python -*-

from __future__ import absolute_import

import gi
import unittest

gi.require_version("Pd", "1.0")

# pylint: disable=wrong-import-position

from gi.repository import Pd

# pylint: enable=wrong-import-position


class TestProperty(unittest.TestCase):

    json = """{
        \"key\": \"foo\",
        \"value\": \"bar\"
    }"""

    def test_construct(self):
        prop = Pd.Property.new("foo", "bar")
        self.assertEqual(prop.get_key(), "foo")
        self.assertEqual(prop.get_value(), "bar")
        prop.set_key("baz")
        prop.set_value("meh")
        self.assertEqual(prop.get_key(), "baz")
        self.assertEqual(prop.get_value(), "meh")

    def test_serialize(self):
        prop = Pd.Property.new("foo", "bar")
        data = prop.serialize()
        data = data.replace("\n", "")
        data = data.replace(" ", "")
        json_cmp = self.json.replace("\n", "")
        json_cmp = json_cmp.replace(" ", "")
        self.assertEqual(data, json_cmp)

    def test_deserialize(self):
        prop = Pd.Property.new("", "")
        prop.deserialize(self.json)
        self.assertEqual(prop.get_key(), "foo")
        self.assertEqual(prop.get_value(), "bar")


if __name__ == "__main__":
    unittest.main()
