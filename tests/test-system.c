#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <plantd/plantd.h>

static void
test_system_construct (void)
{
  /* g_autoptr (PdSystem) object = NULL; */

  /*object = pd_system_new ();*/

  // g_assert_...
}

/*
 *static const gchar *json = "{ \
 *}";
 */

static void
test_system_json_serialize (void)
{
  /* g_autoptr (PdSystem) object = NULL; */
}

static void
test_system_json_deserialize (void)
{
  /* g_autoptr (PdSystem) object = NULL; */
}

gint
main (gint argc, gchar *argv[])
{
  g_test_init (&argc, &argv, NULL);

  g_test_add_func ("/system/construct", test_system_construct);
  g_test_add_func ("/system/json-serialize", test_system_json_serialize);
  g_test_add_func ("/system/json-deserialize", test_system_json_deserialize);

  return g_test_run ();
}
