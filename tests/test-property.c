#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <plantd/plantd.h>

#include "plantd/plantd-utils.h"

static void
test_property_construct (void)
{
  g_autoptr (PdProperty) prop = NULL;

  prop = pd_property_new ("key", NULL);
  g_assert_nonnull (prop);

  pd_property_set_value (prop, "value");

  g_assert_cmpstr ("key", ==, pd_property_get_key (prop));
  g_assert_cmpstr ("value", ==, pd_property_get_value (prop));
}

static const gchar *json = "{ \
  \"key\": \"test-key\", \
  \"value\": \"test-value\" \
}";

static void
test_property_json_serialize (void)
{
  g_autoptr (PdProperty) prop = NULL;

  gchar *data = NULL;
  gchar *data1 = NULL;
  gchar *data2 = NULL;

  prop = pd_property_new ("test-key", NULL);
  g_assert_nonnull (prop);

  pd_property_set_value (prop, "test-value");

  data = pd_property_serialize (prop);
  data1 = remove_whitespace (json);
  data2 = remove_whitespace (data);

  g_assert_cmpstr (data1, ==, data2);

  g_free (data);
  g_free (data1);
  g_free (data2);
}

static void
test_property_json_deserialize (void)
{
  g_autoptr (PdProperty) prop = NULL;

  prop = pd_property_new ("test", NULL);
  g_assert_nonnull (prop);

  pd_property_deserialize (prop, json);

  g_assert_cmpstr ("test-key", ==, pd_property_get_key (prop));
  g_assert_cmpstr ("test-value", ==, pd_property_get_value (prop));
}

gint
main (gint argc, gchar *argv[])
{
  g_test_init (&argc, &argv, NULL);

  g_test_add_func ("/property/construct", test_property_construct);
  g_test_add_func ("/property/json-serialize", test_property_json_serialize);
  g_test_add_func ("/property/json-deserialize", test_property_json_deserialize);

  return g_test_run ();
}
