#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <plantd/plantd.h>

static void
test_transition_construct (void)
{
  g_autoptr (PdTransition) object = NULL;

  object = pd_transition_new (1);

  g_assert_nonnull (object);
}

static void
test_transition_properties (void)
{
  g_autoptr (PdTransition) transition = NULL;
  g_autoptr (PdState) start = NULL;
  g_autoptr (PdState) end = NULL;

  transition = pd_transition_new (1);

  g_assert_nonnull (transition);
  g_assert_cmpint (pd_transition_get_id (transition), ==, 1);

  start = pd_state_new_full (1, "start", "start state");
  end = pd_state_new_full (2, "end", "end state");

  g_assert_nonnull (start);
  g_assert_nonnull (end);

  pd_transition_set_id (transition, 99);
  pd_transition_set_start_state (transition, start);
  pd_transition_set_end_state (transition, end);

  g_assert_cmpint (pd_transition_get_id (transition), ==, 99);

  {
    g_autoptr (PdState) s = NULL;
    g_autoptr (PdState) e = NULL;

    s = pd_transition_ref_start_state (transition);
    e = pd_transition_ref_end_state (transition);

    g_assert_nonnull (s);
    g_assert_nonnull (e);
    g_assert_cmpint (pd_state_get_id (s), ==, 1);
    g_assert_cmpint (pd_state_get_id (e), ==, 2);
  }
}

gint
main (gint argc, gchar *argv[])
{
  g_test_init (&argc, &argv, NULL);

  g_test_add_func ("/transition/construct", test_transition_construct);
  g_test_add_func ("/transition/properties", test_transition_properties);

  return g_test_run ();
}
