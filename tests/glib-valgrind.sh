#!/bin/bash

export G_SLICE=always-malloc
export G_DEBUG=gc-friendly

valgrind --tool=memcheck \
    --leak-check=full \
    --show-leak-kinds=all \
    --leak-resolution=high \
    --num-callers=20 \
    --suppressions=tests/glib.supp \
    --suppressions=tests/zeromq.supp \
    "$@"
