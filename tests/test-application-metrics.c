#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <plantd/plantd.h>

#include "lib/test-application.h"
#include "lib/test-sink.h"

static void
test_message_shutdown (PdClient *client)
{
  pd_client_send_request (client,
                            "test-app",
                            "shutdown",
                            "{}");
}

static gboolean
test_metrics_cb (gpointer data)
{
  g_autoptr (PdMetric) metric = NULL;
  g_autoptr (PdMetricTable) table = NULL;
  g_autoptr (PdMetricTableHeader) header = NULL;
  g_autoptr (PdClient) client = NULL;
  PdApplication *app;

  g_usleep (200000);

  app = PD_APPLICATION (data);

  table = pd_metric_table_new ();
  header = pd_metric_table_header_new ("calibration");

  pd_metric_table_header_add_column (header, "column_1", 1.0);
  pd_metric_table_header_add_column (header, "column_2", 0.0);
  pd_metric_table_header_add_column (header, "column_3", 1.0);
  pd_metric_table_header_add_column (header, "column_4", 0.0);
  pd_metric_table_header_add_column (header, "column_5", 1.0);
  pd_metric_table_header_add_column (header, "column_6", 0.0);

  pd_metric_table_set_name (table, "baz");
  pd_metric_table_set_timestamp (table, "2019-09-30 18:30:0.00-00");
  pd_metric_table_set_header (table, header);
  pd_metric_table_add_entry (table, "value_1", 1.0);
  pd_metric_table_add_entry (table, "value_2", 2.0);
  pd_metric_table_add_entry (table, "value_3", 3.0);

  metric = pd_metric_new_full ("foo", "bar", table);
  pd_application_send_metric (app, metric, NULL);

  g_usleep (200000);
  g_assert_cmpint (test_sink_get_message_count (), ==, 1);

  client = pd_client_new ("tcp://localhost:19999");
  g_usleep (100000);
  test_message_shutdown (client);

  return FALSE;
}

static void
test_metrics_activate_cb (GApplication *application)
{
  PdApplication *app;
  PdSource *source;
  PdSink *sink;

  app = PD_APPLICATION (application);

  source = pd_application_get_source (app, "metric");
  pd_source_start (source);

  sink = pd_application_get_sink (app, "metric");
  pd_sink_start (sink);
}

static void
test_application_metrics (void)
{
  gchar *binpath = g_test_build_filename (G_TEST_BUILT, "unimportant", NULL);
  gchar *argv[] = { binpath, NULL };

  g_autoptr (PdApplication) app = NULL;
  g_autoptr (PdSource) source = NULL;
  g_autoptr (PdSink) sink = NULL;
  g_autoptr (PdHub) hub = NULL;

  app = test_application_new ();
  pd_application_set_id (app, "org.plantd.TestApplication");
  pd_application_set_service (app, "test-app");

  source = pd_source_new ("tcp://localhost:21998", "");
  pd_application_add_source (PD_APPLICATION (app), "metric", source);
  g_assert_true (pd_application_has_source (PD_APPLICATION (app), "metric"));

  sink = test_sink_new ("tcp://localhost:21999", "");
  test_sink_set_message_type (1);
  pd_application_add_sink (PD_APPLICATION (app), "metric", sink);

  hub = pd_hub_new ("tcp://*:21998", "tcp://*:21999");
  pd_hub_start (hub);

  g_timeout_add (1, test_metrics_cb, app);
  g_signal_connect (app, "activate", G_CALLBACK (test_metrics_activate_cb), NULL);

  g_application_run (G_APPLICATION (app), G_N_ELEMENTS (argv) - 1, argv);

  pd_hub_stop (hub);

  g_free (binpath);
}

gint
main (gint argc, gchar *argv[])
{
  g_test_init (&argc, &argv, NULL);

  g_test_add_func ("/application/metrics", test_application_metrics);

  return g_test_run ();
}
