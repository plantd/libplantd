#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <plantd/plantd.h>

static void
test_sink_construct (void)
{
  g_autoptr (PdSink) object = NULL;

  object = pd_sink_new ("tcp://localhost:5555", "");

  g_assert_nonnull (object);
}

/*
 *static const gchar *json = "{ \
 *  \"\": \"\", \
 *}";
 */

static void
test_sink_serialize (void)
{
  G_GNUC_UNUSED g_autoptr (PdSink) object = NULL;
}

static void
test_sink_deserialize (void)
{
  G_GNUC_UNUSED g_autoptr (PdSink) object = NULL;
}

gint
main (gint argc, gchar *argv[])
{
  g_test_init (&argc, &argv, NULL);

  g_test_add_func ("/sink/construct", test_sink_construct);
  g_test_add_func ("/sink/serialize", test_sink_serialize);
  g_test_add_func ("/sink/deserialize", test_sink_deserialize);

  return g_test_run ();
}
