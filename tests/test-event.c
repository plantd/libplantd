#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <plantd/plantd.h>

#include "plantd/plantd-utils.h"

static void
test_event_construct (void)
{
  g_autoptr (PdEvent) object = NULL;

  object = pd_event_new ();
  g_assert_nonnull (object);

  pd_event_set_id (object, 1);
  pd_event_set_name (object, "Test");
  pd_event_set_description (object, "A test event");

  g_assert_cmpint (pd_event_get_id (object), ==, 1);
  g_assert_cmpstr (pd_event_get_name (object), ==, "Test");
  g_assert_cmpstr (pd_event_get_description (object), ==, "A test event");
}

static void
test_event_construct_full (void)
{
  g_autoptr (PdEvent) object = NULL;

  object = pd_event_new_full (1, "Test", "A test event");
  g_assert_nonnull (object);

  g_assert_cmpint (pd_event_get_id (object), ==, 1);
  g_assert_cmpstr (pd_event_get_name (object), ==, "Test");
  g_assert_cmpstr (pd_event_get_description (object), ==, "A test event");
}

static const gchar *strevent = "99@@test-event@@A test event";

static void
test_event_basic_serialize (void)
{
  g_autoptr (PdEvent) object = NULL;

  g_autofree gchar *data = NULL;

  object = pd_event_new_full (99, "test-event", "A test event");
  g_assert_nonnull (object);

  data = pd_event_to_data (object);

  g_assert_cmpstr (strevent, ==, data);
}

static void
test_event_basic_deserialize (void)
{
  g_autoptr (PdEvent) object = NULL;

  object = pd_event_from_data (strevent);
  g_assert_nonnull (object);

  g_assert_cmpint (pd_event_get_id (object), ==, 99);
  g_assert_cmpstr (pd_event_get_name (object), ==, "test-event");
  g_assert_cmpstr (pd_event_get_description (object), ==, "A test event");
}

static const gchar *json = "{ \
  \"id\": 99, \
  \"name\": \"test-event\", \
  \"description\": \"A test event\" \
}";

static void
test_event_json_serialize (void)
{
  g_autoptr (PdEvent) object = NULL;

  g_autofree gchar *data = NULL;
  g_autofree gchar *data1 = NULL;
  g_autofree gchar *data2 = NULL;

  object = pd_event_new ();
  g_assert_nonnull (object);

  pd_event_set_id (object, 99);
  pd_event_set_name (object, "test-event");
  pd_event_set_description (object, "A test event");

  data = pd_event_serialize (object);
  data1 = remove_whitespace (json);
  data2 = remove_whitespace (data);

  g_assert_cmpstr (data1, ==, data2);
}

static void
test_event_json_deserialize (void)
{
  g_autoptr (PdEvent) object = NULL;

  object = pd_event_new ();
  g_assert_nonnull (object);

  pd_event_deserialize (object, json);

  g_assert_cmpint (pd_event_get_id (object), ==, 99);
  g_assert_cmpstr (pd_event_get_name (object), ==, "test-event");
  g_assert_cmpstr (pd_event_get_description (object), ==, "A test event");
}

gint
main (gint argc, gchar *argv[])
{
  g_test_init (&argc, &argv, NULL);

  g_test_add_func ("/event/construct", test_event_construct);
  g_test_add_func ("/event/construct-full", test_event_construct_full);
  g_test_add_func ("/event/basic-serialize", test_event_basic_serialize);
  g_test_add_func ("/event/basic-deserialize", test_event_basic_deserialize);
  g_test_add_func ("/event/json-serialize", test_event_json_serialize);
  g_test_add_func ("/event/json-deserialize", test_event_json_deserialize);

  return g_test_run ();
}
