#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <json-glib/json-glib.h>
#include <plantd/plantd.h>

#include "lib/test-application.h"

/* Test application construction and get/set methods */
static void
test_application_construct (void)
{
  g_autoptr (PdApplication) app = NULL;
  g_autoptr (PdProperty) prop1 = NULL;
  g_autoptr (PdProperty) prop2 = NULL;

  app = test_application_new ();
  pd_application_set_id (app, "org.plantd.TestApplication");
  pd_application_set_endpoint (app, "tcp://localhost:19999");
  pd_application_set_service (app, "test-app");

  g_assert_nonnull (app);
  g_assert_cmpstr (pd_application_get_id (app), ==, "org.plantd.TestApplication");
  g_assert_cmpstr (pd_application_get_endpoint (app), ==, "tcp://localhost:19999");
  g_assert_cmpstr (pd_application_get_service (app), ==, "test-app");

  prop1 = pd_property_new ("key1", "prop1");
  prop2 = pd_property_new ("key2", "prop2");

  pd_application_add_property (app, prop1);
  pd_application_add_property (app, prop2);

  g_assert_true (pd_application_has_property (app, "key1"));
  g_assert_true (pd_application_has_property (app, "key2"));

  pd_application_remove_property (app, "key2");

  g_assert_true (!pd_application_has_property (app, "key2"));

  {
    PdProperty *prop;

    g_assert_true (pd_application_update_property (app, "key1", "foo"));

    prop = pd_application_lookup_property (app, "key1");

    g_assert_nonnull (prop);
    g_assert_cmpstr (pd_property_get_value (prop), ==, "foo");
  }

  {
    for (gint i = 0; i < 100; i++)
      {
        g_autoptr (PdProperty) p = NULL;
        g_autofree gchar *key = NULL;
        g_autofree gchar *value = NULL;
        key = g_strdup_printf ("key-list%d", i);
        value = g_strdup_printf ("value-list%d", i);
        p = pd_property_new (key, value);
        pd_application_add_property (app, p);
        g_assert_true (pd_application_has_property (app, key));
      }
  }
}

/* Tests for various message handling calls. */

static void
test_message_get_configuration (PdClient *client)
{
  g_autoptr (PdConfigurationRequest) req = NULL;
  g_autoptr (PdConfigurationResponse) resp = NULL;
  g_autofree const gchar *msg = NULL;
  g_autofree const gchar *data = NULL;
  g_autofree gchar *id = NULL;
  PdConfiguration *config;

  req = pd_configuration_request_new ();
  pd_configuration_request_set_id (req, "test-app");
  msg = pd_configuration_request_serialize (req);
  pd_client_send_request (client,
                              "test-app",
                              "get-configuration",
                              msg);
  data = pd_client_recv_response (client);
  g_assert_nonnull (data);

  resp = pd_configuration_response_new ();
  pd_configuration_response_deserialize (resp, data);

  config = pd_configuration_response_get_configuration (resp);
  id = g_strdup (pd_configuration_get_id (config));

  if (g_test_verbose ())
    g_print ("%s\n", data);

  g_assert_nonnull (id);
  g_assert_cmpint (pd_configuration_get_namespace (config),
                   ==,
                   PD_CONFIGURATION_NAMESPACE_ACQUIRE);
}

static void
test_message_get_property (PdClient *client)
{
  g_autoptr (PdPropertyRequest) req = NULL;
  g_autoptr (PdPropertyResponse) resp = NULL;
  g_autofree const gchar *msg = NULL;
  g_autofree const gchar *data = NULL;

  req = pd_property_request_new ("test-app", "key1", NULL);
  msg = pd_property_request_serialize (req);
  pd_client_send_request (client,
                              "test-app",
                              "get-property",
                              msg);
  data = pd_client_recv_response (client);
  g_assert_nonnull (data);

  resp = pd_property_response_new ();
  pd_property_response_deserialize (resp, data);

  {
    g_autoptr (JsonParser) parser = NULL;
    g_autoptr (JsonReader) reader = NULL;
    g_autoptr (GError) err = NULL;

    parser = json_parser_new ();
    json_parser_load_from_data (parser, data, -1, &err);
    g_assert_no_error (err);

    reader = json_reader_new (json_parser_get_root (parser));
    json_reader_read_member (reader, "property");
    g_assert (json_reader_is_object (reader));

    json_reader_read_member (reader, "key");
    const gchar *key = json_reader_get_string_value (reader);
    json_reader_end_member (reader);
    json_reader_read_member (reader, "value");
    const gchar *value = json_reader_get_string_value (reader);
    json_reader_end_member (reader);

    g_assert_cmpstr (key, ==, "key1");
    g_assert_cmpstr (value, ==, "prop1");
  }
}

static void
test_message_set_property (PdClient *client)
{
  g_autoptr (PdPropertyRequest) req = NULL;
  g_autoptr (PdPropertyResponse) resp = NULL;
  g_autofree const gchar *msg = NULL;
  g_autofree const gchar *data = NULL;

  {
    req = pd_property_request_new ("test-app", "key1", "new-prop");
    msg = pd_property_request_serialize (req);
    pd_client_send_request (client,
                                "test-app",
                                "set-property",
                                msg);
    data = pd_client_recv_response (client);
    g_assert_nonnull (data);

    resp = pd_property_response_new ();
    pd_property_response_deserialize (resp, data);

    {
      g_autoptr (JsonParser) parser = NULL;
      g_autoptr (JsonReader) reader = NULL;
      g_autoptr (GError) err = NULL;

      parser = json_parser_new ();
      json_parser_load_from_data (parser, data, -1, &err);
      g_assert_no_error (err);

      reader = json_reader_new (json_parser_get_root (parser));
      json_reader_read_member (reader, "property");
      g_assert (json_reader_is_object (reader));

      json_reader_read_member (reader, "key");
      const gchar *key = json_reader_get_string_value (reader);
      json_reader_end_member (reader);
      json_reader_read_member (reader, "value");
      const gchar *value = json_reader_get_string_value (reader);
      json_reader_end_member (reader);

      g_assert_cmpstr (key, ==, "key1");
      g_assert_cmpstr (value, ==, "new-prop");
    }
  }

  {
    req = pd_property_request_new ("test-app", "key1", NULL);
    msg = pd_property_request_serialize (req);
    pd_client_send_request (client,
                                "test-app",
                                "get-property",
                                msg);
    data = pd_client_recv_response (client);
    g_assert_nonnull (data);

    resp = pd_property_response_new ();
    pd_property_response_deserialize (resp, data);

    {
      g_autoptr (JsonParser) parser = NULL;
      g_autoptr (JsonReader) reader = NULL;
      g_autoptr (GError) err = NULL;

      parser = json_parser_new ();
      json_parser_load_from_data (parser, data, -1, &err);
      g_assert_no_error (err);

      reader = json_reader_new (json_parser_get_root (parser));
      json_reader_read_member (reader, "property");
      g_assert (json_reader_is_object (reader));

      json_reader_read_member (reader, "key");
      const gchar *key = json_reader_get_string_value (reader);
      json_reader_end_member (reader);
      json_reader_read_member (reader, "value");
      const gchar *value = json_reader_get_string_value (reader);
      json_reader_end_member (reader);

      g_assert_cmpstr (key, ==, "key1");
      g_assert_cmpstr (value, ==, "new-prop");
    }
  }
}

static void
test_message_get_properties (PdClient *client)
{
  g_autoptr (PdPropertiesRequest) req = NULL;
  g_autoptr (PdPropertiesResponse) resp = NULL;
  g_autoptr (PdProperty) prop1 = NULL;
  g_autoptr (PdProperty) prop2 = NULL;
  g_autofree const gchar *msg = NULL;
  g_autofree const gchar *data = NULL;

  {
    // reset application properties
    g_autoptr (PdPropertyRequest) preq1 = NULL;
    g_autoptr (PdPropertyRequest) preq2 = NULL;

    preq1 = pd_property_request_new ("test-app", "key1", "prop1");
    preq2 = pd_property_request_new ("test-app", "key2", "prop2");
    pd_client_send_request (client,
                                "test-app",
                                "set-property",
                                pd_property_request_serialize (preq1));
    g_assert_nonnull (pd_client_recv_response (client));
    pd_client_send_request (client,
                                "test-app",
                                "set-property",
                                pd_property_request_serialize (preq2));
    g_assert_nonnull (pd_client_recv_response (client));
  }

  req = pd_properties_request_new ("test-app");
  prop1 = pd_property_new ("key1", NULL);
  prop2 = pd_property_new ("key2", NULL);
  pd_properties_request_add (req, prop1);
  pd_properties_request_add (req, prop2);
  msg = pd_properties_request_serialize (req);
  pd_client_send_request (client,
                              "test-app",
                              "get-properties",
                              msg);
  data = pd_client_recv_response (client);
  g_assert_nonnull (data);

  resp = pd_properties_response_new ();
  pd_properties_response_deserialize (resp, data);

  {
    g_autoptr (JsonParser) parser = NULL;
    g_autoptr (JsonReader) reader = NULL;
    g_autoptr (GError) err = NULL;

    parser = json_parser_new ();
    json_parser_load_from_data (parser, data, -1, &err);
    g_assert_no_error (err);

    reader = json_reader_new (json_parser_get_root (parser));
    json_reader_read_member (reader, "properties");
    g_assert (json_reader_is_array (reader));

    json_reader_read_element (reader, 0);
    json_reader_read_member (reader, "key");
    g_assert_cmpstr (json_reader_get_string_value (reader), ==, "key1");
    json_reader_end_member (reader);
    json_reader_read_member (reader, "value");
    g_assert_cmpstr (json_reader_get_string_value (reader), ==, "prop1");
    json_reader_end_member (reader);
    json_reader_end_element (reader);
  }
}

/*
 *static void
 *test_message_set_properties (PdClient *client)
 *{
 *  g_autoptr (PdPropertiesRequest) req = NULL;
 *  g_autoptr (PdPropertiesResponse) resp = NULL;
 *  g_autofree const gchar *msg = NULL;
 *  g_autofree const gchar *data = NULL;
 *
 *  // TODO: ... create message
 *
 *  pd_client_send_request (client,
 *                            "test-app",
 *                            "set-properties",
 *                            msg);
 *  data = pd_client_recv_response (client);
 *  g_assert_nonnull (data);
 *
 *  // TODO: ... create response
 *
 *  if (g_test_verbose ())
 *    g_print ("%s\n", data);
 *
 *  // TODO: ... assert things
 *}
 */

static void
test_message_get_status (PdClient *client)
{
  g_autoptr (PdStatusRequest) req = NULL;
  g_autoptr (PdStatusResponse) resp = NULL;
  g_autofree const gchar *msg = NULL;
  g_autofree const gchar *data = NULL;

  req = pd_status_request_new ();
  msg = pd_status_request_serialize (req);
  pd_client_send_request (client,
                              "test-app",
                              "get-status",
                              msg);
  data = pd_client_recv_response (client);
  g_assert_nonnull (data);

  resp = pd_status_response_new ();
  pd_status_response_deserialize (resp, data);

  {
    g_autoptr (JsonParser) parser = NULL;
    g_autoptr (JsonReader) reader = NULL;
    g_autoptr (GError) err = NULL;

    parser = json_parser_new ();
    json_parser_load_from_data (parser, data, -1, &err);
    g_assert_no_error (err);

    reader = json_reader_new (json_parser_get_root (parser));
    json_reader_read_member (reader, "status");
    g_assert (json_reader_is_object (reader));

    json_reader_read_member (reader, "enabled");
    gboolean enabled = json_reader_get_boolean_value (reader);
    json_reader_end_member (reader);
    json_reader_read_member (reader, "active");
    gboolean active = json_reader_get_boolean_value (reader);
    json_reader_end_member (reader);
    json_reader_read_member (reader, "loaded");
    gboolean loaded = json_reader_get_boolean_value (reader);
    json_reader_end_member (reader);

    g_assert_true (enabled);
    g_assert_true (active);
    g_assert_false (loaded);

    json_reader_read_member (reader, "details");
    g_assert (json_reader_is_object (reader));

    json_reader_read_member (reader, "foo");
    const gchar *foo = json_reader_get_string_value (reader);
    json_reader_end_member (reader);
    json_reader_read_member (reader, "baz");
    const gchar *baz = json_reader_get_string_value (reader);
    json_reader_end_member (reader);

    g_assert_cmpstr (foo, ==, "bar");
    g_assert_cmpstr (baz, ==, "qux");
  }
}

static void
test_message_get_settings (PdClient *client)
{
  g_autoptr (PdSettingsRequest) req = NULL;
  g_autoptr (PdSettingsResponse) resp = NULL;
  g_autofree const gchar *msg = NULL;
  g_autofree const gchar *data = NULL;

  req = pd_settings_request_new ();
  msg = pd_settings_request_serialize (req);
  pd_client_send_request (client,
                              "test-app",
                              "get-settings",
                              msg);
  data = pd_client_recv_response (client);
  g_assert_nonnull (data);

  resp = pd_settings_response_new ();
  pd_settings_response_deserialize (resp, data);

  {
    g_autoptr (JsonParser) parser = NULL;
    g_autoptr (JsonReader) reader = NULL;
    g_autoptr (GError) err = NULL;

    parser = json_parser_new ();
    json_parser_load_from_data (parser, data, -1, &err);
    g_assert_no_error (err);

    reader = json_reader_new (json_parser_get_root (parser));

    json_reader_read_member (reader, "id");
    const gchar *id = json_reader_get_string_value (reader);
    json_reader_end_member (reader);

    g_assert_cmpstr (id, ==, "org.plantd.TestApplication");

    json_reader_read_member (reader, "settings");
    g_assert (json_reader_is_object (reader));

    json_reader_read_member (reader, "foo");
    const gchar *foo = json_reader_get_string_value (reader);
    json_reader_end_member (reader);
    json_reader_read_member (reader, "baz");
    const gchar *baz = json_reader_get_string_value (reader);
    json_reader_end_member (reader);

    g_assert_cmpstr (foo, ==, "bar");
    g_assert_cmpstr (baz, ==, "qux");
  }
}

/*
 *static void
 *test_message_get_job (PdClient *client)
 *{
 *  g_autoptr (PdJobRequest) req = NULL;
 *  g_autoptr (PdJobResponse) resp = NULL;
 *  g_autofree const gchar *msg = NULL;
 *  g_autofree const gchar *data = NULL;
 *
 *  // TODO: ... create message
 *
 *  pd_client_send_request (client,
 *                            "test-app",
 *                            "get-job",
 *                            msg);
 *  data = pd_client_recv_response (client);
 *  g_assert_nonnull (data);
 *
 *  // TODO: ... create response
 *
 *  if (g_test_verbose ())
 *    g_print ("%s\n", data);
 *
 *  // TODO: ... assert things
 *}
 */

/*
 *static void
 *test_message_get_jobs (PdClient *client)
 *{
 *  g_autoptr (PdEmpty) req = NULL;
 *  g_autoptr (PdJobsResponse) resp = NULL;
 *  g_autofree const gchar *msg = NULL;
 *  g_autofree const gchar *data = NULL;
 *
 *  // TODO: ... create message
 *
 *  pd_client_send_request (client,
 *                            "test-app",
 *                            "get-jobs",
 *                            msg);
 *  data = pd_client_recv_response (client);
 *  g_assert_nonnull (data);
 *
 *  // TODO: ... create response
 *
 *  if (g_test_verbose ())
 *    g_print ("%s\n", data);
 *
 *  // TODO: ... assert things
 *}
 */

/*
 *static void
 *test_message_get_active_job (PdClient *client)
 *{
 *  g_autoptr (PdEmpty) req = NULL;
 *  g_autoptr (PdJobResponse) resp = NULL;
 *  g_autofree const gchar *msg = NULL;
 *  g_autofree const gchar *data = NULL;
 *
 *  // TODO: ... create message
 *
 *  pd_client_send_request (client,
 *                            "test-app",
 *                            "get-active-job",
 *                            msg);
 *  data = pd_client_recv_response (client);
 *  g_assert_nonnull (data);
 *
 *  // TODO: ... create response
 *
 *  if (g_test_verbose ())
 *    g_print ("%s\n", data);
 *
 *  // TODO: ... assert things
 *}
 */

/*
 *static void
 *test_message_cancel_job (PdClient *client)
 *{
 *  g_autoptr (PdJobRequest) req = NULL;
 *  g_autoptr (PdJobResponse) resp = NULL;
 *  g_autofree const gchar *msg = NULL;
 *  g_autofree const gchar *data = NULL;
 *
 *  // TODO: ... create message
 *
 *  pd_client_send_request (client,
 *                            "test-app",
 *                            "cancel-job",
 *                            msg);
 *  data = pd_client_recv_response (client);
 *  g_assert_nonnull (data);
 *
 *  // TODO: ... create response
 *
 *  if (g_test_verbose ())
 *    g_print ("%s\n", data);
 *
 *  // TODO: ... assert things
 *}
 */

/*
 *static void
 *test_message_submit_job (PdClient *client)
 *{
 *  g_autoptr (PdJobRequest) req = NULL;
 *  g_autoptr (PdJobResponse) resp = NULL;
 *  g_autofree const gchar *msg = NULL;
 *  g_autofree const gchar *data = NULL;
 *
 *  // TODO: ... create message
 *
 *  pd_client_send_request (client,
 *                            "test-app",
 *                            "submit-job",
 *                            msg);
 *  data = pd_client_recv_response (client);
 *  g_assert_nonnull (data);
 *
 *  // TODO: ... create response
 *
 *  if (g_test_verbose ())
 *    g_print ("%s\n", data);
 *
 *  // TODO: ... assert things
 *}
 */

/*
 *static void
 *test_message_submit_event (PdClient *client)
 *{
 *  g_autoptr (PdEventRequest) req = NULL;
 *  g_autoptr (PdEventResponse) resp = NULL;
 *  g_autofree const gchar *msg = NULL;
 *  g_autofree const gchar *data = NULL;
 *
 *  // TODO: ... create message
 *
 *  pd_client_send_request (client,
 *                            "test-app",
 *                            "submit-event",
 *                            msg);
 *  data = pd_client_recv_response (client);
 *  g_assert_nonnull (data);
 *
 *  // TODO: ... create response
 *
 *  if (g_test_verbose ())
 *    g_print ("%s\n", data);
 *
 *  // TODO: ... assert things
 *}
 */

/*
 *static void
 *test_message_available_events (PdClient *client)
 *{
 *  g_autoptr (PdEmpty) req = NULL;
 *  g_autoptr (PdEventResponse) resp = NULL;
 *  g_autofree const gchar *msg = NULL;
 *  g_autofree const gchar *data = NULL;
 *
 *  // TODO: ... create message
 *
 *  pd_client_send_request (client,
 *                            "test-app",
 *                            "available-events",
 *                            msg);
 *  data = pd_client_recv_response (client);
 *  g_assert_nonnull (data);
 *
 *  // TODO: ... create response
 *
 *  if (g_test_verbose ())
 *    g_print ("%s\n", data);
 *
 *  // TODO: ... assert things
 *}
 */

static void
test_message_shutdown (PdClient *client)
{
  pd_client_send_request (client,
                              "test-app",
                              "shutdown",
                              "{}");
}

static gboolean
test_messages_cb (gpointer data)
{
  PdClient *client;

  client = PD_CLIENT (data);

  test_message_get_configuration (client);
  test_message_get_property (client);
  test_message_set_property (client);
  test_message_get_properties (client);
  /*test_message_set_properties (client);*/
  test_message_get_status (client);
  test_message_get_settings (client);
  /*test_message_get_job (client);*/
  /*test_message_get_jobs (client);*/
  /*test_message_get_active_job (client);*/
  /*test_message_cancel_job (client);*/
  /*test_message_submit_job (client);*/
  /*test_message_submit_event (client);*/
  /*test_message_available_events (client);*/
  test_message_shutdown (client);

  return FALSE;
}

static void
test_application_messages (void)
{
  gchar *binpath = g_test_build_filename (G_TEST_BUILT, "unimportant", NULL);
  gchar *argv[] = { binpath, NULL };

  g_autoptr (PdApplication) app = NULL;
  g_autoptr (PdProperty) prop1 = NULL;
  g_autoptr (PdProperty) prop2 = NULL;
  g_autoptr (PdClient) client = NULL;

  app = test_application_new ();
  pd_application_set_id (app, "org.plantd.TestApplication");
  pd_application_set_service (app, "test-app");

  prop1 = pd_property_new ("key1", "prop1");
  prop2 = pd_property_new ("key2", "prop2");
  pd_application_add_property (app, prop1);
  pd_application_add_property (app, prop2);

  client = pd_client_new ("tcp://localhost:19999");

  g_timeout_add (1, test_messages_cb, client);

  g_application_run (G_APPLICATION (app), G_N_ELEMENTS (argv) - 1, argv);

  g_free (binpath);
}

static void
test_application_jobs (void)
{
  g_autoptr (PdApplication) app = NULL;

  app = test_application_new ();
  pd_application_set_id (app, "org.plantd.TestApplication");
  pd_application_set_service (app, "test-app");
}

gint
main (gint argc, gchar *argv[])
{
  g_test_init (&argc, &argv, NULL);

  g_test_add_func ("/application/construct", test_application_construct);
  g_test_add_func ("/application/messages", test_application_messages);
  g_test_add_func ("/application/jobs", test_application_jobs);

  return g_test_run ();
}
