<a name="unreleased"></a>
## [Unreleased]

### Chore
- bump version

### Ci
- add artifacts to job

### Doc
- update changelog
- update README

### Feat
- generate coverage information
- add change log

### Fix
- deb package paths
- retain property in list
- handle property copy


<a name="v0.3.8"></a>
## [v0.3.8] - 2021-09-08
### Chore
- merge staging into master

### Merge Requests
- Merge branch 'staging' into 'master'


<a name="v0.3.7"></a>
## [v0.3.7] - 2021-07-18
### Chore
- bump version

### Ci
- add artifacts to job

### Doc
- update changelog
- update README

### Feat
- generate coverage information
- add change log

### Merge Requests
- Merge branch 'coverage' into 'staging'
- Merge branch 'changelog' into 'staging'
- Merge branch 'ci-coverage' into 'staging'
- Merge branch 'test-updates' into 'staging'
- Merge branch 'fix/worker-recv' into 'staging'
- Merge branch 'set-sast-config-1' into 'staging'
- Merge branch 'feat/docker' into 'master'


<a name="v0.3.6"></a>
## [v0.3.6] - 2021-01-27
### Fix
- transfer annotation

### Merge Requests
- Merge branch 'fix/apex-configuration' into 'master'


<a name="v0.3.5"></a>
## [v0.3.5] - 2020-12-08
### Chore
- exclude settings
- replace deprecated GTimeVal with GDateTime
- change old style prototype

### Doc
- add correct header string

### Feat
- add basic event serialization
- add debug statements
- add runner class

### Fix
- gitlab pipeline
- remove call to apex_stop_broker
- unit test for metric-table-header
- correct shell script, add prebuild script
- change arg count after removal
- ref count add in runner
- syntax issue
- repo naming
- use correct meson version

### Tests
- add basic serialization to event

### Wip
- testing launch

### Merge Requests
- Merge branch 'dev/deb-fpm' into 'master'
- Merge branch 'feat/integration-tests' into 'master'
- Merge branch 'dev/unit-tests' into 'master'
- Merge branch 'fix/memleak' into 'master'


<a name="v0.3.4"></a>
## [v0.3.4] - 2020-02-28
### Build
- hide protobuf wrappers behind flag
- hide protobuf enums if flag not set
- hide protobuf sources behind flag

### Chore
- lower required meson version
- test clean up
- clean up messages

### Feat
- add tests container
- start systemtap container
- add serialization test in application
- add python application test
- add python test classes
- add python test files
- annotate job request serialzation
- add python test framework
- add poller to sink class
- clean up sockets on shutdown
- add PUBSUB tests to application
- add PUBSUB proxy hub
- add zeromq valgrind suppressions
- add shutdown signal to handler
- run application test in standalone
- add data model class

### Fix
- missing endif
- remove leak in metric test
- memory leak in config deserialize
- syntax and logic errors
- transfer full on serialize
- clean up client read data
- plug leaks in message handlers
- serialization in job request

### Refactor
- clean up logging messages
- split message handling


<a name="v0.3.3"></a>
## [v0.3.3] - 2019-09-13
### Core
- bump version

### Feat
- add broker endpoint env
- add application standalone mode
- add broker class

### Refactor
- move job queue into class
- move message handler into class


<a name="v0.3.2"></a>
## [v0.3.2] - 2019-08-09
### Core
- bump version

### Fix
- use correct transfer annotation
- simplify unit test report generate
- change response data mutability


<a name="v0.3.1"></a>
## [v0.3.1] - 2019-08-08
### Core
- bump version

### Feat
- use table wrapper in configuration
- use table wrapper in object
- use table wrapper in metric table
- use table wrapper in metric header
- add leak free hash table wrapper

### Fix
- use test data in build
- memory leaks
- drop missing build stage

### Refactor
- clean up test assertions


<a name="v0.3.0"></a>
## [v0.3.0] - 2019-07-30
### Chore
- formatting

### Core
- bump version

### Feat
- add Dockerfiles
- add coverage generation
- add CI build and test scripts
- add property method annotations
- add unit test for error enum

### Fix
- property method docstring
- use proper container transfer
- handle empty lists on serialize
- handle zero length arrays

### Refactor
- move build check


<a name="v0.2.2"></a>
## [v0.2.2] - 2019-07-17
### Build
- update debian control file

### Chore
- clean up compiler warnings

### Fix
- metric object leaks
- metric related memory leaks
- various memory leaks
- plug various minor leaks
- raise log level in example
- minor debian control edits


<a name="v0.2.1"></a>
## [v0.2.1] - 2019-06-25
### Chore
- clean up logging

### Core
- bump version

### Feat
- add HUP signal for shutdown
- add verbosity args to application
- add vala examples
- add logging facility

### Fix
- add missing header


<a name="v0.2.0"></a>
## [v0.2.0] - 2019-06-05
### Build
- add vapi support

### Chore
- add transfer annotations
- add transfer annotations
- clean up compiler warnings

### Core
- bump version

### Dist
- generate doc and dev packages

### Doc
- add missing classes
- update with new classes
- add documentation sgml
- add gtk-doc generation

### Feat
- add MDP broker class
- add signal to state machine
- add state changed signal to fsm
- switch to hash table for configuration lists
- return error body in get-properties
- add files for debian packaging
- derive response in properties message
- add request and response base classes
- add properties to job request
- add trace message
- add metric-source example for record-tsdb.
- add prop changed signal to application
- add input params to submit job vfunc
- add empty unit tests for message types
- add definitions for template generation
- add get/set properties message handlers
- switch to array of properties
- add property update to application
- add metadata for vapi generation
- add property annotations
- add properties container to application
- add property request/response types
- add metric sink example
- add metric source example
- add support for metric types
- add string calls to API client
- add state machine support
- add pubsub types to application
- add error types

### Fix
- plug leak during queue in source
- use transfer none in container adds
- change state machine logic
- minor packaging edits
- revert doc
- debian packaging files
- remove dlang from packaging
- clean up includes
- change includes to allow derived types
- add missing node in message serialize
- revert job request serialization
- change job request serialization
- add property transfer annotation to submit job
- build examples
- remove debug output
- job properties property name
- change configuration serialization
- change property serialization
- un-break virtual functions for Python

### Refactor
- switch to a hash table for object lists
- switch ifdef to pragma once
- cleanup warnings
- clean up unnecessary props


<a name="v0.1.1"></a>
## [v0.1.1] - 2019-04-01
### Feat
- implement message queue in source
- add example that extends sink
- implement vfunc message handler in sink
- add examples to test buses
- add bus type implementations
- add configuration to application
- add object stubs
- add types for message buses
- add default vfunc implementations
- add settings response message
- handle HashTable serialization
- remove protobuf base from status type
- add signal handler
- get job name from request
- add job ID to module job request

### Fix
- improve array serialization
- build on systems that fail zmq link
- stop crash on serialize

### Refactor
- simplify test build
- clean up log statements

### Test
- add unit tests


<a name="v0.1.0"></a>
## v0.1.0 - 2019-03-11
### Build
- add missing header

### Ci
- change build type
- accept dep install
- fix build
- add gitlab build steps

### Core
- add LICENSE
- initial import

### Doc
- add gtk-doc headers to module
- add DOAP file

### Feat
- add job to module example
- add module message type
- add javascript example
- add job pool
- add task vfunc to job
- add vfunc for job submit
- add async task to monitor job queue
- serialize enum type in configuration
- add more request/response envelopes
- add remaining object prototypes
- add class implementations
- add lua module example
- add empty request/response envelopes
- add job response envelope message
- add unit tests
- add python overrides
- add props and unit tests
- add enums to match protobuf defs
- add virtual functions to module class
- add empty request/response messages
- add python module example
- add better application module example
- add boxed objects to object type
- add serializable interface to object
- add boxed properties to object
- add base objects for core types
- add types and messages
- add message types
- add gir, typelib, and vapi to output
- add examples

### Fix
- prevent application timeout
- fix properties g_param_spec type in apex-module-request
- fix docstring prefix, add example
- property creation type fail
- array length mismatch issue
- install headers to correct place
- make client work
- update API for current CZMQ

### Refactor
- move common init into application
- move module into application class
- clean up build files

### Wip
- add apex-module-event-request implementation
- add apex-module-response implementation
- add apex-module-request implementation
- add apex-module-request implementation
- add apex-jobs-response implementation
- add apex-module-job-request implementation
- add apex-job-status-resonse implementation


[Unreleased]: https://gitlab.com/plantd/libplantd/compare/v0.3.8...HEAD
[v0.3.8]: https://gitlab.com/plantd/libplantd/compare/v0.3.7...v0.3.8
[v0.3.7]: https://gitlab.com/plantd/libplantd/compare/v0.3.6...v0.3.7
[v0.3.6]: https://gitlab.com/plantd/libplantd/compare/v0.3.5...v0.3.6
[v0.3.5]: https://gitlab.com/plantd/libplantd/compare/v0.3.4...v0.3.5
[v0.3.4]: https://gitlab.com/plantd/libplantd/compare/v0.3.3...v0.3.4
[v0.3.3]: https://gitlab.com/plantd/libplantd/compare/v0.3.2...v0.3.3
[v0.3.2]: https://gitlab.com/plantd/libplantd/compare/v0.3.1...v0.3.2
[v0.3.1]: https://gitlab.com/plantd/libplantd/compare/v0.3.0...v0.3.1
[v0.3.0]: https://gitlab.com/plantd/libplantd/compare/v0.2.2...v0.3.0
[v0.2.2]: https://gitlab.com/plantd/libplantd/compare/v0.2.1...v0.2.2
[v0.2.1]: https://gitlab.com/plantd/libplantd/compare/v0.2.0...v0.2.1
[v0.2.0]: https://gitlab.com/plantd/libplantd/compare/v0.1.1...v0.2.0
[v0.1.1]: https://gitlab.com/plantd/libplantd/compare/v0.1.0...v0.1.1
