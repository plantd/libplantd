## Code Coverage

To generate code coverage one of `lcov`/`genhtml`/`gcovr` is required to use
with `ninja`.

```sh
meson configure -Db_coverage=true _build
ninja -C _build coverage-html
```

The output will be in `_build/meson-logs/`. This unfortunately includes all
project files, and won't easily allow for exclusions using `meson`. A better
report can be generated using:

```sh
cd _build
gcovr -r .. -e ../tests/ -e ../examples/ --html -o coverage.html
```