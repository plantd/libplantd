# Concept: State Management

## Notes

* add a state manager and backend concept
* shouldn't implement backend in library (?), or just use JSON?
* create a `plantd-state` service as a separate repo with a Redis backend
* how should state be submitted, `set-property`/`set-properties`?
* how should state be retrieved, `get-property`.`get-properties`?
* a module should be able to load/write all state
* a module should be able to continuously sync state with the manager
