## Setup

### Debian (buster)
```shell
xargs sudo apt install -y < build/dependencies-deb.txt
```

### Ubuntu (16.04/18.04/20.04)

```shell
sudo apt install meson ninja-build libglib2.0-dev libczmq-dev \
  gobject-introspection libjson-glib-dev \
  libgirepository1.0-dev
```

### Fedora (27/28/29)

```shell
sudo dnf install meson ninja-build glib2-devel czmq-devel \
  gobject-introspection-devel json-glib-devel
```

### macOS

```shell
brew install cairo gobject-introspection json-glib vala
pyenv virtualenv 3.8.7 libplantd
pyenv activate libplantd
pip install poetry
poetry install
```

## Build

The `-Dshared-lib=true` option must be set for macOS in the following.

```shell
meson _build
ninja -C _build
```

Compiling examples can be done by setting the `meson` flag and recompiling.

```shell
meson configure -Denable-examples=true _build
ninja -C _build
```

## Build (tested)

In order to use the library it must be built with the following instructions:

```shell
meson -Dprefix=/usr -Dshared-lib=true -Dwith-vapi=true _build
ninja -C _build
sudo ninja -C _build install
```

Most `plantd` modules appear to rely on the presence of `libplantd.so` in
the `/usr/local/lib/` path:
```
/usr/local/lib/x86_64-linux-gnu/libplantd.so
```
This is accomplished with `Dprefix=/usr`.

Compiling examples is done by setting the `meson` flag and recompiling.

```shell
meson configure -Denable-examples=true _build
ninja -C _build
```
