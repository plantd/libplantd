## Documentation

Documentation is generated using `gtkdoc` during installation provided that the
`enable-gtk-doc` build flag has been set to `true`. Vala documentation can also
be created using `valadoc`, but currently this must be done manually.

```sh
valadoc --force -o _build/doc/valadoc/ \
  --deps --doclet=html \
  --package-name=Plantd \
  --package-version=0.2.1 \
  --pkg=libplantd-1.0 \
  --pkg=glib-2.0 \
  --pkg=libczmq \
  --vapidir=data/vapi/
```
