#!/bin/bash
# This script needs to be called from the repository root.

VERSION="$(git describe | sed -e 's/[vV]//g')" # version number cannot start with char.
BINARY_NAME="libplantd"
DESCRIPTION="Messaging using ZeroMQ and GObject"
LICENSE=$"MIT LICENSE"
URL=$"https://gitlab.com/plantd/libplantd"
MAINTAINER=$"Mirko Moeller <mirko.moeller@coanda.ca>"
VENDOR=$""
PKG_NAME=$"libplantd-1.0-0"
INSTALLDIR=$"/tmp/installdir" # Must tell meson to put installation files here

export BINARY_NAME

if which fpm; then
  fpm --input-type dir \
    --output-type deb \
    --name "$PKG_NAME" --version "$VERSION" \
    --package libplantd-1.0-0_VERSION_ARCH.deb \
    --description "$DESCRIPTION" \
    --license "$LICENSE" \
    --url "$URL" \
    --maintainer "$MAINTAINER" \
    --vendor "$VENDOR" \
    --depends "libc6 >= 2.28" \
    --depends "libczmq4 >= 3.0.2" \
    --depends "libglib2.0-0 >= 2.51.2" \
    --depends "libjson-glib-1.0-0 >= 1.2.0" \
    --depends "libzmq5 >= 3.2.3+dfsg" \
    --depends "libjson-glib-dev >= 1.4.4" \
    --depends "libczmq-dev >= 4.2.0" \
    --chdir "$INSTALLDIR"
else
  echo "fpm not installed or not reachable"
  exit 1
fi
