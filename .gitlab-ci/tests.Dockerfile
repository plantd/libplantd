FROM registry.gitlab.com/plantd/libplantd/debian:v1

USER root

RUN apt-get update -qq && apt-get install --no-install-recommends -qq -y \
    sudo \
    jq \
    vim \
    valgrind

RUN pip3 install --upgrade meson

WORKDIR /home/user
COPY . libplantd/
RUN chown -R user.user libplantd/
RUN gpasswd -a user sudo

USER user
WORKDIR /home/user/libplantd/

RUN meson -Dshared-lib=true _build \
    && ninja -C _build

ENV G_SLICE=always-malloc
ENV G_DEBUG=gc-friendly

# RUN meson test \
#     --wrap='valgrind --tool=memcheck --leak-check=full --show-leak-kinds=definite --errors-for-leak-kinds=definite --leak-resolution=high --num-callers=20 --suppressions=../tests/glib.supp --suppressions=../tests/zeromq.supp' \
#     -C _build
