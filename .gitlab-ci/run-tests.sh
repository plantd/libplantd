#!/bin/bash

set +e

BUILD_DIR="${BUILD_DIR:-_build}"

env LANG=C.UTF-8 LC_ALL=C.UTF-8 dbus-run-session meson test \
  -C "$BUILD_DIR" \
  --timeout-multiplier "${MESON_TEST_TIMEOUT_MULTIPLIER}" \
  --print-errorlogs \
  --no-suite flaky \
  ${MESON_TEST_EXTRA_ARGS}

# sometimes troubleshooting is easier in test using ninja with verbose
# ninja test -C "$BUILD_DIR" -vvv

exit_code=$?

python3 .gitlab-ci/meson-junit-report.py \
        --project-name=libplantd \
        --job-id "${CI_JOB_NAME}" \
        --output "$BUILD_DIR/${CI_JOB_NAME}-report.xml" \
        "$BUILD_DIR"/meson-logs/testlog.json

exit $exit_code
