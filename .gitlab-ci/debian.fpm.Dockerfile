FROM debian:buster

RUN apt-get update -qq && apt-get install --no-install-recommends -qq -y \
    clang \
    clang-tools-6.0 \
    findutils \
    gcc \
    g++ \
    gettext \
    git \
    libc6-dev \
    gtk-doc-tools \
    itstool \
    gcovr \
    libglib2.0-dev \
    libgirepository1.0-dev \
    libjson-glib-dev \
    libzmq3-dev \
    libczmq-dev \
    libxml2-utils \
    libxslt1-dev \
    libz3-dev \
    locales \
    ninja-build \
    python3 \
    python3-pip \
    python3-setuptools \
    python3-wheel \
    python3-gi \
    python3-gi-cairo \
    gir1.2-gtk-3.0 \
    valac \
    xsltproc \
    xz-utils \
    zlib1g-dev \
    curl \
    dh-make \
    build-essential \
    devscripts \
    lintian \
    ruby \
    ruby-dev \
    rubygems \
 && gem install --no-document fpm \
 && rm -rf /usr/share/doc/* /usr/share/man/*

# Need to install meson using buster-backports instead of pip.
# Otherwise we have issues with it not being found (this might be an issue that 
# exists only for debuild).
RUN echo "deb http://deb.debian.org/debian buster-backports main" > /etc/apt/sources.list.d/buster-backports.list \
    && apt update \
    && apt install -y -t buster-backports meson

# Locale for our build
RUN locale-gen C.UTF-8 && /usr/sbin/update-locale LANG=C.UTF-8

ENV LANG=C.UTF-8 LANGUAGE=C.UTF-8 LC_ALL=C.UTF-8

